/**
 * <copyright>
 * </copyright>
 *
 * $Id: UML14AllTests.java,v 1.1 2012/04/23 09:32:45 uqjsuss Exp $
 */
package net.jgsuess.uml14.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>UML14</b></em>' model.
 * <!-- end-user-doc -->
 * @generated
 */
public class UML14AllTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new UML14AllTests("UML14 Tests");
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UML14AllTests(String name) {
		super(name);
	}

} //UML14AllTests
