/**
 * <copyright>
 * </copyright>
 *
 * $Id: Uml14Tests.java,v 1.1 2012/04/23 09:32:45 uqjsuss Exp $
 */
package net.jgsuess.uml14.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>uml14</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class Uml14Tests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new Uml14Tests("uml14 Tests");
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Uml14Tests(String name) {
		super(name);
	}

} //Uml14Tests
