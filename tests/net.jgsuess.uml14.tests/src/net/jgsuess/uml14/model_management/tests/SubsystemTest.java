/**
 * <copyright>
 * </copyright>
 *
 * $Id: SubsystemTest.java,v 1.1 2012/04/23 09:32:42 uqjsuss Exp $
 */
package net.jgsuess.uml14.model_management.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.foundation.core.tests.ClassifierTest;

import net.jgsuess.uml14.model_management.Model_managementFactory;
import net.jgsuess.uml14.model_management.Subsystem;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Subsystem</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SubsystemTest extends ClassifierTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SubsystemTest.class);
	}

	/**
	 * Constructs a new Subsystem test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubsystemTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Subsystem test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Subsystem getFixture() {
		return (Subsystem)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Model_managementFactory.eINSTANCE.createSubsystem());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //SubsystemTest
