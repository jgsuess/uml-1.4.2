/**
 * <copyright>
 * </copyright>
 *
 * $Id: Model_managementTests.java,v 1.1 2012/04/23 09:32:42 uqjsuss Exp $
 */
package net.jgsuess.uml14.model_management.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>model_management</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class Model_managementTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new Model_managementTests("model_management Tests");
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model_managementTests(String name) {
		super(name);
	}

} //Model_managementTests
