/**
 * <copyright>
 * </copyright>
 *
 * $Id: ActionStateTest.java,v 1.1 2012/04/23 09:32:31 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.activity_graphs.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.ActionState;
import net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsFactory;

import net.jgsuess.uml14.behavioral_elements.state_machines.tests.SimpleStateTest;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Action State</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ActionStateTest extends SimpleStateTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ActionStateTest.class);
	}

	/**
	 * Constructs a new Action State test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionStateTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Action State test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ActionState getFixture() {
		return (ActionState)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Activity_graphsFactory.eINSTANCE.createActionState());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ActionStateTest
