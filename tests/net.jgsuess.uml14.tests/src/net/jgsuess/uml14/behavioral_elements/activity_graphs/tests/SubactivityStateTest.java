/**
 * <copyright>
 * </copyright>
 *
 * $Id: SubactivityStateTest.java,v 1.1 2012/04/23 09:32:31 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.activity_graphs.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsFactory;
import net.jgsuess.uml14.behavioral_elements.activity_graphs.SubactivityState;

import net.jgsuess.uml14.behavioral_elements.state_machines.tests.SubmachineStateTest;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Subactivity State</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SubactivityStateTest extends SubmachineStateTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SubactivityStateTest.class);
	}

	/**
	 * Constructs a new Subactivity State test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubactivityStateTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Subactivity State test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected SubactivityState getFixture() {
		return (SubactivityState)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Activity_graphsFactory.eINSTANCE.createSubactivityState());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //SubactivityStateTest
