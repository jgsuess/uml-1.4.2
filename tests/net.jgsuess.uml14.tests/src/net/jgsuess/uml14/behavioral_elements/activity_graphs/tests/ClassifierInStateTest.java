/**
 * <copyright>
 * </copyright>
 *
 * $Id: ClassifierInStateTest.java,v 1.1 2012/04/23 09:32:31 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.activity_graphs.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsFactory;
import net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState;

import net.jgsuess.uml14.foundation.core.tests.ClassifierTest;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Classifier In State</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClassifierInStateTest extends ClassifierTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ClassifierInStateTest.class);
	}

	/**
	 * Constructs a new Classifier In State test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassifierInStateTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Classifier In State test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ClassifierInState getFixture() {
		return (ClassifierInState)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Activity_graphsFactory.eINSTANCE.createClassifierInState());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ClassifierInStateTest
