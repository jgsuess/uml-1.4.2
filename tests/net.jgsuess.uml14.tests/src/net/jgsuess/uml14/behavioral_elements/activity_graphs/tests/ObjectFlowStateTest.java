/**
 * <copyright>
 * </copyright>
 *
 * $Id: ObjectFlowStateTest.java,v 1.1 2012/04/23 09:32:31 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.activity_graphs.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsFactory;
import net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState;

import net.jgsuess.uml14.behavioral_elements.state_machines.tests.SimpleStateTest;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Object Flow State</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ObjectFlowStateTest extends SimpleStateTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ObjectFlowStateTest.class);
	}

	/**
	 * Constructs a new Object Flow State test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectFlowStateTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Object Flow State test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ObjectFlowState getFixture() {
		return (ObjectFlowState)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Activity_graphsFactory.eINSTANCE.createObjectFlowState());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ObjectFlowStateTest
