/**
 * <copyright>
 * </copyright>
 *
 * $Id: CallStateTest.java,v 1.1 2012/04/23 09:32:31 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.activity_graphs.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsFactory;
import net.jgsuess.uml14.behavioral_elements.activity_graphs.CallState;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Call State</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CallStateTest extends ActionStateTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CallStateTest.class);
	}

	/**
	 * Constructs a new Call State test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallStateTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Call State test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CallState getFixture() {
		return (CallState)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Activity_graphsFactory.eINSTANCE.createCallState());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CallStateTest
