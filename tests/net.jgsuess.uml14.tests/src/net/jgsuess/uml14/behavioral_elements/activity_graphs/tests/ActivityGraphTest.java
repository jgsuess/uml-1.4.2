/**
 * <copyright>
 * </copyright>
 *
 * $Id: ActivityGraphTest.java,v 1.1 2012/04/23 09:32:31 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.activity_graphs.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.ActivityGraph;
import net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsFactory;

import net.jgsuess.uml14.behavioral_elements.state_machines.tests.StateMachineTest;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Activity Graph</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ActivityGraphTest extends StateMachineTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ActivityGraphTest.class);
	}

	/**
	 * Constructs a new Activity Graph test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityGraphTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Activity Graph test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ActivityGraph getFixture() {
		return (ActivityGraph)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Activity_graphsFactory.eINSTANCE.createActivityGraph());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ActivityGraphTest
