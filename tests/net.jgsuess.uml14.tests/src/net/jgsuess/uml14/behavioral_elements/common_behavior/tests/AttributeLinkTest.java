/**
 * <copyright>
 * </copyright>
 *
 * $Id: AttributeLinkTest.java,v 1.1 2012/04/23 09:32:37 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.behavioral_elements.common_behavior.AttributeLink;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorFactory;

import net.jgsuess.uml14.foundation.core.tests.ModelElementTest;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Attribute Link</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class AttributeLinkTest extends ModelElementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(AttributeLinkTest.class);
	}

	/**
	 * Constructs a new Attribute Link test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeLinkTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Attribute Link test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected AttributeLink getFixture() {
		return (AttributeLink)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Common_behaviorFactory.eINSTANCE.createAttributeLink());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //AttributeLinkTest
