/**
 * <copyright>
 * </copyright>
 *
 * $Id: SendActionTest.java,v 1.1 2012/04/23 09:32:35 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorFactory;
import net.jgsuess.uml14.behavioral_elements.common_behavior.SendAction;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Send Action</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SendActionTest extends ActionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SendActionTest.class);
	}

	/**
	 * Constructs a new Send Action test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SendActionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Send Action test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected SendAction getFixture() {
		return (SendAction)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Common_behaviorFactory.eINSTANCE.createSendAction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //SendActionTest
