/**
 * <copyright>
 * </copyright>
 *
 * $Id: UninterpretedActionTest.java,v 1.1 2012/04/23 09:32:35 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorFactory;
import net.jgsuess.uml14.behavioral_elements.common_behavior.UninterpretedAction;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Uninterpreted Action</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UninterpretedActionTest extends ActionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UninterpretedActionTest.class);
	}

	/**
	 * Constructs a new Uninterpreted Action test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UninterpretedActionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Uninterpreted Action test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected UninterpretedAction getFixture() {
		return (UninterpretedAction)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Common_behaviorFactory.eINSTANCE.createUninterpretedAction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UninterpretedActionTest
