/**
 * <copyright>
 * </copyright>
 *
 * $Id: ExceptionTest.java,v 1.1 2012/04/23 09:32:37 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Exception</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ExceptionTest extends SignalTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ExceptionTest.class);
	}

	/**
	 * Constructs a new Exception test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExceptionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Exception test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected net.jgsuess.uml14.behavioral_elements.common_behavior.Exception getFixture() {
		return (net.jgsuess.uml14.behavioral_elements.common_behavior.Exception)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Common_behaviorFactory.eINSTANCE.createException());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ExceptionTest
