/**
 * <copyright>
 * </copyright>
 *
 * $Id: Common_behaviorTests.java,v 1.1 2012/04/23 09:32:35 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>common_behavior</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class Common_behaviorTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new Common_behaviorTests("common_behavior Tests");
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Common_behaviorTests(String name) {
		super(name);
	}

} //Common_behaviorTests
