/**
 * <copyright>
 * </copyright>
 *
 * $Id: CallActionTest.java,v 1.1 2012/04/23 09:32:35 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.behavioral_elements.common_behavior.CallAction;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Call Action</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CallActionTest extends ActionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CallActionTest.class);
	}

	/**
	 * Constructs a new Call Action test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallActionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Call Action test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CallAction getFixture() {
		return (CallAction)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Common_behaviorFactory.eINSTANCE.createCallAction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CallActionTest
