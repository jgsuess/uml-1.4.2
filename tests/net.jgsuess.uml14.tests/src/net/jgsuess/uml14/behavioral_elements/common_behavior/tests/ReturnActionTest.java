/**
 * <copyright>
 * </copyright>
 *
 * $Id: ReturnActionTest.java,v 1.1 2012/04/23 09:32:37 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorFactory;
import net.jgsuess.uml14.behavioral_elements.common_behavior.ReturnAction;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Return Action</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ReturnActionTest extends ActionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ReturnActionTest.class);
	}

	/**
	 * Constructs a new Return Action test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReturnActionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Return Action test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ReturnAction getFixture() {
		return (ReturnAction)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Common_behaviorFactory.eINSTANCE.createReturnAction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ReturnActionTest
