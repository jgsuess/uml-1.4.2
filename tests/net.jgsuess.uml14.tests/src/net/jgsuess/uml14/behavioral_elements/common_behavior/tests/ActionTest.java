/**
 * <copyright>
 * </copyright>
 *
 * $Id: ActionTest.java,v 1.1 2012/04/23 09:32:36 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior.tests;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Action;

import net.jgsuess.uml14.foundation.core.tests.ModelElementTest;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ActionTest extends ModelElementTest {

	/**
	 * Constructs a new Action test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Action test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Action getFixture() {
		return (Action)fixture;
	}

} //ActionTest
