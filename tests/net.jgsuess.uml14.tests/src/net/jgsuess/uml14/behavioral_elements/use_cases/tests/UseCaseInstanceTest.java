/**
 * <copyright>
 * </copyright>
 *
 * $Id: UseCaseInstanceTest.java,v 1.1 2012/04/23 09:32:43 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.use_cases.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.behavioral_elements.common_behavior.tests.InstanceTest;

import net.jgsuess.uml14.behavioral_elements.use_cases.UseCaseInstance;
import net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Use Case Instance</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UseCaseInstanceTest extends InstanceTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UseCaseInstanceTest.class);
	}

	/**
	 * Constructs a new Use Case Instance test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseCaseInstanceTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Use Case Instance test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected UseCaseInstance getFixture() {
		return (UseCaseInstance)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Use_casesFactory.eINSTANCE.createUseCaseInstance());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UseCaseInstanceTest
