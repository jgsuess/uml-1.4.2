/**
 * <copyright>
 * </copyright>
 *
 * $Id: SimpleStateTest.java,v 1.1 2012/04/23 09:32:35 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.state_machines.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.behavioral_elements.state_machines.SimpleState;
import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Simple State</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SimpleStateTest extends StateTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SimpleStateTest.class);
	}

	/**
	 * Constructs a new Simple State test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleStateTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Simple State test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected SimpleState getFixture() {
		return (SimpleState)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(State_machinesFactory.eINSTANCE.createSimpleState());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //SimpleStateTest
