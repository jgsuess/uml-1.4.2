/**
 * <copyright>
 * </copyright>
 *
 * $Id: StateVertexTest.java,v 1.1 2012/04/23 09:32:34 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.state_machines.tests;

import net.jgsuess.uml14.behavioral_elements.state_machines.StateVertex;

import net.jgsuess.uml14.foundation.core.tests.ModelElementTest;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>State Vertex</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class StateVertexTest extends ModelElementTest {

	/**
	 * Constructs a new State Vertex test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateVertexTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this State Vertex test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected StateVertex getFixture() {
		return (StateVertex)fixture;
	}

} //StateVertexTest
