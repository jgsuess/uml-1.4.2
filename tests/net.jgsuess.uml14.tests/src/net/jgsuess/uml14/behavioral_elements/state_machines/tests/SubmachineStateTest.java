/**
 * <copyright>
 * </copyright>
 *
 * $Id: SubmachineStateTest.java,v 1.1 2012/04/23 09:32:33 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.state_machines.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesFactory;
import net.jgsuess.uml14.behavioral_elements.state_machines.SubmachineState;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Submachine State</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SubmachineStateTest extends CompositeStateTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SubmachineStateTest.class);
	}

	/**
	 * Constructs a new Submachine State test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubmachineStateTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Submachine State test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected SubmachineState getFixture() {
		return (SubmachineState)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(State_machinesFactory.eINSTANCE.createSubmachineState());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //SubmachineStateTest
