/**
 * <copyright>
 * </copyright>
 *
 * $Id: TimeEventTest.java,v 1.1 2012/04/23 09:32:34 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.state_machines.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesFactory;
import net.jgsuess.uml14.behavioral_elements.state_machines.TimeEvent;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Time Event</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class TimeEventTest extends EventTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(TimeEventTest.class);
	}

	/**
	 * Constructs a new Time Event test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeEventTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Time Event test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected TimeEvent getFixture() {
		return (TimeEvent)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(State_machinesFactory.eINSTANCE.createTimeEvent());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //TimeEventTest
