/**
 * <copyright>
 * </copyright>
 *
 * $Id: AssociationEndRoleTest.java,v 1.1 2012/04/23 09:32:43 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsFactory;

import net.jgsuess.uml14.foundation.core.tests.AssociationEndTest;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Association End Role</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class AssociationEndRoleTest extends AssociationEndTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(AssociationEndRoleTest.class);
	}

	/**
	 * Constructs a new Association End Role test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationEndRoleTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Association End Role test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected AssociationEndRole getFixture() {
		return (AssociationEndRole)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CollaborationsFactory.eINSTANCE.createAssociationEndRole());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //AssociationEndRoleTest
