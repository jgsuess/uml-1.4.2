/**
 * <copyright>
 * </copyright>
 *
 * $Id: ClassifierRoleTest.java,v 1.1 2012/04/23 09:32:43 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsFactory;

import net.jgsuess.uml14.foundation.core.tests.ClassifierTest;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Classifier Role</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClassifierRoleTest extends ClassifierTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ClassifierRoleTest.class);
	}

	/**
	 * Constructs a new Classifier Role test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassifierRoleTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Classifier Role test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ClassifierRole getFixture() {
		return (ClassifierRole)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CollaborationsFactory.eINSTANCE.createClassifierRole());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ClassifierRoleTest
