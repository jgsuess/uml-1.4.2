/**
 * <copyright>
 * </copyright>
 *
 * $Id: InteractionInstanceSetTest.java,v 1.1 2012/04/23 09:32:43 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsFactory;
import net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet;

import net.jgsuess.uml14.foundation.core.tests.ModelElementTest;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Interaction Instance Set</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class InteractionInstanceSetTest extends ModelElementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(InteractionInstanceSetTest.class);
	}

	/**
	 * Constructs a new Interaction Instance Set test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionInstanceSetTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Interaction Instance Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected InteractionInstanceSet getFixture() {
		return (InteractionInstanceSet)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CollaborationsFactory.eINSTANCE.createInteractionInstanceSet());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //InteractionInstanceSetTest
