/**
 * <copyright>
 * </copyright>
 *
 * $Id: CollaborationInstanceSetTest.java,v 1.1 2012/04/23 09:32:43 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationInstanceSet;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsFactory;

import net.jgsuess.uml14.foundation.core.tests.ModelElementTest;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Collaboration Instance Set</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CollaborationInstanceSetTest extends ModelElementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CollaborationInstanceSetTest.class);
	}

	/**
	 * Constructs a new Collaboration Instance Set test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CollaborationInstanceSetTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Collaboration Instance Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CollaborationInstanceSet getFixture() {
		return (CollaborationInstanceSet)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CollaborationsFactory.eINSTANCE.createCollaborationInstanceSet());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CollaborationInstanceSetTest
