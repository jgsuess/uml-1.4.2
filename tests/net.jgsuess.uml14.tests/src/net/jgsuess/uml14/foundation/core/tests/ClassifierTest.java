/**
 * <copyright>
 * </copyright>
 *
 * $Id: ClassifierTest.java,v 1.1 2012/04/23 09:32:42 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.tests;

import net.jgsuess.uml14.foundation.core.Classifier;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Classifier</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ClassifierTest extends GeneralizableElementTest {

	/**
	 * Constructs a new Classifier test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassifierTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Classifier test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Classifier getFixture() {
		return (Classifier)fixture;
	}

} //ClassifierTest
