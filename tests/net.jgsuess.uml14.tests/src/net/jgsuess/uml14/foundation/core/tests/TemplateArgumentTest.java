/**
 * <copyright>
 * </copyright>
 *
 * $Id: TemplateArgumentTest.java,v 1.1 2012/04/23 09:32:40 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import net.jgsuess.uml14.foundation.core.CoreFactory;
import net.jgsuess.uml14.foundation.core.TemplateArgument;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Template Argument</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class TemplateArgumentTest extends TestCase {

	/**
	 * The fixture for this Template Argument test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TemplateArgument fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(TemplateArgumentTest.class);
	}

	/**
	 * Constructs a new Template Argument test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemplateArgumentTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Template Argument test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(TemplateArgument fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Template Argument test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TemplateArgument getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreFactory.eINSTANCE.createTemplateArgument());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //TemplateArgumentTest
