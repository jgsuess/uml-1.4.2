/**
 * <copyright>
 * </copyright>
 *
 * $Id: TagDefinitionTest.java,v 1.1 2012/04/23 09:32:37 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.foundation.core.CoreFactory;
import net.jgsuess.uml14.foundation.core.TagDefinition;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Tag Definition</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class TagDefinitionTest extends ModelElementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(TagDefinitionTest.class);
	}

	/**
	 * Constructs a new Tag Definition test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TagDefinitionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Tag Definition test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected TagDefinition getFixture() {
		return (TagDefinition)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreFactory.eINSTANCE.createTagDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //TagDefinitionTest
