/**
 * <copyright>
 * </copyright>
 *
 * $Id: ProgrammingLanguageDataTypeTest.java,v 1.1 2012/04/23 09:32:41 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.foundation.core.CoreFactory;
import net.jgsuess.uml14.foundation.core.ProgrammingLanguageDataType;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Programming Language Data Type</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ProgrammingLanguageDataTypeTest extends DataTypeTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ProgrammingLanguageDataTypeTest.class);
	}

	/**
	 * Constructs a new Programming Language Data Type test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProgrammingLanguageDataTypeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Programming Language Data Type test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ProgrammingLanguageDataType getFixture() {
		return (ProgrammingLanguageDataType)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreFactory.eINSTANCE.createProgrammingLanguageDataType());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ProgrammingLanguageDataTypeTest
