/**
 * <copyright>
 * </copyright>
 *
 * $Id: PresentationElementTest.java,v 1.1 2012/04/23 09:32:42 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.tests;

import net.jgsuess.uml14.foundation.core.PresentationElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Presentation Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class PresentationElementTest extends ElementTest {

	/**
	 * Constructs a new Presentation Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PresentationElementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Presentation Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected PresentationElement getFixture() {
		return (PresentationElement)fixture;
	}

} //PresentationElementTest
