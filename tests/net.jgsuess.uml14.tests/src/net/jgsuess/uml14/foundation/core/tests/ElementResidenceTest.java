/**
 * <copyright>
 * </copyright>
 *
 * $Id: ElementResidenceTest.java,v 1.1 2012/04/23 09:32:38 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import net.jgsuess.uml14.foundation.core.CoreFactory;
import net.jgsuess.uml14.foundation.core.ElementResidence;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Element Residence</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ElementResidenceTest extends TestCase {

	/**
	 * The fixture for this Element Residence test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElementResidence fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ElementResidenceTest.class);
	}

	/**
	 * Constructs a new Element Residence test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementResidenceTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Element Residence test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ElementResidence fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Element Residence test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElementResidence getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreFactory.eINSTANCE.createElementResidence());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ElementResidenceTest
