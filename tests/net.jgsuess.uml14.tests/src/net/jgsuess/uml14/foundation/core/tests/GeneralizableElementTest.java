/**
 * <copyright>
 * </copyright>
 *
 * $Id: GeneralizableElementTest.java,v 1.1 2012/04/23 09:32:37 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.tests;

import net.jgsuess.uml14.foundation.core.GeneralizableElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Generalizable Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class GeneralizableElementTest extends ModelElementTest {

	/**
	 * Constructs a new Generalizable Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeneralizableElementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Generalizable Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected GeneralizableElement getFixture() {
		return (GeneralizableElement)fixture;
	}

} //GeneralizableElementTest
