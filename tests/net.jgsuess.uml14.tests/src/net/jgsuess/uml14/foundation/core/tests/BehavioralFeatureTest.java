/**
 * <copyright>
 * </copyright>
 *
 * $Id: BehavioralFeatureTest.java,v 1.1 2012/04/23 09:32:40 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.tests;

import net.jgsuess.uml14.foundation.core.BehavioralFeature;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Behavioral Feature</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class BehavioralFeatureTest extends FeatureTest {

	/**
	 * Constructs a new Behavioral Feature test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BehavioralFeatureTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Behavioral Feature test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected BehavioralFeature getFixture() {
		return (BehavioralFeature)fixture;
	}

} //BehavioralFeatureTest
