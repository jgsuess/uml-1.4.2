/**
 * <copyright>
 * </copyright>
 *
 * $Id: NamespaceTest.java,v 1.1 2012/04/23 09:32:39 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.tests;

import net.jgsuess.uml14.foundation.core.Namespace;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Namespace</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class NamespaceTest extends ModelElementTest {

	/**
	 * Constructs a new Namespace test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamespaceTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Namespace test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Namespace getFixture() {
		return (Namespace)fixture;
	}

} //NamespaceTest
