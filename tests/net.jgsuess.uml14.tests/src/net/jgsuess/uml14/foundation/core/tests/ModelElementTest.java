/**
 * <copyright>
 * </copyright>
 *
 * $Id: ModelElementTest.java,v 1.1 2012/04/23 09:32:39 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.tests;

import net.jgsuess.uml14.foundation.core.ModelElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Model Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ModelElementTest extends ElementTest {

	/**
	 * Constructs a new Model Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelElementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Model Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ModelElement getFixture() {
		return (ModelElement)fixture;
	}

} //ModelElementTest
