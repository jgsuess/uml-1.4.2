/**
 * <copyright>
 * </copyright>
 *
 * $Id: IterationExpressionTest.java,v 1.1 2012/04/23 09:32:33 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.data_types.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.foundation.data_types.Data_typesFactory;
import net.jgsuess.uml14.foundation.data_types.IterationExpression;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Iteration Expression</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class IterationExpressionTest extends ExpressionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(IterationExpressionTest.class);
	}

	/**
	 * Constructs a new Iteration Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IterationExpressionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Iteration Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected IterationExpression getFixture() {
		return (IterationExpression)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Data_typesFactory.eINSTANCE.createIterationExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //IterationExpressionTest
