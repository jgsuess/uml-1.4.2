/**
 * <copyright>
 * </copyright>
 *
 * $Id: MultiplicityRangeTest.java,v 1.1 2012/04/23 09:32:31 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.data_types.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import net.jgsuess.uml14.foundation.data_types.Data_typesFactory;
import net.jgsuess.uml14.foundation.data_types.MultiplicityRange;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Multiplicity Range</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class MultiplicityRangeTest extends TestCase {

	/**
	 * The fixture for this Multiplicity Range test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MultiplicityRange fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(MultiplicityRangeTest.class);
	}

	/**
	 * Constructs a new Multiplicity Range test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplicityRangeTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Multiplicity Range test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(MultiplicityRange fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Multiplicity Range test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MultiplicityRange getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Data_typesFactory.eINSTANCE.createMultiplicityRange());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //MultiplicityRangeTest
