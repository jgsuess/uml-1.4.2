/**
 * <copyright>
 * </copyright>
 *
 * $Id: ArgListsExpressionTest.java,v 1.1 2012/04/23 09:32:33 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.data_types.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.foundation.data_types.ArgListsExpression;
import net.jgsuess.uml14.foundation.data_types.Data_typesFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Arg Lists Expression</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ArgListsExpressionTest extends ExpressionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ArgListsExpressionTest.class);
	}

	/**
	 * Constructs a new Arg Lists Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArgListsExpressionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Arg Lists Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ArgListsExpression getFixture() {
		return (ArgListsExpression)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Data_typesFactory.eINSTANCE.createArgListsExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ArgListsExpressionTest
