/**
 * <copyright>
 * </copyright>
 *
 * $Id: BooleanExpressionTest.java,v 1.1 2012/04/23 09:32:31 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.data_types.tests;

import junit.textui.TestRunner;

import net.jgsuess.uml14.foundation.data_types.BooleanExpression;
import net.jgsuess.uml14.foundation.data_types.Data_typesFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Boolean Expression</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class BooleanExpressionTest extends ExpressionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(BooleanExpressionTest.class);
	}

	/**
	 * Constructs a new Boolean Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanExpressionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Boolean Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected BooleanExpression getFixture() {
		return (BooleanExpression)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Data_typesFactory.eINSTANCE.createBooleanExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //BooleanExpressionTest
