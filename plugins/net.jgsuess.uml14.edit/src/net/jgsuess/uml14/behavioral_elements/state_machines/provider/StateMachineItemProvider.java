/**
 * <copyright>
 * </copyright>
 *
 * $Id: StateMachineItemProvider.java,v 1.1 2012/04/23 09:32:57 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.state_machines.provider;


import java.util.Collection;
import java.util.List;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsFactory;

import net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine;
import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesFactory;
import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage;

import net.jgsuess.uml14.foundation.core.provider.ModelElementItemProvider;

import net.jgsuess.uml14.provider.UML14EditPlugin;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class StateMachineItemProvider
	extends ModelElementItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateMachineItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addContextPropertyDescriptor(object);
			addSubmachineStatePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Context feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContextPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_StateMachine_context_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_StateMachine_context_feature", "_UI_StateMachine_type"),
				 State_machinesPackage.Literals.STATE_MACHINE__CONTEXT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Submachine State feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSubmachineStatePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_StateMachine_submachineState_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_StateMachine_submachineState_feature", "_UI_StateMachine_type"),
				 State_machinesPackage.Literals.STATE_MACHINE__SUBMACHINE_STATE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(State_machinesPackage.Literals.STATE_MACHINE__TOP);
			childrenFeatures.add(State_machinesPackage.Literals.STATE_MACHINE__TRANSITIONS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns StateMachine.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/StateMachine"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((StateMachine)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_StateMachine_type") :
			getString("_UI_StateMachine_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(StateMachine.class)) {
			case State_machinesPackage.STATE_MACHINE__TOP:
			case State_machinesPackage.STATE_MACHINE__TRANSITIONS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(State_machinesPackage.Literals.STATE_MACHINE__TOP,
				 State_machinesFactory.eINSTANCE.createCompositeState()));

		newChildDescriptors.add
			(createChildParameter
				(State_machinesPackage.Literals.STATE_MACHINE__TOP,
				 State_machinesFactory.eINSTANCE.createSimpleState()));

		newChildDescriptors.add
			(createChildParameter
				(State_machinesPackage.Literals.STATE_MACHINE__TOP,
				 State_machinesFactory.eINSTANCE.createSubmachineState()));

		newChildDescriptors.add
			(createChildParameter
				(State_machinesPackage.Literals.STATE_MACHINE__TOP,
				 State_machinesFactory.eINSTANCE.createFinalState()));

		newChildDescriptors.add
			(createChildParameter
				(State_machinesPackage.Literals.STATE_MACHINE__TOP,
				 Activity_graphsFactory.eINSTANCE.createSubactivityState()));

		newChildDescriptors.add
			(createChildParameter
				(State_machinesPackage.Literals.STATE_MACHINE__TOP,
				 Activity_graphsFactory.eINSTANCE.createActionState()));

		newChildDescriptors.add
			(createChildParameter
				(State_machinesPackage.Literals.STATE_MACHINE__TOP,
				 Activity_graphsFactory.eINSTANCE.createCallState()));

		newChildDescriptors.add
			(createChildParameter
				(State_machinesPackage.Literals.STATE_MACHINE__TOP,
				 Activity_graphsFactory.eINSTANCE.createObjectFlowState()));

		newChildDescriptors.add
			(createChildParameter
				(State_machinesPackage.Literals.STATE_MACHINE__TRANSITIONS,
				 State_machinesFactory.eINSTANCE.createTransition()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return UML14EditPlugin.INSTANCE;
	}

}
