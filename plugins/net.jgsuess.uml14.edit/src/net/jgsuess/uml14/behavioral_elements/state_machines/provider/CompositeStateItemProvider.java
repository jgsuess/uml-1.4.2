/**
 * <copyright>
 * </copyright>
 *
 * $Id: CompositeStateItemProvider.java,v 1.1 2012/04/23 09:32:58 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.state_machines.provider;


import java.util.Collection;
import java.util.List;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsFactory;

import net.jgsuess.uml14.behavioral_elements.state_machines.CompositeState;
import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesFactory;
import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link net.jgsuess.uml14.behavioral_elements.state_machines.CompositeState} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class CompositeStateItemProvider
	extends StateItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeStateItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addIsConcurrentPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Is Concurrent feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsConcurrentPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CompositeState_isConcurrent_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CompositeState_isConcurrent_feature", "_UI_CompositeState_type"),
				 State_machinesPackage.Literals.COMPOSITE_STATE__IS_CONCURRENT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(State_machinesPackage.Literals.COMPOSITE_STATE__SUBVERTEX);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns CompositeState.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/CompositeState"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((CompositeState)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_CompositeState_type") :
			getString("_UI_CompositeState_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(CompositeState.class)) {
			case State_machinesPackage.COMPOSITE_STATE__IS_CONCURRENT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case State_machinesPackage.COMPOSITE_STATE__SUBVERTEX:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(State_machinesPackage.Literals.COMPOSITE_STATE__SUBVERTEX,
				 State_machinesFactory.eINSTANCE.createCompositeState()));

		newChildDescriptors.add
			(createChildParameter
				(State_machinesPackage.Literals.COMPOSITE_STATE__SUBVERTEX,
				 State_machinesFactory.eINSTANCE.createPseudostate()));

		newChildDescriptors.add
			(createChildParameter
				(State_machinesPackage.Literals.COMPOSITE_STATE__SUBVERTEX,
				 State_machinesFactory.eINSTANCE.createSimpleState()));

		newChildDescriptors.add
			(createChildParameter
				(State_machinesPackage.Literals.COMPOSITE_STATE__SUBVERTEX,
				 State_machinesFactory.eINSTANCE.createSubmachineState()));

		newChildDescriptors.add
			(createChildParameter
				(State_machinesPackage.Literals.COMPOSITE_STATE__SUBVERTEX,
				 State_machinesFactory.eINSTANCE.createSynchState()));

		newChildDescriptors.add
			(createChildParameter
				(State_machinesPackage.Literals.COMPOSITE_STATE__SUBVERTEX,
				 State_machinesFactory.eINSTANCE.createStubState()));

		newChildDescriptors.add
			(createChildParameter
				(State_machinesPackage.Literals.COMPOSITE_STATE__SUBVERTEX,
				 State_machinesFactory.eINSTANCE.createFinalState()));

		newChildDescriptors.add
			(createChildParameter
				(State_machinesPackage.Literals.COMPOSITE_STATE__SUBVERTEX,
				 Activity_graphsFactory.eINSTANCE.createSubactivityState()));

		newChildDescriptors.add
			(createChildParameter
				(State_machinesPackage.Literals.COMPOSITE_STATE__SUBVERTEX,
				 Activity_graphsFactory.eINSTANCE.createActionState()));

		newChildDescriptors.add
			(createChildParameter
				(State_machinesPackage.Literals.COMPOSITE_STATE__SUBVERTEX,
				 Activity_graphsFactory.eINSTANCE.createCallState()));

		newChildDescriptors.add
			(createChildParameter
				(State_machinesPackage.Literals.COMPOSITE_STATE__SUBVERTEX,
				 Activity_graphsFactory.eINSTANCE.createObjectFlowState()));
	}

}
