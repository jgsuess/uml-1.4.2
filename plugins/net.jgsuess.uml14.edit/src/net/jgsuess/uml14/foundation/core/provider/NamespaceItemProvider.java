/**
 * <copyright>
 * </copyright>
 *
 * $Id: NamespaceItemProvider.java,v 1.1 2012/04/23 09:32:54 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.provider;


import java.util.Collection;
import java.util.List;

import net.jgsuess.uml14.Uml14Factory;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsFactory;

import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsFactory;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorFactory;

import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesFactory;

import net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesFactory;

import net.jgsuess.uml14.foundation.core.CoreFactory;
import net.jgsuess.uml14.foundation.core.CorePackage;
import net.jgsuess.uml14.foundation.core.Namespace;

import net.jgsuess.uml14.model_management.Model_managementFactory;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link net.jgsuess.uml14.foundation.core.Namespace} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class NamespaceItemProvider
	extends ModelElementItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamespaceItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Namespace)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Namespace_type") :
			getString("_UI_Namespace_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Namespace.class)) {
			case CorePackage.NAMESPACE__OWNED_ELEMENT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createClass()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createDataType()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createAssociationEnd()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createInterface()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createConstraint()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createAssociation()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createOperation()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createParameter()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createMethod()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createGeneralization()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createAssociationClass()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createDependency()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createAbstraction()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createUsage()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createBinding()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createComponent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createNode()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createPermission()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createComment()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createFlow()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createPrimitive()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createEnumeration()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createEnumerationLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createStereotype()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createTagDefinition()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createTaggedValue()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createProgrammingLanguageDataType()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CoreFactory.eINSTANCE.createArtifact()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Uml14Factory.eINSTANCE.createModel()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Model_managementFactory.eINSTANCE.createPackage()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Model_managementFactory.eINSTANCE.createSubsystem()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createSignal()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createCreateAction()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createDestroyAction()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createUninterpretedAction()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createAttributeLink()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createObject()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createLink()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createLinkObject()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createDataValue()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createCallAction()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createSendAction()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createActionSequence()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createArgument()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createReception()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createLinkEnd()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createReturnAction()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createTerminateAction()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createStimulus()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createException()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createComponentInstance()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createNodeInstance()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Common_behaviorFactory.eINSTANCE.createSubsystemInstance()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Use_casesFactory.eINSTANCE.createUseCase()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Use_casesFactory.eINSTANCE.createActor()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Use_casesFactory.eINSTANCE.createUseCaseInstance()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Use_casesFactory.eINSTANCE.createExtend()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Use_casesFactory.eINSTANCE.createInclude()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Use_casesFactory.eINSTANCE.createExtensionPoint()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 State_machinesFactory.eINSTANCE.createStateMachine()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 State_machinesFactory.eINSTANCE.createTimeEvent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 State_machinesFactory.eINSTANCE.createCallEvent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 State_machinesFactory.eINSTANCE.createSignalEvent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 State_machinesFactory.eINSTANCE.createTransition()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 State_machinesFactory.eINSTANCE.createCompositeState()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 State_machinesFactory.eINSTANCE.createChangeEvent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 State_machinesFactory.eINSTANCE.createGuard()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 State_machinesFactory.eINSTANCE.createPseudostate()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 State_machinesFactory.eINSTANCE.createSimpleState()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 State_machinesFactory.eINSTANCE.createSubmachineState()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 State_machinesFactory.eINSTANCE.createSynchState()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 State_machinesFactory.eINSTANCE.createStubState()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 State_machinesFactory.eINSTANCE.createFinalState()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CollaborationsFactory.eINSTANCE.createCollaboration()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CollaborationsFactory.eINSTANCE.createClassifierRole()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CollaborationsFactory.eINSTANCE.createAssociationRole()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CollaborationsFactory.eINSTANCE.createAssociationEndRole()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CollaborationsFactory.eINSTANCE.createMessage()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CollaborationsFactory.eINSTANCE.createInteraction()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CollaborationsFactory.eINSTANCE.createInteractionInstanceSet()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 CollaborationsFactory.eINSTANCE.createCollaborationInstanceSet()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Activity_graphsFactory.eINSTANCE.createActivityGraph()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Activity_graphsFactory.eINSTANCE.createPartition()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Activity_graphsFactory.eINSTANCE.createSubactivityState()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Activity_graphsFactory.eINSTANCE.createActionState()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Activity_graphsFactory.eINSTANCE.createCallState()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Activity_graphsFactory.eINSTANCE.createObjectFlowState()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.NAMESPACE__OWNED_ELEMENT,
				 Activity_graphsFactory.eINSTANCE.createClassifierInState()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == CorePackage.Literals.MODEL_ELEMENT__TAGGED_VALUE ||
			childFeature == CorePackage.Literals.NAMESPACE__OWNED_ELEMENT;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
