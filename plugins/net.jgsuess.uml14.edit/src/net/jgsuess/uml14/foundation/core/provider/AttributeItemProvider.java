/**
 * <copyright>
 * </copyright>
 *
 * $Id: AttributeItemProvider.java,v 1.1 2012/04/23 09:32:56 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.provider;


import java.util.Collection;
import java.util.List;

import net.jgsuess.uml14.foundation.core.Attribute;
import net.jgsuess.uml14.foundation.core.CorePackage;

import net.jgsuess.uml14.foundation.data_types.Data_typesFactory;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link net.jgsuess.uml14.foundation.core.Attribute} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AttributeItemProvider
	extends StructuralFeatureItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAttributeLinkPropertyDescriptor(object);
			addAssociationEndRolePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Attribute Link feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAttributeLinkPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Attribute_attributeLink_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Attribute_attributeLink_feature", "_UI_Attribute_type"),
				 CorePackage.Literals.ATTRIBUTE__ATTRIBUTE_LINK,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Association End Role feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAssociationEndRolePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Attribute_associationEndRole_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Attribute_associationEndRole_feature", "_UI_Attribute_type"),
				 CorePackage.Literals.ATTRIBUTE__ASSOCIATION_END_ROLE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(CorePackage.Literals.ATTRIBUTE__INITIAL_VALUE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Attribute.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Attribute"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Attribute)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Attribute_type") :
			getString("_UI_Attribute_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Attribute.class)) {
			case CorePackage.ATTRIBUTE__INITIAL_VALUE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.ATTRIBUTE__INITIAL_VALUE,
				 Data_typesFactory.eINSTANCE.createExpression()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.ATTRIBUTE__INITIAL_VALUE,
				 Data_typesFactory.eINSTANCE.createBooleanExpression()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.ATTRIBUTE__INITIAL_VALUE,
				 Data_typesFactory.eINSTANCE.createTypeExpression()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.ATTRIBUTE__INITIAL_VALUE,
				 Data_typesFactory.eINSTANCE.createMappingExpression()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.ATTRIBUTE__INITIAL_VALUE,
				 Data_typesFactory.eINSTANCE.createProcedureExpression()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.ATTRIBUTE__INITIAL_VALUE,
				 Data_typesFactory.eINSTANCE.createObjectSetExpression()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.ATTRIBUTE__INITIAL_VALUE,
				 Data_typesFactory.eINSTANCE.createActionExpression()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.ATTRIBUTE__INITIAL_VALUE,
				 Data_typesFactory.eINSTANCE.createIterationExpression()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.ATTRIBUTE__INITIAL_VALUE,
				 Data_typesFactory.eINSTANCE.createTimeExpression()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.ATTRIBUTE__INITIAL_VALUE,
				 Data_typesFactory.eINSTANCE.createArgListsExpression()));
	}

}
