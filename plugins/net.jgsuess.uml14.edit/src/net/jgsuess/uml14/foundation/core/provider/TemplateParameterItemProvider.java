/**
 * <copyright>
 * </copyright>
 *
 * $Id: TemplateParameterItemProvider.java,v 1.1 2012/04/23 09:32:54 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.provider;


import java.util.Collection;
import java.util.List;

import net.jgsuess.uml14.Uml14Factory;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsFactory;

import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsFactory;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorFactory;

import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesFactory;

import net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesFactory;

import net.jgsuess.uml14.foundation.core.CoreFactory;
import net.jgsuess.uml14.foundation.core.CorePackage;
import net.jgsuess.uml14.foundation.core.TemplateParameter;

import net.jgsuess.uml14.model_management.Model_managementFactory;

import net.jgsuess.uml14.provider.UML14EditPlugin;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link net.jgsuess.uml14.foundation.core.TemplateParameter} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TemplateParameterItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemplateParameterItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addDefaultElementPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Default Element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDefaultElementPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TemplateParameter_defaultElement_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TemplateParameter_defaultElement_feature", "_UI_TemplateParameter_type"),
				 CorePackage.Literals.TEMPLATE_PARAMETER__DEFAULT_ELEMENT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns TemplateParameter.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/TemplateParameter"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_TemplateParameter_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(TemplateParameter.class)) {
			case CorePackage.TEMPLATE_PARAMETER__PARAMETER:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createClass()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createDataType()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createAssociationEnd()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createInterface()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createConstraint()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createAssociation()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createOperation()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createParameter()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createMethod()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createGeneralization()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createAssociationClass()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createDependency()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createAbstraction()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createUsage()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createBinding()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createComponent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createNode()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createPermission()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createComment()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createFlow()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createPrimitive()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createEnumeration()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createEnumerationLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createStereotype()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createTagDefinition()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createTaggedValue()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createProgrammingLanguageDataType()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CoreFactory.eINSTANCE.createArtifact()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Uml14Factory.eINSTANCE.createModel()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Model_managementFactory.eINSTANCE.createPackage()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Model_managementFactory.eINSTANCE.createSubsystem()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createSignal()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createCreateAction()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createDestroyAction()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createUninterpretedAction()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createAttributeLink()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createObject()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createLink()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createLinkObject()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createDataValue()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createCallAction()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createSendAction()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createActionSequence()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createArgument()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createReception()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createLinkEnd()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createReturnAction()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createTerminateAction()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createStimulus()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createException()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createComponentInstance()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createNodeInstance()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Common_behaviorFactory.eINSTANCE.createSubsystemInstance()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Use_casesFactory.eINSTANCE.createUseCase()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Use_casesFactory.eINSTANCE.createActor()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Use_casesFactory.eINSTANCE.createUseCaseInstance()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Use_casesFactory.eINSTANCE.createExtend()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Use_casesFactory.eINSTANCE.createInclude()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Use_casesFactory.eINSTANCE.createExtensionPoint()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 State_machinesFactory.eINSTANCE.createStateMachine()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 State_machinesFactory.eINSTANCE.createTimeEvent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 State_machinesFactory.eINSTANCE.createCallEvent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 State_machinesFactory.eINSTANCE.createSignalEvent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 State_machinesFactory.eINSTANCE.createTransition()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 State_machinesFactory.eINSTANCE.createCompositeState()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 State_machinesFactory.eINSTANCE.createChangeEvent()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 State_machinesFactory.eINSTANCE.createGuard()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 State_machinesFactory.eINSTANCE.createPseudostate()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 State_machinesFactory.eINSTANCE.createSimpleState()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 State_machinesFactory.eINSTANCE.createSubmachineState()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 State_machinesFactory.eINSTANCE.createSynchState()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 State_machinesFactory.eINSTANCE.createStubState()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 State_machinesFactory.eINSTANCE.createFinalState()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CollaborationsFactory.eINSTANCE.createCollaboration()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CollaborationsFactory.eINSTANCE.createClassifierRole()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CollaborationsFactory.eINSTANCE.createAssociationRole()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CollaborationsFactory.eINSTANCE.createAssociationEndRole()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CollaborationsFactory.eINSTANCE.createMessage()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CollaborationsFactory.eINSTANCE.createInteraction()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CollaborationsFactory.eINSTANCE.createInteractionInstanceSet()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 CollaborationsFactory.eINSTANCE.createCollaborationInstanceSet()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Activity_graphsFactory.eINSTANCE.createActivityGraph()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Activity_graphsFactory.eINSTANCE.createPartition()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Activity_graphsFactory.eINSTANCE.createSubactivityState()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Activity_graphsFactory.eINSTANCE.createActionState()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Activity_graphsFactory.eINSTANCE.createCallState()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Activity_graphsFactory.eINSTANCE.createObjectFlowState()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.TEMPLATE_PARAMETER__PARAMETER,
				 Activity_graphsFactory.eINSTANCE.createClassifierInState()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return UML14EditPlugin.INSTANCE;
	}

}
