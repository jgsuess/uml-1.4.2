/**
 * <copyright>
 * </copyright>
 *
 * $Id: AssociationEndRole.java,v 1.1 2012/04/23 09:31:24 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations;

import net.jgsuess.uml14.foundation.core.AssociationEnd;
import net.jgsuess.uml14.foundation.core.Attribute;

import net.jgsuess.uml14.foundation.data_types.Multiplicity;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Association End Role</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole#getCollaborationMultiplicity <em>Collaboration Multiplicity</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole#getBase <em>Base</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole#getAvailableQualifier <em>Available Qualifier</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getAssociationEndRole()
 * @model
 * @generated
 */
public interface AssociationEndRole extends AssociationEnd {
	/**
	 * Returns the value of the '<em><b>Collaboration Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collaboration Multiplicity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collaboration Multiplicity</em>' containment reference.
	 * @see #setCollaborationMultiplicity(Multiplicity)
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getAssociationEndRole_CollaborationMultiplicity()
	 * @model containment="true"
	 * @generated
	 */
	Multiplicity getCollaborationMultiplicity();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole#getCollaborationMultiplicity <em>Collaboration Multiplicity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Collaboration Multiplicity</em>' containment reference.
	 * @see #getCollaborationMultiplicity()
	 * @generated
	 */
	void setCollaborationMultiplicity(Multiplicity value);

	/**
	 * Returns the value of the '<em><b>Base</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getAssociationEndRole <em>Association End Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base</em>' reference.
	 * @see #setBase(AssociationEnd)
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getAssociationEndRole_Base()
	 * @see net.jgsuess.uml14.foundation.core.AssociationEnd#getAssociationEndRole
	 * @model opposite="associationEndRole"
	 * @generated
	 */
	AssociationEnd getBase();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole#getBase <em>Base</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base</em>' reference.
	 * @see #getBase()
	 * @generated
	 */
	void setBase(AssociationEnd value);

	/**
	 * Returns the value of the '<em><b>Available Qualifier</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Attribute}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Attribute#getAssociationEndRole <em>Association End Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Available Qualifier</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Available Qualifier</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getAssociationEndRole_AvailableQualifier()
	 * @see net.jgsuess.uml14.foundation.core.Attribute#getAssociationEndRole
	 * @model opposite="associationEndRole"
	 * @generated
	 */
	EList<Attribute> getAvailableQualifier();

} // AssociationEndRole
