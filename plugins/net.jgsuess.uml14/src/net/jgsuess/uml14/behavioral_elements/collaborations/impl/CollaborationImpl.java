/**
 * <copyright>
 * </copyright>
 *
 * $Id: CollaborationImpl.java,v 1.1 2012/04/23 09:31:40 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.collaborations.Collaboration;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage;
import net.jgsuess.uml14.behavioral_elements.collaborations.Interaction;

import net.jgsuess.uml14.foundation.core.CorePackage;
import net.jgsuess.uml14.foundation.core.ModelElement;
import net.jgsuess.uml14.foundation.core.Namespace;

import net.jgsuess.uml14.foundation.core.impl.GeneralizableElementImpl;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Collaboration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationImpl#getOwnedElement <em>Owned Element</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationImpl#getInteraction <em>Interaction</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationImpl#getConstrainingElement <em>Constraining Element</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CollaborationImpl extends GeneralizableElementImpl implements Collaboration {
	/**
	 * The cached value of the '{@link #getOwnedElement() <em>Owned Element</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedElement()
	 * @generated
	 * @ordered
	 */
	protected EList<ModelElement> ownedElement;

	/**
	 * The cached value of the '{@link #getInteraction() <em>Interaction</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInteraction()
	 * @generated
	 * @ordered
	 */
	protected EList<Interaction> interaction;

	/**
	 * The cached value of the '{@link #getConstrainingElement() <em>Constraining Element</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstrainingElement()
	 * @generated
	 * @ordered
	 */
	protected EList<ModelElement> constrainingElement;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CollaborationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CollaborationsPackage.Literals.COLLABORATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModelElement> getOwnedElement() {
		if (ownedElement == null) {
			ownedElement = new EObjectContainmentWithInverseEList<ModelElement>(ModelElement.class, this, CollaborationsPackage.COLLABORATION__OWNED_ELEMENT, CorePackage.MODEL_ELEMENT__NAMESPACE);
		}
		return ownedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Interaction> getInteraction() {
		if (interaction == null) {
			interaction = new EObjectContainmentWithInverseEList<Interaction>(Interaction.class, this, CollaborationsPackage.COLLABORATION__INTERACTION, CollaborationsPackage.INTERACTION__CONTEXT);
		}
		return interaction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModelElement> getConstrainingElement() {
		if (constrainingElement == null) {
			constrainingElement = new EObjectWithInverseResolvingEList.ManyInverse<ModelElement>(ModelElement.class, this, CollaborationsPackage.COLLABORATION__CONSTRAINING_ELEMENT, CorePackage.MODEL_ELEMENT__COLLABORATION);
		}
		return constrainingElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CollaborationsPackage.COLLABORATION__OWNED_ELEMENT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOwnedElement()).basicAdd(otherEnd, msgs);
			case CollaborationsPackage.COLLABORATION__INTERACTION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getInteraction()).basicAdd(otherEnd, msgs);
			case CollaborationsPackage.COLLABORATION__CONSTRAINING_ELEMENT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getConstrainingElement()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CollaborationsPackage.COLLABORATION__OWNED_ELEMENT:
				return ((InternalEList<?>)getOwnedElement()).basicRemove(otherEnd, msgs);
			case CollaborationsPackage.COLLABORATION__INTERACTION:
				return ((InternalEList<?>)getInteraction()).basicRemove(otherEnd, msgs);
			case CollaborationsPackage.COLLABORATION__CONSTRAINING_ELEMENT:
				return ((InternalEList<?>)getConstrainingElement()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CollaborationsPackage.COLLABORATION__OWNED_ELEMENT:
				return getOwnedElement();
			case CollaborationsPackage.COLLABORATION__INTERACTION:
				return getInteraction();
			case CollaborationsPackage.COLLABORATION__CONSTRAINING_ELEMENT:
				return getConstrainingElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CollaborationsPackage.COLLABORATION__OWNED_ELEMENT:
				getOwnedElement().clear();
				getOwnedElement().addAll((Collection<? extends ModelElement>)newValue);
				return;
			case CollaborationsPackage.COLLABORATION__INTERACTION:
				getInteraction().clear();
				getInteraction().addAll((Collection<? extends Interaction>)newValue);
				return;
			case CollaborationsPackage.COLLABORATION__CONSTRAINING_ELEMENT:
				getConstrainingElement().clear();
				getConstrainingElement().addAll((Collection<? extends ModelElement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CollaborationsPackage.COLLABORATION__OWNED_ELEMENT:
				getOwnedElement().clear();
				return;
			case CollaborationsPackage.COLLABORATION__INTERACTION:
				getInteraction().clear();
				return;
			case CollaborationsPackage.COLLABORATION__CONSTRAINING_ELEMENT:
				getConstrainingElement().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CollaborationsPackage.COLLABORATION__OWNED_ELEMENT:
				return ownedElement != null && !ownedElement.isEmpty();
			case CollaborationsPackage.COLLABORATION__INTERACTION:
				return interaction != null && !interaction.isEmpty();
			case CollaborationsPackage.COLLABORATION__CONSTRAINING_ELEMENT:
				return constrainingElement != null && !constrainingElement.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Namespace.class) {
			switch (derivedFeatureID) {
				case CollaborationsPackage.COLLABORATION__OWNED_ELEMENT: return CorePackage.NAMESPACE__OWNED_ELEMENT;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Namespace.class) {
			switch (baseFeatureID) {
				case CorePackage.NAMESPACE__OWNED_ELEMENT: return CollaborationsPackage.COLLABORATION__OWNED_ELEMENT;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //CollaborationImpl
