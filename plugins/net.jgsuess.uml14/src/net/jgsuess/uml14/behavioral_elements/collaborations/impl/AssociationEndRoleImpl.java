/**
 * <copyright>
 * </copyright>
 *
 * $Id: AssociationEndRoleImpl.java,v 1.1 2012/04/23 09:31:39 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage;

import net.jgsuess.uml14.foundation.core.AssociationEnd;
import net.jgsuess.uml14.foundation.core.Attribute;
import net.jgsuess.uml14.foundation.core.CorePackage;

import net.jgsuess.uml14.foundation.core.impl.AssociationEndImpl;

import net.jgsuess.uml14.foundation.data_types.Multiplicity;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Association End Role</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.AssociationEndRoleImpl#getCollaborationMultiplicity <em>Collaboration Multiplicity</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.AssociationEndRoleImpl#getBase <em>Base</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.AssociationEndRoleImpl#getAvailableQualifier <em>Available Qualifier</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AssociationEndRoleImpl extends AssociationEndImpl implements AssociationEndRole {
	/**
	 * The cached value of the '{@link #getCollaborationMultiplicity() <em>Collaboration Multiplicity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollaborationMultiplicity()
	 * @generated
	 * @ordered
	 */
	protected Multiplicity collaborationMultiplicity;

	/**
	 * The cached value of the '{@link #getBase() <em>Base</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase()
	 * @generated
	 * @ordered
	 */
	protected AssociationEnd base;

	/**
	 * The cached value of the '{@link #getAvailableQualifier() <em>Available Qualifier</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvailableQualifier()
	 * @generated
	 * @ordered
	 */
	protected EList<Attribute> availableQualifier;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssociationEndRoleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CollaborationsPackage.Literals.ASSOCIATION_END_ROLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Multiplicity getCollaborationMultiplicity() {
		return collaborationMultiplicity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCollaborationMultiplicity(Multiplicity newCollaborationMultiplicity, NotificationChain msgs) {
		Multiplicity oldCollaborationMultiplicity = collaborationMultiplicity;
		collaborationMultiplicity = newCollaborationMultiplicity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CollaborationsPackage.ASSOCIATION_END_ROLE__COLLABORATION_MULTIPLICITY, oldCollaborationMultiplicity, newCollaborationMultiplicity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCollaborationMultiplicity(Multiplicity newCollaborationMultiplicity) {
		if (newCollaborationMultiplicity != collaborationMultiplicity) {
			NotificationChain msgs = null;
			if (collaborationMultiplicity != null)
				msgs = ((InternalEObject)collaborationMultiplicity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CollaborationsPackage.ASSOCIATION_END_ROLE__COLLABORATION_MULTIPLICITY, null, msgs);
			if (newCollaborationMultiplicity != null)
				msgs = ((InternalEObject)newCollaborationMultiplicity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CollaborationsPackage.ASSOCIATION_END_ROLE__COLLABORATION_MULTIPLICITY, null, msgs);
			msgs = basicSetCollaborationMultiplicity(newCollaborationMultiplicity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CollaborationsPackage.ASSOCIATION_END_ROLE__COLLABORATION_MULTIPLICITY, newCollaborationMultiplicity, newCollaborationMultiplicity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationEnd getBase() {
		if (base != null && base.eIsProxy()) {
			InternalEObject oldBase = (InternalEObject)base;
			base = (AssociationEnd)eResolveProxy(oldBase);
			if (base != oldBase) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CollaborationsPackage.ASSOCIATION_END_ROLE__BASE, oldBase, base));
			}
		}
		return base;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationEnd basicGetBase() {
		return base;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBase(AssociationEnd newBase, NotificationChain msgs) {
		AssociationEnd oldBase = base;
		base = newBase;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CollaborationsPackage.ASSOCIATION_END_ROLE__BASE, oldBase, newBase);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase(AssociationEnd newBase) {
		if (newBase != base) {
			NotificationChain msgs = null;
			if (base != null)
				msgs = ((InternalEObject)base).eInverseRemove(this, CorePackage.ASSOCIATION_END__ASSOCIATION_END_ROLE, AssociationEnd.class, msgs);
			if (newBase != null)
				msgs = ((InternalEObject)newBase).eInverseAdd(this, CorePackage.ASSOCIATION_END__ASSOCIATION_END_ROLE, AssociationEnd.class, msgs);
			msgs = basicSetBase(newBase, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CollaborationsPackage.ASSOCIATION_END_ROLE__BASE, newBase, newBase));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Attribute> getAvailableQualifier() {
		if (availableQualifier == null) {
			availableQualifier = new EObjectWithInverseResolvingEList.ManyInverse<Attribute>(Attribute.class, this, CollaborationsPackage.ASSOCIATION_END_ROLE__AVAILABLE_QUALIFIER, CorePackage.ATTRIBUTE__ASSOCIATION_END_ROLE);
		}
		return availableQualifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CollaborationsPackage.ASSOCIATION_END_ROLE__BASE:
				if (base != null)
					msgs = ((InternalEObject)base).eInverseRemove(this, CorePackage.ASSOCIATION_END__ASSOCIATION_END_ROLE, AssociationEnd.class, msgs);
				return basicSetBase((AssociationEnd)otherEnd, msgs);
			case CollaborationsPackage.ASSOCIATION_END_ROLE__AVAILABLE_QUALIFIER:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAvailableQualifier()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CollaborationsPackage.ASSOCIATION_END_ROLE__COLLABORATION_MULTIPLICITY:
				return basicSetCollaborationMultiplicity(null, msgs);
			case CollaborationsPackage.ASSOCIATION_END_ROLE__BASE:
				return basicSetBase(null, msgs);
			case CollaborationsPackage.ASSOCIATION_END_ROLE__AVAILABLE_QUALIFIER:
				return ((InternalEList<?>)getAvailableQualifier()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CollaborationsPackage.ASSOCIATION_END_ROLE__COLLABORATION_MULTIPLICITY:
				return getCollaborationMultiplicity();
			case CollaborationsPackage.ASSOCIATION_END_ROLE__BASE:
				if (resolve) return getBase();
				return basicGetBase();
			case CollaborationsPackage.ASSOCIATION_END_ROLE__AVAILABLE_QUALIFIER:
				return getAvailableQualifier();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CollaborationsPackage.ASSOCIATION_END_ROLE__COLLABORATION_MULTIPLICITY:
				setCollaborationMultiplicity((Multiplicity)newValue);
				return;
			case CollaborationsPackage.ASSOCIATION_END_ROLE__BASE:
				setBase((AssociationEnd)newValue);
				return;
			case CollaborationsPackage.ASSOCIATION_END_ROLE__AVAILABLE_QUALIFIER:
				getAvailableQualifier().clear();
				getAvailableQualifier().addAll((Collection<? extends Attribute>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CollaborationsPackage.ASSOCIATION_END_ROLE__COLLABORATION_MULTIPLICITY:
				setCollaborationMultiplicity((Multiplicity)null);
				return;
			case CollaborationsPackage.ASSOCIATION_END_ROLE__BASE:
				setBase((AssociationEnd)null);
				return;
			case CollaborationsPackage.ASSOCIATION_END_ROLE__AVAILABLE_QUALIFIER:
				getAvailableQualifier().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CollaborationsPackage.ASSOCIATION_END_ROLE__COLLABORATION_MULTIPLICITY:
				return collaborationMultiplicity != null;
			case CollaborationsPackage.ASSOCIATION_END_ROLE__BASE:
				return base != null;
			case CollaborationsPackage.ASSOCIATION_END_ROLE__AVAILABLE_QUALIFIER:
				return availableQualifier != null && !availableQualifier.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AssociationEndRoleImpl
