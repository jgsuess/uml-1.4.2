/**
 * <copyright>
 * </copyright>
 *
 * $Id: CollaborationInstanceSet.java,v 1.1 2012/04/23 09:31:24 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations;

import net.jgsuess.uml14.foundation.core.ModelElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Collaboration Instance Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationInstanceSet#getInteractionInstanceSet <em>Interaction Instance Set</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationInstanceSet#getConstrainingElement <em>Constraining Element</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getCollaborationInstanceSet()
 * @model
 * @generated
 */
public interface CollaborationInstanceSet extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Interaction Instance Set</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet#getContext <em>Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interaction Instance Set</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interaction Instance Set</em>' containment reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getCollaborationInstanceSet_InteractionInstanceSet()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet#getContext
	 * @model opposite="context" containment="true"
	 * @generated
	 */
	EList<InteractionInstanceSet> getInteractionInstanceSet();

	/**
	 * Returns the value of the '<em><b>Constraining Element</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.ModelElement}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.ModelElement#getCollaborationInstanceSet <em>Collaboration Instance Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraining Element</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraining Element</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getCollaborationInstanceSet_ConstrainingElement()
	 * @see net.jgsuess.uml14.foundation.core.ModelElement#getCollaborationInstanceSet
	 * @model opposite="collaborationInstanceSet"
	 * @generated
	 */
	EList<ModelElement> getConstrainingElement();

} // CollaborationInstanceSet
