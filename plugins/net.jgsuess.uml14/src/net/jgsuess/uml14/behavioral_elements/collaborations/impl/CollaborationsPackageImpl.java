/**
 * <copyright>
 * </copyright>
 *
 * $Id: CollaborationsPackageImpl.java,v 1.1 2012/04/23 09:31:39 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations.impl;

import net.jgsuess.uml14.Uml14Package;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.Activity_graphsPackageImpl;

import net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole;
import net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole;
import net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole;
import net.jgsuess.uml14.behavioral_elements.collaborations.Collaboration;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationInstanceSet;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsFactory;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage;
import net.jgsuess.uml14.behavioral_elements.collaborations.Interaction;
import net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet;
import net.jgsuess.uml14.behavioral_elements.collaborations.Message;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;

import net.jgsuess.uml14.behavioral_elements.common_behavior.impl.Common_behaviorPackageImpl;

import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage;

import net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl;

import net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage;

import net.jgsuess.uml14.behavioral_elements.use_cases.impl.Use_casesPackageImpl;

import net.jgsuess.uml14.foundation.core.CorePackage;

import net.jgsuess.uml14.foundation.core.impl.CorePackageImpl;

import net.jgsuess.uml14.foundation.data_types.Data_typesPackage;

import net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl;

import net.jgsuess.uml14.impl.Uml14PackageImpl;

import net.jgsuess.uml14.model_management.Model_managementPackage;

import net.jgsuess.uml14.model_management.impl.Model_managementPackageImpl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CollaborationsPackageImpl extends EPackageImpl implements CollaborationsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass collaborationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass classifierRoleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass associationRoleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass associationEndRoleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass messageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interactionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interactionInstanceSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass collaborationInstanceSetEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CollaborationsPackageImpl() {
		super(eNS_URI, CollaborationsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link CollaborationsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CollaborationsPackage init() {
		if (isInited) return (CollaborationsPackage)EPackage.Registry.INSTANCE.getEPackage(CollaborationsPackage.eNS_URI);

		// Obtain or create and register package
		CollaborationsPackageImpl theCollaborationsPackage = (CollaborationsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CollaborationsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CollaborationsPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		Uml14PackageImpl theUml14Package = (Uml14PackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Uml14Package.eNS_URI) instanceof Uml14PackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Uml14Package.eNS_URI) : Uml14Package.eINSTANCE);
		Model_managementPackageImpl theModel_managementPackage = (Model_managementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Model_managementPackage.eNS_URI) instanceof Model_managementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Model_managementPackage.eNS_URI) : Model_managementPackage.eINSTANCE);
		Data_typesPackageImpl theData_typesPackage = (Data_typesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Data_typesPackage.eNS_URI) instanceof Data_typesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Data_typesPackage.eNS_URI) : Data_typesPackage.eINSTANCE);
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		Common_behaviorPackageImpl theCommon_behaviorPackage = (Common_behaviorPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Common_behaviorPackage.eNS_URI) instanceof Common_behaviorPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Common_behaviorPackage.eNS_URI) : Common_behaviorPackage.eINSTANCE);
		Use_casesPackageImpl theUse_casesPackage = (Use_casesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Use_casesPackage.eNS_URI) instanceof Use_casesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Use_casesPackage.eNS_URI) : Use_casesPackage.eINSTANCE);
		State_machinesPackageImpl theState_machinesPackage = (State_machinesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(State_machinesPackage.eNS_URI) instanceof State_machinesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(State_machinesPackage.eNS_URI) : State_machinesPackage.eINSTANCE);
		Activity_graphsPackageImpl theActivity_graphsPackage = (Activity_graphsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Activity_graphsPackage.eNS_URI) instanceof Activity_graphsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Activity_graphsPackage.eNS_URI) : Activity_graphsPackage.eINSTANCE);

		// Create package meta-data objects
		theCollaborationsPackage.createPackageContents();
		theUml14Package.createPackageContents();
		theModel_managementPackage.createPackageContents();
		theData_typesPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theCommon_behaviorPackage.createPackageContents();
		theUse_casesPackage.createPackageContents();
		theState_machinesPackage.createPackageContents();
		theActivity_graphsPackage.createPackageContents();

		// Initialize created meta-data
		theCollaborationsPackage.initializePackageContents();
		theUml14Package.initializePackageContents();
		theModel_managementPackage.initializePackageContents();
		theData_typesPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theCommon_behaviorPackage.initializePackageContents();
		theUse_casesPackage.initializePackageContents();
		theState_machinesPackage.initializePackageContents();
		theActivity_graphsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCollaborationsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CollaborationsPackage.eNS_URI, theCollaborationsPackage);
		return theCollaborationsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCollaboration() {
		return collaborationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCollaboration_Interaction() {
		return (EReference)collaborationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCollaboration_ConstrainingElement() {
		return (EReference)collaborationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClassifierRole() {
		return classifierRoleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClassifierRole_Multiplicity() {
		return (EReference)classifierRoleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClassifierRole_Message() {
		return (EReference)classifierRoleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClassifierRole_AvailableContents() {
		return (EReference)classifierRoleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClassifierRole_ConformingInstance() {
		return (EReference)classifierRoleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssociationRole() {
		return associationRoleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssociationRole_Multiplicity() {
		return (EReference)associationRoleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssociationRole_Base() {
		return (EReference)associationRoleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssociationRole_Message() {
		return (EReference)associationRoleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssociationRole_ConformingLink() {
		return (EReference)associationRoleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssociationEndRole() {
		return associationEndRoleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssociationEndRole_CollaborationMultiplicity() {
		return (EReference)associationEndRoleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssociationEndRole_Base() {
		return (EReference)associationEndRoleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssociationEndRole_AvailableQualifier() {
		return (EReference)associationEndRoleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMessage() {
		return messageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessage_Interaction() {
		return (EReference)messageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessage_Message() {
		return (EReference)messageEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessage_Activator() {
		return (EReference)messageEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessage_Sender() {
		return (EReference)messageEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessage_Predecessor() {
		return (EReference)messageEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessage_Successor() {
		return (EReference)messageEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessage_CommunicationConnection() {
		return (EReference)messageEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessage_Action() {
		return (EReference)messageEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessage_ConformingStimulus() {
		return (EReference)messageEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInteraction() {
		return interactionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInteraction_Message() {
		return (EReference)interactionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInteraction_Context() {
		return (EReference)interactionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInteraction_InteractionInstanceSet() {
		return (EReference)interactionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInteractionInstanceSet() {
		return interactionInstanceSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInteractionInstanceSet_Context() {
		return (EReference)interactionInstanceSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInteractionInstanceSet_Interaction() {
		return (EReference)interactionInstanceSetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInteractionInstanceSet_ParticipatingStimulus() {
		return (EReference)interactionInstanceSetEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCollaborationInstanceSet() {
		return collaborationInstanceSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCollaborationInstanceSet_InteractionInstanceSet() {
		return (EReference)collaborationInstanceSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCollaborationInstanceSet_ConstrainingElement() {
		return (EReference)collaborationInstanceSetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CollaborationsFactory getCollaborationsFactory() {
		return (CollaborationsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		collaborationEClass = createEClass(COLLABORATION);
		createEReference(collaborationEClass, COLLABORATION__INTERACTION);
		createEReference(collaborationEClass, COLLABORATION__CONSTRAINING_ELEMENT);

		classifierRoleEClass = createEClass(CLASSIFIER_ROLE);
		createEReference(classifierRoleEClass, CLASSIFIER_ROLE__MULTIPLICITY);
		createEReference(classifierRoleEClass, CLASSIFIER_ROLE__MESSAGE);
		createEReference(classifierRoleEClass, CLASSIFIER_ROLE__AVAILABLE_CONTENTS);
		createEReference(classifierRoleEClass, CLASSIFIER_ROLE__CONFORMING_INSTANCE);

		associationRoleEClass = createEClass(ASSOCIATION_ROLE);
		createEReference(associationRoleEClass, ASSOCIATION_ROLE__MULTIPLICITY);
		createEReference(associationRoleEClass, ASSOCIATION_ROLE__BASE);
		createEReference(associationRoleEClass, ASSOCIATION_ROLE__MESSAGE);
		createEReference(associationRoleEClass, ASSOCIATION_ROLE__CONFORMING_LINK);

		associationEndRoleEClass = createEClass(ASSOCIATION_END_ROLE);
		createEReference(associationEndRoleEClass, ASSOCIATION_END_ROLE__COLLABORATION_MULTIPLICITY);
		createEReference(associationEndRoleEClass, ASSOCIATION_END_ROLE__BASE);
		createEReference(associationEndRoleEClass, ASSOCIATION_END_ROLE__AVAILABLE_QUALIFIER);

		messageEClass = createEClass(MESSAGE);
		createEReference(messageEClass, MESSAGE__INTERACTION);
		createEReference(messageEClass, MESSAGE__MESSAGE);
		createEReference(messageEClass, MESSAGE__ACTIVATOR);
		createEReference(messageEClass, MESSAGE__SENDER);
		createEReference(messageEClass, MESSAGE__PREDECESSOR);
		createEReference(messageEClass, MESSAGE__SUCCESSOR);
		createEReference(messageEClass, MESSAGE__COMMUNICATION_CONNECTION);
		createEReference(messageEClass, MESSAGE__ACTION);
		createEReference(messageEClass, MESSAGE__CONFORMING_STIMULUS);

		interactionEClass = createEClass(INTERACTION);
		createEReference(interactionEClass, INTERACTION__MESSAGE);
		createEReference(interactionEClass, INTERACTION__CONTEXT);
		createEReference(interactionEClass, INTERACTION__INTERACTION_INSTANCE_SET);

		interactionInstanceSetEClass = createEClass(INTERACTION_INSTANCE_SET);
		createEReference(interactionInstanceSetEClass, INTERACTION_INSTANCE_SET__CONTEXT);
		createEReference(interactionInstanceSetEClass, INTERACTION_INSTANCE_SET__INTERACTION);
		createEReference(interactionInstanceSetEClass, INTERACTION_INSTANCE_SET__PARTICIPATING_STIMULUS);

		collaborationInstanceSetEClass = createEClass(COLLABORATION_INSTANCE_SET);
		createEReference(collaborationInstanceSetEClass, COLLABORATION_INSTANCE_SET__INTERACTION_INSTANCE_SET);
		createEReference(collaborationInstanceSetEClass, COLLABORATION_INSTANCE_SET__CONSTRAINING_ELEMENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		Data_typesPackage theData_typesPackage = (Data_typesPackage)EPackage.Registry.INSTANCE.getEPackage(Data_typesPackage.eNS_URI);
		Common_behaviorPackage theCommon_behaviorPackage = (Common_behaviorPackage)EPackage.Registry.INSTANCE.getEPackage(Common_behaviorPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		collaborationEClass.getESuperTypes().add(theCorePackage.getGeneralizableElement());
		collaborationEClass.getESuperTypes().add(theCorePackage.getNamespace());
		classifierRoleEClass.getESuperTypes().add(theCorePackage.getClassifier());
		associationRoleEClass.getESuperTypes().add(theCorePackage.getAssociation());
		associationEndRoleEClass.getESuperTypes().add(theCorePackage.getAssociationEnd());
		messageEClass.getESuperTypes().add(theCorePackage.getModelElement());
		interactionEClass.getESuperTypes().add(theCorePackage.getModelElement());
		interactionInstanceSetEClass.getESuperTypes().add(theCorePackage.getModelElement());
		collaborationInstanceSetEClass.getESuperTypes().add(theCorePackage.getModelElement());

		// Initialize classes and features; add operations and parameters
		initEClass(collaborationEClass, Collaboration.class, "Collaboration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCollaboration_Interaction(), this.getInteraction(), this.getInteraction_Context(), "interaction", null, 0, -1, Collaboration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCollaboration_ConstrainingElement(), theCorePackage.getModelElement(), theCorePackage.getModelElement_Collaboration(), "constrainingElement", null, 0, -1, Collaboration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(classifierRoleEClass, ClassifierRole.class, "ClassifierRole", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getClassifierRole_Multiplicity(), theData_typesPackage.getMultiplicity(), null, "multiplicity", null, 0, 1, ClassifierRole.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClassifierRole_Message(), this.getMessage(), this.getMessage_Sender(), "message", null, 0, -1, ClassifierRole.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClassifierRole_AvailableContents(), theCorePackage.getModelElement(), theCorePackage.getModelElement_ClassifierRole(), "availableContents", null, 0, -1, ClassifierRole.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClassifierRole_ConformingInstance(), theCommon_behaviorPackage.getInstance(), theCommon_behaviorPackage.getInstance_PlayedRole(), "conformingInstance", null, 0, -1, ClassifierRole.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(associationRoleEClass, AssociationRole.class, "AssociationRole", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssociationRole_Multiplicity(), theData_typesPackage.getMultiplicity(), null, "multiplicity", null, 0, 1, AssociationRole.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssociationRole_Base(), theCorePackage.getAssociation(), theCorePackage.getAssociation_AssociationRole(), "base", null, 0, 1, AssociationRole.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssociationRole_Message(), this.getMessage(), this.getMessage_CommunicationConnection(), "message", null, 0, -1, AssociationRole.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssociationRole_ConformingLink(), theCommon_behaviorPackage.getLink(), theCommon_behaviorPackage.getLink_PlayedRole(), "conformingLink", null, 0, -1, AssociationRole.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(associationEndRoleEClass, AssociationEndRole.class, "AssociationEndRole", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssociationEndRole_CollaborationMultiplicity(), theData_typesPackage.getMultiplicity(), null, "collaborationMultiplicity", null, 0, 1, AssociationEndRole.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssociationEndRole_Base(), theCorePackage.getAssociationEnd(), theCorePackage.getAssociationEnd_AssociationEndRole(), "base", null, 0, 1, AssociationEndRole.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAssociationEndRole_AvailableQualifier(), theCorePackage.getAttribute(), theCorePackage.getAttribute_AssociationEndRole(), "availableQualifier", null, 0, -1, AssociationEndRole.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(messageEClass, Message.class, "Message", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMessage_Interaction(), this.getInteraction(), this.getInteraction_Message(), "interaction", null, 1, 1, Message.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMessage_Message(), this.getMessage(), this.getMessage_Activator(), "message", null, 0, -1, Message.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMessage_Activator(), this.getMessage(), this.getMessage_Message(), "activator", null, 0, 1, Message.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMessage_Sender(), this.getClassifierRole(), this.getClassifierRole_Message(), "sender", null, 1, 1, Message.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMessage_Predecessor(), this.getMessage(), this.getMessage_Successor(), "predecessor", null, 0, -1, Message.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMessage_Successor(), this.getMessage(), this.getMessage_Predecessor(), "successor", null, 0, -1, Message.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMessage_CommunicationConnection(), this.getAssociationRole(), this.getAssociationRole_Message(), "communicationConnection", null, 0, 1, Message.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMessage_Action(), theCommon_behaviorPackage.getAction(), theCommon_behaviorPackage.getAction_Message(), "action", null, 1, 1, Message.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMessage_ConformingStimulus(), theCommon_behaviorPackage.getStimulus(), theCommon_behaviorPackage.getStimulus_PlayedRole(), "conformingStimulus", null, 0, -1, Message.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(interactionEClass, Interaction.class, "Interaction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInteraction_Message(), this.getMessage(), this.getMessage_Interaction(), "message", null, 1, -1, Interaction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInteraction_Context(), this.getCollaboration(), this.getCollaboration_Interaction(), "context", null, 1, 1, Interaction.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInteraction_InteractionInstanceSet(), this.getInteractionInstanceSet(), this.getInteractionInstanceSet_Interaction(), "interactionInstanceSet", null, 0, -1, Interaction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(interactionInstanceSetEClass, InteractionInstanceSet.class, "InteractionInstanceSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInteractionInstanceSet_Context(), this.getCollaborationInstanceSet(), this.getCollaborationInstanceSet_InteractionInstanceSet(), "context", null, 1, 1, InteractionInstanceSet.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInteractionInstanceSet_Interaction(), this.getInteraction(), this.getInteraction_InteractionInstanceSet(), "interaction", null, 0, 1, InteractionInstanceSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInteractionInstanceSet_ParticipatingStimulus(), theCommon_behaviorPackage.getStimulus(), theCommon_behaviorPackage.getStimulus_InteractionInstanceSet(), "participatingStimulus", null, 1, -1, InteractionInstanceSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(collaborationInstanceSetEClass, CollaborationInstanceSet.class, "CollaborationInstanceSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCollaborationInstanceSet_InteractionInstanceSet(), this.getInteractionInstanceSet(), this.getInteractionInstanceSet_Context(), "interactionInstanceSet", null, 0, -1, CollaborationInstanceSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCollaborationInstanceSet_ConstrainingElement(), theCorePackage.getModelElement(), theCorePackage.getModelElement_CollaborationInstanceSet(), "constrainingElement", null, 0, -1, CollaborationInstanceSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

} //CollaborationsPackageImpl
