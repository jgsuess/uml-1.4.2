/**
 * <copyright>
 * </copyright>
 *
 * $Id: Message.java,v 1.1 2012/04/23 09:31:24 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Action;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus;

import net.jgsuess.uml14.foundation.core.ModelElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getInteraction <em>Interaction</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getMessage <em>Message</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getActivator <em>Activator</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getSender <em>Sender</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getPredecessor <em>Predecessor</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getSuccessor <em>Successor</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getCommunicationConnection <em>Communication Connection</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getAction <em>Action</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getConformingStimulus <em>Conforming Stimulus</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getMessage()
 * @model
 * @generated
 */
public interface Message extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Interaction</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Interaction#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interaction</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interaction</em>' container reference.
	 * @see #setInteraction(Interaction)
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getMessage_Interaction()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Interaction#getMessage
	 * @model opposite="message" required="true"
	 * @generated
	 */
	Interaction getInteraction();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getInteraction <em>Interaction</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interaction</em>' container reference.
	 * @see #getInteraction()
	 * @generated
	 */
	void setInteraction(Interaction value);

	/**
	 * Returns the value of the '<em><b>Message</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.collaborations.Message}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getActivator <em>Activator</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getMessage_Message()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Message#getActivator
	 * @model opposite="activator"
	 * @generated
	 */
	EList<Message> getMessage();

	/**
	 * Returns the value of the '<em><b>Activator</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activator</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activator</em>' reference.
	 * @see #setActivator(Message)
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getMessage_Activator()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Message#getMessage
	 * @model opposite="message"
	 * @generated
	 */
	Message getActivator();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getActivator <em>Activator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Activator</em>' reference.
	 * @see #getActivator()
	 * @generated
	 */
	void setActivator(Message value);

	/**
	 * Returns the value of the '<em><b>Sender</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sender</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sender</em>' reference.
	 * @see #setSender(ClassifierRole)
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getMessage_Sender()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole#getMessage
	 * @model opposite="message" required="true"
	 * @generated
	 */
	ClassifierRole getSender();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getSender <em>Sender</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sender</em>' reference.
	 * @see #getSender()
	 * @generated
	 */
	void setSender(ClassifierRole value);

	/**
	 * Returns the value of the '<em><b>Predecessor</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.collaborations.Message}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getSuccessor <em>Successor</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Predecessor</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Predecessor</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getMessage_Predecessor()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Message#getSuccessor
	 * @model opposite="successor"
	 * @generated
	 */
	EList<Message> getPredecessor();

	/**
	 * Returns the value of the '<em><b>Successor</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.collaborations.Message}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getPredecessor <em>Predecessor</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Successor</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Successor</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getMessage_Successor()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Message#getPredecessor
	 * @model opposite="predecessor"
	 * @generated
	 */
	EList<Message> getSuccessor();

	/**
	 * Returns the value of the '<em><b>Communication Connection</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Communication Connection</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Communication Connection</em>' reference.
	 * @see #setCommunicationConnection(AssociationRole)
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getMessage_CommunicationConnection()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole#getMessage
	 * @model opposite="message"
	 * @generated
	 */
	AssociationRole getCommunicationConnection();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getCommunicationConnection <em>Communication Connection</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Communication Connection</em>' reference.
	 * @see #getCommunicationConnection()
	 * @generated
	 */
	void setCommunicationConnection(AssociationRole value);

	/**
	 * Returns the value of the '<em><b>Action</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' reference.
	 * @see #setAction(Action)
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getMessage_Action()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getMessage
	 * @model opposite="message" required="true"
	 * @generated
	 */
	Action getAction();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getAction <em>Action</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action</em>' reference.
	 * @see #getAction()
	 * @generated
	 */
	void setAction(Action value);

	/**
	 * Returns the value of the '<em><b>Conforming Stimulus</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus#getPlayedRole <em>Played Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conforming Stimulus</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conforming Stimulus</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getMessage_ConformingStimulus()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus#getPlayedRole
	 * @model opposite="playedRole"
	 * @generated
	 */
	EList<Stimulus> getConformingStimulus();

} // Message
