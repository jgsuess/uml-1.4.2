/**
 * <copyright>
 * </copyright>
 *
 * $Id: CollaborationInstanceSetImpl.java,v 1.1 2012/04/23 09:31:40 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationInstanceSet;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage;
import net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet;

import net.jgsuess.uml14.foundation.core.CorePackage;
import net.jgsuess.uml14.foundation.core.ModelElement;

import net.jgsuess.uml14.foundation.core.impl.ModelElementImpl;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Collaboration Instance Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationInstanceSetImpl#getInteractionInstanceSet <em>Interaction Instance Set</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationInstanceSetImpl#getConstrainingElement <em>Constraining Element</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CollaborationInstanceSetImpl extends ModelElementImpl implements CollaborationInstanceSet {
	/**
	 * The cached value of the '{@link #getInteractionInstanceSet() <em>Interaction Instance Set</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInteractionInstanceSet()
	 * @generated
	 * @ordered
	 */
	protected EList<InteractionInstanceSet> interactionInstanceSet;

	/**
	 * The cached value of the '{@link #getConstrainingElement() <em>Constraining Element</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstrainingElement()
	 * @generated
	 * @ordered
	 */
	protected EList<ModelElement> constrainingElement;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CollaborationInstanceSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CollaborationsPackage.Literals.COLLABORATION_INSTANCE_SET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InteractionInstanceSet> getInteractionInstanceSet() {
		if (interactionInstanceSet == null) {
			interactionInstanceSet = new EObjectContainmentWithInverseEList<InteractionInstanceSet>(InteractionInstanceSet.class, this, CollaborationsPackage.COLLABORATION_INSTANCE_SET__INTERACTION_INSTANCE_SET, CollaborationsPackage.INTERACTION_INSTANCE_SET__CONTEXT);
		}
		return interactionInstanceSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModelElement> getConstrainingElement() {
		if (constrainingElement == null) {
			constrainingElement = new EObjectWithInverseResolvingEList.ManyInverse<ModelElement>(ModelElement.class, this, CollaborationsPackage.COLLABORATION_INSTANCE_SET__CONSTRAINING_ELEMENT, CorePackage.MODEL_ELEMENT__COLLABORATION_INSTANCE_SET);
		}
		return constrainingElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CollaborationsPackage.COLLABORATION_INSTANCE_SET__INTERACTION_INSTANCE_SET:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getInteractionInstanceSet()).basicAdd(otherEnd, msgs);
			case CollaborationsPackage.COLLABORATION_INSTANCE_SET__CONSTRAINING_ELEMENT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getConstrainingElement()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CollaborationsPackage.COLLABORATION_INSTANCE_SET__INTERACTION_INSTANCE_SET:
				return ((InternalEList<?>)getInteractionInstanceSet()).basicRemove(otherEnd, msgs);
			case CollaborationsPackage.COLLABORATION_INSTANCE_SET__CONSTRAINING_ELEMENT:
				return ((InternalEList<?>)getConstrainingElement()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CollaborationsPackage.COLLABORATION_INSTANCE_SET__INTERACTION_INSTANCE_SET:
				return getInteractionInstanceSet();
			case CollaborationsPackage.COLLABORATION_INSTANCE_SET__CONSTRAINING_ELEMENT:
				return getConstrainingElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CollaborationsPackage.COLLABORATION_INSTANCE_SET__INTERACTION_INSTANCE_SET:
				getInteractionInstanceSet().clear();
				getInteractionInstanceSet().addAll((Collection<? extends InteractionInstanceSet>)newValue);
				return;
			case CollaborationsPackage.COLLABORATION_INSTANCE_SET__CONSTRAINING_ELEMENT:
				getConstrainingElement().clear();
				getConstrainingElement().addAll((Collection<? extends ModelElement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CollaborationsPackage.COLLABORATION_INSTANCE_SET__INTERACTION_INSTANCE_SET:
				getInteractionInstanceSet().clear();
				return;
			case CollaborationsPackage.COLLABORATION_INSTANCE_SET__CONSTRAINING_ELEMENT:
				getConstrainingElement().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CollaborationsPackage.COLLABORATION_INSTANCE_SET__INTERACTION_INSTANCE_SET:
				return interactionInstanceSet != null && !interactionInstanceSet.isEmpty();
			case CollaborationsPackage.COLLABORATION_INSTANCE_SET__CONSTRAINING_ELEMENT:
				return constrainingElement != null && !constrainingElement.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //CollaborationInstanceSetImpl
