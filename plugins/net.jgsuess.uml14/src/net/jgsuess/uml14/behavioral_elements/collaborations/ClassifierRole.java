/**
 * <copyright>
 * </copyright>
 *
 * $Id: ClassifierRole.java,v 1.1 2012/04/23 09:31:24 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Instance;

import net.jgsuess.uml14.foundation.core.Classifier;
import net.jgsuess.uml14.foundation.core.ModelElement;

import net.jgsuess.uml14.foundation.data_types.Multiplicity;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Classifier Role</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole#getMultiplicity <em>Multiplicity</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole#getMessage <em>Message</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole#getAvailableContents <em>Available Contents</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole#getConformingInstance <em>Conforming Instance</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getClassifierRole()
 * @model
 * @generated
 */
public interface ClassifierRole extends Classifier {
	/**
	 * Returns the value of the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multiplicity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiplicity</em>' containment reference.
	 * @see #setMultiplicity(Multiplicity)
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getClassifierRole_Multiplicity()
	 * @model containment="true"
	 * @generated
	 */
	Multiplicity getMultiplicity();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole#getMultiplicity <em>Multiplicity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Multiplicity</em>' containment reference.
	 * @see #getMultiplicity()
	 * @generated
	 */
	void setMultiplicity(Multiplicity value);

	/**
	 * Returns the value of the '<em><b>Message</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.collaborations.Message}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getSender <em>Sender</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getClassifierRole_Message()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Message#getSender
	 * @model opposite="sender"
	 * @generated
	 */
	EList<Message> getMessage();

	/**
	 * Returns the value of the '<em><b>Available Contents</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.ModelElement}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.ModelElement#getClassifierRole <em>Classifier Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Available Contents</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Available Contents</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getClassifierRole_AvailableContents()
	 * @see net.jgsuess.uml14.foundation.core.ModelElement#getClassifierRole
	 * @model opposite="classifierRole"
	 * @generated
	 */
	EList<ModelElement> getAvailableContents();

	/**
	 * Returns the value of the '<em><b>Conforming Instance</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.Instance}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Instance#getPlayedRole <em>Played Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conforming Instance</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conforming Instance</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getClassifierRole_ConformingInstance()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Instance#getPlayedRole
	 * @model opposite="playedRole"
	 * @generated
	 */
	EList<Instance> getConformingInstance();

} // ClassifierRole
