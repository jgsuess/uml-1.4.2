/**
 * <copyright>
 * </copyright>
 *
 * $Id: CollaborationsFactoryImpl.java,v 1.1 2012/04/23 09:31:39 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations.impl;

import net.jgsuess.uml14.behavioral_elements.collaborations.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CollaborationsFactoryImpl extends EFactoryImpl implements CollaborationsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CollaborationsFactory init() {
		try {
			CollaborationsFactory theCollaborationsFactory = (CollaborationsFactory)EPackage.Registry.INSTANCE.getEFactory("http://jgsuess.net/uml14/collaborations"); 
			if (theCollaborationsFactory != null) {
				return theCollaborationsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CollaborationsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CollaborationsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CollaborationsPackage.COLLABORATION: return createCollaboration();
			case CollaborationsPackage.CLASSIFIER_ROLE: return createClassifierRole();
			case CollaborationsPackage.ASSOCIATION_ROLE: return createAssociationRole();
			case CollaborationsPackage.ASSOCIATION_END_ROLE: return createAssociationEndRole();
			case CollaborationsPackage.MESSAGE: return createMessage();
			case CollaborationsPackage.INTERACTION: return createInteraction();
			case CollaborationsPackage.INTERACTION_INSTANCE_SET: return createInteractionInstanceSet();
			case CollaborationsPackage.COLLABORATION_INSTANCE_SET: return createCollaborationInstanceSet();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Collaboration createCollaboration() {
		CollaborationImpl collaboration = new CollaborationImpl();
		return collaboration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassifierRole createClassifierRole() {
		ClassifierRoleImpl classifierRole = new ClassifierRoleImpl();
		return classifierRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationRole createAssociationRole() {
		AssociationRoleImpl associationRole = new AssociationRoleImpl();
		return associationRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationEndRole createAssociationEndRole() {
		AssociationEndRoleImpl associationEndRole = new AssociationEndRoleImpl();
		return associationEndRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message createMessage() {
		MessageImpl message = new MessageImpl();
		return message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interaction createInteraction() {
		InteractionImpl interaction = new InteractionImpl();
		return interaction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionInstanceSet createInteractionInstanceSet() {
		InteractionInstanceSetImpl interactionInstanceSet = new InteractionInstanceSetImpl();
		return interactionInstanceSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CollaborationInstanceSet createCollaborationInstanceSet() {
		CollaborationInstanceSetImpl collaborationInstanceSet = new CollaborationInstanceSetImpl();
		return collaborationInstanceSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CollaborationsPackage getCollaborationsPackage() {
		return (CollaborationsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CollaborationsPackage getPackage() {
		return CollaborationsPackage.eINSTANCE;
	}

} //CollaborationsFactoryImpl
