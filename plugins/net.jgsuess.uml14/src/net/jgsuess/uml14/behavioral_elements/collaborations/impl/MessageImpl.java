/**
 * <copyright>
 * </copyright>
 *
 * $Id: MessageImpl.java,v 1.1 2012/04/23 09:31:39 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole;
import net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage;
import net.jgsuess.uml14.behavioral_elements.collaborations.Interaction;
import net.jgsuess.uml14.behavioral_elements.collaborations.Message;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Action;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus;

import net.jgsuess.uml14.foundation.core.impl.ModelElementImpl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.MessageImpl#getInteraction <em>Interaction</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.MessageImpl#getMessage <em>Message</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.MessageImpl#getActivator <em>Activator</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.MessageImpl#getSender <em>Sender</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.MessageImpl#getPredecessor <em>Predecessor</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.MessageImpl#getSuccessor <em>Successor</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.MessageImpl#getCommunicationConnection <em>Communication Connection</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.MessageImpl#getAction <em>Action</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.MessageImpl#getConformingStimulus <em>Conforming Stimulus</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MessageImpl extends ModelElementImpl implements Message {
	/**
	 * The cached value of the '{@link #getMessage() <em>Message</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage()
	 * @generated
	 * @ordered
	 */
	protected EList<Message> message;

	/**
	 * The cached value of the '{@link #getActivator() <em>Activator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivator()
	 * @generated
	 * @ordered
	 */
	protected Message activator;

	/**
	 * The cached value of the '{@link #getSender() <em>Sender</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSender()
	 * @generated
	 * @ordered
	 */
	protected ClassifierRole sender;

	/**
	 * The cached value of the '{@link #getPredecessor() <em>Predecessor</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPredecessor()
	 * @generated
	 * @ordered
	 */
	protected EList<Message> predecessor;

	/**
	 * The cached value of the '{@link #getSuccessor() <em>Successor</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuccessor()
	 * @generated
	 * @ordered
	 */
	protected EList<Message> successor;

	/**
	 * The cached value of the '{@link #getCommunicationConnection() <em>Communication Connection</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommunicationConnection()
	 * @generated
	 * @ordered
	 */
	protected AssociationRole communicationConnection;

	/**
	 * The cached value of the '{@link #getAction() <em>Action</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAction()
	 * @generated
	 * @ordered
	 */
	protected Action action;

	/**
	 * The cached value of the '{@link #getConformingStimulus() <em>Conforming Stimulus</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConformingStimulus()
	 * @generated
	 * @ordered
	 */
	protected EList<Stimulus> conformingStimulus;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MessageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CollaborationsPackage.Literals.MESSAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interaction getInteraction() {
		if (eContainerFeatureID() != CollaborationsPackage.MESSAGE__INTERACTION) return null;
		return (Interaction)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInteraction(Interaction newInteraction, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newInteraction, CollaborationsPackage.MESSAGE__INTERACTION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInteraction(Interaction newInteraction) {
		if (newInteraction != eInternalContainer() || (eContainerFeatureID() != CollaborationsPackage.MESSAGE__INTERACTION && newInteraction != null)) {
			if (EcoreUtil.isAncestor(this, newInteraction))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newInteraction != null)
				msgs = ((InternalEObject)newInteraction).eInverseAdd(this, CollaborationsPackage.INTERACTION__MESSAGE, Interaction.class, msgs);
			msgs = basicSetInteraction(newInteraction, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CollaborationsPackage.MESSAGE__INTERACTION, newInteraction, newInteraction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Message> getMessage() {
		if (message == null) {
			message = new EObjectWithInverseResolvingEList<Message>(Message.class, this, CollaborationsPackage.MESSAGE__MESSAGE, CollaborationsPackage.MESSAGE__ACTIVATOR);
		}
		return message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getActivator() {
		if (activator != null && activator.eIsProxy()) {
			InternalEObject oldActivator = (InternalEObject)activator;
			activator = (Message)eResolveProxy(oldActivator);
			if (activator != oldActivator) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CollaborationsPackage.MESSAGE__ACTIVATOR, oldActivator, activator));
			}
		}
		return activator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message basicGetActivator() {
		return activator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetActivator(Message newActivator, NotificationChain msgs) {
		Message oldActivator = activator;
		activator = newActivator;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CollaborationsPackage.MESSAGE__ACTIVATOR, oldActivator, newActivator);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActivator(Message newActivator) {
		if (newActivator != activator) {
			NotificationChain msgs = null;
			if (activator != null)
				msgs = ((InternalEObject)activator).eInverseRemove(this, CollaborationsPackage.MESSAGE__MESSAGE, Message.class, msgs);
			if (newActivator != null)
				msgs = ((InternalEObject)newActivator).eInverseAdd(this, CollaborationsPackage.MESSAGE__MESSAGE, Message.class, msgs);
			msgs = basicSetActivator(newActivator, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CollaborationsPackage.MESSAGE__ACTIVATOR, newActivator, newActivator));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassifierRole getSender() {
		if (sender != null && sender.eIsProxy()) {
			InternalEObject oldSender = (InternalEObject)sender;
			sender = (ClassifierRole)eResolveProxy(oldSender);
			if (sender != oldSender) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CollaborationsPackage.MESSAGE__SENDER, oldSender, sender));
			}
		}
		return sender;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassifierRole basicGetSender() {
		return sender;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSender(ClassifierRole newSender, NotificationChain msgs) {
		ClassifierRole oldSender = sender;
		sender = newSender;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CollaborationsPackage.MESSAGE__SENDER, oldSender, newSender);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSender(ClassifierRole newSender) {
		if (newSender != sender) {
			NotificationChain msgs = null;
			if (sender != null)
				msgs = ((InternalEObject)sender).eInverseRemove(this, CollaborationsPackage.CLASSIFIER_ROLE__MESSAGE, ClassifierRole.class, msgs);
			if (newSender != null)
				msgs = ((InternalEObject)newSender).eInverseAdd(this, CollaborationsPackage.CLASSIFIER_ROLE__MESSAGE, ClassifierRole.class, msgs);
			msgs = basicSetSender(newSender, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CollaborationsPackage.MESSAGE__SENDER, newSender, newSender));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Message> getPredecessor() {
		if (predecessor == null) {
			predecessor = new EObjectWithInverseResolvingEList.ManyInverse<Message>(Message.class, this, CollaborationsPackage.MESSAGE__PREDECESSOR, CollaborationsPackage.MESSAGE__SUCCESSOR);
		}
		return predecessor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Message> getSuccessor() {
		if (successor == null) {
			successor = new EObjectWithInverseResolvingEList.ManyInverse<Message>(Message.class, this, CollaborationsPackage.MESSAGE__SUCCESSOR, CollaborationsPackage.MESSAGE__PREDECESSOR);
		}
		return successor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationRole getCommunicationConnection() {
		if (communicationConnection != null && communicationConnection.eIsProxy()) {
			InternalEObject oldCommunicationConnection = (InternalEObject)communicationConnection;
			communicationConnection = (AssociationRole)eResolveProxy(oldCommunicationConnection);
			if (communicationConnection != oldCommunicationConnection) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CollaborationsPackage.MESSAGE__COMMUNICATION_CONNECTION, oldCommunicationConnection, communicationConnection));
			}
		}
		return communicationConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationRole basicGetCommunicationConnection() {
		return communicationConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCommunicationConnection(AssociationRole newCommunicationConnection, NotificationChain msgs) {
		AssociationRole oldCommunicationConnection = communicationConnection;
		communicationConnection = newCommunicationConnection;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CollaborationsPackage.MESSAGE__COMMUNICATION_CONNECTION, oldCommunicationConnection, newCommunicationConnection);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCommunicationConnection(AssociationRole newCommunicationConnection) {
		if (newCommunicationConnection != communicationConnection) {
			NotificationChain msgs = null;
			if (communicationConnection != null)
				msgs = ((InternalEObject)communicationConnection).eInverseRemove(this, CollaborationsPackage.ASSOCIATION_ROLE__MESSAGE, AssociationRole.class, msgs);
			if (newCommunicationConnection != null)
				msgs = ((InternalEObject)newCommunicationConnection).eInverseAdd(this, CollaborationsPackage.ASSOCIATION_ROLE__MESSAGE, AssociationRole.class, msgs);
			msgs = basicSetCommunicationConnection(newCommunicationConnection, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CollaborationsPackage.MESSAGE__COMMUNICATION_CONNECTION, newCommunicationConnection, newCommunicationConnection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action getAction() {
		if (action != null && action.eIsProxy()) {
			InternalEObject oldAction = (InternalEObject)action;
			action = (Action)eResolveProxy(oldAction);
			if (action != oldAction) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CollaborationsPackage.MESSAGE__ACTION, oldAction, action));
			}
		}
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action basicGetAction() {
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAction(Action newAction, NotificationChain msgs) {
		Action oldAction = action;
		action = newAction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CollaborationsPackage.MESSAGE__ACTION, oldAction, newAction);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAction(Action newAction) {
		if (newAction != action) {
			NotificationChain msgs = null;
			if (action != null)
				msgs = ((InternalEObject)action).eInverseRemove(this, Common_behaviorPackage.ACTION__MESSAGE, Action.class, msgs);
			if (newAction != null)
				msgs = ((InternalEObject)newAction).eInverseAdd(this, Common_behaviorPackage.ACTION__MESSAGE, Action.class, msgs);
			msgs = basicSetAction(newAction, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CollaborationsPackage.MESSAGE__ACTION, newAction, newAction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Stimulus> getConformingStimulus() {
		if (conformingStimulus == null) {
			conformingStimulus = new EObjectWithInverseResolvingEList.ManyInverse<Stimulus>(Stimulus.class, this, CollaborationsPackage.MESSAGE__CONFORMING_STIMULUS, Common_behaviorPackage.STIMULUS__PLAYED_ROLE);
		}
		return conformingStimulus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CollaborationsPackage.MESSAGE__INTERACTION:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetInteraction((Interaction)otherEnd, msgs);
			case CollaborationsPackage.MESSAGE__MESSAGE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getMessage()).basicAdd(otherEnd, msgs);
			case CollaborationsPackage.MESSAGE__ACTIVATOR:
				if (activator != null)
					msgs = ((InternalEObject)activator).eInverseRemove(this, CollaborationsPackage.MESSAGE__MESSAGE, Message.class, msgs);
				return basicSetActivator((Message)otherEnd, msgs);
			case CollaborationsPackage.MESSAGE__SENDER:
				if (sender != null)
					msgs = ((InternalEObject)sender).eInverseRemove(this, CollaborationsPackage.CLASSIFIER_ROLE__MESSAGE, ClassifierRole.class, msgs);
				return basicSetSender((ClassifierRole)otherEnd, msgs);
			case CollaborationsPackage.MESSAGE__PREDECESSOR:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPredecessor()).basicAdd(otherEnd, msgs);
			case CollaborationsPackage.MESSAGE__SUCCESSOR:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSuccessor()).basicAdd(otherEnd, msgs);
			case CollaborationsPackage.MESSAGE__COMMUNICATION_CONNECTION:
				if (communicationConnection != null)
					msgs = ((InternalEObject)communicationConnection).eInverseRemove(this, CollaborationsPackage.ASSOCIATION_ROLE__MESSAGE, AssociationRole.class, msgs);
				return basicSetCommunicationConnection((AssociationRole)otherEnd, msgs);
			case CollaborationsPackage.MESSAGE__ACTION:
				if (action != null)
					msgs = ((InternalEObject)action).eInverseRemove(this, Common_behaviorPackage.ACTION__MESSAGE, Action.class, msgs);
				return basicSetAction((Action)otherEnd, msgs);
			case CollaborationsPackage.MESSAGE__CONFORMING_STIMULUS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getConformingStimulus()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CollaborationsPackage.MESSAGE__INTERACTION:
				return basicSetInteraction(null, msgs);
			case CollaborationsPackage.MESSAGE__MESSAGE:
				return ((InternalEList<?>)getMessage()).basicRemove(otherEnd, msgs);
			case CollaborationsPackage.MESSAGE__ACTIVATOR:
				return basicSetActivator(null, msgs);
			case CollaborationsPackage.MESSAGE__SENDER:
				return basicSetSender(null, msgs);
			case CollaborationsPackage.MESSAGE__PREDECESSOR:
				return ((InternalEList<?>)getPredecessor()).basicRemove(otherEnd, msgs);
			case CollaborationsPackage.MESSAGE__SUCCESSOR:
				return ((InternalEList<?>)getSuccessor()).basicRemove(otherEnd, msgs);
			case CollaborationsPackage.MESSAGE__COMMUNICATION_CONNECTION:
				return basicSetCommunicationConnection(null, msgs);
			case CollaborationsPackage.MESSAGE__ACTION:
				return basicSetAction(null, msgs);
			case CollaborationsPackage.MESSAGE__CONFORMING_STIMULUS:
				return ((InternalEList<?>)getConformingStimulus()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CollaborationsPackage.MESSAGE__INTERACTION:
				return eInternalContainer().eInverseRemove(this, CollaborationsPackage.INTERACTION__MESSAGE, Interaction.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CollaborationsPackage.MESSAGE__INTERACTION:
				return getInteraction();
			case CollaborationsPackage.MESSAGE__MESSAGE:
				return getMessage();
			case CollaborationsPackage.MESSAGE__ACTIVATOR:
				if (resolve) return getActivator();
				return basicGetActivator();
			case CollaborationsPackage.MESSAGE__SENDER:
				if (resolve) return getSender();
				return basicGetSender();
			case CollaborationsPackage.MESSAGE__PREDECESSOR:
				return getPredecessor();
			case CollaborationsPackage.MESSAGE__SUCCESSOR:
				return getSuccessor();
			case CollaborationsPackage.MESSAGE__COMMUNICATION_CONNECTION:
				if (resolve) return getCommunicationConnection();
				return basicGetCommunicationConnection();
			case CollaborationsPackage.MESSAGE__ACTION:
				if (resolve) return getAction();
				return basicGetAction();
			case CollaborationsPackage.MESSAGE__CONFORMING_STIMULUS:
				return getConformingStimulus();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CollaborationsPackage.MESSAGE__INTERACTION:
				setInteraction((Interaction)newValue);
				return;
			case CollaborationsPackage.MESSAGE__MESSAGE:
				getMessage().clear();
				getMessage().addAll((Collection<? extends Message>)newValue);
				return;
			case CollaborationsPackage.MESSAGE__ACTIVATOR:
				setActivator((Message)newValue);
				return;
			case CollaborationsPackage.MESSAGE__SENDER:
				setSender((ClassifierRole)newValue);
				return;
			case CollaborationsPackage.MESSAGE__PREDECESSOR:
				getPredecessor().clear();
				getPredecessor().addAll((Collection<? extends Message>)newValue);
				return;
			case CollaborationsPackage.MESSAGE__SUCCESSOR:
				getSuccessor().clear();
				getSuccessor().addAll((Collection<? extends Message>)newValue);
				return;
			case CollaborationsPackage.MESSAGE__COMMUNICATION_CONNECTION:
				setCommunicationConnection((AssociationRole)newValue);
				return;
			case CollaborationsPackage.MESSAGE__ACTION:
				setAction((Action)newValue);
				return;
			case CollaborationsPackage.MESSAGE__CONFORMING_STIMULUS:
				getConformingStimulus().clear();
				getConformingStimulus().addAll((Collection<? extends Stimulus>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CollaborationsPackage.MESSAGE__INTERACTION:
				setInteraction((Interaction)null);
				return;
			case CollaborationsPackage.MESSAGE__MESSAGE:
				getMessage().clear();
				return;
			case CollaborationsPackage.MESSAGE__ACTIVATOR:
				setActivator((Message)null);
				return;
			case CollaborationsPackage.MESSAGE__SENDER:
				setSender((ClassifierRole)null);
				return;
			case CollaborationsPackage.MESSAGE__PREDECESSOR:
				getPredecessor().clear();
				return;
			case CollaborationsPackage.MESSAGE__SUCCESSOR:
				getSuccessor().clear();
				return;
			case CollaborationsPackage.MESSAGE__COMMUNICATION_CONNECTION:
				setCommunicationConnection((AssociationRole)null);
				return;
			case CollaborationsPackage.MESSAGE__ACTION:
				setAction((Action)null);
				return;
			case CollaborationsPackage.MESSAGE__CONFORMING_STIMULUS:
				getConformingStimulus().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CollaborationsPackage.MESSAGE__INTERACTION:
				return getInteraction() != null;
			case CollaborationsPackage.MESSAGE__MESSAGE:
				return message != null && !message.isEmpty();
			case CollaborationsPackage.MESSAGE__ACTIVATOR:
				return activator != null;
			case CollaborationsPackage.MESSAGE__SENDER:
				return sender != null;
			case CollaborationsPackage.MESSAGE__PREDECESSOR:
				return predecessor != null && !predecessor.isEmpty();
			case CollaborationsPackage.MESSAGE__SUCCESSOR:
				return successor != null && !successor.isEmpty();
			case CollaborationsPackage.MESSAGE__COMMUNICATION_CONNECTION:
				return communicationConnection != null;
			case CollaborationsPackage.MESSAGE__ACTION:
				return action != null;
			case CollaborationsPackage.MESSAGE__CONFORMING_STIMULUS:
				return conformingStimulus != null && !conformingStimulus.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MessageImpl
