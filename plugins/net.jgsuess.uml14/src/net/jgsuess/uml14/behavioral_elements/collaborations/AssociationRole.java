/**
 * <copyright>
 * </copyright>
 *
 * $Id: AssociationRole.java,v 1.1 2012/04/23 09:31:24 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Link;

import net.jgsuess.uml14.foundation.core.Association;

import net.jgsuess.uml14.foundation.data_types.Multiplicity;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Association Role</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole#getMultiplicity <em>Multiplicity</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole#getBase <em>Base</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole#getMessage <em>Message</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole#getConformingLink <em>Conforming Link</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getAssociationRole()
 * @model
 * @generated
 */
public interface AssociationRole extends Association {
	/**
	 * Returns the value of the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multiplicity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiplicity</em>' containment reference.
	 * @see #setMultiplicity(Multiplicity)
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getAssociationRole_Multiplicity()
	 * @model containment="true"
	 * @generated
	 */
	Multiplicity getMultiplicity();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole#getMultiplicity <em>Multiplicity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Multiplicity</em>' containment reference.
	 * @see #getMultiplicity()
	 * @generated
	 */
	void setMultiplicity(Multiplicity value);

	/**
	 * Returns the value of the '<em><b>Base</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Association#getAssociationRole <em>Association Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base</em>' reference.
	 * @see #setBase(Association)
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getAssociationRole_Base()
	 * @see net.jgsuess.uml14.foundation.core.Association#getAssociationRole
	 * @model opposite="associationRole"
	 * @generated
	 */
	Association getBase();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole#getBase <em>Base</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base</em>' reference.
	 * @see #getBase()
	 * @generated
	 */
	void setBase(Association value);

	/**
	 * Returns the value of the '<em><b>Message</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.collaborations.Message}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getCommunicationConnection <em>Communication Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getAssociationRole_Message()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Message#getCommunicationConnection
	 * @model opposite="communicationConnection"
	 * @generated
	 */
	EList<Message> getMessage();

	/**
	 * Returns the value of the '<em><b>Conforming Link</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.Link}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Link#getPlayedRole <em>Played Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conforming Link</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conforming Link</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getAssociationRole_ConformingLink()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Link#getPlayedRole
	 * @model opposite="playedRole"
	 * @generated
	 */
	EList<Link> getConformingLink();

} // AssociationRole
