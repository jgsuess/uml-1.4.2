/**
 * <copyright>
 * </copyright>
 *
 * $Id: ClassifierRoleImpl.java,v 1.1 2012/04/23 09:31:39 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage;
import net.jgsuess.uml14.behavioral_elements.collaborations.Message;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Instance;

import net.jgsuess.uml14.foundation.core.CorePackage;
import net.jgsuess.uml14.foundation.core.ModelElement;

import net.jgsuess.uml14.foundation.core.impl.ClassifierImpl;

import net.jgsuess.uml14.foundation.data_types.Multiplicity;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Classifier Role</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.ClassifierRoleImpl#getMultiplicity <em>Multiplicity</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.ClassifierRoleImpl#getMessage <em>Message</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.ClassifierRoleImpl#getAvailableContents <em>Available Contents</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.ClassifierRoleImpl#getConformingInstance <em>Conforming Instance</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ClassifierRoleImpl extends ClassifierImpl implements ClassifierRole {
	/**
	 * The cached value of the '{@link #getMultiplicity() <em>Multiplicity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMultiplicity()
	 * @generated
	 * @ordered
	 */
	protected Multiplicity multiplicity;

	/**
	 * The cached value of the '{@link #getMessage() <em>Message</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage()
	 * @generated
	 * @ordered
	 */
	protected EList<Message> message;

	/**
	 * The cached value of the '{@link #getAvailableContents() <em>Available Contents</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvailableContents()
	 * @generated
	 * @ordered
	 */
	protected EList<ModelElement> availableContents;

	/**
	 * The cached value of the '{@link #getConformingInstance() <em>Conforming Instance</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConformingInstance()
	 * @generated
	 * @ordered
	 */
	protected EList<Instance> conformingInstance;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassifierRoleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CollaborationsPackage.Literals.CLASSIFIER_ROLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Multiplicity getMultiplicity() {
		return multiplicity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMultiplicity(Multiplicity newMultiplicity, NotificationChain msgs) {
		Multiplicity oldMultiplicity = multiplicity;
		multiplicity = newMultiplicity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CollaborationsPackage.CLASSIFIER_ROLE__MULTIPLICITY, oldMultiplicity, newMultiplicity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMultiplicity(Multiplicity newMultiplicity) {
		if (newMultiplicity != multiplicity) {
			NotificationChain msgs = null;
			if (multiplicity != null)
				msgs = ((InternalEObject)multiplicity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CollaborationsPackage.CLASSIFIER_ROLE__MULTIPLICITY, null, msgs);
			if (newMultiplicity != null)
				msgs = ((InternalEObject)newMultiplicity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CollaborationsPackage.CLASSIFIER_ROLE__MULTIPLICITY, null, msgs);
			msgs = basicSetMultiplicity(newMultiplicity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CollaborationsPackage.CLASSIFIER_ROLE__MULTIPLICITY, newMultiplicity, newMultiplicity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Message> getMessage() {
		if (message == null) {
			message = new EObjectWithInverseResolvingEList<Message>(Message.class, this, CollaborationsPackage.CLASSIFIER_ROLE__MESSAGE, CollaborationsPackage.MESSAGE__SENDER);
		}
		return message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModelElement> getAvailableContents() {
		if (availableContents == null) {
			availableContents = new EObjectWithInverseResolvingEList.ManyInverse<ModelElement>(ModelElement.class, this, CollaborationsPackage.CLASSIFIER_ROLE__AVAILABLE_CONTENTS, CorePackage.MODEL_ELEMENT__CLASSIFIER_ROLE);
		}
		return availableContents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instance> getConformingInstance() {
		if (conformingInstance == null) {
			conformingInstance = new EObjectWithInverseResolvingEList.ManyInverse<Instance>(Instance.class, this, CollaborationsPackage.CLASSIFIER_ROLE__CONFORMING_INSTANCE, Common_behaviorPackage.INSTANCE__PLAYED_ROLE);
		}
		return conformingInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CollaborationsPackage.CLASSIFIER_ROLE__MESSAGE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getMessage()).basicAdd(otherEnd, msgs);
			case CollaborationsPackage.CLASSIFIER_ROLE__AVAILABLE_CONTENTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAvailableContents()).basicAdd(otherEnd, msgs);
			case CollaborationsPackage.CLASSIFIER_ROLE__CONFORMING_INSTANCE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getConformingInstance()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CollaborationsPackage.CLASSIFIER_ROLE__MULTIPLICITY:
				return basicSetMultiplicity(null, msgs);
			case CollaborationsPackage.CLASSIFIER_ROLE__MESSAGE:
				return ((InternalEList<?>)getMessage()).basicRemove(otherEnd, msgs);
			case CollaborationsPackage.CLASSIFIER_ROLE__AVAILABLE_CONTENTS:
				return ((InternalEList<?>)getAvailableContents()).basicRemove(otherEnd, msgs);
			case CollaborationsPackage.CLASSIFIER_ROLE__CONFORMING_INSTANCE:
				return ((InternalEList<?>)getConformingInstance()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CollaborationsPackage.CLASSIFIER_ROLE__MULTIPLICITY:
				return getMultiplicity();
			case CollaborationsPackage.CLASSIFIER_ROLE__MESSAGE:
				return getMessage();
			case CollaborationsPackage.CLASSIFIER_ROLE__AVAILABLE_CONTENTS:
				return getAvailableContents();
			case CollaborationsPackage.CLASSIFIER_ROLE__CONFORMING_INSTANCE:
				return getConformingInstance();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CollaborationsPackage.CLASSIFIER_ROLE__MULTIPLICITY:
				setMultiplicity((Multiplicity)newValue);
				return;
			case CollaborationsPackage.CLASSIFIER_ROLE__MESSAGE:
				getMessage().clear();
				getMessage().addAll((Collection<? extends Message>)newValue);
				return;
			case CollaborationsPackage.CLASSIFIER_ROLE__AVAILABLE_CONTENTS:
				getAvailableContents().clear();
				getAvailableContents().addAll((Collection<? extends ModelElement>)newValue);
				return;
			case CollaborationsPackage.CLASSIFIER_ROLE__CONFORMING_INSTANCE:
				getConformingInstance().clear();
				getConformingInstance().addAll((Collection<? extends Instance>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CollaborationsPackage.CLASSIFIER_ROLE__MULTIPLICITY:
				setMultiplicity((Multiplicity)null);
				return;
			case CollaborationsPackage.CLASSIFIER_ROLE__MESSAGE:
				getMessage().clear();
				return;
			case CollaborationsPackage.CLASSIFIER_ROLE__AVAILABLE_CONTENTS:
				getAvailableContents().clear();
				return;
			case CollaborationsPackage.CLASSIFIER_ROLE__CONFORMING_INSTANCE:
				getConformingInstance().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CollaborationsPackage.CLASSIFIER_ROLE__MULTIPLICITY:
				return multiplicity != null;
			case CollaborationsPackage.CLASSIFIER_ROLE__MESSAGE:
				return message != null && !message.isEmpty();
			case CollaborationsPackage.CLASSIFIER_ROLE__AVAILABLE_CONTENTS:
				return availableContents != null && !availableContents.isEmpty();
			case CollaborationsPackage.CLASSIFIER_ROLE__CONFORMING_INSTANCE:
				return conformingInstance != null && !conformingInstance.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ClassifierRoleImpl
