/**
 * <copyright>
 * </copyright>
 *
 * $Id: InteractionInstanceSet.java,v 1.1 2012/04/23 09:31:24 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus;

import net.jgsuess.uml14.foundation.core.ModelElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interaction Instance Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet#getContext <em>Context</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet#getInteraction <em>Interaction</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet#getParticipatingStimulus <em>Participating Stimulus</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getInteractionInstanceSet()
 * @model
 * @generated
 */
public interface InteractionInstanceSet extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Context</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationInstanceSet#getInteractionInstanceSet <em>Interaction Instance Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Context</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Context</em>' container reference.
	 * @see #setContext(CollaborationInstanceSet)
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getInteractionInstanceSet_Context()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationInstanceSet#getInteractionInstanceSet
	 * @model opposite="interactionInstanceSet" required="true"
	 * @generated
	 */
	CollaborationInstanceSet getContext();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet#getContext <em>Context</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Context</em>' container reference.
	 * @see #getContext()
	 * @generated
	 */
	void setContext(CollaborationInstanceSet value);

	/**
	 * Returns the value of the '<em><b>Interaction</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Interaction#getInteractionInstanceSet <em>Interaction Instance Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interaction</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interaction</em>' reference.
	 * @see #setInteraction(Interaction)
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getInteractionInstanceSet_Interaction()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Interaction#getInteractionInstanceSet
	 * @model opposite="interactionInstanceSet"
	 * @generated
	 */
	Interaction getInteraction();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet#getInteraction <em>Interaction</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interaction</em>' reference.
	 * @see #getInteraction()
	 * @generated
	 */
	void setInteraction(Interaction value);

	/**
	 * Returns the value of the '<em><b>Participating Stimulus</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus#getInteractionInstanceSet <em>Interaction Instance Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Participating Stimulus</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Participating Stimulus</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getInteractionInstanceSet_ParticipatingStimulus()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus#getInteractionInstanceSet
	 * @model opposite="interactionInstanceSet" required="true"
	 * @generated
	 */
	EList<Stimulus> getParticipatingStimulus();

} // InteractionInstanceSet
