/**
 * <copyright>
 * </copyright>
 *
 * $Id: Interaction.java,v 1.1 2012/04/23 09:31:23 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations;

import net.jgsuess.uml14.foundation.core.ModelElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interaction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.Interaction#getMessage <em>Message</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.Interaction#getContext <em>Context</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.Interaction#getInteractionInstanceSet <em>Interaction Instance Set</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getInteraction()
 * @model
 * @generated
 */
public interface Interaction extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Message</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.collaborations.Message}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getInteraction <em>Interaction</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message</em>' containment reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getInteraction_Message()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Message#getInteraction
	 * @model opposite="interaction" containment="true" required="true"
	 * @generated
	 */
	EList<Message> getMessage();

	/**
	 * Returns the value of the '<em><b>Context</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Collaboration#getInteraction <em>Interaction</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Context</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Context</em>' container reference.
	 * @see #setContext(Collaboration)
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getInteraction_Context()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Collaboration#getInteraction
	 * @model opposite="interaction" required="true"
	 * @generated
	 */
	Collaboration getContext();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Interaction#getContext <em>Context</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Context</em>' container reference.
	 * @see #getContext()
	 * @generated
	 */
	void setContext(Collaboration value);

	/**
	 * Returns the value of the '<em><b>Interaction Instance Set</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet#getInteraction <em>Interaction</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interaction Instance Set</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interaction Instance Set</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage#getInteraction_InteractionInstanceSet()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet#getInteraction
	 * @model opposite="interaction"
	 * @generated
	 */
	EList<InteractionInstanceSet> getInteractionInstanceSet();

} // Interaction
