/**
 * <copyright>
 * </copyright>
 *
 * $Id: CollaborationsPackage.java,v 1.1 2012/04/23 09:31:23 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations;

import net.jgsuess.uml14.foundation.core.CorePackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsFactory
 * @model kind="package"
 * @generated
 */
public interface CollaborationsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "collaborations";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://jgsuess.net/uml14/collaborations";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "collaborations";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CollaborationsPackage eINSTANCE = net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationsPackageImpl.init();

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationImpl <em>Collaboration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationImpl
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationsPackageImpl#getCollaboration()
	 * @generated
	 */
	int COLLABORATION = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__NAME = CorePackage.GENERALIZABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__VISIBILITY = CorePackage.GENERALIZABLE_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__IS_SPECIFICATION = CorePackage.GENERALIZABLE_ELEMENT__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__NAMESPACE = CorePackage.GENERALIZABLE_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__CLIENT_DEPENDENCY = CorePackage.GENERALIZABLE_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__CONSTRAINT = CorePackage.GENERALIZABLE_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__SUPPLIER_DEPENDENCY = CorePackage.GENERALIZABLE_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__PRESENTATION = CorePackage.GENERALIZABLE_ELEMENT__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__TARGET_FLOW = CorePackage.GENERALIZABLE_ELEMENT__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__SOURCE_FLOW = CorePackage.GENERALIZABLE_ELEMENT__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__DEFAULTED_PARAMETER = CorePackage.GENERALIZABLE_ELEMENT__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__COMMENT = CorePackage.GENERALIZABLE_ELEMENT__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__ELEMENT_RESIDENCE = CorePackage.GENERALIZABLE_ELEMENT__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__TEMPLATE_PARAMETER = CorePackage.GENERALIZABLE_ELEMENT__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__PARAMETER_TEMPLATE = CorePackage.GENERALIZABLE_ELEMENT__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__STEREOTYPE = CorePackage.GENERALIZABLE_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__TAGGED_VALUE = CorePackage.GENERALIZABLE_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__REFERENCE_TAG = CorePackage.GENERALIZABLE_ELEMENT__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__TEMPLATE_ARGUMENT = CorePackage.GENERALIZABLE_ELEMENT__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__BEHAVIOR = CorePackage.GENERALIZABLE_ELEMENT__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__CLASSIFIER_ROLE = CorePackage.GENERALIZABLE_ELEMENT__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__COLLABORATION = CorePackage.GENERALIZABLE_ELEMENT__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__COLLABORATION_INSTANCE_SET = CorePackage.GENERALIZABLE_ELEMENT__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__PARTITION = CorePackage.GENERALIZABLE_ELEMENT__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__ELEMENT_IMPORT = CorePackage.GENERALIZABLE_ELEMENT__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Is Root</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__IS_ROOT = CorePackage.GENERALIZABLE_ELEMENT__IS_ROOT;

	/**
	 * The feature id for the '<em><b>Is Leaf</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__IS_LEAF = CorePackage.GENERALIZABLE_ELEMENT__IS_LEAF;

	/**
	 * The feature id for the '<em><b>Is Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__IS_ABSTRACT = CorePackage.GENERALIZABLE_ELEMENT__IS_ABSTRACT;

	/**
	 * The feature id for the '<em><b>Generalization</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__GENERALIZATION = CorePackage.GENERALIZABLE_ELEMENT__GENERALIZATION;

	/**
	 * The feature id for the '<em><b>Specialization</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__SPECIALIZATION = CorePackage.GENERALIZABLE_ELEMENT__SPECIALIZATION;

	/**
	 * The feature id for the '<em><b>Owned Element</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__OWNED_ELEMENT = CorePackage.GENERALIZABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Interaction</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__INTERACTION = CorePackage.GENERALIZABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Constraining Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION__CONSTRAINING_ELEMENT = CorePackage.GENERALIZABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Collaboration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_FEATURE_COUNT = CorePackage.GENERALIZABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.ClassifierRoleImpl <em>Classifier Role</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.ClassifierRoleImpl
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationsPackageImpl#getClassifierRole()
	 * @generated
	 */
	int CLASSIFIER_ROLE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__NAME = CorePackage.CLASSIFIER__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__VISIBILITY = CorePackage.CLASSIFIER__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__IS_SPECIFICATION = CorePackage.CLASSIFIER__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__NAMESPACE = CorePackage.CLASSIFIER__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__CLIENT_DEPENDENCY = CorePackage.CLASSIFIER__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__CONSTRAINT = CorePackage.CLASSIFIER__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__SUPPLIER_DEPENDENCY = CorePackage.CLASSIFIER__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__PRESENTATION = CorePackage.CLASSIFIER__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__TARGET_FLOW = CorePackage.CLASSIFIER__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__SOURCE_FLOW = CorePackage.CLASSIFIER__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__DEFAULTED_PARAMETER = CorePackage.CLASSIFIER__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__COMMENT = CorePackage.CLASSIFIER__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__ELEMENT_RESIDENCE = CorePackage.CLASSIFIER__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__TEMPLATE_PARAMETER = CorePackage.CLASSIFIER__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__PARAMETER_TEMPLATE = CorePackage.CLASSIFIER__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__STEREOTYPE = CorePackage.CLASSIFIER__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__TAGGED_VALUE = CorePackage.CLASSIFIER__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__REFERENCE_TAG = CorePackage.CLASSIFIER__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__TEMPLATE_ARGUMENT = CorePackage.CLASSIFIER__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__BEHAVIOR = CorePackage.CLASSIFIER__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__CLASSIFIER_ROLE = CorePackage.CLASSIFIER__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__COLLABORATION = CorePackage.CLASSIFIER__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__COLLABORATION_INSTANCE_SET = CorePackage.CLASSIFIER__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__PARTITION = CorePackage.CLASSIFIER__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__ELEMENT_IMPORT = CorePackage.CLASSIFIER__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Is Root</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__IS_ROOT = CorePackage.CLASSIFIER__IS_ROOT;

	/**
	 * The feature id for the '<em><b>Is Leaf</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__IS_LEAF = CorePackage.CLASSIFIER__IS_LEAF;

	/**
	 * The feature id for the '<em><b>Is Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__IS_ABSTRACT = CorePackage.CLASSIFIER__IS_ABSTRACT;

	/**
	 * The feature id for the '<em><b>Generalization</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__GENERALIZATION = CorePackage.CLASSIFIER__GENERALIZATION;

	/**
	 * The feature id for the '<em><b>Specialization</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__SPECIALIZATION = CorePackage.CLASSIFIER__SPECIALIZATION;

	/**
	 * The feature id for the '<em><b>Owned Element</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__OWNED_ELEMENT = CorePackage.CLASSIFIER__OWNED_ELEMENT;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__FEATURE = CorePackage.CLASSIFIER__FEATURE;

	/**
	 * The feature id for the '<em><b>Typed Feature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__TYPED_FEATURE = CorePackage.CLASSIFIER__TYPED_FEATURE;

	/**
	 * The feature id for the '<em><b>Typed Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__TYPED_PARAMETER = CorePackage.CLASSIFIER__TYPED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Association</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__ASSOCIATION = CorePackage.CLASSIFIER__ASSOCIATION;

	/**
	 * The feature id for the '<em><b>Specified End</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__SPECIFIED_END = CorePackage.CLASSIFIER__SPECIFIED_END;

	/**
	 * The feature id for the '<em><b>Powertype Range</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__POWERTYPE_RANGE = CorePackage.CLASSIFIER__POWERTYPE_RANGE;

	/**
	 * The feature id for the '<em><b>Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__INSTANCE = CorePackage.CLASSIFIER__INSTANCE;

	/**
	 * The feature id for the '<em><b>Create Action</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__CREATE_ACTION = CorePackage.CLASSIFIER__CREATE_ACTION;

	/**
	 * The feature id for the '<em><b>Classifier In State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__CLASSIFIER_IN_STATE = CorePackage.CLASSIFIER__CLASSIFIER_IN_STATE;

	/**
	 * The feature id for the '<em><b>Object Flow State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__OBJECT_FLOW_STATE = CorePackage.CLASSIFIER__OBJECT_FLOW_STATE;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__MULTIPLICITY = CorePackage.CLASSIFIER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__MESSAGE = CorePackage.CLASSIFIER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Available Contents</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__AVAILABLE_CONTENTS = CorePackage.CLASSIFIER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Conforming Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE__CONFORMING_INSTANCE = CorePackage.CLASSIFIER_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Classifier Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_ROLE_FEATURE_COUNT = CorePackage.CLASSIFIER_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.AssociationRoleImpl <em>Association Role</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.AssociationRoleImpl
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationsPackageImpl#getAssociationRole()
	 * @generated
	 */
	int ASSOCIATION_ROLE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__NAME = CorePackage.ASSOCIATION__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__VISIBILITY = CorePackage.ASSOCIATION__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__IS_SPECIFICATION = CorePackage.ASSOCIATION__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__NAMESPACE = CorePackage.ASSOCIATION__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__CLIENT_DEPENDENCY = CorePackage.ASSOCIATION__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__CONSTRAINT = CorePackage.ASSOCIATION__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__SUPPLIER_DEPENDENCY = CorePackage.ASSOCIATION__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__PRESENTATION = CorePackage.ASSOCIATION__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__TARGET_FLOW = CorePackage.ASSOCIATION__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__SOURCE_FLOW = CorePackage.ASSOCIATION__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__DEFAULTED_PARAMETER = CorePackage.ASSOCIATION__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__COMMENT = CorePackage.ASSOCIATION__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__ELEMENT_RESIDENCE = CorePackage.ASSOCIATION__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__TEMPLATE_PARAMETER = CorePackage.ASSOCIATION__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__PARAMETER_TEMPLATE = CorePackage.ASSOCIATION__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__STEREOTYPE = CorePackage.ASSOCIATION__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__TAGGED_VALUE = CorePackage.ASSOCIATION__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__REFERENCE_TAG = CorePackage.ASSOCIATION__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__TEMPLATE_ARGUMENT = CorePackage.ASSOCIATION__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__BEHAVIOR = CorePackage.ASSOCIATION__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__CLASSIFIER_ROLE = CorePackage.ASSOCIATION__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__COLLABORATION = CorePackage.ASSOCIATION__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__COLLABORATION_INSTANCE_SET = CorePackage.ASSOCIATION__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__PARTITION = CorePackage.ASSOCIATION__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__ELEMENT_IMPORT = CorePackage.ASSOCIATION__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Is Root</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__IS_ROOT = CorePackage.ASSOCIATION__IS_ROOT;

	/**
	 * The feature id for the '<em><b>Is Leaf</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__IS_LEAF = CorePackage.ASSOCIATION__IS_LEAF;

	/**
	 * The feature id for the '<em><b>Is Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__IS_ABSTRACT = CorePackage.ASSOCIATION__IS_ABSTRACT;

	/**
	 * The feature id for the '<em><b>Generalization</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__GENERALIZATION = CorePackage.ASSOCIATION__GENERALIZATION;

	/**
	 * The feature id for the '<em><b>Specialization</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__SPECIALIZATION = CorePackage.ASSOCIATION__SPECIALIZATION;

	/**
	 * The feature id for the '<em><b>Connection</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__CONNECTION = CorePackage.ASSOCIATION__CONNECTION;

	/**
	 * The feature id for the '<em><b>Link</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__LINK = CorePackage.ASSOCIATION__LINK;

	/**
	 * The feature id for the '<em><b>Association Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__ASSOCIATION_ROLE = CorePackage.ASSOCIATION__ASSOCIATION_ROLE;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__MULTIPLICITY = CorePackage.ASSOCIATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__BASE = CorePackage.ASSOCIATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__MESSAGE = CorePackage.ASSOCIATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Conforming Link</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE__CONFORMING_LINK = CorePackage.ASSOCIATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Association Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_ROLE_FEATURE_COUNT = CorePackage.ASSOCIATION_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.AssociationEndRoleImpl <em>Association End Role</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.AssociationEndRoleImpl
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationsPackageImpl#getAssociationEndRole()
	 * @generated
	 */
	int ASSOCIATION_END_ROLE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__NAME = CorePackage.ASSOCIATION_END__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__VISIBILITY = CorePackage.ASSOCIATION_END__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__IS_SPECIFICATION = CorePackage.ASSOCIATION_END__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__NAMESPACE = CorePackage.ASSOCIATION_END__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__CLIENT_DEPENDENCY = CorePackage.ASSOCIATION_END__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__CONSTRAINT = CorePackage.ASSOCIATION_END__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__SUPPLIER_DEPENDENCY = CorePackage.ASSOCIATION_END__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__PRESENTATION = CorePackage.ASSOCIATION_END__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__TARGET_FLOW = CorePackage.ASSOCIATION_END__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__SOURCE_FLOW = CorePackage.ASSOCIATION_END__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__DEFAULTED_PARAMETER = CorePackage.ASSOCIATION_END__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__COMMENT = CorePackage.ASSOCIATION_END__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__ELEMENT_RESIDENCE = CorePackage.ASSOCIATION_END__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__TEMPLATE_PARAMETER = CorePackage.ASSOCIATION_END__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__PARAMETER_TEMPLATE = CorePackage.ASSOCIATION_END__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__STEREOTYPE = CorePackage.ASSOCIATION_END__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__TAGGED_VALUE = CorePackage.ASSOCIATION_END__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__REFERENCE_TAG = CorePackage.ASSOCIATION_END__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__TEMPLATE_ARGUMENT = CorePackage.ASSOCIATION_END__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__BEHAVIOR = CorePackage.ASSOCIATION_END__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__CLASSIFIER_ROLE = CorePackage.ASSOCIATION_END__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__COLLABORATION = CorePackage.ASSOCIATION_END__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__COLLABORATION_INSTANCE_SET = CorePackage.ASSOCIATION_END__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__PARTITION = CorePackage.ASSOCIATION_END__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__ELEMENT_IMPORT = CorePackage.ASSOCIATION_END__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Is Navigable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__IS_NAVIGABLE = CorePackage.ASSOCIATION_END__IS_NAVIGABLE;

	/**
	 * The feature id for the '<em><b>Ordering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__ORDERING = CorePackage.ASSOCIATION_END__ORDERING;

	/**
	 * The feature id for the '<em><b>Aggregation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__AGGREGATION = CorePackage.ASSOCIATION_END__AGGREGATION;

	/**
	 * The feature id for the '<em><b>Target Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__TARGET_SCOPE = CorePackage.ASSOCIATION_END__TARGET_SCOPE;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__MULTIPLICITY = CorePackage.ASSOCIATION_END__MULTIPLICITY;

	/**
	 * The feature id for the '<em><b>Changeability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__CHANGEABILITY = CorePackage.ASSOCIATION_END__CHANGEABILITY;

	/**
	 * The feature id for the '<em><b>Association</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__ASSOCIATION = CorePackage.ASSOCIATION_END__ASSOCIATION;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__QUALIFIER = CorePackage.ASSOCIATION_END__QUALIFIER;

	/**
	 * The feature id for the '<em><b>Participant</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__PARTICIPANT = CorePackage.ASSOCIATION_END__PARTICIPANT;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__SPECIFICATION = CorePackage.ASSOCIATION_END__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Link End</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__LINK_END = CorePackage.ASSOCIATION_END__LINK_END;

	/**
	 * The feature id for the '<em><b>Association End Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__ASSOCIATION_END_ROLE = CorePackage.ASSOCIATION_END__ASSOCIATION_END_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__COLLABORATION_MULTIPLICITY = CorePackage.ASSOCIATION_END_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__BASE = CorePackage.ASSOCIATION_END_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Available Qualifier</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE__AVAILABLE_QUALIFIER = CorePackage.ASSOCIATION_END_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Association End Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_END_ROLE_FEATURE_COUNT = CorePackage.ASSOCIATION_END_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.MessageImpl <em>Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.MessageImpl
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationsPackageImpl#getMessage()
	 * @generated
	 */
	int MESSAGE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__IS_SPECIFICATION = CorePackage.MODEL_ELEMENT__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__PRESENTATION = CorePackage.MODEL_ELEMENT__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__TARGET_FLOW = CorePackage.MODEL_ELEMENT__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__SOURCE_FLOW = CorePackage.MODEL_ELEMENT__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__DEFAULTED_PARAMETER = CorePackage.MODEL_ELEMENT__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__COMMENT = CorePackage.MODEL_ELEMENT__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__ELEMENT_RESIDENCE = CorePackage.MODEL_ELEMENT__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__TEMPLATE_PARAMETER = CorePackage.MODEL_ELEMENT__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__PARAMETER_TEMPLATE = CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__REFERENCE_TAG = CorePackage.MODEL_ELEMENT__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__TEMPLATE_ARGUMENT = CorePackage.MODEL_ELEMENT__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__BEHAVIOR = CorePackage.MODEL_ELEMENT__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__CLASSIFIER_ROLE = CorePackage.MODEL_ELEMENT__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__COLLABORATION = CorePackage.MODEL_ELEMENT__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__COLLABORATION_INSTANCE_SET = CorePackage.MODEL_ELEMENT__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__PARTITION = CorePackage.MODEL_ELEMENT__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__ELEMENT_IMPORT = CorePackage.MODEL_ELEMENT__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Interaction</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__INTERACTION = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__MESSAGE = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Activator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__ACTIVATOR = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Sender</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__SENDER = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Predecessor</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__PREDECESSOR = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Successor</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__SUCCESSOR = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Communication Connection</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__COMMUNICATION_CONNECTION = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__ACTION = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Conforming Stimulus</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__CONFORMING_STIMULUS = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.InteractionImpl <em>Interaction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.InteractionImpl
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationsPackageImpl#getInteraction()
	 * @generated
	 */
	int INTERACTION = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__IS_SPECIFICATION = CorePackage.MODEL_ELEMENT__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__PRESENTATION = CorePackage.MODEL_ELEMENT__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__TARGET_FLOW = CorePackage.MODEL_ELEMENT__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__SOURCE_FLOW = CorePackage.MODEL_ELEMENT__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__DEFAULTED_PARAMETER = CorePackage.MODEL_ELEMENT__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__COMMENT = CorePackage.MODEL_ELEMENT__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__ELEMENT_RESIDENCE = CorePackage.MODEL_ELEMENT__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__TEMPLATE_PARAMETER = CorePackage.MODEL_ELEMENT__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__PARAMETER_TEMPLATE = CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__REFERENCE_TAG = CorePackage.MODEL_ELEMENT__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__TEMPLATE_ARGUMENT = CorePackage.MODEL_ELEMENT__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__BEHAVIOR = CorePackage.MODEL_ELEMENT__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__CLASSIFIER_ROLE = CorePackage.MODEL_ELEMENT__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__COLLABORATION = CorePackage.MODEL_ELEMENT__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__COLLABORATION_INSTANCE_SET = CorePackage.MODEL_ELEMENT__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__PARTITION = CorePackage.MODEL_ELEMENT__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__ELEMENT_IMPORT = CorePackage.MODEL_ELEMENT__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Message</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__MESSAGE = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Context</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__CONTEXT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Interaction Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION__INTERACTION_INSTANCE_SET = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Interaction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.InteractionInstanceSetImpl <em>Interaction Instance Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.InteractionInstanceSetImpl
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationsPackageImpl#getInteractionInstanceSet()
	 * @generated
	 */
	int INTERACTION_INSTANCE_SET = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__IS_SPECIFICATION = CorePackage.MODEL_ELEMENT__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__PRESENTATION = CorePackage.MODEL_ELEMENT__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__TARGET_FLOW = CorePackage.MODEL_ELEMENT__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__SOURCE_FLOW = CorePackage.MODEL_ELEMENT__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__DEFAULTED_PARAMETER = CorePackage.MODEL_ELEMENT__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__COMMENT = CorePackage.MODEL_ELEMENT__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__ELEMENT_RESIDENCE = CorePackage.MODEL_ELEMENT__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__TEMPLATE_PARAMETER = CorePackage.MODEL_ELEMENT__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__PARAMETER_TEMPLATE = CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__REFERENCE_TAG = CorePackage.MODEL_ELEMENT__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__TEMPLATE_ARGUMENT = CorePackage.MODEL_ELEMENT__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__BEHAVIOR = CorePackage.MODEL_ELEMENT__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__CLASSIFIER_ROLE = CorePackage.MODEL_ELEMENT__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__COLLABORATION = CorePackage.MODEL_ELEMENT__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__COLLABORATION_INSTANCE_SET = CorePackage.MODEL_ELEMENT__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__PARTITION = CorePackage.MODEL_ELEMENT__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__ELEMENT_IMPORT = CorePackage.MODEL_ELEMENT__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Context</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__CONTEXT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__INTERACTION = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Participating Stimulus</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET__PARTICIPATING_STIMULUS = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Interaction Instance Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_INSTANCE_SET_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationInstanceSetImpl <em>Collaboration Instance Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationInstanceSetImpl
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationsPackageImpl#getCollaborationInstanceSet()
	 * @generated
	 */
	int COLLABORATION_INSTANCE_SET = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__IS_SPECIFICATION = CorePackage.MODEL_ELEMENT__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__PRESENTATION = CorePackage.MODEL_ELEMENT__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__TARGET_FLOW = CorePackage.MODEL_ELEMENT__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__SOURCE_FLOW = CorePackage.MODEL_ELEMENT__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__DEFAULTED_PARAMETER = CorePackage.MODEL_ELEMENT__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__COMMENT = CorePackage.MODEL_ELEMENT__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__ELEMENT_RESIDENCE = CorePackage.MODEL_ELEMENT__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__TEMPLATE_PARAMETER = CorePackage.MODEL_ELEMENT__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__PARAMETER_TEMPLATE = CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__REFERENCE_TAG = CorePackage.MODEL_ELEMENT__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__TEMPLATE_ARGUMENT = CorePackage.MODEL_ELEMENT__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__BEHAVIOR = CorePackage.MODEL_ELEMENT__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__CLASSIFIER_ROLE = CorePackage.MODEL_ELEMENT__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__COLLABORATION = CorePackage.MODEL_ELEMENT__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__COLLABORATION_INSTANCE_SET = CorePackage.MODEL_ELEMENT__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__PARTITION = CorePackage.MODEL_ELEMENT__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__ELEMENT_IMPORT = CorePackage.MODEL_ELEMENT__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Interaction Instance Set</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__INTERACTION_INSTANCE_SET = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constraining Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET__CONSTRAINING_ELEMENT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Collaboration Instance Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLABORATION_INSTANCE_SET_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;


	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Collaboration <em>Collaboration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Collaboration</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Collaboration
	 * @generated
	 */
	EClass getCollaboration();

	/**
	 * Returns the meta object for the containment reference list '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Collaboration#getInteraction <em>Interaction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Interaction</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Collaboration#getInteraction()
	 * @see #getCollaboration()
	 * @generated
	 */
	EReference getCollaboration_Interaction();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Collaboration#getConstrainingElement <em>Constraining Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Constraining Element</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Collaboration#getConstrainingElement()
	 * @see #getCollaboration()
	 * @generated
	 */
	EReference getCollaboration_ConstrainingElement();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole <em>Classifier Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Classifier Role</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole
	 * @generated
	 */
	EClass getClassifierRole();

	/**
	 * Returns the meta object for the containment reference '{@link net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole#getMultiplicity <em>Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Multiplicity</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole#getMultiplicity()
	 * @see #getClassifierRole()
	 * @generated
	 */
	EReference getClassifierRole_Multiplicity();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Message</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole#getMessage()
	 * @see #getClassifierRole()
	 * @generated
	 */
	EReference getClassifierRole_Message();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole#getAvailableContents <em>Available Contents</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Available Contents</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole#getAvailableContents()
	 * @see #getClassifierRole()
	 * @generated
	 */
	EReference getClassifierRole_AvailableContents();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole#getConformingInstance <em>Conforming Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Conforming Instance</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole#getConformingInstance()
	 * @see #getClassifierRole()
	 * @generated
	 */
	EReference getClassifierRole_ConformingInstance();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole <em>Association Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Association Role</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole
	 * @generated
	 */
	EClass getAssociationRole();

	/**
	 * Returns the meta object for the containment reference '{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole#getMultiplicity <em>Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Multiplicity</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole#getMultiplicity()
	 * @see #getAssociationRole()
	 * @generated
	 */
	EReference getAssociationRole_Multiplicity();

	/**
	 * Returns the meta object for the reference '{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole#getBase()
	 * @see #getAssociationRole()
	 * @generated
	 */
	EReference getAssociationRole_Base();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Message</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole#getMessage()
	 * @see #getAssociationRole()
	 * @generated
	 */
	EReference getAssociationRole_Message();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole#getConformingLink <em>Conforming Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Conforming Link</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole#getConformingLink()
	 * @see #getAssociationRole()
	 * @generated
	 */
	EReference getAssociationRole_ConformingLink();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole <em>Association End Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Association End Role</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole
	 * @generated
	 */
	EClass getAssociationEndRole();

	/**
	 * Returns the meta object for the containment reference '{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole#getCollaborationMultiplicity <em>Collaboration Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Collaboration Multiplicity</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole#getCollaborationMultiplicity()
	 * @see #getAssociationEndRole()
	 * @generated
	 */
	EReference getAssociationEndRole_CollaborationMultiplicity();

	/**
	 * Returns the meta object for the reference '{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole#getBase()
	 * @see #getAssociationEndRole()
	 * @generated
	 */
	EReference getAssociationEndRole_Base();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole#getAvailableQualifier <em>Available Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Available Qualifier</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole#getAvailableQualifier()
	 * @see #getAssociationEndRole()
	 * @generated
	 */
	EReference getAssociationEndRole_AvailableQualifier();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Message
	 * @generated
	 */
	EClass getMessage();

	/**
	 * Returns the meta object for the container reference '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getInteraction <em>Interaction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Interaction</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Message#getInteraction()
	 * @see #getMessage()
	 * @generated
	 */
	EReference getMessage_Interaction();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Message</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Message#getMessage()
	 * @see #getMessage()
	 * @generated
	 */
	EReference getMessage_Message();

	/**
	 * Returns the meta object for the reference '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getActivator <em>Activator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Activator</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Message#getActivator()
	 * @see #getMessage()
	 * @generated
	 */
	EReference getMessage_Activator();

	/**
	 * Returns the meta object for the reference '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getSender <em>Sender</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sender</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Message#getSender()
	 * @see #getMessage()
	 * @generated
	 */
	EReference getMessage_Sender();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getPredecessor <em>Predecessor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Predecessor</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Message#getPredecessor()
	 * @see #getMessage()
	 * @generated
	 */
	EReference getMessage_Predecessor();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getSuccessor <em>Successor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Successor</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Message#getSuccessor()
	 * @see #getMessage()
	 * @generated
	 */
	EReference getMessage_Successor();

	/**
	 * Returns the meta object for the reference '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getCommunicationConnection <em>Communication Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Communication Connection</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Message#getCommunicationConnection()
	 * @see #getMessage()
	 * @generated
	 */
	EReference getMessage_CommunicationConnection();

	/**
	 * Returns the meta object for the reference '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Action</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Message#getAction()
	 * @see #getMessage()
	 * @generated
	 */
	EReference getMessage_Action();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getConformingStimulus <em>Conforming Stimulus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Conforming Stimulus</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Message#getConformingStimulus()
	 * @see #getMessage()
	 * @generated
	 */
	EReference getMessage_ConformingStimulus();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Interaction <em>Interaction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interaction</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Interaction
	 * @generated
	 */
	EClass getInteraction();

	/**
	 * Returns the meta object for the containment reference list '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Interaction#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Message</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Interaction#getMessage()
	 * @see #getInteraction()
	 * @generated
	 */
	EReference getInteraction_Message();

	/**
	 * Returns the meta object for the container reference '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Interaction#getContext <em>Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Context</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Interaction#getContext()
	 * @see #getInteraction()
	 * @generated
	 */
	EReference getInteraction_Context();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Interaction#getInteractionInstanceSet <em>Interaction Instance Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Interaction Instance Set</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Interaction#getInteractionInstanceSet()
	 * @see #getInteraction()
	 * @generated
	 */
	EReference getInteraction_InteractionInstanceSet();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet <em>Interaction Instance Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interaction Instance Set</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet
	 * @generated
	 */
	EClass getInteractionInstanceSet();

	/**
	 * Returns the meta object for the container reference '{@link net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet#getContext <em>Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Context</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet#getContext()
	 * @see #getInteractionInstanceSet()
	 * @generated
	 */
	EReference getInteractionInstanceSet_Context();

	/**
	 * Returns the meta object for the reference '{@link net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet#getInteraction <em>Interaction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Interaction</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet#getInteraction()
	 * @see #getInteractionInstanceSet()
	 * @generated
	 */
	EReference getInteractionInstanceSet_Interaction();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet#getParticipatingStimulus <em>Participating Stimulus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Participating Stimulus</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet#getParticipatingStimulus()
	 * @see #getInteractionInstanceSet()
	 * @generated
	 */
	EReference getInteractionInstanceSet_ParticipatingStimulus();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationInstanceSet <em>Collaboration Instance Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Collaboration Instance Set</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationInstanceSet
	 * @generated
	 */
	EClass getCollaborationInstanceSet();

	/**
	 * Returns the meta object for the containment reference list '{@link net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationInstanceSet#getInteractionInstanceSet <em>Interaction Instance Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Interaction Instance Set</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationInstanceSet#getInteractionInstanceSet()
	 * @see #getCollaborationInstanceSet()
	 * @generated
	 */
	EReference getCollaborationInstanceSet_InteractionInstanceSet();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationInstanceSet#getConstrainingElement <em>Constraining Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Constraining Element</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationInstanceSet#getConstrainingElement()
	 * @see #getCollaborationInstanceSet()
	 * @generated
	 */
	EReference getCollaborationInstanceSet_ConstrainingElement();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CollaborationsFactory getCollaborationsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationImpl <em>Collaboration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationImpl
		 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationsPackageImpl#getCollaboration()
		 * @generated
		 */
		EClass COLLABORATION = eINSTANCE.getCollaboration();

		/**
		 * The meta object literal for the '<em><b>Interaction</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COLLABORATION__INTERACTION = eINSTANCE.getCollaboration_Interaction();

		/**
		 * The meta object literal for the '<em><b>Constraining Element</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COLLABORATION__CONSTRAINING_ELEMENT = eINSTANCE.getCollaboration_ConstrainingElement();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.ClassifierRoleImpl <em>Classifier Role</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.ClassifierRoleImpl
		 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationsPackageImpl#getClassifierRole()
		 * @generated
		 */
		EClass CLASSIFIER_ROLE = eINSTANCE.getClassifierRole();

		/**
		 * The meta object literal for the '<em><b>Multiplicity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASSIFIER_ROLE__MULTIPLICITY = eINSTANCE.getClassifierRole_Multiplicity();

		/**
		 * The meta object literal for the '<em><b>Message</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASSIFIER_ROLE__MESSAGE = eINSTANCE.getClassifierRole_Message();

		/**
		 * The meta object literal for the '<em><b>Available Contents</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASSIFIER_ROLE__AVAILABLE_CONTENTS = eINSTANCE.getClassifierRole_AvailableContents();

		/**
		 * The meta object literal for the '<em><b>Conforming Instance</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASSIFIER_ROLE__CONFORMING_INSTANCE = eINSTANCE.getClassifierRole_ConformingInstance();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.AssociationRoleImpl <em>Association Role</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.AssociationRoleImpl
		 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationsPackageImpl#getAssociationRole()
		 * @generated
		 */
		EClass ASSOCIATION_ROLE = eINSTANCE.getAssociationRole();

		/**
		 * The meta object literal for the '<em><b>Multiplicity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSOCIATION_ROLE__MULTIPLICITY = eINSTANCE.getAssociationRole_Multiplicity();

		/**
		 * The meta object literal for the '<em><b>Base</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSOCIATION_ROLE__BASE = eINSTANCE.getAssociationRole_Base();

		/**
		 * The meta object literal for the '<em><b>Message</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSOCIATION_ROLE__MESSAGE = eINSTANCE.getAssociationRole_Message();

		/**
		 * The meta object literal for the '<em><b>Conforming Link</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSOCIATION_ROLE__CONFORMING_LINK = eINSTANCE.getAssociationRole_ConformingLink();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.AssociationEndRoleImpl <em>Association End Role</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.AssociationEndRoleImpl
		 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationsPackageImpl#getAssociationEndRole()
		 * @generated
		 */
		EClass ASSOCIATION_END_ROLE = eINSTANCE.getAssociationEndRole();

		/**
		 * The meta object literal for the '<em><b>Collaboration Multiplicity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSOCIATION_END_ROLE__COLLABORATION_MULTIPLICITY = eINSTANCE.getAssociationEndRole_CollaborationMultiplicity();

		/**
		 * The meta object literal for the '<em><b>Base</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSOCIATION_END_ROLE__BASE = eINSTANCE.getAssociationEndRole_Base();

		/**
		 * The meta object literal for the '<em><b>Available Qualifier</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSOCIATION_END_ROLE__AVAILABLE_QUALIFIER = eINSTANCE.getAssociationEndRole_AvailableQualifier();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.MessageImpl <em>Message</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.MessageImpl
		 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationsPackageImpl#getMessage()
		 * @generated
		 */
		EClass MESSAGE = eINSTANCE.getMessage();

		/**
		 * The meta object literal for the '<em><b>Interaction</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE__INTERACTION = eINSTANCE.getMessage_Interaction();

		/**
		 * The meta object literal for the '<em><b>Message</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE__MESSAGE = eINSTANCE.getMessage_Message();

		/**
		 * The meta object literal for the '<em><b>Activator</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE__ACTIVATOR = eINSTANCE.getMessage_Activator();

		/**
		 * The meta object literal for the '<em><b>Sender</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE__SENDER = eINSTANCE.getMessage_Sender();

		/**
		 * The meta object literal for the '<em><b>Predecessor</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE__PREDECESSOR = eINSTANCE.getMessage_Predecessor();

		/**
		 * The meta object literal for the '<em><b>Successor</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE__SUCCESSOR = eINSTANCE.getMessage_Successor();

		/**
		 * The meta object literal for the '<em><b>Communication Connection</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE__COMMUNICATION_CONNECTION = eINSTANCE.getMessage_CommunicationConnection();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE__ACTION = eINSTANCE.getMessage_Action();

		/**
		 * The meta object literal for the '<em><b>Conforming Stimulus</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE__CONFORMING_STIMULUS = eINSTANCE.getMessage_ConformingStimulus();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.InteractionImpl <em>Interaction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.InteractionImpl
		 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationsPackageImpl#getInteraction()
		 * @generated
		 */
		EClass INTERACTION = eINSTANCE.getInteraction();

		/**
		 * The meta object literal for the '<em><b>Message</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERACTION__MESSAGE = eINSTANCE.getInteraction_Message();

		/**
		 * The meta object literal for the '<em><b>Context</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERACTION__CONTEXT = eINSTANCE.getInteraction_Context();

		/**
		 * The meta object literal for the '<em><b>Interaction Instance Set</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERACTION__INTERACTION_INSTANCE_SET = eINSTANCE.getInteraction_InteractionInstanceSet();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.InteractionInstanceSetImpl <em>Interaction Instance Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.InteractionInstanceSetImpl
		 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationsPackageImpl#getInteractionInstanceSet()
		 * @generated
		 */
		EClass INTERACTION_INSTANCE_SET = eINSTANCE.getInteractionInstanceSet();

		/**
		 * The meta object literal for the '<em><b>Context</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERACTION_INSTANCE_SET__CONTEXT = eINSTANCE.getInteractionInstanceSet_Context();

		/**
		 * The meta object literal for the '<em><b>Interaction</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERACTION_INSTANCE_SET__INTERACTION = eINSTANCE.getInteractionInstanceSet_Interaction();

		/**
		 * The meta object literal for the '<em><b>Participating Stimulus</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERACTION_INSTANCE_SET__PARTICIPATING_STIMULUS = eINSTANCE.getInteractionInstanceSet_ParticipatingStimulus();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationInstanceSetImpl <em>Collaboration Instance Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationInstanceSetImpl
		 * @see net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationsPackageImpl#getCollaborationInstanceSet()
		 * @generated
		 */
		EClass COLLABORATION_INSTANCE_SET = eINSTANCE.getCollaborationInstanceSet();

		/**
		 * The meta object literal for the '<em><b>Interaction Instance Set</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COLLABORATION_INSTANCE_SET__INTERACTION_INSTANCE_SET = eINSTANCE.getCollaborationInstanceSet_InteractionInstanceSet();

		/**
		 * The meta object literal for the '<em><b>Constraining Element</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COLLABORATION_INSTANCE_SET__CONSTRAINING_ELEMENT = eINSTANCE.getCollaborationInstanceSet_ConstrainingElement();

	}

} //CollaborationsPackage
