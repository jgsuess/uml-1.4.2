/**
 * <copyright>
 * </copyright>
 *
 * $Id: AssociationRoleImpl.java,v 1.1 2012/04/23 09:31:40 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage;
import net.jgsuess.uml14.behavioral_elements.collaborations.Message;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Link;

import net.jgsuess.uml14.foundation.core.Association;
import net.jgsuess.uml14.foundation.core.CorePackage;

import net.jgsuess.uml14.foundation.core.impl.AssociationImpl;

import net.jgsuess.uml14.foundation.data_types.Multiplicity;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Association Role</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.AssociationRoleImpl#getMultiplicity <em>Multiplicity</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.AssociationRoleImpl#getBase <em>Base</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.AssociationRoleImpl#getMessage <em>Message</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.AssociationRoleImpl#getConformingLink <em>Conforming Link</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AssociationRoleImpl extends AssociationImpl implements AssociationRole {
	/**
	 * The cached value of the '{@link #getMultiplicity() <em>Multiplicity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMultiplicity()
	 * @generated
	 * @ordered
	 */
	protected Multiplicity multiplicity;

	/**
	 * The cached value of the '{@link #getBase() <em>Base</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase()
	 * @generated
	 * @ordered
	 */
	protected Association base;

	/**
	 * The cached value of the '{@link #getMessage() <em>Message</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage()
	 * @generated
	 * @ordered
	 */
	protected EList<Message> message;

	/**
	 * The cached value of the '{@link #getConformingLink() <em>Conforming Link</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConformingLink()
	 * @generated
	 * @ordered
	 */
	protected EList<Link> conformingLink;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssociationRoleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CollaborationsPackage.Literals.ASSOCIATION_ROLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Multiplicity getMultiplicity() {
		return multiplicity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMultiplicity(Multiplicity newMultiplicity, NotificationChain msgs) {
		Multiplicity oldMultiplicity = multiplicity;
		multiplicity = newMultiplicity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CollaborationsPackage.ASSOCIATION_ROLE__MULTIPLICITY, oldMultiplicity, newMultiplicity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMultiplicity(Multiplicity newMultiplicity) {
		if (newMultiplicity != multiplicity) {
			NotificationChain msgs = null;
			if (multiplicity != null)
				msgs = ((InternalEObject)multiplicity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CollaborationsPackage.ASSOCIATION_ROLE__MULTIPLICITY, null, msgs);
			if (newMultiplicity != null)
				msgs = ((InternalEObject)newMultiplicity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CollaborationsPackage.ASSOCIATION_ROLE__MULTIPLICITY, null, msgs);
			msgs = basicSetMultiplicity(newMultiplicity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CollaborationsPackage.ASSOCIATION_ROLE__MULTIPLICITY, newMultiplicity, newMultiplicity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Association getBase() {
		if (base != null && base.eIsProxy()) {
			InternalEObject oldBase = (InternalEObject)base;
			base = (Association)eResolveProxy(oldBase);
			if (base != oldBase) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CollaborationsPackage.ASSOCIATION_ROLE__BASE, oldBase, base));
			}
		}
		return base;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Association basicGetBase() {
		return base;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBase(Association newBase, NotificationChain msgs) {
		Association oldBase = base;
		base = newBase;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CollaborationsPackage.ASSOCIATION_ROLE__BASE, oldBase, newBase);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase(Association newBase) {
		if (newBase != base) {
			NotificationChain msgs = null;
			if (base != null)
				msgs = ((InternalEObject)base).eInverseRemove(this, CorePackage.ASSOCIATION__ASSOCIATION_ROLE, Association.class, msgs);
			if (newBase != null)
				msgs = ((InternalEObject)newBase).eInverseAdd(this, CorePackage.ASSOCIATION__ASSOCIATION_ROLE, Association.class, msgs);
			msgs = basicSetBase(newBase, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CollaborationsPackage.ASSOCIATION_ROLE__BASE, newBase, newBase));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Message> getMessage() {
		if (message == null) {
			message = new EObjectWithInverseResolvingEList<Message>(Message.class, this, CollaborationsPackage.ASSOCIATION_ROLE__MESSAGE, CollaborationsPackage.MESSAGE__COMMUNICATION_CONNECTION);
		}
		return message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Link> getConformingLink() {
		if (conformingLink == null) {
			conformingLink = new EObjectWithInverseResolvingEList.ManyInverse<Link>(Link.class, this, CollaborationsPackage.ASSOCIATION_ROLE__CONFORMING_LINK, Common_behaviorPackage.LINK__PLAYED_ROLE);
		}
		return conformingLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CollaborationsPackage.ASSOCIATION_ROLE__BASE:
				if (base != null)
					msgs = ((InternalEObject)base).eInverseRemove(this, CorePackage.ASSOCIATION__ASSOCIATION_ROLE, Association.class, msgs);
				return basicSetBase((Association)otherEnd, msgs);
			case CollaborationsPackage.ASSOCIATION_ROLE__MESSAGE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getMessage()).basicAdd(otherEnd, msgs);
			case CollaborationsPackage.ASSOCIATION_ROLE__CONFORMING_LINK:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getConformingLink()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CollaborationsPackage.ASSOCIATION_ROLE__MULTIPLICITY:
				return basicSetMultiplicity(null, msgs);
			case CollaborationsPackage.ASSOCIATION_ROLE__BASE:
				return basicSetBase(null, msgs);
			case CollaborationsPackage.ASSOCIATION_ROLE__MESSAGE:
				return ((InternalEList<?>)getMessage()).basicRemove(otherEnd, msgs);
			case CollaborationsPackage.ASSOCIATION_ROLE__CONFORMING_LINK:
				return ((InternalEList<?>)getConformingLink()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CollaborationsPackage.ASSOCIATION_ROLE__MULTIPLICITY:
				return getMultiplicity();
			case CollaborationsPackage.ASSOCIATION_ROLE__BASE:
				if (resolve) return getBase();
				return basicGetBase();
			case CollaborationsPackage.ASSOCIATION_ROLE__MESSAGE:
				return getMessage();
			case CollaborationsPackage.ASSOCIATION_ROLE__CONFORMING_LINK:
				return getConformingLink();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CollaborationsPackage.ASSOCIATION_ROLE__MULTIPLICITY:
				setMultiplicity((Multiplicity)newValue);
				return;
			case CollaborationsPackage.ASSOCIATION_ROLE__BASE:
				setBase((Association)newValue);
				return;
			case CollaborationsPackage.ASSOCIATION_ROLE__MESSAGE:
				getMessage().clear();
				getMessage().addAll((Collection<? extends Message>)newValue);
				return;
			case CollaborationsPackage.ASSOCIATION_ROLE__CONFORMING_LINK:
				getConformingLink().clear();
				getConformingLink().addAll((Collection<? extends Link>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CollaborationsPackage.ASSOCIATION_ROLE__MULTIPLICITY:
				setMultiplicity((Multiplicity)null);
				return;
			case CollaborationsPackage.ASSOCIATION_ROLE__BASE:
				setBase((Association)null);
				return;
			case CollaborationsPackage.ASSOCIATION_ROLE__MESSAGE:
				getMessage().clear();
				return;
			case CollaborationsPackage.ASSOCIATION_ROLE__CONFORMING_LINK:
				getConformingLink().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CollaborationsPackage.ASSOCIATION_ROLE__MULTIPLICITY:
				return multiplicity != null;
			case CollaborationsPackage.ASSOCIATION_ROLE__BASE:
				return base != null;
			case CollaborationsPackage.ASSOCIATION_ROLE__MESSAGE:
				return message != null && !message.isEmpty();
			case CollaborationsPackage.ASSOCIATION_ROLE__CONFORMING_LINK:
				return conformingLink != null && !conformingLink.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AssociationRoleImpl
