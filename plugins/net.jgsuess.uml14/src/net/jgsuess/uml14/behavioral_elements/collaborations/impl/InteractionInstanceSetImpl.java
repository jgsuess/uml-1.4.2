/**
 * <copyright>
 * </copyright>
 *
 * $Id: InteractionInstanceSetImpl.java,v 1.1 2012/04/23 09:31:39 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationInstanceSet;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage;
import net.jgsuess.uml14.behavioral_elements.collaborations.Interaction;
import net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus;

import net.jgsuess.uml14.foundation.core.impl.ModelElementImpl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interaction Instance Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.InteractionInstanceSetImpl#getContext <em>Context</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.InteractionInstanceSetImpl#getInteraction <em>Interaction</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.InteractionInstanceSetImpl#getParticipatingStimulus <em>Participating Stimulus</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class InteractionInstanceSetImpl extends ModelElementImpl implements InteractionInstanceSet {
	/**
	 * The cached value of the '{@link #getInteraction() <em>Interaction</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInteraction()
	 * @generated
	 * @ordered
	 */
	protected Interaction interaction;

	/**
	 * The cached value of the '{@link #getParticipatingStimulus() <em>Participating Stimulus</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParticipatingStimulus()
	 * @generated
	 * @ordered
	 */
	protected EList<Stimulus> participatingStimulus;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InteractionInstanceSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CollaborationsPackage.Literals.INTERACTION_INSTANCE_SET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CollaborationInstanceSet getContext() {
		if (eContainerFeatureID() != CollaborationsPackage.INTERACTION_INSTANCE_SET__CONTEXT) return null;
		return (CollaborationInstanceSet)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContext(CollaborationInstanceSet newContext, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newContext, CollaborationsPackage.INTERACTION_INSTANCE_SET__CONTEXT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContext(CollaborationInstanceSet newContext) {
		if (newContext != eInternalContainer() || (eContainerFeatureID() != CollaborationsPackage.INTERACTION_INSTANCE_SET__CONTEXT && newContext != null)) {
			if (EcoreUtil.isAncestor(this, newContext))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newContext != null)
				msgs = ((InternalEObject)newContext).eInverseAdd(this, CollaborationsPackage.COLLABORATION_INSTANCE_SET__INTERACTION_INSTANCE_SET, CollaborationInstanceSet.class, msgs);
			msgs = basicSetContext(newContext, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CollaborationsPackage.INTERACTION_INSTANCE_SET__CONTEXT, newContext, newContext));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interaction getInteraction() {
		if (interaction != null && interaction.eIsProxy()) {
			InternalEObject oldInteraction = (InternalEObject)interaction;
			interaction = (Interaction)eResolveProxy(oldInteraction);
			if (interaction != oldInteraction) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CollaborationsPackage.INTERACTION_INSTANCE_SET__INTERACTION, oldInteraction, interaction));
			}
		}
		return interaction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interaction basicGetInteraction() {
		return interaction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInteraction(Interaction newInteraction, NotificationChain msgs) {
		Interaction oldInteraction = interaction;
		interaction = newInteraction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CollaborationsPackage.INTERACTION_INSTANCE_SET__INTERACTION, oldInteraction, newInteraction);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInteraction(Interaction newInteraction) {
		if (newInteraction != interaction) {
			NotificationChain msgs = null;
			if (interaction != null)
				msgs = ((InternalEObject)interaction).eInverseRemove(this, CollaborationsPackage.INTERACTION__INTERACTION_INSTANCE_SET, Interaction.class, msgs);
			if (newInteraction != null)
				msgs = ((InternalEObject)newInteraction).eInverseAdd(this, CollaborationsPackage.INTERACTION__INTERACTION_INSTANCE_SET, Interaction.class, msgs);
			msgs = basicSetInteraction(newInteraction, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CollaborationsPackage.INTERACTION_INSTANCE_SET__INTERACTION, newInteraction, newInteraction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Stimulus> getParticipatingStimulus() {
		if (participatingStimulus == null) {
			participatingStimulus = new EObjectWithInverseResolvingEList.ManyInverse<Stimulus>(Stimulus.class, this, CollaborationsPackage.INTERACTION_INSTANCE_SET__PARTICIPATING_STIMULUS, Common_behaviorPackage.STIMULUS__INTERACTION_INSTANCE_SET);
		}
		return participatingStimulus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CollaborationsPackage.INTERACTION_INSTANCE_SET__CONTEXT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetContext((CollaborationInstanceSet)otherEnd, msgs);
			case CollaborationsPackage.INTERACTION_INSTANCE_SET__INTERACTION:
				if (interaction != null)
					msgs = ((InternalEObject)interaction).eInverseRemove(this, CollaborationsPackage.INTERACTION__INTERACTION_INSTANCE_SET, Interaction.class, msgs);
				return basicSetInteraction((Interaction)otherEnd, msgs);
			case CollaborationsPackage.INTERACTION_INSTANCE_SET__PARTICIPATING_STIMULUS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getParticipatingStimulus()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CollaborationsPackage.INTERACTION_INSTANCE_SET__CONTEXT:
				return basicSetContext(null, msgs);
			case CollaborationsPackage.INTERACTION_INSTANCE_SET__INTERACTION:
				return basicSetInteraction(null, msgs);
			case CollaborationsPackage.INTERACTION_INSTANCE_SET__PARTICIPATING_STIMULUS:
				return ((InternalEList<?>)getParticipatingStimulus()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CollaborationsPackage.INTERACTION_INSTANCE_SET__CONTEXT:
				return eInternalContainer().eInverseRemove(this, CollaborationsPackage.COLLABORATION_INSTANCE_SET__INTERACTION_INSTANCE_SET, CollaborationInstanceSet.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CollaborationsPackage.INTERACTION_INSTANCE_SET__CONTEXT:
				return getContext();
			case CollaborationsPackage.INTERACTION_INSTANCE_SET__INTERACTION:
				if (resolve) return getInteraction();
				return basicGetInteraction();
			case CollaborationsPackage.INTERACTION_INSTANCE_SET__PARTICIPATING_STIMULUS:
				return getParticipatingStimulus();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CollaborationsPackage.INTERACTION_INSTANCE_SET__CONTEXT:
				setContext((CollaborationInstanceSet)newValue);
				return;
			case CollaborationsPackage.INTERACTION_INSTANCE_SET__INTERACTION:
				setInteraction((Interaction)newValue);
				return;
			case CollaborationsPackage.INTERACTION_INSTANCE_SET__PARTICIPATING_STIMULUS:
				getParticipatingStimulus().clear();
				getParticipatingStimulus().addAll((Collection<? extends Stimulus>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CollaborationsPackage.INTERACTION_INSTANCE_SET__CONTEXT:
				setContext((CollaborationInstanceSet)null);
				return;
			case CollaborationsPackage.INTERACTION_INSTANCE_SET__INTERACTION:
				setInteraction((Interaction)null);
				return;
			case CollaborationsPackage.INTERACTION_INSTANCE_SET__PARTICIPATING_STIMULUS:
				getParticipatingStimulus().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CollaborationsPackage.INTERACTION_INSTANCE_SET__CONTEXT:
				return getContext() != null;
			case CollaborationsPackage.INTERACTION_INSTANCE_SET__INTERACTION:
				return interaction != null;
			case CollaborationsPackage.INTERACTION_INSTANCE_SET__PARTICIPATING_STIMULUS:
				return participatingStimulus != null && !participatingStimulus.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //InteractionInstanceSetImpl
