/**
 * <copyright>
 * </copyright>
 *
 * $Id: CollaborationsFactory.java,v 1.1 2012/04/23 09:31:23 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage
 * @generated
 */
public interface CollaborationsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CollaborationsFactory eINSTANCE = net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Collaboration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Collaboration</em>'.
	 * @generated
	 */
	Collaboration createCollaboration();

	/**
	 * Returns a new object of class '<em>Classifier Role</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Classifier Role</em>'.
	 * @generated
	 */
	ClassifierRole createClassifierRole();

	/**
	 * Returns a new object of class '<em>Association Role</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Association Role</em>'.
	 * @generated
	 */
	AssociationRole createAssociationRole();

	/**
	 * Returns a new object of class '<em>Association End Role</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Association End Role</em>'.
	 * @generated
	 */
	AssociationEndRole createAssociationEndRole();

	/**
	 * Returns a new object of class '<em>Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Message</em>'.
	 * @generated
	 */
	Message createMessage();

	/**
	 * Returns a new object of class '<em>Interaction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Interaction</em>'.
	 * @generated
	 */
	Interaction createInteraction();

	/**
	 * Returns a new object of class '<em>Interaction Instance Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Interaction Instance Set</em>'.
	 * @generated
	 */
	InteractionInstanceSet createInteractionInstanceSet();

	/**
	 * Returns a new object of class '<em>Collaboration Instance Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Collaboration Instance Set</em>'.
	 * @generated
	 */
	CollaborationInstanceSet createCollaborationInstanceSet();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	CollaborationsPackage getCollaborationsPackage();

} //CollaborationsFactory
