/**
 * <copyright>
 * </copyright>
 *
 * $Id: InteractionImpl.java,v 1.1 2012/04/23 09:31:39 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.collaborations.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.collaborations.Collaboration;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage;
import net.jgsuess.uml14.behavioral_elements.collaborations.Interaction;
import net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet;
import net.jgsuess.uml14.behavioral_elements.collaborations.Message;

import net.jgsuess.uml14.foundation.core.impl.ModelElementImpl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interaction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.InteractionImpl#getMessage <em>Message</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.InteractionImpl#getContext <em>Context</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.collaborations.impl.InteractionImpl#getInteractionInstanceSet <em>Interaction Instance Set</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class InteractionImpl extends ModelElementImpl implements Interaction {
	/**
	 * The cached value of the '{@link #getMessage() <em>Message</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage()
	 * @generated
	 * @ordered
	 */
	protected EList<Message> message;

	/**
	 * The cached value of the '{@link #getInteractionInstanceSet() <em>Interaction Instance Set</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInteractionInstanceSet()
	 * @generated
	 * @ordered
	 */
	protected EList<InteractionInstanceSet> interactionInstanceSet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InteractionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CollaborationsPackage.Literals.INTERACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Message> getMessage() {
		if (message == null) {
			message = new EObjectContainmentWithInverseEList<Message>(Message.class, this, CollaborationsPackage.INTERACTION__MESSAGE, CollaborationsPackage.MESSAGE__INTERACTION);
		}
		return message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Collaboration getContext() {
		if (eContainerFeatureID() != CollaborationsPackage.INTERACTION__CONTEXT) return null;
		return (Collaboration)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContext(Collaboration newContext, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newContext, CollaborationsPackage.INTERACTION__CONTEXT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContext(Collaboration newContext) {
		if (newContext != eInternalContainer() || (eContainerFeatureID() != CollaborationsPackage.INTERACTION__CONTEXT && newContext != null)) {
			if (EcoreUtil.isAncestor(this, newContext))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newContext != null)
				msgs = ((InternalEObject)newContext).eInverseAdd(this, CollaborationsPackage.COLLABORATION__INTERACTION, Collaboration.class, msgs);
			msgs = basicSetContext(newContext, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CollaborationsPackage.INTERACTION__CONTEXT, newContext, newContext));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InteractionInstanceSet> getInteractionInstanceSet() {
		if (interactionInstanceSet == null) {
			interactionInstanceSet = new EObjectWithInverseResolvingEList<InteractionInstanceSet>(InteractionInstanceSet.class, this, CollaborationsPackage.INTERACTION__INTERACTION_INSTANCE_SET, CollaborationsPackage.INTERACTION_INSTANCE_SET__INTERACTION);
		}
		return interactionInstanceSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CollaborationsPackage.INTERACTION__MESSAGE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getMessage()).basicAdd(otherEnd, msgs);
			case CollaborationsPackage.INTERACTION__CONTEXT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetContext((Collaboration)otherEnd, msgs);
			case CollaborationsPackage.INTERACTION__INTERACTION_INSTANCE_SET:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getInteractionInstanceSet()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CollaborationsPackage.INTERACTION__MESSAGE:
				return ((InternalEList<?>)getMessage()).basicRemove(otherEnd, msgs);
			case CollaborationsPackage.INTERACTION__CONTEXT:
				return basicSetContext(null, msgs);
			case CollaborationsPackage.INTERACTION__INTERACTION_INSTANCE_SET:
				return ((InternalEList<?>)getInteractionInstanceSet()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CollaborationsPackage.INTERACTION__CONTEXT:
				return eInternalContainer().eInverseRemove(this, CollaborationsPackage.COLLABORATION__INTERACTION, Collaboration.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CollaborationsPackage.INTERACTION__MESSAGE:
				return getMessage();
			case CollaborationsPackage.INTERACTION__CONTEXT:
				return getContext();
			case CollaborationsPackage.INTERACTION__INTERACTION_INSTANCE_SET:
				return getInteractionInstanceSet();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CollaborationsPackage.INTERACTION__MESSAGE:
				getMessage().clear();
				getMessage().addAll((Collection<? extends Message>)newValue);
				return;
			case CollaborationsPackage.INTERACTION__CONTEXT:
				setContext((Collaboration)newValue);
				return;
			case CollaborationsPackage.INTERACTION__INTERACTION_INSTANCE_SET:
				getInteractionInstanceSet().clear();
				getInteractionInstanceSet().addAll((Collection<? extends InteractionInstanceSet>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CollaborationsPackage.INTERACTION__MESSAGE:
				getMessage().clear();
				return;
			case CollaborationsPackage.INTERACTION__CONTEXT:
				setContext((Collaboration)null);
				return;
			case CollaborationsPackage.INTERACTION__INTERACTION_INSTANCE_SET:
				getInteractionInstanceSet().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CollaborationsPackage.INTERACTION__MESSAGE:
				return message != null && !message.isEmpty();
			case CollaborationsPackage.INTERACTION__CONTEXT:
				return getContext() != null;
			case CollaborationsPackage.INTERACTION__INTERACTION_INSTANCE_SET:
				return interactionInstanceSet != null && !interactionInstanceSet.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //InteractionImpl
