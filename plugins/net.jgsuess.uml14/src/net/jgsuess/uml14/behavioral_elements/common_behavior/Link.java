/**
 * <copyright>
 * </copyright>
 *
 * $Id: Link.java,v 1.1 2012/04/23 09:31:36 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior;

import net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole;

import net.jgsuess.uml14.foundation.core.Association;
import net.jgsuess.uml14.foundation.core.ModelElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Link</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Link#getAssociation <em>Association</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Link#getConnection <em>Connection</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Link#getStimulus <em>Stimulus</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Link#getOwner <em>Owner</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Link#getPlayedRole <em>Played Role</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getLink()
 * @model
 * @generated
 */
public interface Link extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Association</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Association#getLink <em>Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Association</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Association</em>' reference.
	 * @see #setAssociation(Association)
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getLink_Association()
	 * @see net.jgsuess.uml14.foundation.core.Association#getLink
	 * @model opposite="link" required="true"
	 * @generated
	 */
	Association getAssociation();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Link#getAssociation <em>Association</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Association</em>' reference.
	 * @see #getAssociation()
	 * @generated
	 */
	void setAssociation(Association value);

	/**
	 * Returns the value of the '<em><b>Connection</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.LinkEnd}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.LinkEnd#getLink <em>Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connection</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connection</em>' containment reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getLink_Connection()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.LinkEnd#getLink
	 * @model opposite="link" containment="true" lower="2"
	 * @generated
	 */
	EList<LinkEnd> getConnection();

	/**
	 * Returns the value of the '<em><b>Stimulus</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus#getCommunicationLink <em>Communication Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stimulus</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stimulus</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getLink_Stimulus()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus#getCommunicationLink
	 * @model opposite="communicationLink"
	 * @generated
	 */
	EList<Stimulus> getStimulus();

	/**
	 * Returns the value of the '<em><b>Owner</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Instance#getOwnedLink <em>Owned Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owner</em>' container reference.
	 * @see #setOwner(Instance)
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getLink_Owner()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Instance#getOwnedLink
	 * @model opposite="ownedLink"
	 * @generated
	 */
	Instance getOwner();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Link#getOwner <em>Owner</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owner</em>' container reference.
	 * @see #getOwner()
	 * @generated
	 */
	void setOwner(Instance value);

	/**
	 * Returns the value of the '<em><b>Played Role</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole#getConformingLink <em>Conforming Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Played Role</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Played Role</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getLink_PlayedRole()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole#getConformingLink
	 * @model opposite="conformingLink"
	 * @generated
	 */
	EList<AssociationRole> getPlayedRole();

} // Link
