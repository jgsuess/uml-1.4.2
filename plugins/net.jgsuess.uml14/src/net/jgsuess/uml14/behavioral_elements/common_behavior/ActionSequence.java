/**
 * <copyright>
 * </copyright>
 *
 * $Id: ActionSequence.java,v 1.1 2012/04/23 09:31:36 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action Sequence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.ActionSequence#getAction <em>Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getActionSequence()
 * @model
 * @generated
 */
public interface ActionSequence extends Action {
	/**
	 * Returns the value of the '<em><b>Action</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getActionSequence <em>Action Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' containment reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getActionSequence_Action()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getActionSequence
	 * @model opposite="actionSequence" containment="true"
	 * @generated
	 */
	EList<Action> getAction();

} // ActionSequence
