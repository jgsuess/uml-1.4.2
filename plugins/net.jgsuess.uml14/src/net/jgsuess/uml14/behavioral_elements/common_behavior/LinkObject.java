/**
 * <copyright>
 * </copyright>
 *
 * $Id: LinkObject.java,v 1.1 2012/04/23 09:31:36 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Link Object</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getLinkObject()
 * @model
 * @generated
 */
public interface LinkObject extends net.jgsuess.uml14.behavioral_elements.common_behavior.Object {
} // LinkObject
