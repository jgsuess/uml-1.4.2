/**
 * <copyright>
 * </copyright>
 *
 * $Id: ComponentInstanceImpl.java,v 1.1 2012/04/23 09:31:14 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;
import net.jgsuess.uml14.behavioral_elements.common_behavior.ComponentInstance;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Instance;
import net.jgsuess.uml14.behavioral_elements.common_behavior.NodeInstance;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.ComponentInstanceImpl#getNodeInstance <em>Node Instance</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.ComponentInstanceImpl#getResident <em>Resident</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ComponentInstanceImpl extends InstanceImpl implements ComponentInstance {
	/**
	 * The cached value of the '{@link #getNodeInstance() <em>Node Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodeInstance()
	 * @generated
	 * @ordered
	 */
	protected NodeInstance nodeInstance;

	/**
	 * The cached value of the '{@link #getResident() <em>Resident</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResident()
	 * @generated
	 * @ordered
	 */
	protected EList<Instance> resident;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Common_behaviorPackage.Literals.COMPONENT_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeInstance getNodeInstance() {
		if (nodeInstance != null && nodeInstance.eIsProxy()) {
			InternalEObject oldNodeInstance = (InternalEObject)nodeInstance;
			nodeInstance = (NodeInstance)eResolveProxy(oldNodeInstance);
			if (nodeInstance != oldNodeInstance) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Common_behaviorPackage.COMPONENT_INSTANCE__NODE_INSTANCE, oldNodeInstance, nodeInstance));
			}
		}
		return nodeInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeInstance basicGetNodeInstance() {
		return nodeInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNodeInstance(NodeInstance newNodeInstance, NotificationChain msgs) {
		NodeInstance oldNodeInstance = nodeInstance;
		nodeInstance = newNodeInstance;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.COMPONENT_INSTANCE__NODE_INSTANCE, oldNodeInstance, newNodeInstance);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNodeInstance(NodeInstance newNodeInstance) {
		if (newNodeInstance != nodeInstance) {
			NotificationChain msgs = null;
			if (nodeInstance != null)
				msgs = ((InternalEObject)nodeInstance).eInverseRemove(this, Common_behaviorPackage.NODE_INSTANCE__RESIDENT, NodeInstance.class, msgs);
			if (newNodeInstance != null)
				msgs = ((InternalEObject)newNodeInstance).eInverseAdd(this, Common_behaviorPackage.NODE_INSTANCE__RESIDENT, NodeInstance.class, msgs);
			msgs = basicSetNodeInstance(newNodeInstance, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.COMPONENT_INSTANCE__NODE_INSTANCE, newNodeInstance, newNodeInstance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instance> getResident() {
		if (resident == null) {
			resident = new EObjectWithInverseResolvingEList<Instance>(Instance.class, this, Common_behaviorPackage.COMPONENT_INSTANCE__RESIDENT, Common_behaviorPackage.INSTANCE__COMPONENT_INSTANCE);
		}
		return resident;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Common_behaviorPackage.COMPONENT_INSTANCE__NODE_INSTANCE:
				if (nodeInstance != null)
					msgs = ((InternalEObject)nodeInstance).eInverseRemove(this, Common_behaviorPackage.NODE_INSTANCE__RESIDENT, NodeInstance.class, msgs);
				return basicSetNodeInstance((NodeInstance)otherEnd, msgs);
			case Common_behaviorPackage.COMPONENT_INSTANCE__RESIDENT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getResident()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Common_behaviorPackage.COMPONENT_INSTANCE__NODE_INSTANCE:
				return basicSetNodeInstance(null, msgs);
			case Common_behaviorPackage.COMPONENT_INSTANCE__RESIDENT:
				return ((InternalEList<?>)getResident()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Common_behaviorPackage.COMPONENT_INSTANCE__NODE_INSTANCE:
				if (resolve) return getNodeInstance();
				return basicGetNodeInstance();
			case Common_behaviorPackage.COMPONENT_INSTANCE__RESIDENT:
				return getResident();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Common_behaviorPackage.COMPONENT_INSTANCE__NODE_INSTANCE:
				setNodeInstance((NodeInstance)newValue);
				return;
			case Common_behaviorPackage.COMPONENT_INSTANCE__RESIDENT:
				getResident().clear();
				getResident().addAll((Collection<? extends Instance>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Common_behaviorPackage.COMPONENT_INSTANCE__NODE_INSTANCE:
				setNodeInstance((NodeInstance)null);
				return;
			case Common_behaviorPackage.COMPONENT_INSTANCE__RESIDENT:
				getResident().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Common_behaviorPackage.COMPONENT_INSTANCE__NODE_INSTANCE:
				return nodeInstance != null;
			case Common_behaviorPackage.COMPONENT_INSTANCE__RESIDENT:
				return resident != null && !resident.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ComponentInstanceImpl
