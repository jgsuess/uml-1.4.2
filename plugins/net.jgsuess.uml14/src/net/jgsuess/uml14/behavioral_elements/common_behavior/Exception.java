/**
 * <copyright>
 * </copyright>
 *
 * $Id: Exception.java,v 1.1 2012/04/23 09:31:36 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exception</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getException()
 * @model
 * @generated
 */
public interface Exception extends Signal {
} // Exception
