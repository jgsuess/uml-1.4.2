/**
 * <copyright>
 * </copyright>
 *
 * $Id: SubsystemInstance.java,v 1.1 2012/04/23 09:31:37 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Subsystem Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getSubsystemInstance()
 * @model
 * @generated
 */
public interface SubsystemInstance extends Instance {
} // SubsystemInstance
