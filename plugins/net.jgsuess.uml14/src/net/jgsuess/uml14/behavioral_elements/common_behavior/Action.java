/**
 * <copyright>
 * </copyright>
 *
 * $Id: Action.java,v 1.1 2012/04/23 09:31:36 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior;

import net.jgsuess.uml14.behavioral_elements.collaborations.Message;

import net.jgsuess.uml14.behavioral_elements.state_machines.State;
import net.jgsuess.uml14.behavioral_elements.state_machines.Transition;

import net.jgsuess.uml14.foundation.core.ModelElement;

import net.jgsuess.uml14.foundation.data_types.ActionExpression;
import net.jgsuess.uml14.foundation.data_types.IterationExpression;
import net.jgsuess.uml14.foundation.data_types.ObjectSetExpression;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getRecurrence <em>Recurrence</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getTarget <em>Target</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#isIsAsynchronous <em>Is Asynchronous</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getScript <em>Script</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getActualArgument <em>Actual Argument</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getActionSequence <em>Action Sequence</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getStimulus <em>Stimulus</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getState <em>State</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getTransition <em>Transition</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getMessage <em>Message</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getAction()
 * @model abstract="true"
 * @generated
 */
public interface Action extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Recurrence</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Recurrence</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Recurrence</em>' containment reference.
	 * @see #setRecurrence(IterationExpression)
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getAction_Recurrence()
	 * @model containment="true"
	 * @generated
	 */
	IterationExpression getRecurrence();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getRecurrence <em>Recurrence</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Recurrence</em>' containment reference.
	 * @see #getRecurrence()
	 * @generated
	 */
	void setRecurrence(IterationExpression value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' containment reference.
	 * @see #setTarget(ObjectSetExpression)
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getAction_Target()
	 * @model containment="true"
	 * @generated
	 */
	ObjectSetExpression getTarget();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getTarget <em>Target</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' containment reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(ObjectSetExpression value);

	/**
	 * Returns the value of the '<em><b>Is Asynchronous</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Asynchronous</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Asynchronous</em>' attribute.
	 * @see #setIsAsynchronous(boolean)
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getAction_IsAsynchronous()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Boolean"
	 * @generated
	 */
	boolean isIsAsynchronous();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#isIsAsynchronous <em>Is Asynchronous</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Asynchronous</em>' attribute.
	 * @see #isIsAsynchronous()
	 * @generated
	 */
	void setIsAsynchronous(boolean value);

	/**
	 * Returns the value of the '<em><b>Script</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Script</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Script</em>' containment reference.
	 * @see #setScript(ActionExpression)
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getAction_Script()
	 * @model containment="true"
	 * @generated
	 */
	ActionExpression getScript();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getScript <em>Script</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Script</em>' containment reference.
	 * @see #getScript()
	 * @generated
	 */
	void setScript(ActionExpression value);

	/**
	 * Returns the value of the '<em><b>Actual Argument</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.Argument}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Argument#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actual Argument</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actual Argument</em>' containment reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getAction_ActualArgument()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Argument#getAction
	 * @model opposite="action" containment="true"
	 * @generated
	 */
	EList<Argument> getActualArgument();

	/**
	 * Returns the value of the '<em><b>Action Sequence</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.ActionSequence#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action Sequence</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action Sequence</em>' container reference.
	 * @see #setActionSequence(ActionSequence)
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getAction_ActionSequence()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.ActionSequence#getAction
	 * @model opposite="action"
	 * @generated
	 */
	ActionSequence getActionSequence();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getActionSequence <em>Action Sequence</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action Sequence</em>' container reference.
	 * @see #getActionSequence()
	 * @generated
	 */
	void setActionSequence(ActionSequence value);

	/**
	 * Returns the value of the '<em><b>Stimulus</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus#getDispatchAction <em>Dispatch Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stimulus</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stimulus</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getAction_Stimulus()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus#getDispatchAction
	 * @model opposite="dispatchAction"
	 * @generated
	 */
	EList<Stimulus> getStimulus();

	/**
	 * Returns the value of the '<em><b>State</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.state_machines.State#getEntry <em>Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' container reference.
	 * @see #setState(State)
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getAction_State()
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State#getEntry
	 * @model opposite="entry"
	 * @generated
	 */
	State getState();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getState <em>State</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State</em>' container reference.
	 * @see #getState()
	 * @generated
	 */
	void setState(State value);

	/**
	 * Returns the value of the '<em><b>Transition</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getEffect <em>Effect</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transition</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition</em>' container reference.
	 * @see #setTransition(Transition)
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getAction_Transition()
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getEffect
	 * @model opposite="effect"
	 * @generated
	 */
	Transition getTransition();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getTransition <em>Transition</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transition</em>' container reference.
	 * @see #getTransition()
	 * @generated
	 */
	void setTransition(Transition value);

	/**
	 * Returns the value of the '<em><b>Message</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.collaborations.Message}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getAction_Message()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Message#getAction
	 * @model opposite="action"
	 * @generated
	 */
	EList<Message> getMessage();

} // Action
