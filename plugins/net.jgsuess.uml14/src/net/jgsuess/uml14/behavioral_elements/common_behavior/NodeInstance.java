/**
 * <copyright>
 * </copyright>
 *
 * $Id: NodeInstance.java,v 1.1 2012/04/23 09:31:37 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.NodeInstance#getResident <em>Resident</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getNodeInstance()
 * @model
 * @generated
 */
public interface NodeInstance extends Instance {
	/**
	 * Returns the value of the '<em><b>Resident</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.ComponentInstance}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.ComponentInstance#getNodeInstance <em>Node Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resident</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resident</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getNodeInstance_Resident()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.ComponentInstance#getNodeInstance
	 * @model opposite="nodeInstance"
	 * @generated
	 */
	EList<ComponentInstance> getResident();

} // NodeInstance
