/**
 * <copyright>
 * </copyright>
 *
 * $Id: CreateAction.java,v 1.1 2012/04/23 09:31:35 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior;

import net.jgsuess.uml14.foundation.core.Classifier;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Create Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.CreateAction#getInstantiation <em>Instantiation</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getCreateAction()
 * @model
 * @generated
 */
public interface CreateAction extends Action {
	/**
	 * Returns the value of the '<em><b>Instantiation</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Classifier#getCreateAction <em>Create Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instantiation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instantiation</em>' reference.
	 * @see #setInstantiation(Classifier)
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getCreateAction_Instantiation()
	 * @see net.jgsuess.uml14.foundation.core.Classifier#getCreateAction
	 * @model opposite="createAction" required="true"
	 * @generated
	 */
	Classifier getInstantiation();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.CreateAction#getInstantiation <em>Instantiation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Instantiation</em>' reference.
	 * @see #getInstantiation()
	 * @generated
	 */
	void setInstantiation(Classifier value);

} // CreateAction
