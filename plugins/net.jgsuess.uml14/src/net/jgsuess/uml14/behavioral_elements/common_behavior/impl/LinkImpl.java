/**
 * <copyright>
 * </copyright>
 *
 * $Id: LinkImpl.java,v 1.1 2012/04/23 09:31:14 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Instance;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Link;
import net.jgsuess.uml14.behavioral_elements.common_behavior.LinkEnd;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus;

import net.jgsuess.uml14.foundation.core.Association;
import net.jgsuess.uml14.foundation.core.CorePackage;

import net.jgsuess.uml14.foundation.core.impl.ModelElementImpl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Link</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.LinkImpl#getAssociation <em>Association</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.LinkImpl#getConnection <em>Connection</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.LinkImpl#getStimulus <em>Stimulus</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.LinkImpl#getOwner <em>Owner</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.LinkImpl#getPlayedRole <em>Played Role</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LinkImpl extends ModelElementImpl implements Link {
	/**
	 * The cached value of the '{@link #getAssociation() <em>Association</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociation()
	 * @generated
	 * @ordered
	 */
	protected Association association;

	/**
	 * The cached value of the '{@link #getConnection() <em>Connection</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnection()
	 * @generated
	 * @ordered
	 */
	protected EList<LinkEnd> connection;

	/**
	 * The cached value of the '{@link #getStimulus() <em>Stimulus</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStimulus()
	 * @generated
	 * @ordered
	 */
	protected EList<Stimulus> stimulus;

	/**
	 * The cached value of the '{@link #getPlayedRole() <em>Played Role</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPlayedRole()
	 * @generated
	 * @ordered
	 */
	protected EList<AssociationRole> playedRole;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Common_behaviorPackage.Literals.LINK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Association getAssociation() {
		if (association != null && association.eIsProxy()) {
			InternalEObject oldAssociation = (InternalEObject)association;
			association = (Association)eResolveProxy(oldAssociation);
			if (association != oldAssociation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Common_behaviorPackage.LINK__ASSOCIATION, oldAssociation, association));
			}
		}
		return association;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Association basicGetAssociation() {
		return association;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssociation(Association newAssociation, NotificationChain msgs) {
		Association oldAssociation = association;
		association = newAssociation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.LINK__ASSOCIATION, oldAssociation, newAssociation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssociation(Association newAssociation) {
		if (newAssociation != association) {
			NotificationChain msgs = null;
			if (association != null)
				msgs = ((InternalEObject)association).eInverseRemove(this, CorePackage.ASSOCIATION__LINK, Association.class, msgs);
			if (newAssociation != null)
				msgs = ((InternalEObject)newAssociation).eInverseAdd(this, CorePackage.ASSOCIATION__LINK, Association.class, msgs);
			msgs = basicSetAssociation(newAssociation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.LINK__ASSOCIATION, newAssociation, newAssociation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LinkEnd> getConnection() {
		if (connection == null) {
			connection = new EObjectContainmentWithInverseEList<LinkEnd>(LinkEnd.class, this, Common_behaviorPackage.LINK__CONNECTION, Common_behaviorPackage.LINK_END__LINK);
		}
		return connection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Stimulus> getStimulus() {
		if (stimulus == null) {
			stimulus = new EObjectWithInverseResolvingEList<Stimulus>(Stimulus.class, this, Common_behaviorPackage.LINK__STIMULUS, Common_behaviorPackage.STIMULUS__COMMUNICATION_LINK);
		}
		return stimulus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instance getOwner() {
		if (eContainerFeatureID() != Common_behaviorPackage.LINK__OWNER) return null;
		return (Instance)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOwner(Instance newOwner, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newOwner, Common_behaviorPackage.LINK__OWNER, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOwner(Instance newOwner) {
		if (newOwner != eInternalContainer() || (eContainerFeatureID() != Common_behaviorPackage.LINK__OWNER && newOwner != null)) {
			if (EcoreUtil.isAncestor(this, newOwner))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newOwner != null)
				msgs = ((InternalEObject)newOwner).eInverseAdd(this, Common_behaviorPackage.INSTANCE__OWNED_LINK, Instance.class, msgs);
			msgs = basicSetOwner(newOwner, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.LINK__OWNER, newOwner, newOwner));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssociationRole> getPlayedRole() {
		if (playedRole == null) {
			playedRole = new EObjectWithInverseResolvingEList.ManyInverse<AssociationRole>(AssociationRole.class, this, Common_behaviorPackage.LINK__PLAYED_ROLE, CollaborationsPackage.ASSOCIATION_ROLE__CONFORMING_LINK);
		}
		return playedRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Common_behaviorPackage.LINK__ASSOCIATION:
				if (association != null)
					msgs = ((InternalEObject)association).eInverseRemove(this, CorePackage.ASSOCIATION__LINK, Association.class, msgs);
				return basicSetAssociation((Association)otherEnd, msgs);
			case Common_behaviorPackage.LINK__CONNECTION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getConnection()).basicAdd(otherEnd, msgs);
			case Common_behaviorPackage.LINK__STIMULUS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getStimulus()).basicAdd(otherEnd, msgs);
			case Common_behaviorPackage.LINK__OWNER:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetOwner((Instance)otherEnd, msgs);
			case Common_behaviorPackage.LINK__PLAYED_ROLE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPlayedRole()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Common_behaviorPackage.LINK__ASSOCIATION:
				return basicSetAssociation(null, msgs);
			case Common_behaviorPackage.LINK__CONNECTION:
				return ((InternalEList<?>)getConnection()).basicRemove(otherEnd, msgs);
			case Common_behaviorPackage.LINK__STIMULUS:
				return ((InternalEList<?>)getStimulus()).basicRemove(otherEnd, msgs);
			case Common_behaviorPackage.LINK__OWNER:
				return basicSetOwner(null, msgs);
			case Common_behaviorPackage.LINK__PLAYED_ROLE:
				return ((InternalEList<?>)getPlayedRole()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case Common_behaviorPackage.LINK__OWNER:
				return eInternalContainer().eInverseRemove(this, Common_behaviorPackage.INSTANCE__OWNED_LINK, Instance.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Common_behaviorPackage.LINK__ASSOCIATION:
				if (resolve) return getAssociation();
				return basicGetAssociation();
			case Common_behaviorPackage.LINK__CONNECTION:
				return getConnection();
			case Common_behaviorPackage.LINK__STIMULUS:
				return getStimulus();
			case Common_behaviorPackage.LINK__OWNER:
				return getOwner();
			case Common_behaviorPackage.LINK__PLAYED_ROLE:
				return getPlayedRole();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Common_behaviorPackage.LINK__ASSOCIATION:
				setAssociation((Association)newValue);
				return;
			case Common_behaviorPackage.LINK__CONNECTION:
				getConnection().clear();
				getConnection().addAll((Collection<? extends LinkEnd>)newValue);
				return;
			case Common_behaviorPackage.LINK__STIMULUS:
				getStimulus().clear();
				getStimulus().addAll((Collection<? extends Stimulus>)newValue);
				return;
			case Common_behaviorPackage.LINK__OWNER:
				setOwner((Instance)newValue);
				return;
			case Common_behaviorPackage.LINK__PLAYED_ROLE:
				getPlayedRole().clear();
				getPlayedRole().addAll((Collection<? extends AssociationRole>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Common_behaviorPackage.LINK__ASSOCIATION:
				setAssociation((Association)null);
				return;
			case Common_behaviorPackage.LINK__CONNECTION:
				getConnection().clear();
				return;
			case Common_behaviorPackage.LINK__STIMULUS:
				getStimulus().clear();
				return;
			case Common_behaviorPackage.LINK__OWNER:
				setOwner((Instance)null);
				return;
			case Common_behaviorPackage.LINK__PLAYED_ROLE:
				getPlayedRole().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Common_behaviorPackage.LINK__ASSOCIATION:
				return association != null;
			case Common_behaviorPackage.LINK__CONNECTION:
				return connection != null && !connection.isEmpty();
			case Common_behaviorPackage.LINK__STIMULUS:
				return stimulus != null && !stimulus.isEmpty();
			case Common_behaviorPackage.LINK__OWNER:
				return getOwner() != null;
			case Common_behaviorPackage.LINK__PLAYED_ROLE:
				return playedRole != null && !playedRole.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //LinkImpl
