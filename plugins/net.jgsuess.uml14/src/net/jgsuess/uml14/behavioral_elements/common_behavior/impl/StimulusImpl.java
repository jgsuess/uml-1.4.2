/**
 * <copyright>
 * </copyright>
 *
 * $Id: StimulusImpl.java,v 1.1 2012/04/23 09:31:17 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage;
import net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet;
import net.jgsuess.uml14.behavioral_elements.collaborations.Message;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Action;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Instance;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Link;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus;

import net.jgsuess.uml14.foundation.core.impl.ModelElementImpl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Stimulus</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.StimulusImpl#getArgument <em>Argument</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.StimulusImpl#getCommunicationLink <em>Communication Link</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.StimulusImpl#getDispatchAction <em>Dispatch Action</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.StimulusImpl#getPlayedRole <em>Played Role</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.StimulusImpl#getInteractionInstanceSet <em>Interaction Instance Set</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class StimulusImpl extends ModelElementImpl implements Stimulus {
	/**
	 * The cached value of the '{@link #getArgument() <em>Argument</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArgument()
	 * @generated
	 * @ordered
	 */
	protected EList<Instance> argument;

	/**
	 * The cached value of the '{@link #getCommunicationLink() <em>Communication Link</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommunicationLink()
	 * @generated
	 * @ordered
	 */
	protected Link communicationLink;

	/**
	 * The cached value of the '{@link #getDispatchAction() <em>Dispatch Action</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDispatchAction()
	 * @generated
	 * @ordered
	 */
	protected Action dispatchAction;

	/**
	 * The cached value of the '{@link #getPlayedRole() <em>Played Role</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPlayedRole()
	 * @generated
	 * @ordered
	 */
	protected EList<Message> playedRole;

	/**
	 * The cached value of the '{@link #getInteractionInstanceSet() <em>Interaction Instance Set</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInteractionInstanceSet()
	 * @generated
	 * @ordered
	 */
	protected EList<InteractionInstanceSet> interactionInstanceSet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StimulusImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Common_behaviorPackage.Literals.STIMULUS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instance> getArgument() {
		if (argument == null) {
			argument = new EObjectWithInverseResolvingEList.ManyInverse<Instance>(Instance.class, this, Common_behaviorPackage.STIMULUS__ARGUMENT, Common_behaviorPackage.INSTANCE__STIMULUS);
		}
		return argument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Link getCommunicationLink() {
		if (communicationLink != null && communicationLink.eIsProxy()) {
			InternalEObject oldCommunicationLink = (InternalEObject)communicationLink;
			communicationLink = (Link)eResolveProxy(oldCommunicationLink);
			if (communicationLink != oldCommunicationLink) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Common_behaviorPackage.STIMULUS__COMMUNICATION_LINK, oldCommunicationLink, communicationLink));
			}
		}
		return communicationLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Link basicGetCommunicationLink() {
		return communicationLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCommunicationLink(Link newCommunicationLink, NotificationChain msgs) {
		Link oldCommunicationLink = communicationLink;
		communicationLink = newCommunicationLink;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.STIMULUS__COMMUNICATION_LINK, oldCommunicationLink, newCommunicationLink);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCommunicationLink(Link newCommunicationLink) {
		if (newCommunicationLink != communicationLink) {
			NotificationChain msgs = null;
			if (communicationLink != null)
				msgs = ((InternalEObject)communicationLink).eInverseRemove(this, Common_behaviorPackage.LINK__STIMULUS, Link.class, msgs);
			if (newCommunicationLink != null)
				msgs = ((InternalEObject)newCommunicationLink).eInverseAdd(this, Common_behaviorPackage.LINK__STIMULUS, Link.class, msgs);
			msgs = basicSetCommunicationLink(newCommunicationLink, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.STIMULUS__COMMUNICATION_LINK, newCommunicationLink, newCommunicationLink));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action getDispatchAction() {
		if (dispatchAction != null && dispatchAction.eIsProxy()) {
			InternalEObject oldDispatchAction = (InternalEObject)dispatchAction;
			dispatchAction = (Action)eResolveProxy(oldDispatchAction);
			if (dispatchAction != oldDispatchAction) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Common_behaviorPackage.STIMULUS__DISPATCH_ACTION, oldDispatchAction, dispatchAction));
			}
		}
		return dispatchAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action basicGetDispatchAction() {
		return dispatchAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDispatchAction(Action newDispatchAction, NotificationChain msgs) {
		Action oldDispatchAction = dispatchAction;
		dispatchAction = newDispatchAction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.STIMULUS__DISPATCH_ACTION, oldDispatchAction, newDispatchAction);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDispatchAction(Action newDispatchAction) {
		if (newDispatchAction != dispatchAction) {
			NotificationChain msgs = null;
			if (dispatchAction != null)
				msgs = ((InternalEObject)dispatchAction).eInverseRemove(this, Common_behaviorPackage.ACTION__STIMULUS, Action.class, msgs);
			if (newDispatchAction != null)
				msgs = ((InternalEObject)newDispatchAction).eInverseAdd(this, Common_behaviorPackage.ACTION__STIMULUS, Action.class, msgs);
			msgs = basicSetDispatchAction(newDispatchAction, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.STIMULUS__DISPATCH_ACTION, newDispatchAction, newDispatchAction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Message> getPlayedRole() {
		if (playedRole == null) {
			playedRole = new EObjectWithInverseResolvingEList.ManyInverse<Message>(Message.class, this, Common_behaviorPackage.STIMULUS__PLAYED_ROLE, CollaborationsPackage.MESSAGE__CONFORMING_STIMULUS);
		}
		return playedRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InteractionInstanceSet> getInteractionInstanceSet() {
		if (interactionInstanceSet == null) {
			interactionInstanceSet = new EObjectWithInverseResolvingEList.ManyInverse<InteractionInstanceSet>(InteractionInstanceSet.class, this, Common_behaviorPackage.STIMULUS__INTERACTION_INSTANCE_SET, CollaborationsPackage.INTERACTION_INSTANCE_SET__PARTICIPATING_STIMULUS);
		}
		return interactionInstanceSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Common_behaviorPackage.STIMULUS__ARGUMENT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getArgument()).basicAdd(otherEnd, msgs);
			case Common_behaviorPackage.STIMULUS__COMMUNICATION_LINK:
				if (communicationLink != null)
					msgs = ((InternalEObject)communicationLink).eInverseRemove(this, Common_behaviorPackage.LINK__STIMULUS, Link.class, msgs);
				return basicSetCommunicationLink((Link)otherEnd, msgs);
			case Common_behaviorPackage.STIMULUS__DISPATCH_ACTION:
				if (dispatchAction != null)
					msgs = ((InternalEObject)dispatchAction).eInverseRemove(this, Common_behaviorPackage.ACTION__STIMULUS, Action.class, msgs);
				return basicSetDispatchAction((Action)otherEnd, msgs);
			case Common_behaviorPackage.STIMULUS__PLAYED_ROLE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPlayedRole()).basicAdd(otherEnd, msgs);
			case Common_behaviorPackage.STIMULUS__INTERACTION_INSTANCE_SET:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getInteractionInstanceSet()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Common_behaviorPackage.STIMULUS__ARGUMENT:
				return ((InternalEList<?>)getArgument()).basicRemove(otherEnd, msgs);
			case Common_behaviorPackage.STIMULUS__COMMUNICATION_LINK:
				return basicSetCommunicationLink(null, msgs);
			case Common_behaviorPackage.STIMULUS__DISPATCH_ACTION:
				return basicSetDispatchAction(null, msgs);
			case Common_behaviorPackage.STIMULUS__PLAYED_ROLE:
				return ((InternalEList<?>)getPlayedRole()).basicRemove(otherEnd, msgs);
			case Common_behaviorPackage.STIMULUS__INTERACTION_INSTANCE_SET:
				return ((InternalEList<?>)getInteractionInstanceSet()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Common_behaviorPackage.STIMULUS__ARGUMENT:
				return getArgument();
			case Common_behaviorPackage.STIMULUS__COMMUNICATION_LINK:
				if (resolve) return getCommunicationLink();
				return basicGetCommunicationLink();
			case Common_behaviorPackage.STIMULUS__DISPATCH_ACTION:
				if (resolve) return getDispatchAction();
				return basicGetDispatchAction();
			case Common_behaviorPackage.STIMULUS__PLAYED_ROLE:
				return getPlayedRole();
			case Common_behaviorPackage.STIMULUS__INTERACTION_INSTANCE_SET:
				return getInteractionInstanceSet();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Common_behaviorPackage.STIMULUS__ARGUMENT:
				getArgument().clear();
				getArgument().addAll((Collection<? extends Instance>)newValue);
				return;
			case Common_behaviorPackage.STIMULUS__COMMUNICATION_LINK:
				setCommunicationLink((Link)newValue);
				return;
			case Common_behaviorPackage.STIMULUS__DISPATCH_ACTION:
				setDispatchAction((Action)newValue);
				return;
			case Common_behaviorPackage.STIMULUS__PLAYED_ROLE:
				getPlayedRole().clear();
				getPlayedRole().addAll((Collection<? extends Message>)newValue);
				return;
			case Common_behaviorPackage.STIMULUS__INTERACTION_INSTANCE_SET:
				getInteractionInstanceSet().clear();
				getInteractionInstanceSet().addAll((Collection<? extends InteractionInstanceSet>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Common_behaviorPackage.STIMULUS__ARGUMENT:
				getArgument().clear();
				return;
			case Common_behaviorPackage.STIMULUS__COMMUNICATION_LINK:
				setCommunicationLink((Link)null);
				return;
			case Common_behaviorPackage.STIMULUS__DISPATCH_ACTION:
				setDispatchAction((Action)null);
				return;
			case Common_behaviorPackage.STIMULUS__PLAYED_ROLE:
				getPlayedRole().clear();
				return;
			case Common_behaviorPackage.STIMULUS__INTERACTION_INSTANCE_SET:
				getInteractionInstanceSet().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Common_behaviorPackage.STIMULUS__ARGUMENT:
				return argument != null && !argument.isEmpty();
			case Common_behaviorPackage.STIMULUS__COMMUNICATION_LINK:
				return communicationLink != null;
			case Common_behaviorPackage.STIMULUS__DISPATCH_ACTION:
				return dispatchAction != null;
			case Common_behaviorPackage.STIMULUS__PLAYED_ROLE:
				return playedRole != null && !playedRole.isEmpty();
			case Common_behaviorPackage.STIMULUS__INTERACTION_INSTANCE_SET:
				return interactionInstanceSet != null && !interactionInstanceSet.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //StimulusImpl
