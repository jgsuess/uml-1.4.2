/**
 * <copyright>
 * </copyright>
 *
 * $Id: AttributeLinkImpl.java,v 1.1 2012/04/23 09:31:17 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior.impl;

import net.jgsuess.uml14.behavioral_elements.common_behavior.AttributeLink;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Instance;
import net.jgsuess.uml14.behavioral_elements.common_behavior.LinkEnd;

import net.jgsuess.uml14.foundation.core.Attribute;
import net.jgsuess.uml14.foundation.core.CorePackage;

import net.jgsuess.uml14.foundation.core.impl.ModelElementImpl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute Link</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.AttributeLinkImpl#getAttribute <em>Attribute</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.AttributeLinkImpl#getValue <em>Value</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.AttributeLinkImpl#getInstance <em>Instance</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.AttributeLinkImpl#getLinkEnd <em>Link End</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AttributeLinkImpl extends ModelElementImpl implements AttributeLink {
	/**
	 * The cached value of the '{@link #getAttribute() <em>Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttribute()
	 * @generated
	 * @ordered
	 */
	protected Attribute attribute;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected Instance value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttributeLinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Common_behaviorPackage.Literals.ATTRIBUTE_LINK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute getAttribute() {
		if (attribute != null && attribute.eIsProxy()) {
			InternalEObject oldAttribute = (InternalEObject)attribute;
			attribute = (Attribute)eResolveProxy(oldAttribute);
			if (attribute != oldAttribute) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Common_behaviorPackage.ATTRIBUTE_LINK__ATTRIBUTE, oldAttribute, attribute));
			}
		}
		return attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute basicGetAttribute() {
		return attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttribute(Attribute newAttribute, NotificationChain msgs) {
		Attribute oldAttribute = attribute;
		attribute = newAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.ATTRIBUTE_LINK__ATTRIBUTE, oldAttribute, newAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttribute(Attribute newAttribute) {
		if (newAttribute != attribute) {
			NotificationChain msgs = null;
			if (attribute != null)
				msgs = ((InternalEObject)attribute).eInverseRemove(this, CorePackage.ATTRIBUTE__ATTRIBUTE_LINK, Attribute.class, msgs);
			if (newAttribute != null)
				msgs = ((InternalEObject)newAttribute).eInverseAdd(this, CorePackage.ATTRIBUTE__ATTRIBUTE_LINK, Attribute.class, msgs);
			msgs = basicSetAttribute(newAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.ATTRIBUTE_LINK__ATTRIBUTE, newAttribute, newAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instance getValue() {
		if (value != null && value.eIsProxy()) {
			InternalEObject oldValue = (InternalEObject)value;
			value = (Instance)eResolveProxy(oldValue);
			if (value != oldValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Common_behaviorPackage.ATTRIBUTE_LINK__VALUE, oldValue, value));
			}
		}
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instance basicGetValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValue(Instance newValue, NotificationChain msgs) {
		Instance oldValue = value;
		value = newValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.ATTRIBUTE_LINK__VALUE, oldValue, newValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(Instance newValue) {
		if (newValue != value) {
			NotificationChain msgs = null;
			if (value != null)
				msgs = ((InternalEObject)value).eInverseRemove(this, Common_behaviorPackage.INSTANCE__ATTRIBUTE_LINK, Instance.class, msgs);
			if (newValue != null)
				msgs = ((InternalEObject)newValue).eInverseAdd(this, Common_behaviorPackage.INSTANCE__ATTRIBUTE_LINK, Instance.class, msgs);
			msgs = basicSetValue(newValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.ATTRIBUTE_LINK__VALUE, newValue, newValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instance getInstance() {
		if (eContainerFeatureID() != Common_behaviorPackage.ATTRIBUTE_LINK__INSTANCE) return null;
		return (Instance)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInstance(Instance newInstance, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newInstance, Common_behaviorPackage.ATTRIBUTE_LINK__INSTANCE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInstance(Instance newInstance) {
		if (newInstance != eInternalContainer() || (eContainerFeatureID() != Common_behaviorPackage.ATTRIBUTE_LINK__INSTANCE && newInstance != null)) {
			if (EcoreUtil.isAncestor(this, newInstance))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newInstance != null)
				msgs = ((InternalEObject)newInstance).eInverseAdd(this, Common_behaviorPackage.INSTANCE__SLOT, Instance.class, msgs);
			msgs = basicSetInstance(newInstance, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.ATTRIBUTE_LINK__INSTANCE, newInstance, newInstance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkEnd getLinkEnd() {
		if (eContainerFeatureID() != Common_behaviorPackage.ATTRIBUTE_LINK__LINK_END) return null;
		return (LinkEnd)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLinkEnd(LinkEnd newLinkEnd, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newLinkEnd, Common_behaviorPackage.ATTRIBUTE_LINK__LINK_END, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLinkEnd(LinkEnd newLinkEnd) {
		if (newLinkEnd != eInternalContainer() || (eContainerFeatureID() != Common_behaviorPackage.ATTRIBUTE_LINK__LINK_END && newLinkEnd != null)) {
			if (EcoreUtil.isAncestor(this, newLinkEnd))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newLinkEnd != null)
				msgs = ((InternalEObject)newLinkEnd).eInverseAdd(this, Common_behaviorPackage.LINK_END__QUALIFIED_VALUE, LinkEnd.class, msgs);
			msgs = basicSetLinkEnd(newLinkEnd, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.ATTRIBUTE_LINK__LINK_END, newLinkEnd, newLinkEnd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Common_behaviorPackage.ATTRIBUTE_LINK__ATTRIBUTE:
				if (attribute != null)
					msgs = ((InternalEObject)attribute).eInverseRemove(this, CorePackage.ATTRIBUTE__ATTRIBUTE_LINK, Attribute.class, msgs);
				return basicSetAttribute((Attribute)otherEnd, msgs);
			case Common_behaviorPackage.ATTRIBUTE_LINK__VALUE:
				if (value != null)
					msgs = ((InternalEObject)value).eInverseRemove(this, Common_behaviorPackage.INSTANCE__ATTRIBUTE_LINK, Instance.class, msgs);
				return basicSetValue((Instance)otherEnd, msgs);
			case Common_behaviorPackage.ATTRIBUTE_LINK__INSTANCE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetInstance((Instance)otherEnd, msgs);
			case Common_behaviorPackage.ATTRIBUTE_LINK__LINK_END:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetLinkEnd((LinkEnd)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Common_behaviorPackage.ATTRIBUTE_LINK__ATTRIBUTE:
				return basicSetAttribute(null, msgs);
			case Common_behaviorPackage.ATTRIBUTE_LINK__VALUE:
				return basicSetValue(null, msgs);
			case Common_behaviorPackage.ATTRIBUTE_LINK__INSTANCE:
				return basicSetInstance(null, msgs);
			case Common_behaviorPackage.ATTRIBUTE_LINK__LINK_END:
				return basicSetLinkEnd(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case Common_behaviorPackage.ATTRIBUTE_LINK__INSTANCE:
				return eInternalContainer().eInverseRemove(this, Common_behaviorPackage.INSTANCE__SLOT, Instance.class, msgs);
			case Common_behaviorPackage.ATTRIBUTE_LINK__LINK_END:
				return eInternalContainer().eInverseRemove(this, Common_behaviorPackage.LINK_END__QUALIFIED_VALUE, LinkEnd.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Common_behaviorPackage.ATTRIBUTE_LINK__ATTRIBUTE:
				if (resolve) return getAttribute();
				return basicGetAttribute();
			case Common_behaviorPackage.ATTRIBUTE_LINK__VALUE:
				if (resolve) return getValue();
				return basicGetValue();
			case Common_behaviorPackage.ATTRIBUTE_LINK__INSTANCE:
				return getInstance();
			case Common_behaviorPackage.ATTRIBUTE_LINK__LINK_END:
				return getLinkEnd();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Common_behaviorPackage.ATTRIBUTE_LINK__ATTRIBUTE:
				setAttribute((Attribute)newValue);
				return;
			case Common_behaviorPackage.ATTRIBUTE_LINK__VALUE:
				setValue((Instance)newValue);
				return;
			case Common_behaviorPackage.ATTRIBUTE_LINK__INSTANCE:
				setInstance((Instance)newValue);
				return;
			case Common_behaviorPackage.ATTRIBUTE_LINK__LINK_END:
				setLinkEnd((LinkEnd)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Common_behaviorPackage.ATTRIBUTE_LINK__ATTRIBUTE:
				setAttribute((Attribute)null);
				return;
			case Common_behaviorPackage.ATTRIBUTE_LINK__VALUE:
				setValue((Instance)null);
				return;
			case Common_behaviorPackage.ATTRIBUTE_LINK__INSTANCE:
				setInstance((Instance)null);
				return;
			case Common_behaviorPackage.ATTRIBUTE_LINK__LINK_END:
				setLinkEnd((LinkEnd)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Common_behaviorPackage.ATTRIBUTE_LINK__ATTRIBUTE:
				return attribute != null;
			case Common_behaviorPackage.ATTRIBUTE_LINK__VALUE:
				return value != null;
			case Common_behaviorPackage.ATTRIBUTE_LINK__INSTANCE:
				return getInstance() != null;
			case Common_behaviorPackage.ATTRIBUTE_LINK__LINK_END:
				return getLinkEnd() != null;
		}
		return super.eIsSet(featureID);
	}

} //AttributeLinkImpl
