/**
 * <copyright>
 * </copyright>
 *
 * $Id: Argument.java,v 1.1 2012/04/23 09:31:36 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior;

import net.jgsuess.uml14.foundation.core.ModelElement;

import net.jgsuess.uml14.foundation.data_types.Expression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Argument</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Argument#getValue <em>Value</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Argument#getAction <em>Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getArgument()
 * @model
 * @generated
 */
public interface Argument extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(Expression)
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getArgument_Value()
	 * @model containment="true"
	 * @generated
	 */
	Expression getValue();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Argument#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Expression value);

	/**
	 * Returns the value of the '<em><b>Action</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getActualArgument <em>Actual Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' container reference.
	 * @see #setAction(Action)
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getArgument_Action()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getActualArgument
	 * @model opposite="actualArgument"
	 * @generated
	 */
	Action getAction();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Argument#getAction <em>Action</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action</em>' container reference.
	 * @see #getAction()
	 * @generated
	 */
	void setAction(Action value);

} // Argument
