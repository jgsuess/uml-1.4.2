/**
 * <copyright>
 * </copyright>
 *
 * $Id: ActionImpl.java,v 1.1 2012/04/23 09:31:17 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage;
import net.jgsuess.uml14.behavioral_elements.collaborations.Message;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Action;
import net.jgsuess.uml14.behavioral_elements.common_behavior.ActionSequence;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Argument;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus;

import net.jgsuess.uml14.behavioral_elements.state_machines.State;
import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage;
import net.jgsuess.uml14.behavioral_elements.state_machines.Transition;

import net.jgsuess.uml14.foundation.core.impl.ModelElementImpl;

import net.jgsuess.uml14.foundation.data_types.ActionExpression;
import net.jgsuess.uml14.foundation.data_types.IterationExpression;
import net.jgsuess.uml14.foundation.data_types.ObjectSetExpression;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.ActionImpl#getRecurrence <em>Recurrence</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.ActionImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.ActionImpl#isIsAsynchronous <em>Is Asynchronous</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.ActionImpl#getScript <em>Script</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.ActionImpl#getActualArgument <em>Actual Argument</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.ActionImpl#getActionSequence <em>Action Sequence</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.ActionImpl#getStimulus <em>Stimulus</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.ActionImpl#getState <em>State</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.ActionImpl#getTransition <em>Transition</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.ActionImpl#getMessage <em>Message</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class ActionImpl extends ModelElementImpl implements Action {
	/**
	 * The cached value of the '{@link #getRecurrence() <em>Recurrence</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecurrence()
	 * @generated
	 * @ordered
	 */
	protected IterationExpression recurrence;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected ObjectSetExpression target;

	/**
	 * The default value of the '{@link #isIsAsynchronous() <em>Is Asynchronous</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsAsynchronous()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_ASYNCHRONOUS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsAsynchronous() <em>Is Asynchronous</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsAsynchronous()
	 * @generated
	 * @ordered
	 */
	protected boolean isAsynchronous = IS_ASYNCHRONOUS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getScript() <em>Script</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScript()
	 * @generated
	 * @ordered
	 */
	protected ActionExpression script;

	/**
	 * The cached value of the '{@link #getActualArgument() <em>Actual Argument</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActualArgument()
	 * @generated
	 * @ordered
	 */
	protected EList<Argument> actualArgument;

	/**
	 * The cached value of the '{@link #getStimulus() <em>Stimulus</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStimulus()
	 * @generated
	 * @ordered
	 */
	protected EList<Stimulus> stimulus;

	/**
	 * The cached value of the '{@link #getMessage() <em>Message</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage()
	 * @generated
	 * @ordered
	 */
	protected EList<Message> message;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Common_behaviorPackage.Literals.ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IterationExpression getRecurrence() {
		return recurrence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRecurrence(IterationExpression newRecurrence, NotificationChain msgs) {
		IterationExpression oldRecurrence = recurrence;
		recurrence = newRecurrence;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.ACTION__RECURRENCE, oldRecurrence, newRecurrence);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecurrence(IterationExpression newRecurrence) {
		if (newRecurrence != recurrence) {
			NotificationChain msgs = null;
			if (recurrence != null)
				msgs = ((InternalEObject)recurrence).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Common_behaviorPackage.ACTION__RECURRENCE, null, msgs);
			if (newRecurrence != null)
				msgs = ((InternalEObject)newRecurrence).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Common_behaviorPackage.ACTION__RECURRENCE, null, msgs);
			msgs = basicSetRecurrence(newRecurrence, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.ACTION__RECURRENCE, newRecurrence, newRecurrence));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectSetExpression getTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTarget(ObjectSetExpression newTarget, NotificationChain msgs) {
		ObjectSetExpression oldTarget = target;
		target = newTarget;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.ACTION__TARGET, oldTarget, newTarget);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget(ObjectSetExpression newTarget) {
		if (newTarget != target) {
			NotificationChain msgs = null;
			if (target != null)
				msgs = ((InternalEObject)target).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Common_behaviorPackage.ACTION__TARGET, null, msgs);
			if (newTarget != null)
				msgs = ((InternalEObject)newTarget).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Common_behaviorPackage.ACTION__TARGET, null, msgs);
			msgs = basicSetTarget(newTarget, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.ACTION__TARGET, newTarget, newTarget));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsAsynchronous() {
		return isAsynchronous;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsAsynchronous(boolean newIsAsynchronous) {
		boolean oldIsAsynchronous = isAsynchronous;
		isAsynchronous = newIsAsynchronous;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.ACTION__IS_ASYNCHRONOUS, oldIsAsynchronous, isAsynchronous));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionExpression getScript() {
		return script;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetScript(ActionExpression newScript, NotificationChain msgs) {
		ActionExpression oldScript = script;
		script = newScript;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.ACTION__SCRIPT, oldScript, newScript);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScript(ActionExpression newScript) {
		if (newScript != script) {
			NotificationChain msgs = null;
			if (script != null)
				msgs = ((InternalEObject)script).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Common_behaviorPackage.ACTION__SCRIPT, null, msgs);
			if (newScript != null)
				msgs = ((InternalEObject)newScript).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Common_behaviorPackage.ACTION__SCRIPT, null, msgs);
			msgs = basicSetScript(newScript, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.ACTION__SCRIPT, newScript, newScript));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Argument> getActualArgument() {
		if (actualArgument == null) {
			actualArgument = new EObjectContainmentWithInverseEList<Argument>(Argument.class, this, Common_behaviorPackage.ACTION__ACTUAL_ARGUMENT, Common_behaviorPackage.ARGUMENT__ACTION);
		}
		return actualArgument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionSequence getActionSequence() {
		if (eContainerFeatureID() != Common_behaviorPackage.ACTION__ACTION_SEQUENCE) return null;
		return (ActionSequence)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetActionSequence(ActionSequence newActionSequence, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newActionSequence, Common_behaviorPackage.ACTION__ACTION_SEQUENCE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActionSequence(ActionSequence newActionSequence) {
		if (newActionSequence != eInternalContainer() || (eContainerFeatureID() != Common_behaviorPackage.ACTION__ACTION_SEQUENCE && newActionSequence != null)) {
			if (EcoreUtil.isAncestor(this, newActionSequence))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newActionSequence != null)
				msgs = ((InternalEObject)newActionSequence).eInverseAdd(this, Common_behaviorPackage.ACTION_SEQUENCE__ACTION, ActionSequence.class, msgs);
			msgs = basicSetActionSequence(newActionSequence, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.ACTION__ACTION_SEQUENCE, newActionSequence, newActionSequence));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Stimulus> getStimulus() {
		if (stimulus == null) {
			stimulus = new EObjectWithInverseResolvingEList<Stimulus>(Stimulus.class, this, Common_behaviorPackage.ACTION__STIMULUS, Common_behaviorPackage.STIMULUS__DISPATCH_ACTION);
		}
		return stimulus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getState() {
		if (eContainerFeatureID() != Common_behaviorPackage.ACTION__STATE) return null;
		return (State)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetState(State newState, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newState, Common_behaviorPackage.ACTION__STATE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setState(State newState) {
		if (newState != eInternalContainer() || (eContainerFeatureID() != Common_behaviorPackage.ACTION__STATE && newState != null)) {
			if (EcoreUtil.isAncestor(this, newState))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newState != null)
				msgs = ((InternalEObject)newState).eInverseAdd(this, State_machinesPackage.STATE__ENTRY, State.class, msgs);
			msgs = basicSetState(newState, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.ACTION__STATE, newState, newState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition getTransition() {
		if (eContainerFeatureID() != Common_behaviorPackage.ACTION__TRANSITION) return null;
		return (Transition)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTransition(Transition newTransition, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newTransition, Common_behaviorPackage.ACTION__TRANSITION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransition(Transition newTransition) {
		if (newTransition != eInternalContainer() || (eContainerFeatureID() != Common_behaviorPackage.ACTION__TRANSITION && newTransition != null)) {
			if (EcoreUtil.isAncestor(this, newTransition))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newTransition != null)
				msgs = ((InternalEObject)newTransition).eInverseAdd(this, State_machinesPackage.TRANSITION__EFFECT, Transition.class, msgs);
			msgs = basicSetTransition(newTransition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.ACTION__TRANSITION, newTransition, newTransition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Message> getMessage() {
		if (message == null) {
			message = new EObjectWithInverseResolvingEList<Message>(Message.class, this, Common_behaviorPackage.ACTION__MESSAGE, CollaborationsPackage.MESSAGE__ACTION);
		}
		return message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Common_behaviorPackage.ACTION__ACTUAL_ARGUMENT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getActualArgument()).basicAdd(otherEnd, msgs);
			case Common_behaviorPackage.ACTION__ACTION_SEQUENCE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetActionSequence((ActionSequence)otherEnd, msgs);
			case Common_behaviorPackage.ACTION__STIMULUS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getStimulus()).basicAdd(otherEnd, msgs);
			case Common_behaviorPackage.ACTION__STATE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetState((State)otherEnd, msgs);
			case Common_behaviorPackage.ACTION__TRANSITION:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetTransition((Transition)otherEnd, msgs);
			case Common_behaviorPackage.ACTION__MESSAGE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getMessage()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Common_behaviorPackage.ACTION__RECURRENCE:
				return basicSetRecurrence(null, msgs);
			case Common_behaviorPackage.ACTION__TARGET:
				return basicSetTarget(null, msgs);
			case Common_behaviorPackage.ACTION__SCRIPT:
				return basicSetScript(null, msgs);
			case Common_behaviorPackage.ACTION__ACTUAL_ARGUMENT:
				return ((InternalEList<?>)getActualArgument()).basicRemove(otherEnd, msgs);
			case Common_behaviorPackage.ACTION__ACTION_SEQUENCE:
				return basicSetActionSequence(null, msgs);
			case Common_behaviorPackage.ACTION__STIMULUS:
				return ((InternalEList<?>)getStimulus()).basicRemove(otherEnd, msgs);
			case Common_behaviorPackage.ACTION__STATE:
				return basicSetState(null, msgs);
			case Common_behaviorPackage.ACTION__TRANSITION:
				return basicSetTransition(null, msgs);
			case Common_behaviorPackage.ACTION__MESSAGE:
				return ((InternalEList<?>)getMessage()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case Common_behaviorPackage.ACTION__ACTION_SEQUENCE:
				return eInternalContainer().eInverseRemove(this, Common_behaviorPackage.ACTION_SEQUENCE__ACTION, ActionSequence.class, msgs);
			case Common_behaviorPackage.ACTION__STATE:
				return eInternalContainer().eInverseRemove(this, State_machinesPackage.STATE__ENTRY, State.class, msgs);
			case Common_behaviorPackage.ACTION__TRANSITION:
				return eInternalContainer().eInverseRemove(this, State_machinesPackage.TRANSITION__EFFECT, Transition.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Common_behaviorPackage.ACTION__RECURRENCE:
				return getRecurrence();
			case Common_behaviorPackage.ACTION__TARGET:
				return getTarget();
			case Common_behaviorPackage.ACTION__IS_ASYNCHRONOUS:
				return isIsAsynchronous();
			case Common_behaviorPackage.ACTION__SCRIPT:
				return getScript();
			case Common_behaviorPackage.ACTION__ACTUAL_ARGUMENT:
				return getActualArgument();
			case Common_behaviorPackage.ACTION__ACTION_SEQUENCE:
				return getActionSequence();
			case Common_behaviorPackage.ACTION__STIMULUS:
				return getStimulus();
			case Common_behaviorPackage.ACTION__STATE:
				return getState();
			case Common_behaviorPackage.ACTION__TRANSITION:
				return getTransition();
			case Common_behaviorPackage.ACTION__MESSAGE:
				return getMessage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Common_behaviorPackage.ACTION__RECURRENCE:
				setRecurrence((IterationExpression)newValue);
				return;
			case Common_behaviorPackage.ACTION__TARGET:
				setTarget((ObjectSetExpression)newValue);
				return;
			case Common_behaviorPackage.ACTION__IS_ASYNCHRONOUS:
				setIsAsynchronous((Boolean)newValue);
				return;
			case Common_behaviorPackage.ACTION__SCRIPT:
				setScript((ActionExpression)newValue);
				return;
			case Common_behaviorPackage.ACTION__ACTUAL_ARGUMENT:
				getActualArgument().clear();
				getActualArgument().addAll((Collection<? extends Argument>)newValue);
				return;
			case Common_behaviorPackage.ACTION__ACTION_SEQUENCE:
				setActionSequence((ActionSequence)newValue);
				return;
			case Common_behaviorPackage.ACTION__STIMULUS:
				getStimulus().clear();
				getStimulus().addAll((Collection<? extends Stimulus>)newValue);
				return;
			case Common_behaviorPackage.ACTION__STATE:
				setState((State)newValue);
				return;
			case Common_behaviorPackage.ACTION__TRANSITION:
				setTransition((Transition)newValue);
				return;
			case Common_behaviorPackage.ACTION__MESSAGE:
				getMessage().clear();
				getMessage().addAll((Collection<? extends Message>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Common_behaviorPackage.ACTION__RECURRENCE:
				setRecurrence((IterationExpression)null);
				return;
			case Common_behaviorPackage.ACTION__TARGET:
				setTarget((ObjectSetExpression)null);
				return;
			case Common_behaviorPackage.ACTION__IS_ASYNCHRONOUS:
				setIsAsynchronous(IS_ASYNCHRONOUS_EDEFAULT);
				return;
			case Common_behaviorPackage.ACTION__SCRIPT:
				setScript((ActionExpression)null);
				return;
			case Common_behaviorPackage.ACTION__ACTUAL_ARGUMENT:
				getActualArgument().clear();
				return;
			case Common_behaviorPackage.ACTION__ACTION_SEQUENCE:
				setActionSequence((ActionSequence)null);
				return;
			case Common_behaviorPackage.ACTION__STIMULUS:
				getStimulus().clear();
				return;
			case Common_behaviorPackage.ACTION__STATE:
				setState((State)null);
				return;
			case Common_behaviorPackage.ACTION__TRANSITION:
				setTransition((Transition)null);
				return;
			case Common_behaviorPackage.ACTION__MESSAGE:
				getMessage().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Common_behaviorPackage.ACTION__RECURRENCE:
				return recurrence != null;
			case Common_behaviorPackage.ACTION__TARGET:
				return target != null;
			case Common_behaviorPackage.ACTION__IS_ASYNCHRONOUS:
				return isAsynchronous != IS_ASYNCHRONOUS_EDEFAULT;
			case Common_behaviorPackage.ACTION__SCRIPT:
				return script != null;
			case Common_behaviorPackage.ACTION__ACTUAL_ARGUMENT:
				return actualArgument != null && !actualArgument.isEmpty();
			case Common_behaviorPackage.ACTION__ACTION_SEQUENCE:
				return getActionSequence() != null;
			case Common_behaviorPackage.ACTION__STIMULUS:
				return stimulus != null && !stimulus.isEmpty();
			case Common_behaviorPackage.ACTION__STATE:
				return getState() != null;
			case Common_behaviorPackage.ACTION__TRANSITION:
				return getTransition() != null;
			case Common_behaviorPackage.ACTION__MESSAGE:
				return message != null && !message.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isAsynchronous: ");
		result.append(isAsynchronous);
		result.append(')');
		return result.toString();
	}

} //ActionImpl
