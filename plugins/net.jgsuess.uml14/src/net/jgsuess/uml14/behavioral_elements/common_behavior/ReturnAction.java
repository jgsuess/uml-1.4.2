/**
 * <copyright>
 * </copyright>
 *
 * $Id: ReturnAction.java,v 1.1 2012/04/23 09:31:37 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Return Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getReturnAction()
 * @model
 * @generated
 */
public interface ReturnAction extends Action {
} // ReturnAction
