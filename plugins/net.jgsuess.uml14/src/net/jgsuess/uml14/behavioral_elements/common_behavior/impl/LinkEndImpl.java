/**
 * <copyright>
 * </copyright>
 *
 * $Id: LinkEndImpl.java,v 1.1 2012/04/23 09:31:14 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.common_behavior.AttributeLink;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Instance;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Link;
import net.jgsuess.uml14.behavioral_elements.common_behavior.LinkEnd;

import net.jgsuess.uml14.foundation.core.AssociationEnd;
import net.jgsuess.uml14.foundation.core.CorePackage;

import net.jgsuess.uml14.foundation.core.impl.ModelElementImpl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Link End</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.LinkEndImpl#getInstance <em>Instance</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.LinkEndImpl#getLink <em>Link</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.LinkEndImpl#getAssociationEnd <em>Association End</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.LinkEndImpl#getQualifiedValue <em>Qualified Value</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LinkEndImpl extends ModelElementImpl implements LinkEnd {
	/**
	 * The cached value of the '{@link #getInstance() <em>Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstance()
	 * @generated
	 * @ordered
	 */
	protected Instance instance;

	/**
	 * The cached value of the '{@link #getAssociationEnd() <em>Association End</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociationEnd()
	 * @generated
	 * @ordered
	 */
	protected AssociationEnd associationEnd;

	/**
	 * The cached value of the '{@link #getQualifiedValue() <em>Qualified Value</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQualifiedValue()
	 * @generated
	 * @ordered
	 */
	protected EList<AttributeLink> qualifiedValue;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LinkEndImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Common_behaviorPackage.Literals.LINK_END;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instance getInstance() {
		if (instance != null && instance.eIsProxy()) {
			InternalEObject oldInstance = (InternalEObject)instance;
			instance = (Instance)eResolveProxy(oldInstance);
			if (instance != oldInstance) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Common_behaviorPackage.LINK_END__INSTANCE, oldInstance, instance));
			}
		}
		return instance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instance basicGetInstance() {
		return instance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInstance(Instance newInstance, NotificationChain msgs) {
		Instance oldInstance = instance;
		instance = newInstance;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.LINK_END__INSTANCE, oldInstance, newInstance);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInstance(Instance newInstance) {
		if (newInstance != instance) {
			NotificationChain msgs = null;
			if (instance != null)
				msgs = ((InternalEObject)instance).eInverseRemove(this, Common_behaviorPackage.INSTANCE__LINK_END, Instance.class, msgs);
			if (newInstance != null)
				msgs = ((InternalEObject)newInstance).eInverseAdd(this, Common_behaviorPackage.INSTANCE__LINK_END, Instance.class, msgs);
			msgs = basicSetInstance(newInstance, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.LINK_END__INSTANCE, newInstance, newInstance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Link getLink() {
		if (eContainerFeatureID() != Common_behaviorPackage.LINK_END__LINK) return null;
		return (Link)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLink(Link newLink, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newLink, Common_behaviorPackage.LINK_END__LINK, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLink(Link newLink) {
		if (newLink != eInternalContainer() || (eContainerFeatureID() != Common_behaviorPackage.LINK_END__LINK && newLink != null)) {
			if (EcoreUtil.isAncestor(this, newLink))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newLink != null)
				msgs = ((InternalEObject)newLink).eInverseAdd(this, Common_behaviorPackage.LINK__CONNECTION, Link.class, msgs);
			msgs = basicSetLink(newLink, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.LINK_END__LINK, newLink, newLink));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationEnd getAssociationEnd() {
		if (associationEnd != null && associationEnd.eIsProxy()) {
			InternalEObject oldAssociationEnd = (InternalEObject)associationEnd;
			associationEnd = (AssociationEnd)eResolveProxy(oldAssociationEnd);
			if (associationEnd != oldAssociationEnd) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Common_behaviorPackage.LINK_END__ASSOCIATION_END, oldAssociationEnd, associationEnd));
			}
		}
		return associationEnd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationEnd basicGetAssociationEnd() {
		return associationEnd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssociationEnd(AssociationEnd newAssociationEnd, NotificationChain msgs) {
		AssociationEnd oldAssociationEnd = associationEnd;
		associationEnd = newAssociationEnd;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.LINK_END__ASSOCIATION_END, oldAssociationEnd, newAssociationEnd);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssociationEnd(AssociationEnd newAssociationEnd) {
		if (newAssociationEnd != associationEnd) {
			NotificationChain msgs = null;
			if (associationEnd != null)
				msgs = ((InternalEObject)associationEnd).eInverseRemove(this, CorePackage.ASSOCIATION_END__LINK_END, AssociationEnd.class, msgs);
			if (newAssociationEnd != null)
				msgs = ((InternalEObject)newAssociationEnd).eInverseAdd(this, CorePackage.ASSOCIATION_END__LINK_END, AssociationEnd.class, msgs);
			msgs = basicSetAssociationEnd(newAssociationEnd, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.LINK_END__ASSOCIATION_END, newAssociationEnd, newAssociationEnd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttributeLink> getQualifiedValue() {
		if (qualifiedValue == null) {
			qualifiedValue = new EObjectContainmentWithInverseEList<AttributeLink>(AttributeLink.class, this, Common_behaviorPackage.LINK_END__QUALIFIED_VALUE, Common_behaviorPackage.ATTRIBUTE_LINK__LINK_END);
		}
		return qualifiedValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Common_behaviorPackage.LINK_END__INSTANCE:
				if (instance != null)
					msgs = ((InternalEObject)instance).eInverseRemove(this, Common_behaviorPackage.INSTANCE__LINK_END, Instance.class, msgs);
				return basicSetInstance((Instance)otherEnd, msgs);
			case Common_behaviorPackage.LINK_END__LINK:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetLink((Link)otherEnd, msgs);
			case Common_behaviorPackage.LINK_END__ASSOCIATION_END:
				if (associationEnd != null)
					msgs = ((InternalEObject)associationEnd).eInverseRemove(this, CorePackage.ASSOCIATION_END__LINK_END, AssociationEnd.class, msgs);
				return basicSetAssociationEnd((AssociationEnd)otherEnd, msgs);
			case Common_behaviorPackage.LINK_END__QUALIFIED_VALUE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getQualifiedValue()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Common_behaviorPackage.LINK_END__INSTANCE:
				return basicSetInstance(null, msgs);
			case Common_behaviorPackage.LINK_END__LINK:
				return basicSetLink(null, msgs);
			case Common_behaviorPackage.LINK_END__ASSOCIATION_END:
				return basicSetAssociationEnd(null, msgs);
			case Common_behaviorPackage.LINK_END__QUALIFIED_VALUE:
				return ((InternalEList<?>)getQualifiedValue()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case Common_behaviorPackage.LINK_END__LINK:
				return eInternalContainer().eInverseRemove(this, Common_behaviorPackage.LINK__CONNECTION, Link.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Common_behaviorPackage.LINK_END__INSTANCE:
				if (resolve) return getInstance();
				return basicGetInstance();
			case Common_behaviorPackage.LINK_END__LINK:
				return getLink();
			case Common_behaviorPackage.LINK_END__ASSOCIATION_END:
				if (resolve) return getAssociationEnd();
				return basicGetAssociationEnd();
			case Common_behaviorPackage.LINK_END__QUALIFIED_VALUE:
				return getQualifiedValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Common_behaviorPackage.LINK_END__INSTANCE:
				setInstance((Instance)newValue);
				return;
			case Common_behaviorPackage.LINK_END__LINK:
				setLink((Link)newValue);
				return;
			case Common_behaviorPackage.LINK_END__ASSOCIATION_END:
				setAssociationEnd((AssociationEnd)newValue);
				return;
			case Common_behaviorPackage.LINK_END__QUALIFIED_VALUE:
				getQualifiedValue().clear();
				getQualifiedValue().addAll((Collection<? extends AttributeLink>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Common_behaviorPackage.LINK_END__INSTANCE:
				setInstance((Instance)null);
				return;
			case Common_behaviorPackage.LINK_END__LINK:
				setLink((Link)null);
				return;
			case Common_behaviorPackage.LINK_END__ASSOCIATION_END:
				setAssociationEnd((AssociationEnd)null);
				return;
			case Common_behaviorPackage.LINK_END__QUALIFIED_VALUE:
				getQualifiedValue().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Common_behaviorPackage.LINK_END__INSTANCE:
				return instance != null;
			case Common_behaviorPackage.LINK_END__LINK:
				return getLink() != null;
			case Common_behaviorPackage.LINK_END__ASSOCIATION_END:
				return associationEnd != null;
			case Common_behaviorPackage.LINK_END__QUALIFIED_VALUE:
				return qualifiedValue != null && !qualifiedValue.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //LinkEndImpl
