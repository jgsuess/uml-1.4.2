/**
 * <copyright>
 * </copyright>
 *
 * $Id: SignalImpl.java,v 1.1 2012/04/23 09:31:16 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Reception;
import net.jgsuess.uml14.behavioral_elements.common_behavior.SendAction;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Signal;

import net.jgsuess.uml14.behavioral_elements.state_machines.SignalEvent;
import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage;

import net.jgsuess.uml14.foundation.core.BehavioralFeature;
import net.jgsuess.uml14.foundation.core.CorePackage;

import net.jgsuess.uml14.foundation.core.impl.ClassifierImpl;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.SignalImpl#getReception <em>Reception</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.SignalImpl#getContext <em>Context</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.SignalImpl#getSendAction <em>Send Action</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.SignalImpl#getOccurrence <em>Occurrence</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SignalImpl extends ClassifierImpl implements Signal {
	/**
	 * The cached value of the '{@link #getReception() <em>Reception</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReception()
	 * @generated
	 * @ordered
	 */
	protected EList<Reception> reception;

	/**
	 * The cached value of the '{@link #getContext() <em>Context</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContext()
	 * @generated
	 * @ordered
	 */
	protected EList<BehavioralFeature> context;

	/**
	 * The cached value of the '{@link #getSendAction() <em>Send Action</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSendAction()
	 * @generated
	 * @ordered
	 */
	protected EList<SendAction> sendAction;

	/**
	 * The cached value of the '{@link #getOccurrence() <em>Occurrence</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOccurrence()
	 * @generated
	 * @ordered
	 */
	protected EList<SignalEvent> occurrence;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SignalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Common_behaviorPackage.Literals.SIGNAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Reception> getReception() {
		if (reception == null) {
			reception = new EObjectWithInverseResolvingEList<Reception>(Reception.class, this, Common_behaviorPackage.SIGNAL__RECEPTION, Common_behaviorPackage.RECEPTION__SIGNAL);
		}
		return reception;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BehavioralFeature> getContext() {
		if (context == null) {
			context = new EObjectWithInverseResolvingEList.ManyInverse<BehavioralFeature>(BehavioralFeature.class, this, Common_behaviorPackage.SIGNAL__CONTEXT, CorePackage.BEHAVIORAL_FEATURE__RAISED_SIGNAL);
		}
		return context;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SendAction> getSendAction() {
		if (sendAction == null) {
			sendAction = new EObjectWithInverseResolvingEList<SendAction>(SendAction.class, this, Common_behaviorPackage.SIGNAL__SEND_ACTION, Common_behaviorPackage.SEND_ACTION__SIGNAL);
		}
		return sendAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SignalEvent> getOccurrence() {
		if (occurrence == null) {
			occurrence = new EObjectWithInverseResolvingEList<SignalEvent>(SignalEvent.class, this, Common_behaviorPackage.SIGNAL__OCCURRENCE, State_machinesPackage.SIGNAL_EVENT__SIGNAL);
		}
		return occurrence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Common_behaviorPackage.SIGNAL__RECEPTION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getReception()).basicAdd(otherEnd, msgs);
			case Common_behaviorPackage.SIGNAL__CONTEXT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getContext()).basicAdd(otherEnd, msgs);
			case Common_behaviorPackage.SIGNAL__SEND_ACTION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSendAction()).basicAdd(otherEnd, msgs);
			case Common_behaviorPackage.SIGNAL__OCCURRENCE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOccurrence()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Common_behaviorPackage.SIGNAL__RECEPTION:
				return ((InternalEList<?>)getReception()).basicRemove(otherEnd, msgs);
			case Common_behaviorPackage.SIGNAL__CONTEXT:
				return ((InternalEList<?>)getContext()).basicRemove(otherEnd, msgs);
			case Common_behaviorPackage.SIGNAL__SEND_ACTION:
				return ((InternalEList<?>)getSendAction()).basicRemove(otherEnd, msgs);
			case Common_behaviorPackage.SIGNAL__OCCURRENCE:
				return ((InternalEList<?>)getOccurrence()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Common_behaviorPackage.SIGNAL__RECEPTION:
				return getReception();
			case Common_behaviorPackage.SIGNAL__CONTEXT:
				return getContext();
			case Common_behaviorPackage.SIGNAL__SEND_ACTION:
				return getSendAction();
			case Common_behaviorPackage.SIGNAL__OCCURRENCE:
				return getOccurrence();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Common_behaviorPackage.SIGNAL__RECEPTION:
				getReception().clear();
				getReception().addAll((Collection<? extends Reception>)newValue);
				return;
			case Common_behaviorPackage.SIGNAL__CONTEXT:
				getContext().clear();
				getContext().addAll((Collection<? extends BehavioralFeature>)newValue);
				return;
			case Common_behaviorPackage.SIGNAL__SEND_ACTION:
				getSendAction().clear();
				getSendAction().addAll((Collection<? extends SendAction>)newValue);
				return;
			case Common_behaviorPackage.SIGNAL__OCCURRENCE:
				getOccurrence().clear();
				getOccurrence().addAll((Collection<? extends SignalEvent>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Common_behaviorPackage.SIGNAL__RECEPTION:
				getReception().clear();
				return;
			case Common_behaviorPackage.SIGNAL__CONTEXT:
				getContext().clear();
				return;
			case Common_behaviorPackage.SIGNAL__SEND_ACTION:
				getSendAction().clear();
				return;
			case Common_behaviorPackage.SIGNAL__OCCURRENCE:
				getOccurrence().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Common_behaviorPackage.SIGNAL__RECEPTION:
				return reception != null && !reception.isEmpty();
			case Common_behaviorPackage.SIGNAL__CONTEXT:
				return context != null && !context.isEmpty();
			case Common_behaviorPackage.SIGNAL__SEND_ACTION:
				return sendAction != null && !sendAction.isEmpty();
			case Common_behaviorPackage.SIGNAL__OCCURRENCE:
				return occurrence != null && !occurrence.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SignalImpl
