/**
 * <copyright>
 * </copyright>
 *
 * $Id: LinkEnd.java,v 1.1 2012/04/23 09:31:35 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior;

import net.jgsuess.uml14.foundation.core.AssociationEnd;
import net.jgsuess.uml14.foundation.core.ModelElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Link End</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.LinkEnd#getInstance <em>Instance</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.LinkEnd#getLink <em>Link</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.LinkEnd#getAssociationEnd <em>Association End</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.LinkEnd#getQualifiedValue <em>Qualified Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getLinkEnd()
 * @model
 * @generated
 */
public interface LinkEnd extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Instance</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Instance#getLinkEnd <em>Link End</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance</em>' reference.
	 * @see #setInstance(Instance)
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getLinkEnd_Instance()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Instance#getLinkEnd
	 * @model opposite="linkEnd" required="true"
	 * @generated
	 */
	Instance getInstance();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.LinkEnd#getInstance <em>Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Instance</em>' reference.
	 * @see #getInstance()
	 * @generated
	 */
	void setInstance(Instance value);

	/**
	 * Returns the value of the '<em><b>Link</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Link#getConnection <em>Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Link</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Link</em>' container reference.
	 * @see #setLink(Link)
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getLinkEnd_Link()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Link#getConnection
	 * @model opposite="connection" required="true"
	 * @generated
	 */
	Link getLink();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.LinkEnd#getLink <em>Link</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Link</em>' container reference.
	 * @see #getLink()
	 * @generated
	 */
	void setLink(Link value);

	/**
	 * Returns the value of the '<em><b>Association End</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getLinkEnd <em>Link End</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Association End</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Association End</em>' reference.
	 * @see #setAssociationEnd(AssociationEnd)
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getLinkEnd_AssociationEnd()
	 * @see net.jgsuess.uml14.foundation.core.AssociationEnd#getLinkEnd
	 * @model opposite="linkEnd" required="true"
	 * @generated
	 */
	AssociationEnd getAssociationEnd();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.LinkEnd#getAssociationEnd <em>Association End</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Association End</em>' reference.
	 * @see #getAssociationEnd()
	 * @generated
	 */
	void setAssociationEnd(AssociationEnd value);

	/**
	 * Returns the value of the '<em><b>Qualified Value</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.AttributeLink}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.AttributeLink#getLinkEnd <em>Link End</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Qualified Value</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qualified Value</em>' containment reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getLinkEnd_QualifiedValue()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.AttributeLink#getLinkEnd
	 * @model opposite="linkEnd" containment="true"
	 * @generated
	 */
	EList<AttributeLink> getQualifiedValue();

} // LinkEnd
