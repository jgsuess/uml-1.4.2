/**
 * <copyright>
 * </copyright>
 *
 * $Id: DestroyActionImpl.java,v 1.1 2012/04/23 09:31:16 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior.impl;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;
import net.jgsuess.uml14.behavioral_elements.common_behavior.DestroyAction;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Destroy Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class DestroyActionImpl extends ActionImpl implements DestroyAction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DestroyActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Common_behaviorPackage.Literals.DESTROY_ACTION;
	}

} //DestroyActionImpl
