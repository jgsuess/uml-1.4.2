/**
 * <copyright>
 * </copyright>
 *
 * $Id: ComponentInstance.java,v 1.1 2012/04/23 09:31:37 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.ComponentInstance#getNodeInstance <em>Node Instance</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.ComponentInstance#getResident <em>Resident</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getComponentInstance()
 * @model
 * @generated
 */
public interface ComponentInstance extends Instance {
	/**
	 * Returns the value of the '<em><b>Node Instance</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.NodeInstance#getResident <em>Resident</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node Instance</em>' reference.
	 * @see #setNodeInstance(NodeInstance)
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getComponentInstance_NodeInstance()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.NodeInstance#getResident
	 * @model opposite="resident"
	 * @generated
	 */
	NodeInstance getNodeInstance();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.ComponentInstance#getNodeInstance <em>Node Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node Instance</em>' reference.
	 * @see #getNodeInstance()
	 * @generated
	 */
	void setNodeInstance(NodeInstance value);

	/**
	 * Returns the value of the '<em><b>Resident</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.Instance}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Instance#getComponentInstance <em>Component Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resident</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resident</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getComponentInstance_Resident()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Instance#getComponentInstance
	 * @model opposite="componentInstance"
	 * @generated
	 */
	EList<Instance> getResident();

} // ComponentInstance
