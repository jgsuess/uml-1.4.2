/**
 * <copyright>
 * </copyright>
 *
 * $Id: UninterpretedAction.java,v 1.1 2012/04/23 09:31:35 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Uninterpreted Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getUninterpretedAction()
 * @model
 * @generated
 */
public interface UninterpretedAction extends Action {
} // UninterpretedAction
