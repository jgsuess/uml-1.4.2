/**
 * <copyright>
 * </copyright>
 *
 * $Id: Stimulus.java,v 1.1 2012/04/23 09:31:36 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior;

import net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet;
import net.jgsuess.uml14.behavioral_elements.collaborations.Message;

import net.jgsuess.uml14.foundation.core.ModelElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stimulus</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus#getArgument <em>Argument</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus#getCommunicationLink <em>Communication Link</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus#getDispatchAction <em>Dispatch Action</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus#getPlayedRole <em>Played Role</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus#getInteractionInstanceSet <em>Interaction Instance Set</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getStimulus()
 * @model
 * @generated
 */
public interface Stimulus extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Argument</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.Instance}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Instance#getStimulus <em>Stimulus</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Argument</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Argument</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getStimulus_Argument()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Instance#getStimulus
	 * @model opposite="stimulus"
	 * @generated
	 */
	EList<Instance> getArgument();

	/**
	 * Returns the value of the '<em><b>Communication Link</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Link#getStimulus <em>Stimulus</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Communication Link</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Communication Link</em>' reference.
	 * @see #setCommunicationLink(Link)
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getStimulus_CommunicationLink()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Link#getStimulus
	 * @model opposite="stimulus"
	 * @generated
	 */
	Link getCommunicationLink();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus#getCommunicationLink <em>Communication Link</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Communication Link</em>' reference.
	 * @see #getCommunicationLink()
	 * @generated
	 */
	void setCommunicationLink(Link value);

	/**
	 * Returns the value of the '<em><b>Dispatch Action</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getStimulus <em>Stimulus</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dispatch Action</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dispatch Action</em>' reference.
	 * @see #setDispatchAction(Action)
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getStimulus_DispatchAction()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getStimulus
	 * @model opposite="stimulus" required="true"
	 * @generated
	 */
	Action getDispatchAction();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus#getDispatchAction <em>Dispatch Action</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dispatch Action</em>' reference.
	 * @see #getDispatchAction()
	 * @generated
	 */
	void setDispatchAction(Action value);

	/**
	 * Returns the value of the '<em><b>Played Role</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.collaborations.Message}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Message#getConformingStimulus <em>Conforming Stimulus</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Played Role</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Played Role</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getStimulus_PlayedRole()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Message#getConformingStimulus
	 * @model opposite="conformingStimulus"
	 * @generated
	 */
	EList<Message> getPlayedRole();

	/**
	 * Returns the value of the '<em><b>Interaction Instance Set</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet#getParticipatingStimulus <em>Participating Stimulus</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interaction Instance Set</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interaction Instance Set</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getStimulus_InteractionInstanceSet()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.InteractionInstanceSet#getParticipatingStimulus
	 * @model opposite="participatingStimulus"
	 * @generated
	 */
	EList<InteractionInstanceSet> getInteractionInstanceSet();

} // Stimulus
