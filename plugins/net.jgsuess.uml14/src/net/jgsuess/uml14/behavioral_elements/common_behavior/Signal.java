/**
 * <copyright>
 * </copyright>
 *
 * $Id: Signal.java,v 1.1 2012/04/23 09:31:36 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior;

import net.jgsuess.uml14.behavioral_elements.state_machines.SignalEvent;

import net.jgsuess.uml14.foundation.core.BehavioralFeature;
import net.jgsuess.uml14.foundation.core.Classifier;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Signal#getReception <em>Reception</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Signal#getContext <em>Context</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Signal#getSendAction <em>Send Action</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Signal#getOccurrence <em>Occurrence</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getSignal()
 * @model
 * @generated
 */
public interface Signal extends Classifier {
	/**
	 * Returns the value of the '<em><b>Reception</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.Reception}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Reception#getSignal <em>Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reception</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reception</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getSignal_Reception()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Reception#getSignal
	 * @model opposite="signal"
	 * @generated
	 */
	EList<Reception> getReception();

	/**
	 * Returns the value of the '<em><b>Context</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.BehavioralFeature}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.BehavioralFeature#getRaisedSignal <em>Raised Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Context</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Context</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getSignal_Context()
	 * @see net.jgsuess.uml14.foundation.core.BehavioralFeature#getRaisedSignal
	 * @model opposite="raisedSignal"
	 * @generated
	 */
	EList<BehavioralFeature> getContext();

	/**
	 * Returns the value of the '<em><b>Send Action</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.SendAction}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.SendAction#getSignal <em>Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Send Action</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Send Action</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getSignal_SendAction()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.SendAction#getSignal
	 * @model opposite="signal"
	 * @generated
	 */
	EList<SendAction> getSendAction();

	/**
	 * Returns the value of the '<em><b>Occurrence</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.state_machines.SignalEvent}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.state_machines.SignalEvent#getSignal <em>Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Occurrence</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Occurrence</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage#getSignal_Occurrence()
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.SignalEvent#getSignal
	 * @model opposite="signal"
	 * @generated
	 */
	EList<SignalEvent> getOccurrence();

} // Signal
