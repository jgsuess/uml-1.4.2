/**
 * <copyright>
 * </copyright>
 *
 * $Id: Common_behaviorFactoryImpl.java,v 1.1 2012/04/23 09:31:17 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior.impl;

import net.jgsuess.uml14.behavioral_elements.common_behavior.ActionSequence;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Argument;
import net.jgsuess.uml14.behavioral_elements.common_behavior.AttributeLink;
import net.jgsuess.uml14.behavioral_elements.common_behavior.CallAction;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorFactory;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;
import net.jgsuess.uml14.behavioral_elements.common_behavior.ComponentInstance;
import net.jgsuess.uml14.behavioral_elements.common_behavior.CreateAction;
import net.jgsuess.uml14.behavioral_elements.common_behavior.DataValue;
import net.jgsuess.uml14.behavioral_elements.common_behavior.DestroyAction;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Link;
import net.jgsuess.uml14.behavioral_elements.common_behavior.LinkEnd;
import net.jgsuess.uml14.behavioral_elements.common_behavior.LinkObject;
import net.jgsuess.uml14.behavioral_elements.common_behavior.NodeInstance;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Reception;
import net.jgsuess.uml14.behavioral_elements.common_behavior.ReturnAction;
import net.jgsuess.uml14.behavioral_elements.common_behavior.SendAction;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Signal;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus;
import net.jgsuess.uml14.behavioral_elements.common_behavior.SubsystemInstance;
import net.jgsuess.uml14.behavioral_elements.common_behavior.TerminateAction;
import net.jgsuess.uml14.behavioral_elements.common_behavior.UninterpretedAction;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Common_behaviorFactoryImpl extends EFactoryImpl implements Common_behaviorFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Common_behaviorFactory init() {
		try {
			Common_behaviorFactory theCommon_behaviorFactory = (Common_behaviorFactory)EPackage.Registry.INSTANCE.getEFactory("http://jgsuess.net/uml14/common_behavior"); 
			if (theCommon_behaviorFactory != null) {
				return theCommon_behaviorFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Common_behaviorFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Common_behaviorFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Common_behaviorPackage.SIGNAL: return createSignal();
			case Common_behaviorPackage.CREATE_ACTION: return createCreateAction();
			case Common_behaviorPackage.DESTROY_ACTION: return createDestroyAction();
			case Common_behaviorPackage.UNINTERPRETED_ACTION: return createUninterpretedAction();
			case Common_behaviorPackage.ATTRIBUTE_LINK: return createAttributeLink();
			case Common_behaviorPackage.OBJECT: return createObject();
			case Common_behaviorPackage.LINK: return createLink();
			case Common_behaviorPackage.LINK_OBJECT: return createLinkObject();
			case Common_behaviorPackage.DATA_VALUE: return createDataValue();
			case Common_behaviorPackage.CALL_ACTION: return createCallAction();
			case Common_behaviorPackage.SEND_ACTION: return createSendAction();
			case Common_behaviorPackage.ACTION_SEQUENCE: return createActionSequence();
			case Common_behaviorPackage.ARGUMENT: return createArgument();
			case Common_behaviorPackage.RECEPTION: return createReception();
			case Common_behaviorPackage.LINK_END: return createLinkEnd();
			case Common_behaviorPackage.RETURN_ACTION: return createReturnAction();
			case Common_behaviorPackage.TERMINATE_ACTION: return createTerminateAction();
			case Common_behaviorPackage.STIMULUS: return createStimulus();
			case Common_behaviorPackage.EXCEPTION: return createException();
			case Common_behaviorPackage.COMPONENT_INSTANCE: return createComponentInstance();
			case Common_behaviorPackage.NODE_INSTANCE: return createNodeInstance();
			case Common_behaviorPackage.SUBSYSTEM_INSTANCE: return createSubsystemInstance();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Signal createSignal() {
		SignalImpl signal = new SignalImpl();
		return signal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CreateAction createCreateAction() {
		CreateActionImpl createAction = new CreateActionImpl();
		return createAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DestroyAction createDestroyAction() {
		DestroyActionImpl destroyAction = new DestroyActionImpl();
		return destroyAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UninterpretedAction createUninterpretedAction() {
		UninterpretedActionImpl uninterpretedAction = new UninterpretedActionImpl();
		return uninterpretedAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeLink createAttributeLink() {
		AttributeLinkImpl attributeLink = new AttributeLinkImpl();
		return attributeLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public net.jgsuess.uml14.behavioral_elements.common_behavior.Object createObject() {
		ObjectImpl object = new ObjectImpl();
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Link createLink() {
		LinkImpl link = new LinkImpl();
		return link;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkObject createLinkObject() {
		LinkObjectImpl linkObject = new LinkObjectImpl();
		return linkObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataValue createDataValue() {
		DataValueImpl dataValue = new DataValueImpl();
		return dataValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallAction createCallAction() {
		CallActionImpl callAction = new CallActionImpl();
		return callAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SendAction createSendAction() {
		SendActionImpl sendAction = new SendActionImpl();
		return sendAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionSequence createActionSequence() {
		ActionSequenceImpl actionSequence = new ActionSequenceImpl();
		return actionSequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Argument createArgument() {
		ArgumentImpl argument = new ArgumentImpl();
		return argument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reception createReception() {
		ReceptionImpl reception = new ReceptionImpl();
		return reception;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkEnd createLinkEnd() {
		LinkEndImpl linkEnd = new LinkEndImpl();
		return linkEnd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReturnAction createReturnAction() {
		ReturnActionImpl returnAction = new ReturnActionImpl();
		return returnAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TerminateAction createTerminateAction() {
		TerminateActionImpl terminateAction = new TerminateActionImpl();
		return terminateAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Stimulus createStimulus() {
		StimulusImpl stimulus = new StimulusImpl();
		return stimulus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public net.jgsuess.uml14.behavioral_elements.common_behavior.Exception createException() {
		ExceptionImpl exception = new ExceptionImpl();
		return exception;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance createComponentInstance() {
		ComponentInstanceImpl componentInstance = new ComponentInstanceImpl();
		return componentInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeInstance createNodeInstance() {
		NodeInstanceImpl nodeInstance = new NodeInstanceImpl();
		return nodeInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubsystemInstance createSubsystemInstance() {
		SubsystemInstanceImpl subsystemInstance = new SubsystemInstanceImpl();
		return subsystemInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Common_behaviorPackage getCommon_behaviorPackage() {
		return (Common_behaviorPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Common_behaviorPackage getPackage() {
		return Common_behaviorPackage.eINSTANCE;
	}

} //Common_behaviorFactoryImpl
