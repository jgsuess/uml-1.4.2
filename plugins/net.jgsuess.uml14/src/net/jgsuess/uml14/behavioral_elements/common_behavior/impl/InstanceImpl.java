/**
 * <copyright>
 * </copyright>
 *
 * $Id: InstanceImpl.java,v 1.1 2012/04/23 09:31:14 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.common_behavior.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage;

import net.jgsuess.uml14.behavioral_elements.common_behavior.AttributeLink;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;
import net.jgsuess.uml14.behavioral_elements.common_behavior.ComponentInstance;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Instance;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Link;
import net.jgsuess.uml14.behavioral_elements.common_behavior.LinkEnd;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Stimulus;

import net.jgsuess.uml14.foundation.core.Classifier;
import net.jgsuess.uml14.foundation.core.CorePackage;

import net.jgsuess.uml14.foundation.core.impl.ModelElementImpl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.InstanceImpl#getClassifier <em>Classifier</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.InstanceImpl#getAttributeLink <em>Attribute Link</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.InstanceImpl#getLinkEnd <em>Link End</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.InstanceImpl#getSlot <em>Slot</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.InstanceImpl#getStimulus <em>Stimulus</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.InstanceImpl#getComponentInstance <em>Component Instance</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.InstanceImpl#getOwnedInstance <em>Owned Instance</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.InstanceImpl#getOwner <em>Owner</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.InstanceImpl#getOwnedLink <em>Owned Link</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.common_behavior.impl.InstanceImpl#getPlayedRole <em>Played Role</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class InstanceImpl extends ModelElementImpl implements Instance {
	/**
	 * The cached value of the '{@link #getClassifier() <em>Classifier</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassifier()
	 * @generated
	 * @ordered
	 */
	protected EList<Classifier> classifier;

	/**
	 * The cached value of the '{@link #getAttributeLink() <em>Attribute Link</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributeLink()
	 * @generated
	 * @ordered
	 */
	protected EList<AttributeLink> attributeLink;

	/**
	 * The cached value of the '{@link #getLinkEnd() <em>Link End</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkEnd()
	 * @generated
	 * @ordered
	 */
	protected EList<LinkEnd> linkEnd;

	/**
	 * The cached value of the '{@link #getSlot() <em>Slot</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSlot()
	 * @generated
	 * @ordered
	 */
	protected EList<AttributeLink> slot;

	/**
	 * The cached value of the '{@link #getStimulus() <em>Stimulus</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStimulus()
	 * @generated
	 * @ordered
	 */
	protected EList<Stimulus> stimulus;

	/**
	 * The cached value of the '{@link #getComponentInstance() <em>Component Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentInstance()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance componentInstance;

	/**
	 * The cached value of the '{@link #getOwnedInstance() <em>Owned Instance</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedInstance()
	 * @generated
	 * @ordered
	 */
	protected EList<Instance> ownedInstance;

	/**
	 * The cached value of the '{@link #getOwnedLink() <em>Owned Link</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedLink()
	 * @generated
	 * @ordered
	 */
	protected EList<Link> ownedLink;

	/**
	 * The cached value of the '{@link #getPlayedRole() <em>Played Role</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPlayedRole()
	 * @generated
	 * @ordered
	 */
	protected EList<ClassifierRole> playedRole;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Common_behaviorPackage.Literals.INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Classifier> getClassifier() {
		if (classifier == null) {
			classifier = new EObjectWithInverseResolvingEList.ManyInverse<Classifier>(Classifier.class, this, Common_behaviorPackage.INSTANCE__CLASSIFIER, CorePackage.CLASSIFIER__INSTANCE);
		}
		return classifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttributeLink> getAttributeLink() {
		if (attributeLink == null) {
			attributeLink = new EObjectWithInverseResolvingEList<AttributeLink>(AttributeLink.class, this, Common_behaviorPackage.INSTANCE__ATTRIBUTE_LINK, Common_behaviorPackage.ATTRIBUTE_LINK__VALUE);
		}
		return attributeLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LinkEnd> getLinkEnd() {
		if (linkEnd == null) {
			linkEnd = new EObjectWithInverseResolvingEList<LinkEnd>(LinkEnd.class, this, Common_behaviorPackage.INSTANCE__LINK_END, Common_behaviorPackage.LINK_END__INSTANCE);
		}
		return linkEnd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttributeLink> getSlot() {
		if (slot == null) {
			slot = new EObjectContainmentWithInverseEList<AttributeLink>(AttributeLink.class, this, Common_behaviorPackage.INSTANCE__SLOT, Common_behaviorPackage.ATTRIBUTE_LINK__INSTANCE);
		}
		return slot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Stimulus> getStimulus() {
		if (stimulus == null) {
			stimulus = new EObjectWithInverseResolvingEList.ManyInverse<Stimulus>(Stimulus.class, this, Common_behaviorPackage.INSTANCE__STIMULUS, Common_behaviorPackage.STIMULUS__ARGUMENT);
		}
		return stimulus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getComponentInstance() {
		if (componentInstance != null && componentInstance.eIsProxy()) {
			InternalEObject oldComponentInstance = (InternalEObject)componentInstance;
			componentInstance = (ComponentInstance)eResolveProxy(oldComponentInstance);
			if (componentInstance != oldComponentInstance) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Common_behaviorPackage.INSTANCE__COMPONENT_INSTANCE, oldComponentInstance, componentInstance));
			}
		}
		return componentInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetComponentInstance() {
		return componentInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComponentInstance(ComponentInstance newComponentInstance, NotificationChain msgs) {
		ComponentInstance oldComponentInstance = componentInstance;
		componentInstance = newComponentInstance;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.INSTANCE__COMPONENT_INSTANCE, oldComponentInstance, newComponentInstance);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentInstance(ComponentInstance newComponentInstance) {
		if (newComponentInstance != componentInstance) {
			NotificationChain msgs = null;
			if (componentInstance != null)
				msgs = ((InternalEObject)componentInstance).eInverseRemove(this, Common_behaviorPackage.COMPONENT_INSTANCE__RESIDENT, ComponentInstance.class, msgs);
			if (newComponentInstance != null)
				msgs = ((InternalEObject)newComponentInstance).eInverseAdd(this, Common_behaviorPackage.COMPONENT_INSTANCE__RESIDENT, ComponentInstance.class, msgs);
			msgs = basicSetComponentInstance(newComponentInstance, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.INSTANCE__COMPONENT_INSTANCE, newComponentInstance, newComponentInstance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instance> getOwnedInstance() {
		if (ownedInstance == null) {
			ownedInstance = new EObjectContainmentWithInverseEList<Instance>(Instance.class, this, Common_behaviorPackage.INSTANCE__OWNED_INSTANCE, Common_behaviorPackage.INSTANCE__OWNER);
		}
		return ownedInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Instance getOwner() {
		if (eContainerFeatureID() != Common_behaviorPackage.INSTANCE__OWNER) return null;
		return (Instance)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOwner(Instance newOwner, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newOwner, Common_behaviorPackage.INSTANCE__OWNER, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOwner(Instance newOwner) {
		if (newOwner != eInternalContainer() || (eContainerFeatureID() != Common_behaviorPackage.INSTANCE__OWNER && newOwner != null)) {
			if (EcoreUtil.isAncestor(this, newOwner))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newOwner != null)
				msgs = ((InternalEObject)newOwner).eInverseAdd(this, Common_behaviorPackage.INSTANCE__OWNED_INSTANCE, Instance.class, msgs);
			msgs = basicSetOwner(newOwner, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Common_behaviorPackage.INSTANCE__OWNER, newOwner, newOwner));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Link> getOwnedLink() {
		if (ownedLink == null) {
			ownedLink = new EObjectContainmentWithInverseEList<Link>(Link.class, this, Common_behaviorPackage.INSTANCE__OWNED_LINK, Common_behaviorPackage.LINK__OWNER);
		}
		return ownedLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ClassifierRole> getPlayedRole() {
		if (playedRole == null) {
			playedRole = new EObjectWithInverseResolvingEList.ManyInverse<ClassifierRole>(ClassifierRole.class, this, Common_behaviorPackage.INSTANCE__PLAYED_ROLE, CollaborationsPackage.CLASSIFIER_ROLE__CONFORMING_INSTANCE);
		}
		return playedRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Common_behaviorPackage.INSTANCE__CLASSIFIER:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getClassifier()).basicAdd(otherEnd, msgs);
			case Common_behaviorPackage.INSTANCE__ATTRIBUTE_LINK:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAttributeLink()).basicAdd(otherEnd, msgs);
			case Common_behaviorPackage.INSTANCE__LINK_END:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getLinkEnd()).basicAdd(otherEnd, msgs);
			case Common_behaviorPackage.INSTANCE__SLOT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSlot()).basicAdd(otherEnd, msgs);
			case Common_behaviorPackage.INSTANCE__STIMULUS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getStimulus()).basicAdd(otherEnd, msgs);
			case Common_behaviorPackage.INSTANCE__COMPONENT_INSTANCE:
				if (componentInstance != null)
					msgs = ((InternalEObject)componentInstance).eInverseRemove(this, Common_behaviorPackage.COMPONENT_INSTANCE__RESIDENT, ComponentInstance.class, msgs);
				return basicSetComponentInstance((ComponentInstance)otherEnd, msgs);
			case Common_behaviorPackage.INSTANCE__OWNED_INSTANCE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOwnedInstance()).basicAdd(otherEnd, msgs);
			case Common_behaviorPackage.INSTANCE__OWNER:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetOwner((Instance)otherEnd, msgs);
			case Common_behaviorPackage.INSTANCE__OWNED_LINK:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOwnedLink()).basicAdd(otherEnd, msgs);
			case Common_behaviorPackage.INSTANCE__PLAYED_ROLE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPlayedRole()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Common_behaviorPackage.INSTANCE__CLASSIFIER:
				return ((InternalEList<?>)getClassifier()).basicRemove(otherEnd, msgs);
			case Common_behaviorPackage.INSTANCE__ATTRIBUTE_LINK:
				return ((InternalEList<?>)getAttributeLink()).basicRemove(otherEnd, msgs);
			case Common_behaviorPackage.INSTANCE__LINK_END:
				return ((InternalEList<?>)getLinkEnd()).basicRemove(otherEnd, msgs);
			case Common_behaviorPackage.INSTANCE__SLOT:
				return ((InternalEList<?>)getSlot()).basicRemove(otherEnd, msgs);
			case Common_behaviorPackage.INSTANCE__STIMULUS:
				return ((InternalEList<?>)getStimulus()).basicRemove(otherEnd, msgs);
			case Common_behaviorPackage.INSTANCE__COMPONENT_INSTANCE:
				return basicSetComponentInstance(null, msgs);
			case Common_behaviorPackage.INSTANCE__OWNED_INSTANCE:
				return ((InternalEList<?>)getOwnedInstance()).basicRemove(otherEnd, msgs);
			case Common_behaviorPackage.INSTANCE__OWNER:
				return basicSetOwner(null, msgs);
			case Common_behaviorPackage.INSTANCE__OWNED_LINK:
				return ((InternalEList<?>)getOwnedLink()).basicRemove(otherEnd, msgs);
			case Common_behaviorPackage.INSTANCE__PLAYED_ROLE:
				return ((InternalEList<?>)getPlayedRole()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case Common_behaviorPackage.INSTANCE__OWNER:
				return eInternalContainer().eInverseRemove(this, Common_behaviorPackage.INSTANCE__OWNED_INSTANCE, Instance.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Common_behaviorPackage.INSTANCE__CLASSIFIER:
				return getClassifier();
			case Common_behaviorPackage.INSTANCE__ATTRIBUTE_LINK:
				return getAttributeLink();
			case Common_behaviorPackage.INSTANCE__LINK_END:
				return getLinkEnd();
			case Common_behaviorPackage.INSTANCE__SLOT:
				return getSlot();
			case Common_behaviorPackage.INSTANCE__STIMULUS:
				return getStimulus();
			case Common_behaviorPackage.INSTANCE__COMPONENT_INSTANCE:
				if (resolve) return getComponentInstance();
				return basicGetComponentInstance();
			case Common_behaviorPackage.INSTANCE__OWNED_INSTANCE:
				return getOwnedInstance();
			case Common_behaviorPackage.INSTANCE__OWNER:
				return getOwner();
			case Common_behaviorPackage.INSTANCE__OWNED_LINK:
				return getOwnedLink();
			case Common_behaviorPackage.INSTANCE__PLAYED_ROLE:
				return getPlayedRole();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Common_behaviorPackage.INSTANCE__CLASSIFIER:
				getClassifier().clear();
				getClassifier().addAll((Collection<? extends Classifier>)newValue);
				return;
			case Common_behaviorPackage.INSTANCE__ATTRIBUTE_LINK:
				getAttributeLink().clear();
				getAttributeLink().addAll((Collection<? extends AttributeLink>)newValue);
				return;
			case Common_behaviorPackage.INSTANCE__LINK_END:
				getLinkEnd().clear();
				getLinkEnd().addAll((Collection<? extends LinkEnd>)newValue);
				return;
			case Common_behaviorPackage.INSTANCE__SLOT:
				getSlot().clear();
				getSlot().addAll((Collection<? extends AttributeLink>)newValue);
				return;
			case Common_behaviorPackage.INSTANCE__STIMULUS:
				getStimulus().clear();
				getStimulus().addAll((Collection<? extends Stimulus>)newValue);
				return;
			case Common_behaviorPackage.INSTANCE__COMPONENT_INSTANCE:
				setComponentInstance((ComponentInstance)newValue);
				return;
			case Common_behaviorPackage.INSTANCE__OWNED_INSTANCE:
				getOwnedInstance().clear();
				getOwnedInstance().addAll((Collection<? extends Instance>)newValue);
				return;
			case Common_behaviorPackage.INSTANCE__OWNER:
				setOwner((Instance)newValue);
				return;
			case Common_behaviorPackage.INSTANCE__OWNED_LINK:
				getOwnedLink().clear();
				getOwnedLink().addAll((Collection<? extends Link>)newValue);
				return;
			case Common_behaviorPackage.INSTANCE__PLAYED_ROLE:
				getPlayedRole().clear();
				getPlayedRole().addAll((Collection<? extends ClassifierRole>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Common_behaviorPackage.INSTANCE__CLASSIFIER:
				getClassifier().clear();
				return;
			case Common_behaviorPackage.INSTANCE__ATTRIBUTE_LINK:
				getAttributeLink().clear();
				return;
			case Common_behaviorPackage.INSTANCE__LINK_END:
				getLinkEnd().clear();
				return;
			case Common_behaviorPackage.INSTANCE__SLOT:
				getSlot().clear();
				return;
			case Common_behaviorPackage.INSTANCE__STIMULUS:
				getStimulus().clear();
				return;
			case Common_behaviorPackage.INSTANCE__COMPONENT_INSTANCE:
				setComponentInstance((ComponentInstance)null);
				return;
			case Common_behaviorPackage.INSTANCE__OWNED_INSTANCE:
				getOwnedInstance().clear();
				return;
			case Common_behaviorPackage.INSTANCE__OWNER:
				setOwner((Instance)null);
				return;
			case Common_behaviorPackage.INSTANCE__OWNED_LINK:
				getOwnedLink().clear();
				return;
			case Common_behaviorPackage.INSTANCE__PLAYED_ROLE:
				getPlayedRole().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Common_behaviorPackage.INSTANCE__CLASSIFIER:
				return classifier != null && !classifier.isEmpty();
			case Common_behaviorPackage.INSTANCE__ATTRIBUTE_LINK:
				return attributeLink != null && !attributeLink.isEmpty();
			case Common_behaviorPackage.INSTANCE__LINK_END:
				return linkEnd != null && !linkEnd.isEmpty();
			case Common_behaviorPackage.INSTANCE__SLOT:
				return slot != null && !slot.isEmpty();
			case Common_behaviorPackage.INSTANCE__STIMULUS:
				return stimulus != null && !stimulus.isEmpty();
			case Common_behaviorPackage.INSTANCE__COMPONENT_INSTANCE:
				return componentInstance != null;
			case Common_behaviorPackage.INSTANCE__OWNED_INSTANCE:
				return ownedInstance != null && !ownedInstance.isEmpty();
			case Common_behaviorPackage.INSTANCE__OWNER:
				return getOwner() != null;
			case Common_behaviorPackage.INSTANCE__OWNED_LINK:
				return ownedLink != null && !ownedLink.isEmpty();
			case Common_behaviorPackage.INSTANCE__PLAYED_ROLE:
				return playedRole != null && !playedRole.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //InstanceImpl
