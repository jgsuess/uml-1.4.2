/**
 * <copyright>
 * </copyright>
 *
 * $Id: Activity_graphsFactoryImpl.java,v 1.1 2012/04/23 09:31:37 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.activity_graphs.impl;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Activity_graphsFactoryImpl extends EFactoryImpl implements Activity_graphsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Activity_graphsFactory init() {
		try {
			Activity_graphsFactory theActivity_graphsFactory = (Activity_graphsFactory)EPackage.Registry.INSTANCE.getEFactory("http://jgsuess.net/uml14/activity_graphs"); 
			if (theActivity_graphsFactory != null) {
				return theActivity_graphsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Activity_graphsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Activity_graphsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Activity_graphsPackage.ACTIVITY_GRAPH: return createActivityGraph();
			case Activity_graphsPackage.PARTITION: return createPartition();
			case Activity_graphsPackage.SUBACTIVITY_STATE: return createSubactivityState();
			case Activity_graphsPackage.ACTION_STATE: return createActionState();
			case Activity_graphsPackage.CALL_STATE: return createCallState();
			case Activity_graphsPackage.OBJECT_FLOW_STATE: return createObjectFlowState();
			case Activity_graphsPackage.CLASSIFIER_IN_STATE: return createClassifierInState();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityGraph createActivityGraph() {
		ActivityGraphImpl activityGraph = new ActivityGraphImpl();
		return activityGraph;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Partition createPartition() {
		PartitionImpl partition = new PartitionImpl();
		return partition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubactivityState createSubactivityState() {
		SubactivityStateImpl subactivityState = new SubactivityStateImpl();
		return subactivityState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionState createActionState() {
		ActionStateImpl actionState = new ActionStateImpl();
		return actionState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallState createCallState() {
		CallStateImpl callState = new CallStateImpl();
		return callState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectFlowState createObjectFlowState() {
		ObjectFlowStateImpl objectFlowState = new ObjectFlowStateImpl();
		return objectFlowState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassifierInState createClassifierInState() {
		ClassifierInStateImpl classifierInState = new ClassifierInStateImpl();
		return classifierInState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Activity_graphsPackage getActivity_graphsPackage() {
		return (Activity_graphsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Activity_graphsPackage getPackage() {
		return Activity_graphsPackage.eINSTANCE;
	}

} //Activity_graphsFactoryImpl
