/**
 * <copyright>
 * </copyright>
 *
 * $Id: CallState.java,v 1.1 2012/04/23 09:31:22 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.activity_graphs;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Call State</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage#getCallState()
 * @model
 * @generated
 */
public interface CallState extends ActionState {
} // CallState
