/**
 * <copyright>
 * </copyright>
 *
 * $Id: Activity_graphsPackage.java,v 1.1 2012/04/23 09:31:22 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.activity_graphs;

import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage;

import net.jgsuess.uml14.foundation.core.CorePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsFactory
 * @model kind="package"
 * @generated
 */
public interface Activity_graphsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "activity_graphs";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://jgsuess.net/uml14/activity_graphs";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "activity_graphs";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Activity_graphsPackage eINSTANCE = net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.Activity_graphsPackageImpl.init();

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.ActivityGraphImpl <em>Activity Graph</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.ActivityGraphImpl
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.Activity_graphsPackageImpl#getActivityGraph()
	 * @generated
	 */
	int ACTIVITY_GRAPH = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__NAME = State_machinesPackage.STATE_MACHINE__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__VISIBILITY = State_machinesPackage.STATE_MACHINE__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__IS_SPECIFICATION = State_machinesPackage.STATE_MACHINE__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__NAMESPACE = State_machinesPackage.STATE_MACHINE__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__CLIENT_DEPENDENCY = State_machinesPackage.STATE_MACHINE__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__CONSTRAINT = State_machinesPackage.STATE_MACHINE__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__SUPPLIER_DEPENDENCY = State_machinesPackage.STATE_MACHINE__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__PRESENTATION = State_machinesPackage.STATE_MACHINE__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__TARGET_FLOW = State_machinesPackage.STATE_MACHINE__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__SOURCE_FLOW = State_machinesPackage.STATE_MACHINE__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__DEFAULTED_PARAMETER = State_machinesPackage.STATE_MACHINE__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__COMMENT = State_machinesPackage.STATE_MACHINE__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__ELEMENT_RESIDENCE = State_machinesPackage.STATE_MACHINE__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__TEMPLATE_PARAMETER = State_machinesPackage.STATE_MACHINE__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__PARAMETER_TEMPLATE = State_machinesPackage.STATE_MACHINE__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__STEREOTYPE = State_machinesPackage.STATE_MACHINE__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__TAGGED_VALUE = State_machinesPackage.STATE_MACHINE__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__REFERENCE_TAG = State_machinesPackage.STATE_MACHINE__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__TEMPLATE_ARGUMENT = State_machinesPackage.STATE_MACHINE__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__BEHAVIOR = State_machinesPackage.STATE_MACHINE__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__CLASSIFIER_ROLE = State_machinesPackage.STATE_MACHINE__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__COLLABORATION = State_machinesPackage.STATE_MACHINE__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__COLLABORATION_INSTANCE_SET = State_machinesPackage.STATE_MACHINE__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__PARTITION = State_machinesPackage.STATE_MACHINE__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__ELEMENT_IMPORT = State_machinesPackage.STATE_MACHINE__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__CONTEXT = State_machinesPackage.STATE_MACHINE__CONTEXT;

	/**
	 * The feature id for the '<em><b>Top</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__TOP = State_machinesPackage.STATE_MACHINE__TOP;

	/**
	 * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__TRANSITIONS = State_machinesPackage.STATE_MACHINE__TRANSITIONS;

	/**
	 * The feature id for the '<em><b>Submachine State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH__SUBMACHINE_STATE = State_machinesPackage.STATE_MACHINE__SUBMACHINE_STATE;

	/**
	 * The number of structural features of the '<em>Activity Graph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_GRAPH_FEATURE_COUNT = State_machinesPackage.STATE_MACHINE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.PartitionImpl <em>Partition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.PartitionImpl
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.Activity_graphsPackageImpl#getPartition()
	 * @generated
	 */
	int PARTITION = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__IS_SPECIFICATION = CorePackage.MODEL_ELEMENT__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__PRESENTATION = CorePackage.MODEL_ELEMENT__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__TARGET_FLOW = CorePackage.MODEL_ELEMENT__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__SOURCE_FLOW = CorePackage.MODEL_ELEMENT__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__DEFAULTED_PARAMETER = CorePackage.MODEL_ELEMENT__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__COMMENT = CorePackage.MODEL_ELEMENT__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__ELEMENT_RESIDENCE = CorePackage.MODEL_ELEMENT__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__TEMPLATE_PARAMETER = CorePackage.MODEL_ELEMENT__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__PARAMETER_TEMPLATE = CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__REFERENCE_TAG = CorePackage.MODEL_ELEMENT__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__TEMPLATE_ARGUMENT = CorePackage.MODEL_ELEMENT__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__BEHAVIOR = CorePackage.MODEL_ELEMENT__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__CLASSIFIER_ROLE = CorePackage.MODEL_ELEMENT__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__COLLABORATION = CorePackage.MODEL_ELEMENT__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__COLLABORATION_INSTANCE_SET = CorePackage.MODEL_ELEMENT__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__PARTITION = CorePackage.MODEL_ELEMENT__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__ELEMENT_IMPORT = CorePackage.MODEL_ELEMENT__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Contents</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION__CONTENTS = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Partition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTITION_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.SubactivityStateImpl <em>Subactivity State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.SubactivityStateImpl
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.Activity_graphsPackageImpl#getSubactivityState()
	 * @generated
	 */
	int SUBACTIVITY_STATE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__NAME = State_machinesPackage.SUBMACHINE_STATE__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__VISIBILITY = State_machinesPackage.SUBMACHINE_STATE__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__IS_SPECIFICATION = State_machinesPackage.SUBMACHINE_STATE__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__NAMESPACE = State_machinesPackage.SUBMACHINE_STATE__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__CLIENT_DEPENDENCY = State_machinesPackage.SUBMACHINE_STATE__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__CONSTRAINT = State_machinesPackage.SUBMACHINE_STATE__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__SUPPLIER_DEPENDENCY = State_machinesPackage.SUBMACHINE_STATE__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__PRESENTATION = State_machinesPackage.SUBMACHINE_STATE__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__TARGET_FLOW = State_machinesPackage.SUBMACHINE_STATE__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__SOURCE_FLOW = State_machinesPackage.SUBMACHINE_STATE__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__DEFAULTED_PARAMETER = State_machinesPackage.SUBMACHINE_STATE__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__COMMENT = State_machinesPackage.SUBMACHINE_STATE__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__ELEMENT_RESIDENCE = State_machinesPackage.SUBMACHINE_STATE__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__TEMPLATE_PARAMETER = State_machinesPackage.SUBMACHINE_STATE__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__PARAMETER_TEMPLATE = State_machinesPackage.SUBMACHINE_STATE__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__STEREOTYPE = State_machinesPackage.SUBMACHINE_STATE__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__TAGGED_VALUE = State_machinesPackage.SUBMACHINE_STATE__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__REFERENCE_TAG = State_machinesPackage.SUBMACHINE_STATE__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__TEMPLATE_ARGUMENT = State_machinesPackage.SUBMACHINE_STATE__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__BEHAVIOR = State_machinesPackage.SUBMACHINE_STATE__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__CLASSIFIER_ROLE = State_machinesPackage.SUBMACHINE_STATE__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__COLLABORATION = State_machinesPackage.SUBMACHINE_STATE__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__COLLABORATION_INSTANCE_SET = State_machinesPackage.SUBMACHINE_STATE__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__PARTITION = State_machinesPackage.SUBMACHINE_STATE__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__ELEMENT_IMPORT = State_machinesPackage.SUBMACHINE_STATE__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__CONTAINER = State_machinesPackage.SUBMACHINE_STATE__CONTAINER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__OUTGOING = State_machinesPackage.SUBMACHINE_STATE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__INCOMING = State_machinesPackage.SUBMACHINE_STATE__INCOMING;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__ENTRY = State_machinesPackage.SUBMACHINE_STATE__ENTRY;

	/**
	 * The feature id for the '<em><b>State Machine</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__STATE_MACHINE = State_machinesPackage.SUBMACHINE_STATE__STATE_MACHINE;

	/**
	 * The feature id for the '<em><b>Deferrable Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__DEFERRABLE_EVENT = State_machinesPackage.SUBMACHINE_STATE__DEFERRABLE_EVENT;

	/**
	 * The feature id for the '<em><b>Internal Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__INTERNAL_TRANSITION = State_machinesPackage.SUBMACHINE_STATE__INTERNAL_TRANSITION;

	/**
	 * The feature id for the '<em><b>Classifier In State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__CLASSIFIER_IN_STATE = State_machinesPackage.SUBMACHINE_STATE__CLASSIFIER_IN_STATE;

	/**
	 * The feature id for the '<em><b>Is Concurrent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__IS_CONCURRENT = State_machinesPackage.SUBMACHINE_STATE__IS_CONCURRENT;

	/**
	 * The feature id for the '<em><b>Subvertex</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__SUBVERTEX = State_machinesPackage.SUBMACHINE_STATE__SUBVERTEX;

	/**
	 * The feature id for the '<em><b>Submachine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__SUBMACHINE = State_machinesPackage.SUBMACHINE_STATE__SUBMACHINE;

	/**
	 * The feature id for the '<em><b>Is Dynamic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__IS_DYNAMIC = State_machinesPackage.SUBMACHINE_STATE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dynamic Arguments</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__DYNAMIC_ARGUMENTS = State_machinesPackage.SUBMACHINE_STATE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Dynamic Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE__DYNAMIC_MULTIPLICITY = State_machinesPackage.SUBMACHINE_STATE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Subactivity State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBACTIVITY_STATE_FEATURE_COUNT = State_machinesPackage.SUBMACHINE_STATE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.ActionStateImpl <em>Action State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.ActionStateImpl
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.Activity_graphsPackageImpl#getActionState()
	 * @generated
	 */
	int ACTION_STATE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__NAME = State_machinesPackage.SIMPLE_STATE__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__VISIBILITY = State_machinesPackage.SIMPLE_STATE__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__IS_SPECIFICATION = State_machinesPackage.SIMPLE_STATE__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__NAMESPACE = State_machinesPackage.SIMPLE_STATE__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__CLIENT_DEPENDENCY = State_machinesPackage.SIMPLE_STATE__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__CONSTRAINT = State_machinesPackage.SIMPLE_STATE__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__SUPPLIER_DEPENDENCY = State_machinesPackage.SIMPLE_STATE__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__PRESENTATION = State_machinesPackage.SIMPLE_STATE__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__TARGET_FLOW = State_machinesPackage.SIMPLE_STATE__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__SOURCE_FLOW = State_machinesPackage.SIMPLE_STATE__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__DEFAULTED_PARAMETER = State_machinesPackage.SIMPLE_STATE__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__COMMENT = State_machinesPackage.SIMPLE_STATE__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__ELEMENT_RESIDENCE = State_machinesPackage.SIMPLE_STATE__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__TEMPLATE_PARAMETER = State_machinesPackage.SIMPLE_STATE__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__PARAMETER_TEMPLATE = State_machinesPackage.SIMPLE_STATE__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__STEREOTYPE = State_machinesPackage.SIMPLE_STATE__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__TAGGED_VALUE = State_machinesPackage.SIMPLE_STATE__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__REFERENCE_TAG = State_machinesPackage.SIMPLE_STATE__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__TEMPLATE_ARGUMENT = State_machinesPackage.SIMPLE_STATE__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__BEHAVIOR = State_machinesPackage.SIMPLE_STATE__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__CLASSIFIER_ROLE = State_machinesPackage.SIMPLE_STATE__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__COLLABORATION = State_machinesPackage.SIMPLE_STATE__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__COLLABORATION_INSTANCE_SET = State_machinesPackage.SIMPLE_STATE__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__PARTITION = State_machinesPackage.SIMPLE_STATE__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__ELEMENT_IMPORT = State_machinesPackage.SIMPLE_STATE__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__CONTAINER = State_machinesPackage.SIMPLE_STATE__CONTAINER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__OUTGOING = State_machinesPackage.SIMPLE_STATE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__INCOMING = State_machinesPackage.SIMPLE_STATE__INCOMING;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__ENTRY = State_machinesPackage.SIMPLE_STATE__ENTRY;

	/**
	 * The feature id for the '<em><b>State Machine</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__STATE_MACHINE = State_machinesPackage.SIMPLE_STATE__STATE_MACHINE;

	/**
	 * The feature id for the '<em><b>Deferrable Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__DEFERRABLE_EVENT = State_machinesPackage.SIMPLE_STATE__DEFERRABLE_EVENT;

	/**
	 * The feature id for the '<em><b>Internal Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__INTERNAL_TRANSITION = State_machinesPackage.SIMPLE_STATE__INTERNAL_TRANSITION;

	/**
	 * The feature id for the '<em><b>Classifier In State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__CLASSIFIER_IN_STATE = State_machinesPackage.SIMPLE_STATE__CLASSIFIER_IN_STATE;

	/**
	 * The feature id for the '<em><b>Is Dynamic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__IS_DYNAMIC = State_machinesPackage.SIMPLE_STATE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dynamic Arguments</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__DYNAMIC_ARGUMENTS = State_machinesPackage.SIMPLE_STATE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Dynamic Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE__DYNAMIC_MULTIPLICITY = State_machinesPackage.SIMPLE_STATE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Action State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_STATE_FEATURE_COUNT = State_machinesPackage.SIMPLE_STATE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.CallStateImpl <em>Call State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.CallStateImpl
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.Activity_graphsPackageImpl#getCallState()
	 * @generated
	 */
	int CALL_STATE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__NAME = ACTION_STATE__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__VISIBILITY = ACTION_STATE__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__IS_SPECIFICATION = ACTION_STATE__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__NAMESPACE = ACTION_STATE__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__CLIENT_DEPENDENCY = ACTION_STATE__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__CONSTRAINT = ACTION_STATE__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__SUPPLIER_DEPENDENCY = ACTION_STATE__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__PRESENTATION = ACTION_STATE__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__TARGET_FLOW = ACTION_STATE__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__SOURCE_FLOW = ACTION_STATE__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__DEFAULTED_PARAMETER = ACTION_STATE__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__COMMENT = ACTION_STATE__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__ELEMENT_RESIDENCE = ACTION_STATE__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__TEMPLATE_PARAMETER = ACTION_STATE__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__PARAMETER_TEMPLATE = ACTION_STATE__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__STEREOTYPE = ACTION_STATE__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__TAGGED_VALUE = ACTION_STATE__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__REFERENCE_TAG = ACTION_STATE__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__TEMPLATE_ARGUMENT = ACTION_STATE__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__BEHAVIOR = ACTION_STATE__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__CLASSIFIER_ROLE = ACTION_STATE__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__COLLABORATION = ACTION_STATE__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__COLLABORATION_INSTANCE_SET = ACTION_STATE__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__PARTITION = ACTION_STATE__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__ELEMENT_IMPORT = ACTION_STATE__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__CONTAINER = ACTION_STATE__CONTAINER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__OUTGOING = ACTION_STATE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__INCOMING = ACTION_STATE__INCOMING;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__ENTRY = ACTION_STATE__ENTRY;

	/**
	 * The feature id for the '<em><b>State Machine</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__STATE_MACHINE = ACTION_STATE__STATE_MACHINE;

	/**
	 * The feature id for the '<em><b>Deferrable Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__DEFERRABLE_EVENT = ACTION_STATE__DEFERRABLE_EVENT;

	/**
	 * The feature id for the '<em><b>Internal Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__INTERNAL_TRANSITION = ACTION_STATE__INTERNAL_TRANSITION;

	/**
	 * The feature id for the '<em><b>Classifier In State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__CLASSIFIER_IN_STATE = ACTION_STATE__CLASSIFIER_IN_STATE;

	/**
	 * The feature id for the '<em><b>Is Dynamic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__IS_DYNAMIC = ACTION_STATE__IS_DYNAMIC;

	/**
	 * The feature id for the '<em><b>Dynamic Arguments</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__DYNAMIC_ARGUMENTS = ACTION_STATE__DYNAMIC_ARGUMENTS;

	/**
	 * The feature id for the '<em><b>Dynamic Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE__DYNAMIC_MULTIPLICITY = ACTION_STATE__DYNAMIC_MULTIPLICITY;

	/**
	 * The number of structural features of the '<em>Call State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_STATE_FEATURE_COUNT = ACTION_STATE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.ObjectFlowStateImpl <em>Object Flow State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.ObjectFlowStateImpl
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.Activity_graphsPackageImpl#getObjectFlowState()
	 * @generated
	 */
	int OBJECT_FLOW_STATE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__NAME = State_machinesPackage.SIMPLE_STATE__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__VISIBILITY = State_machinesPackage.SIMPLE_STATE__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__IS_SPECIFICATION = State_machinesPackage.SIMPLE_STATE__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__NAMESPACE = State_machinesPackage.SIMPLE_STATE__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__CLIENT_DEPENDENCY = State_machinesPackage.SIMPLE_STATE__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__CONSTRAINT = State_machinesPackage.SIMPLE_STATE__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__SUPPLIER_DEPENDENCY = State_machinesPackage.SIMPLE_STATE__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__PRESENTATION = State_machinesPackage.SIMPLE_STATE__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__TARGET_FLOW = State_machinesPackage.SIMPLE_STATE__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__SOURCE_FLOW = State_machinesPackage.SIMPLE_STATE__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__DEFAULTED_PARAMETER = State_machinesPackage.SIMPLE_STATE__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__COMMENT = State_machinesPackage.SIMPLE_STATE__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__ELEMENT_RESIDENCE = State_machinesPackage.SIMPLE_STATE__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__TEMPLATE_PARAMETER = State_machinesPackage.SIMPLE_STATE__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__PARAMETER_TEMPLATE = State_machinesPackage.SIMPLE_STATE__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__STEREOTYPE = State_machinesPackage.SIMPLE_STATE__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__TAGGED_VALUE = State_machinesPackage.SIMPLE_STATE__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__REFERENCE_TAG = State_machinesPackage.SIMPLE_STATE__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__TEMPLATE_ARGUMENT = State_machinesPackage.SIMPLE_STATE__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__BEHAVIOR = State_machinesPackage.SIMPLE_STATE__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__CLASSIFIER_ROLE = State_machinesPackage.SIMPLE_STATE__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__COLLABORATION = State_machinesPackage.SIMPLE_STATE__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__COLLABORATION_INSTANCE_SET = State_machinesPackage.SIMPLE_STATE__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__PARTITION = State_machinesPackage.SIMPLE_STATE__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__ELEMENT_IMPORT = State_machinesPackage.SIMPLE_STATE__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__CONTAINER = State_machinesPackage.SIMPLE_STATE__CONTAINER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__OUTGOING = State_machinesPackage.SIMPLE_STATE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__INCOMING = State_machinesPackage.SIMPLE_STATE__INCOMING;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__ENTRY = State_machinesPackage.SIMPLE_STATE__ENTRY;

	/**
	 * The feature id for the '<em><b>State Machine</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__STATE_MACHINE = State_machinesPackage.SIMPLE_STATE__STATE_MACHINE;

	/**
	 * The feature id for the '<em><b>Deferrable Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__DEFERRABLE_EVENT = State_machinesPackage.SIMPLE_STATE__DEFERRABLE_EVENT;

	/**
	 * The feature id for the '<em><b>Internal Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__INTERNAL_TRANSITION = State_machinesPackage.SIMPLE_STATE__INTERNAL_TRANSITION;

	/**
	 * The feature id for the '<em><b>Classifier In State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__CLASSIFIER_IN_STATE = State_machinesPackage.SIMPLE_STATE__CLASSIFIER_IN_STATE;

	/**
	 * The feature id for the '<em><b>Is Synch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__IS_SYNCH = State_machinesPackage.SIMPLE_STATE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__PARAMETER = State_machinesPackage.SIMPLE_STATE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE__TYPE = State_machinesPackage.SIMPLE_STATE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Object Flow State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FLOW_STATE_FEATURE_COUNT = State_machinesPackage.SIMPLE_STATE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.ClassifierInStateImpl <em>Classifier In State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.ClassifierInStateImpl
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.Activity_graphsPackageImpl#getClassifierInState()
	 * @generated
	 */
	int CLASSIFIER_IN_STATE = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__NAME = CorePackage.CLASSIFIER__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__VISIBILITY = CorePackage.CLASSIFIER__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__IS_SPECIFICATION = CorePackage.CLASSIFIER__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__NAMESPACE = CorePackage.CLASSIFIER__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__CLIENT_DEPENDENCY = CorePackage.CLASSIFIER__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__CONSTRAINT = CorePackage.CLASSIFIER__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__SUPPLIER_DEPENDENCY = CorePackage.CLASSIFIER__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__PRESENTATION = CorePackage.CLASSIFIER__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__TARGET_FLOW = CorePackage.CLASSIFIER__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__SOURCE_FLOW = CorePackage.CLASSIFIER__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__DEFAULTED_PARAMETER = CorePackage.CLASSIFIER__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__COMMENT = CorePackage.CLASSIFIER__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__ELEMENT_RESIDENCE = CorePackage.CLASSIFIER__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__TEMPLATE_PARAMETER = CorePackage.CLASSIFIER__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__PARAMETER_TEMPLATE = CorePackage.CLASSIFIER__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__STEREOTYPE = CorePackage.CLASSIFIER__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__TAGGED_VALUE = CorePackage.CLASSIFIER__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__REFERENCE_TAG = CorePackage.CLASSIFIER__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__TEMPLATE_ARGUMENT = CorePackage.CLASSIFIER__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__BEHAVIOR = CorePackage.CLASSIFIER__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__CLASSIFIER_ROLE = CorePackage.CLASSIFIER__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__COLLABORATION = CorePackage.CLASSIFIER__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__COLLABORATION_INSTANCE_SET = CorePackage.CLASSIFIER__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__PARTITION = CorePackage.CLASSIFIER__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__ELEMENT_IMPORT = CorePackage.CLASSIFIER__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Is Root</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__IS_ROOT = CorePackage.CLASSIFIER__IS_ROOT;

	/**
	 * The feature id for the '<em><b>Is Leaf</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__IS_LEAF = CorePackage.CLASSIFIER__IS_LEAF;

	/**
	 * The feature id for the '<em><b>Is Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__IS_ABSTRACT = CorePackage.CLASSIFIER__IS_ABSTRACT;

	/**
	 * The feature id for the '<em><b>Generalization</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__GENERALIZATION = CorePackage.CLASSIFIER__GENERALIZATION;

	/**
	 * The feature id for the '<em><b>Specialization</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__SPECIALIZATION = CorePackage.CLASSIFIER__SPECIALIZATION;

	/**
	 * The feature id for the '<em><b>Owned Element</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__OWNED_ELEMENT = CorePackage.CLASSIFIER__OWNED_ELEMENT;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__FEATURE = CorePackage.CLASSIFIER__FEATURE;

	/**
	 * The feature id for the '<em><b>Typed Feature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__TYPED_FEATURE = CorePackage.CLASSIFIER__TYPED_FEATURE;

	/**
	 * The feature id for the '<em><b>Typed Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__TYPED_PARAMETER = CorePackage.CLASSIFIER__TYPED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Association</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__ASSOCIATION = CorePackage.CLASSIFIER__ASSOCIATION;

	/**
	 * The feature id for the '<em><b>Specified End</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__SPECIFIED_END = CorePackage.CLASSIFIER__SPECIFIED_END;

	/**
	 * The feature id for the '<em><b>Powertype Range</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__POWERTYPE_RANGE = CorePackage.CLASSIFIER__POWERTYPE_RANGE;

	/**
	 * The feature id for the '<em><b>Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__INSTANCE = CorePackage.CLASSIFIER__INSTANCE;

	/**
	 * The feature id for the '<em><b>Create Action</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__CREATE_ACTION = CorePackage.CLASSIFIER__CREATE_ACTION;

	/**
	 * The feature id for the '<em><b>Classifier In State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__CLASSIFIER_IN_STATE = CorePackage.CLASSIFIER__CLASSIFIER_IN_STATE;

	/**
	 * The feature id for the '<em><b>Object Flow State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__OBJECT_FLOW_STATE = CorePackage.CLASSIFIER__OBJECT_FLOW_STATE;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__TYPE = CorePackage.CLASSIFIER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>In State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE__IN_STATE = CorePackage.CLASSIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Classifier In State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASSIFIER_IN_STATE_FEATURE_COUNT = CorePackage.CLASSIFIER_FEATURE_COUNT + 2;


	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ActivityGraph <em>Activity Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Activity Graph</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.ActivityGraph
	 * @generated
	 */
	EClass getActivityGraph();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.Partition <em>Partition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Partition</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.Partition
	 * @generated
	 */
	EClass getPartition();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.Partition#getContents <em>Contents</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Contents</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.Partition#getContents()
	 * @see #getPartition()
	 * @generated
	 */
	EReference getPartition_Contents();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.SubactivityState <em>Subactivity State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Subactivity State</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.SubactivityState
	 * @generated
	 */
	EClass getSubactivityState();

	/**
	 * Returns the meta object for the attribute '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.SubactivityState#isIsDynamic <em>Is Dynamic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Dynamic</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.SubactivityState#isIsDynamic()
	 * @see #getSubactivityState()
	 * @generated
	 */
	EAttribute getSubactivityState_IsDynamic();

	/**
	 * Returns the meta object for the containment reference '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.SubactivityState#getDynamicArguments <em>Dynamic Arguments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Dynamic Arguments</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.SubactivityState#getDynamicArguments()
	 * @see #getSubactivityState()
	 * @generated
	 */
	EReference getSubactivityState_DynamicArguments();

	/**
	 * Returns the meta object for the containment reference '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.SubactivityState#getDynamicMultiplicity <em>Dynamic Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Dynamic Multiplicity</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.SubactivityState#getDynamicMultiplicity()
	 * @see #getSubactivityState()
	 * @generated
	 */
	EReference getSubactivityState_DynamicMultiplicity();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ActionState <em>Action State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action State</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.ActionState
	 * @generated
	 */
	EClass getActionState();

	/**
	 * Returns the meta object for the attribute '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ActionState#isIsDynamic <em>Is Dynamic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Dynamic</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.ActionState#isIsDynamic()
	 * @see #getActionState()
	 * @generated
	 */
	EAttribute getActionState_IsDynamic();

	/**
	 * Returns the meta object for the containment reference '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ActionState#getDynamicArguments <em>Dynamic Arguments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Dynamic Arguments</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.ActionState#getDynamicArguments()
	 * @see #getActionState()
	 * @generated
	 */
	EReference getActionState_DynamicArguments();

	/**
	 * Returns the meta object for the containment reference '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ActionState#getDynamicMultiplicity <em>Dynamic Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Dynamic Multiplicity</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.ActionState#getDynamicMultiplicity()
	 * @see #getActionState()
	 * @generated
	 */
	EReference getActionState_DynamicMultiplicity();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.CallState <em>Call State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Call State</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.CallState
	 * @generated
	 */
	EClass getCallState();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState <em>Object Flow State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Flow State</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState
	 * @generated
	 */
	EClass getObjectFlowState();

	/**
	 * Returns the meta object for the attribute '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState#isIsSynch <em>Is Synch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Synch</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState#isIsSynch()
	 * @see #getObjectFlowState()
	 * @generated
	 */
	EAttribute getObjectFlowState_IsSynch();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Parameter</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState#getParameter()
	 * @see #getObjectFlowState()
	 * @generated
	 */
	EReference getObjectFlowState_Parameter();

	/**
	 * Returns the meta object for the reference '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState#getType()
	 * @see #getObjectFlowState()
	 * @generated
	 */
	EReference getObjectFlowState_Type();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState <em>Classifier In State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Classifier In State</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState
	 * @generated
	 */
	EClass getClassifierInState();

	/**
	 * Returns the meta object for the reference '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState#getType()
	 * @see #getClassifierInState()
	 * @generated
	 */
	EReference getClassifierInState_Type();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState#getInState <em>In State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>In State</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState#getInState()
	 * @see #getClassifierInState()
	 * @generated
	 */
	EReference getClassifierInState_InState();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Activity_graphsFactory getActivity_graphsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.ActivityGraphImpl <em>Activity Graph</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.ActivityGraphImpl
		 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.Activity_graphsPackageImpl#getActivityGraph()
		 * @generated
		 */
		EClass ACTIVITY_GRAPH = eINSTANCE.getActivityGraph();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.PartitionImpl <em>Partition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.PartitionImpl
		 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.Activity_graphsPackageImpl#getPartition()
		 * @generated
		 */
		EClass PARTITION = eINSTANCE.getPartition();

		/**
		 * The meta object literal for the '<em><b>Contents</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARTITION__CONTENTS = eINSTANCE.getPartition_Contents();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.SubactivityStateImpl <em>Subactivity State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.SubactivityStateImpl
		 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.Activity_graphsPackageImpl#getSubactivityState()
		 * @generated
		 */
		EClass SUBACTIVITY_STATE = eINSTANCE.getSubactivityState();

		/**
		 * The meta object literal for the '<em><b>Is Dynamic</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUBACTIVITY_STATE__IS_DYNAMIC = eINSTANCE.getSubactivityState_IsDynamic();

		/**
		 * The meta object literal for the '<em><b>Dynamic Arguments</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBACTIVITY_STATE__DYNAMIC_ARGUMENTS = eINSTANCE.getSubactivityState_DynamicArguments();

		/**
		 * The meta object literal for the '<em><b>Dynamic Multiplicity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBACTIVITY_STATE__DYNAMIC_MULTIPLICITY = eINSTANCE.getSubactivityState_DynamicMultiplicity();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.ActionStateImpl <em>Action State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.ActionStateImpl
		 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.Activity_graphsPackageImpl#getActionState()
		 * @generated
		 */
		EClass ACTION_STATE = eINSTANCE.getActionState();

		/**
		 * The meta object literal for the '<em><b>Is Dynamic</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION_STATE__IS_DYNAMIC = eINSTANCE.getActionState_IsDynamic();

		/**
		 * The meta object literal for the '<em><b>Dynamic Arguments</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION_STATE__DYNAMIC_ARGUMENTS = eINSTANCE.getActionState_DynamicArguments();

		/**
		 * The meta object literal for the '<em><b>Dynamic Multiplicity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION_STATE__DYNAMIC_MULTIPLICITY = eINSTANCE.getActionState_DynamicMultiplicity();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.CallStateImpl <em>Call State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.CallStateImpl
		 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.Activity_graphsPackageImpl#getCallState()
		 * @generated
		 */
		EClass CALL_STATE = eINSTANCE.getCallState();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.ObjectFlowStateImpl <em>Object Flow State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.ObjectFlowStateImpl
		 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.Activity_graphsPackageImpl#getObjectFlowState()
		 * @generated
		 */
		EClass OBJECT_FLOW_STATE = eINSTANCE.getObjectFlowState();

		/**
		 * The meta object literal for the '<em><b>Is Synch</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBJECT_FLOW_STATE__IS_SYNCH = eINSTANCE.getObjectFlowState_IsSynch();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_FLOW_STATE__PARAMETER = eINSTANCE.getObjectFlowState_Parameter();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_FLOW_STATE__TYPE = eINSTANCE.getObjectFlowState_Type();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.ClassifierInStateImpl <em>Classifier In State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.ClassifierInStateImpl
		 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.Activity_graphsPackageImpl#getClassifierInState()
		 * @generated
		 */
		EClass CLASSIFIER_IN_STATE = eINSTANCE.getClassifierInState();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASSIFIER_IN_STATE__TYPE = eINSTANCE.getClassifierInState_Type();

		/**
		 * The meta object literal for the '<em><b>In State</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASSIFIER_IN_STATE__IN_STATE = eINSTANCE.getClassifierInState_InState();

	}

} //Activity_graphsPackage
