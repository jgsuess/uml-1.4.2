/**
 * <copyright>
 * </copyright>
 *
 * $Id: CallStateImpl.java,v 1.1 2012/04/23 09:31:37 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.activity_graphs.impl;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage;
import net.jgsuess.uml14.behavioral_elements.activity_graphs.CallState;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Call State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class CallStateImpl extends ActionStateImpl implements CallState {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CallStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Activity_graphsPackage.Literals.CALL_STATE;
	}

} //CallStateImpl
