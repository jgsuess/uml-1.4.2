/**
 * <copyright>
 * </copyright>
 *
 * $Id: ActionStateImpl.java,v 1.1 2012/04/23 09:31:37 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.activity_graphs.impl;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.ActionState;
import net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage;

import net.jgsuess.uml14.behavioral_elements.state_machines.impl.SimpleStateImpl;

import net.jgsuess.uml14.foundation.data_types.ArgListsExpression;
import net.jgsuess.uml14.foundation.data_types.Multiplicity;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Action State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.ActionStateImpl#isIsDynamic <em>Is Dynamic</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.ActionStateImpl#getDynamicArguments <em>Dynamic Arguments</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.ActionStateImpl#getDynamicMultiplicity <em>Dynamic Multiplicity</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ActionStateImpl extends SimpleStateImpl implements ActionState {
	/**
	 * The default value of the '{@link #isIsDynamic() <em>Is Dynamic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsDynamic()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_DYNAMIC_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsDynamic() <em>Is Dynamic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsDynamic()
	 * @generated
	 * @ordered
	 */
	protected boolean isDynamic = IS_DYNAMIC_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDynamicArguments() <em>Dynamic Arguments</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDynamicArguments()
	 * @generated
	 * @ordered
	 */
	protected ArgListsExpression dynamicArguments;

	/**
	 * The cached value of the '{@link #getDynamicMultiplicity() <em>Dynamic Multiplicity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDynamicMultiplicity()
	 * @generated
	 * @ordered
	 */
	protected Multiplicity dynamicMultiplicity;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActionStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Activity_graphsPackage.Literals.ACTION_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsDynamic() {
		return isDynamic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsDynamic(boolean newIsDynamic) {
		boolean oldIsDynamic = isDynamic;
		isDynamic = newIsDynamic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Activity_graphsPackage.ACTION_STATE__IS_DYNAMIC, oldIsDynamic, isDynamic));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArgListsExpression getDynamicArguments() {
		return dynamicArguments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDynamicArguments(ArgListsExpression newDynamicArguments, NotificationChain msgs) {
		ArgListsExpression oldDynamicArguments = dynamicArguments;
		dynamicArguments = newDynamicArguments;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Activity_graphsPackage.ACTION_STATE__DYNAMIC_ARGUMENTS, oldDynamicArguments, newDynamicArguments);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDynamicArguments(ArgListsExpression newDynamicArguments) {
		if (newDynamicArguments != dynamicArguments) {
			NotificationChain msgs = null;
			if (dynamicArguments != null)
				msgs = ((InternalEObject)dynamicArguments).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Activity_graphsPackage.ACTION_STATE__DYNAMIC_ARGUMENTS, null, msgs);
			if (newDynamicArguments != null)
				msgs = ((InternalEObject)newDynamicArguments).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Activity_graphsPackage.ACTION_STATE__DYNAMIC_ARGUMENTS, null, msgs);
			msgs = basicSetDynamicArguments(newDynamicArguments, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Activity_graphsPackage.ACTION_STATE__DYNAMIC_ARGUMENTS, newDynamicArguments, newDynamicArguments));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Multiplicity getDynamicMultiplicity() {
		return dynamicMultiplicity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDynamicMultiplicity(Multiplicity newDynamicMultiplicity, NotificationChain msgs) {
		Multiplicity oldDynamicMultiplicity = dynamicMultiplicity;
		dynamicMultiplicity = newDynamicMultiplicity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Activity_graphsPackage.ACTION_STATE__DYNAMIC_MULTIPLICITY, oldDynamicMultiplicity, newDynamicMultiplicity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDynamicMultiplicity(Multiplicity newDynamicMultiplicity) {
		if (newDynamicMultiplicity != dynamicMultiplicity) {
			NotificationChain msgs = null;
			if (dynamicMultiplicity != null)
				msgs = ((InternalEObject)dynamicMultiplicity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Activity_graphsPackage.ACTION_STATE__DYNAMIC_MULTIPLICITY, null, msgs);
			if (newDynamicMultiplicity != null)
				msgs = ((InternalEObject)newDynamicMultiplicity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Activity_graphsPackage.ACTION_STATE__DYNAMIC_MULTIPLICITY, null, msgs);
			msgs = basicSetDynamicMultiplicity(newDynamicMultiplicity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Activity_graphsPackage.ACTION_STATE__DYNAMIC_MULTIPLICITY, newDynamicMultiplicity, newDynamicMultiplicity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Activity_graphsPackage.ACTION_STATE__DYNAMIC_ARGUMENTS:
				return basicSetDynamicArguments(null, msgs);
			case Activity_graphsPackage.ACTION_STATE__DYNAMIC_MULTIPLICITY:
				return basicSetDynamicMultiplicity(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Activity_graphsPackage.ACTION_STATE__IS_DYNAMIC:
				return isIsDynamic();
			case Activity_graphsPackage.ACTION_STATE__DYNAMIC_ARGUMENTS:
				return getDynamicArguments();
			case Activity_graphsPackage.ACTION_STATE__DYNAMIC_MULTIPLICITY:
				return getDynamicMultiplicity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Activity_graphsPackage.ACTION_STATE__IS_DYNAMIC:
				setIsDynamic((Boolean)newValue);
				return;
			case Activity_graphsPackage.ACTION_STATE__DYNAMIC_ARGUMENTS:
				setDynamicArguments((ArgListsExpression)newValue);
				return;
			case Activity_graphsPackage.ACTION_STATE__DYNAMIC_MULTIPLICITY:
				setDynamicMultiplicity((Multiplicity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Activity_graphsPackage.ACTION_STATE__IS_DYNAMIC:
				setIsDynamic(IS_DYNAMIC_EDEFAULT);
				return;
			case Activity_graphsPackage.ACTION_STATE__DYNAMIC_ARGUMENTS:
				setDynamicArguments((ArgListsExpression)null);
				return;
			case Activity_graphsPackage.ACTION_STATE__DYNAMIC_MULTIPLICITY:
				setDynamicMultiplicity((Multiplicity)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Activity_graphsPackage.ACTION_STATE__IS_DYNAMIC:
				return isDynamic != IS_DYNAMIC_EDEFAULT;
			case Activity_graphsPackage.ACTION_STATE__DYNAMIC_ARGUMENTS:
				return dynamicArguments != null;
			case Activity_graphsPackage.ACTION_STATE__DYNAMIC_MULTIPLICITY:
				return dynamicMultiplicity != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isDynamic: ");
		result.append(isDynamic);
		result.append(')');
		return result.toString();
	}

} //ActionStateImpl
