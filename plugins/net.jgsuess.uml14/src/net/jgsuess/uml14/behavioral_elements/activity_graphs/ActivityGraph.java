/**
 * <copyright>
 * </copyright>
 *
 * $Id: ActivityGraph.java,v 1.1 2012/04/23 09:31:22 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.activity_graphs;

import net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Activity Graph</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage#getActivityGraph()
 * @model
 * @generated
 */
public interface ActivityGraph extends StateMachine {
} // ActivityGraph
