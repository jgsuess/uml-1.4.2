/**
 * <copyright>
 * </copyright>
 *
 * $Id: ActionState.java,v 1.1 2012/04/23 09:31:22 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.activity_graphs;

import net.jgsuess.uml14.behavioral_elements.state_machines.SimpleState;

import net.jgsuess.uml14.foundation.data_types.ArgListsExpression;
import net.jgsuess.uml14.foundation.data_types.Multiplicity;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ActionState#isIsDynamic <em>Is Dynamic</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ActionState#getDynamicArguments <em>Dynamic Arguments</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ActionState#getDynamicMultiplicity <em>Dynamic Multiplicity</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage#getActionState()
 * @model
 * @generated
 */
public interface ActionState extends SimpleState {
	/**
	 * Returns the value of the '<em><b>Is Dynamic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Dynamic</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Dynamic</em>' attribute.
	 * @see #setIsDynamic(boolean)
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage#getActionState_IsDynamic()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Boolean"
	 * @generated
	 */
	boolean isIsDynamic();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ActionState#isIsDynamic <em>Is Dynamic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Dynamic</em>' attribute.
	 * @see #isIsDynamic()
	 * @generated
	 */
	void setIsDynamic(boolean value);

	/**
	 * Returns the value of the '<em><b>Dynamic Arguments</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dynamic Arguments</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dynamic Arguments</em>' containment reference.
	 * @see #setDynamicArguments(ArgListsExpression)
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage#getActionState_DynamicArguments()
	 * @model containment="true"
	 * @generated
	 */
	ArgListsExpression getDynamicArguments();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ActionState#getDynamicArguments <em>Dynamic Arguments</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dynamic Arguments</em>' containment reference.
	 * @see #getDynamicArguments()
	 * @generated
	 */
	void setDynamicArguments(ArgListsExpression value);

	/**
	 * Returns the value of the '<em><b>Dynamic Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dynamic Multiplicity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dynamic Multiplicity</em>' containment reference.
	 * @see #setDynamicMultiplicity(Multiplicity)
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage#getActionState_DynamicMultiplicity()
	 * @model containment="true"
	 * @generated
	 */
	Multiplicity getDynamicMultiplicity();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ActionState#getDynamicMultiplicity <em>Dynamic Multiplicity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dynamic Multiplicity</em>' containment reference.
	 * @see #getDynamicMultiplicity()
	 * @generated
	 */
	void setDynamicMultiplicity(Multiplicity value);

} // ActionState
