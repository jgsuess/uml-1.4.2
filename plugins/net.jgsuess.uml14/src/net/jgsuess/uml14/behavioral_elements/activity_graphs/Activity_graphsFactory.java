/**
 * <copyright>
 * </copyright>
 *
 * $Id: Activity_graphsFactory.java,v 1.1 2012/04/23 09:31:22 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.activity_graphs;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage
 * @generated
 */
public interface Activity_graphsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Activity_graphsFactory eINSTANCE = net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.Activity_graphsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Activity Graph</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Activity Graph</em>'.
	 * @generated
	 */
	ActivityGraph createActivityGraph();

	/**
	 * Returns a new object of class '<em>Partition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Partition</em>'.
	 * @generated
	 */
	Partition createPartition();

	/**
	 * Returns a new object of class '<em>Subactivity State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Subactivity State</em>'.
	 * @generated
	 */
	SubactivityState createSubactivityState();

	/**
	 * Returns a new object of class '<em>Action State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Action State</em>'.
	 * @generated
	 */
	ActionState createActionState();

	/**
	 * Returns a new object of class '<em>Call State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Call State</em>'.
	 * @generated
	 */
	CallState createCallState();

	/**
	 * Returns a new object of class '<em>Object Flow State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Object Flow State</em>'.
	 * @generated
	 */
	ObjectFlowState createObjectFlowState();

	/**
	 * Returns a new object of class '<em>Classifier In State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Classifier In State</em>'.
	 * @generated
	 */
	ClassifierInState createClassifierInState();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Activity_graphsPackage getActivity_graphsPackage();

} //Activity_graphsFactory
