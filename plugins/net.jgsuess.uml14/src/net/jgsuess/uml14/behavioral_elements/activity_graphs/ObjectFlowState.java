/**
 * <copyright>
 * </copyright>
 *
 * $Id: ObjectFlowState.java,v 1.1 2012/04/23 09:31:23 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.activity_graphs;

import net.jgsuess.uml14.behavioral_elements.state_machines.SimpleState;

import net.jgsuess.uml14.foundation.core.Classifier;
import net.jgsuess.uml14.foundation.core.Parameter;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object Flow State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState#isIsSynch <em>Is Synch</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState#getParameter <em>Parameter</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage#getObjectFlowState()
 * @model
 * @generated
 */
public interface ObjectFlowState extends SimpleState {
	/**
	 * Returns the value of the '<em><b>Is Synch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Synch</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Synch</em>' attribute.
	 * @see #setIsSynch(boolean)
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage#getObjectFlowState_IsSynch()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Boolean"
	 * @generated
	 */
	boolean isIsSynch();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState#isIsSynch <em>Is Synch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Synch</em>' attribute.
	 * @see #isIsSynch()
	 * @generated
	 */
	void setIsSynch(boolean value);

	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Parameter}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Parameter#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage#getObjectFlowState_Parameter()
	 * @see net.jgsuess.uml14.foundation.core.Parameter#getState
	 * @model opposite="state"
	 * @generated
	 */
	EList<Parameter> getParameter();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Classifier#getObjectFlowState <em>Object Flow State</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(Classifier)
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage#getObjectFlowState_Type()
	 * @see net.jgsuess.uml14.foundation.core.Classifier#getObjectFlowState
	 * @model opposite="objectFlowState" required="true"
	 * @generated
	 */
	Classifier getType();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Classifier value);

} // ObjectFlowState
