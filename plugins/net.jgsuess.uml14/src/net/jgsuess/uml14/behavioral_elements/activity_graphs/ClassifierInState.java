/**
 * <copyright>
 * </copyright>
 *
 * $Id: ClassifierInState.java,v 1.1 2012/04/23 09:31:22 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.activity_graphs;

import net.jgsuess.uml14.behavioral_elements.state_machines.State;

import net.jgsuess.uml14.foundation.core.Classifier;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Classifier In State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState#getType <em>Type</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState#getInState <em>In State</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage#getClassifierInState()
 * @model
 * @generated
 */
public interface ClassifierInState extends Classifier {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Classifier#getClassifierInState <em>Classifier In State</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(Classifier)
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage#getClassifierInState_Type()
	 * @see net.jgsuess.uml14.foundation.core.Classifier#getClassifierInState
	 * @model opposite="classifierInState" required="true"
	 * @generated
	 */
	Classifier getType();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Classifier value);

	/**
	 * Returns the value of the '<em><b>In State</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.state_machines.State}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.state_machines.State#getClassifierInState <em>Classifier In State</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In State</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In State</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage#getClassifierInState_InState()
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State#getClassifierInState
	 * @model opposite="classifierInState" required="true"
	 * @generated
	 */
	EList<State> getInState();

} // ClassifierInState
