/**
 * <copyright>
 * </copyright>
 *
 * $Id: Activity_graphsAdapterFactory.java,v 1.1 2012/04/23 09:31:40 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.activity_graphs.util;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.*;

import net.jgsuess.uml14.behavioral_elements.state_machines.CompositeState;
import net.jgsuess.uml14.behavioral_elements.state_machines.SimpleState;
import net.jgsuess.uml14.behavioral_elements.state_machines.State;
import net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine;
import net.jgsuess.uml14.behavioral_elements.state_machines.StateVertex;
import net.jgsuess.uml14.behavioral_elements.state_machines.SubmachineState;

import net.jgsuess.uml14.foundation.core.Classifier;
import net.jgsuess.uml14.foundation.core.Element;
import net.jgsuess.uml14.foundation.core.GeneralizableElement;
import net.jgsuess.uml14.foundation.core.ModelElement;
import net.jgsuess.uml14.foundation.core.Namespace;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage
 * @generated
 */
public class Activity_graphsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Activity_graphsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Activity_graphsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = Activity_graphsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Activity_graphsSwitch<Adapter> modelSwitch =
		new Activity_graphsSwitch<Adapter>() {
			@Override
			public Adapter caseActivityGraph(ActivityGraph object) {
				return createActivityGraphAdapter();
			}
			@Override
			public Adapter casePartition(Partition object) {
				return createPartitionAdapter();
			}
			@Override
			public Adapter caseSubactivityState(SubactivityState object) {
				return createSubactivityStateAdapter();
			}
			@Override
			public Adapter caseActionState(ActionState object) {
				return createActionStateAdapter();
			}
			@Override
			public Adapter caseCallState(CallState object) {
				return createCallStateAdapter();
			}
			@Override
			public Adapter caseObjectFlowState(ObjectFlowState object) {
				return createObjectFlowStateAdapter();
			}
			@Override
			public Adapter caseClassifierInState(ClassifierInState object) {
				return createClassifierInStateAdapter();
			}
			@Override
			public Adapter caseElement(Element object) {
				return createElementAdapter();
			}
			@Override
			public Adapter caseModelElement(ModelElement object) {
				return createModelElementAdapter();
			}
			@Override
			public Adapter caseStateMachine(StateMachine object) {
				return createStateMachineAdapter();
			}
			@Override
			public Adapter caseStateVertex(StateVertex object) {
				return createStateVertexAdapter();
			}
			@Override
			public Adapter caseState(State object) {
				return createStateAdapter();
			}
			@Override
			public Adapter caseCompositeState(CompositeState object) {
				return createCompositeStateAdapter();
			}
			@Override
			public Adapter caseSubmachineState(SubmachineState object) {
				return createSubmachineStateAdapter();
			}
			@Override
			public Adapter caseSimpleState(SimpleState object) {
				return createSimpleStateAdapter();
			}
			@Override
			public Adapter caseGeneralizableElement(GeneralizableElement object) {
				return createGeneralizableElementAdapter();
			}
			@Override
			public Adapter caseNamespace(Namespace object) {
				return createNamespaceAdapter();
			}
			@Override
			public Adapter caseClassifier(Classifier object) {
				return createClassifierAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ActivityGraph <em>Activity Graph</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.ActivityGraph
	 * @generated
	 */
	public Adapter createActivityGraphAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.Partition <em>Partition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.Partition
	 * @generated
	 */
	public Adapter createPartitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.SubactivityState <em>Subactivity State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.SubactivityState
	 * @generated
	 */
	public Adapter createSubactivityStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ActionState <em>Action State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.ActionState
	 * @generated
	 */
	public Adapter createActionStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.CallState <em>Call State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.CallState
	 * @generated
	 */
	public Adapter createCallStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState <em>Object Flow State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState
	 * @generated
	 */
	public Adapter createObjectFlowStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState <em>Classifier In State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState
	 * @generated
	 */
	public Adapter createClassifierInStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link net.jgsuess.uml14.foundation.core.Element <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see net.jgsuess.uml14.foundation.core.Element
	 * @generated
	 */
	public Adapter createElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link net.jgsuess.uml14.foundation.core.ModelElement <em>Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see net.jgsuess.uml14.foundation.core.ModelElement
	 * @generated
	 */
	public Adapter createModelElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine <em>State Machine</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine
	 * @generated
	 */
	public Adapter createStateMachineAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.StateVertex <em>State Vertex</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.StateVertex
	 * @generated
	 */
	public Adapter createStateVertexAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State
	 * @generated
	 */
	public Adapter createStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.CompositeState <em>Composite State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.CompositeState
	 * @generated
	 */
	public Adapter createCompositeStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.SubmachineState <em>Submachine State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.SubmachineState
	 * @generated
	 */
	public Adapter createSubmachineStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.SimpleState <em>Simple State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.SimpleState
	 * @generated
	 */
	public Adapter createSimpleStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link net.jgsuess.uml14.foundation.core.GeneralizableElement <em>Generalizable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see net.jgsuess.uml14.foundation.core.GeneralizableElement
	 * @generated
	 */
	public Adapter createGeneralizableElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link net.jgsuess.uml14.foundation.core.Namespace <em>Namespace</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see net.jgsuess.uml14.foundation.core.Namespace
	 * @generated
	 */
	public Adapter createNamespaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link net.jgsuess.uml14.foundation.core.Classifier <em>Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see net.jgsuess.uml14.foundation.core.Classifier
	 * @generated
	 */
	public Adapter createClassifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //Activity_graphsAdapterFactory
