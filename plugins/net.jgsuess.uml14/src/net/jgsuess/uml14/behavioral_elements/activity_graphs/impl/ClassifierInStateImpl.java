/**
 * <copyright>
 * </copyright>
 *
 * $Id: ClassifierInStateImpl.java,v 1.1 2012/04/23 09:31:37 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.activity_graphs.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage;
import net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState;

import net.jgsuess.uml14.behavioral_elements.state_machines.State;
import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage;

import net.jgsuess.uml14.foundation.core.Classifier;
import net.jgsuess.uml14.foundation.core.CorePackage;

import net.jgsuess.uml14.foundation.core.impl.ClassifierImpl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Classifier In State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.ClassifierInStateImpl#getType <em>Type</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.ClassifierInStateImpl#getInState <em>In State</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ClassifierInStateImpl extends ClassifierImpl implements ClassifierInState {
	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected Classifier type;

	/**
	 * The cached value of the '{@link #getInState() <em>In State</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInState()
	 * @generated
	 * @ordered
	 */
	protected EList<State> inState;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassifierInStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Activity_graphsPackage.Literals.CLASSIFIER_IN_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Classifier getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (Classifier)eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Activity_graphsPackage.CLASSIFIER_IN_STATE__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Classifier basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetType(Classifier newType, NotificationChain msgs) {
		Classifier oldType = type;
		type = newType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Activity_graphsPackage.CLASSIFIER_IN_STATE__TYPE, oldType, newType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(Classifier newType) {
		if (newType != type) {
			NotificationChain msgs = null;
			if (type != null)
				msgs = ((InternalEObject)type).eInverseRemove(this, CorePackage.CLASSIFIER__CLASSIFIER_IN_STATE, Classifier.class, msgs);
			if (newType != null)
				msgs = ((InternalEObject)newType).eInverseAdd(this, CorePackage.CLASSIFIER__CLASSIFIER_IN_STATE, Classifier.class, msgs);
			msgs = basicSetType(newType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Activity_graphsPackage.CLASSIFIER_IN_STATE__TYPE, newType, newType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<State> getInState() {
		if (inState == null) {
			inState = new EObjectWithInverseResolvingEList.ManyInverse<State>(State.class, this, Activity_graphsPackage.CLASSIFIER_IN_STATE__IN_STATE, State_machinesPackage.STATE__CLASSIFIER_IN_STATE);
		}
		return inState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Activity_graphsPackage.CLASSIFIER_IN_STATE__TYPE:
				if (type != null)
					msgs = ((InternalEObject)type).eInverseRemove(this, CorePackage.CLASSIFIER__CLASSIFIER_IN_STATE, Classifier.class, msgs);
				return basicSetType((Classifier)otherEnd, msgs);
			case Activity_graphsPackage.CLASSIFIER_IN_STATE__IN_STATE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getInState()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Activity_graphsPackage.CLASSIFIER_IN_STATE__TYPE:
				return basicSetType(null, msgs);
			case Activity_graphsPackage.CLASSIFIER_IN_STATE__IN_STATE:
				return ((InternalEList<?>)getInState()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Activity_graphsPackage.CLASSIFIER_IN_STATE__TYPE:
				if (resolve) return getType();
				return basicGetType();
			case Activity_graphsPackage.CLASSIFIER_IN_STATE__IN_STATE:
				return getInState();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Activity_graphsPackage.CLASSIFIER_IN_STATE__TYPE:
				setType((Classifier)newValue);
				return;
			case Activity_graphsPackage.CLASSIFIER_IN_STATE__IN_STATE:
				getInState().clear();
				getInState().addAll((Collection<? extends State>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Activity_graphsPackage.CLASSIFIER_IN_STATE__TYPE:
				setType((Classifier)null);
				return;
			case Activity_graphsPackage.CLASSIFIER_IN_STATE__IN_STATE:
				getInState().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Activity_graphsPackage.CLASSIFIER_IN_STATE__TYPE:
				return type != null;
			case Activity_graphsPackage.CLASSIFIER_IN_STATE__IN_STATE:
				return inState != null && !inState.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ClassifierInStateImpl
