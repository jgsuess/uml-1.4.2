/**
 * <copyright>
 * </copyright>
 *
 * $Id: Pseudostate.java,v 1.1 2012/04/23 09:31:19 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.state_machines;

import net.jgsuess.uml14.foundation.data_types.PseudostateKind;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pseudostate</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.Pseudostate#getKind <em>Kind</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getPseudostate()
 * @model
 * @generated
 */
public interface Pseudostate extends StateVertex {
	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link net.jgsuess.uml14.foundation.data_types.PseudostateKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.PseudostateKind
	 * @see #setKind(PseudostateKind)
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getPseudostate_Kind()
	 * @model
	 * @generated
	 */
	PseudostateKind getKind();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Pseudostate#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.PseudostateKind
	 * @see #getKind()
	 * @generated
	 */
	void setKind(PseudostateKind value);

} // Pseudostate
