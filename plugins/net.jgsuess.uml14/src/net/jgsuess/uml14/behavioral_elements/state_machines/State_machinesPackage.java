/**
 * <copyright>
 * </copyright>
 *
 * $Id: State_machinesPackage.java,v 1.1 2012/04/23 09:31:18 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.state_machines;

import net.jgsuess.uml14.foundation.core.CorePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesFactory
 * @model kind="package"
 * @generated
 */
public interface State_machinesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "state_machines";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://jgsuess.net/uml14/state_machines";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "state_machines";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	State_machinesPackage eINSTANCE = net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl.init();

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.StateMachineImpl <em>State Machine</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.StateMachineImpl
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getStateMachine()
	 * @generated
	 */
	int STATE_MACHINE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__IS_SPECIFICATION = CorePackage.MODEL_ELEMENT__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__PRESENTATION = CorePackage.MODEL_ELEMENT__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__TARGET_FLOW = CorePackage.MODEL_ELEMENT__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__SOURCE_FLOW = CorePackage.MODEL_ELEMENT__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__DEFAULTED_PARAMETER = CorePackage.MODEL_ELEMENT__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__COMMENT = CorePackage.MODEL_ELEMENT__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__ELEMENT_RESIDENCE = CorePackage.MODEL_ELEMENT__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__TEMPLATE_PARAMETER = CorePackage.MODEL_ELEMENT__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__PARAMETER_TEMPLATE = CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__REFERENCE_TAG = CorePackage.MODEL_ELEMENT__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__TEMPLATE_ARGUMENT = CorePackage.MODEL_ELEMENT__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__BEHAVIOR = CorePackage.MODEL_ELEMENT__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__CLASSIFIER_ROLE = CorePackage.MODEL_ELEMENT__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__COLLABORATION = CorePackage.MODEL_ELEMENT__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__COLLABORATION_INSTANCE_SET = CorePackage.MODEL_ELEMENT__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__PARTITION = CorePackage.MODEL_ELEMENT__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__ELEMENT_IMPORT = CorePackage.MODEL_ELEMENT__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__CONTEXT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Top</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__TOP = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__TRANSITIONS = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Submachine State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__SUBMACHINE_STATE = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>State Machine</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.EventImpl
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__IS_SPECIFICATION = CorePackage.MODEL_ELEMENT__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__PRESENTATION = CorePackage.MODEL_ELEMENT__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__TARGET_FLOW = CorePackage.MODEL_ELEMENT__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__SOURCE_FLOW = CorePackage.MODEL_ELEMENT__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__DEFAULTED_PARAMETER = CorePackage.MODEL_ELEMENT__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__COMMENT = CorePackage.MODEL_ELEMENT__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__ELEMENT_RESIDENCE = CorePackage.MODEL_ELEMENT__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__TEMPLATE_PARAMETER = CorePackage.MODEL_ELEMENT__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__PARAMETER_TEMPLATE = CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__REFERENCE_TAG = CorePackage.MODEL_ELEMENT__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__TEMPLATE_ARGUMENT = CorePackage.MODEL_ELEMENT__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__BEHAVIOR = CorePackage.MODEL_ELEMENT__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__CLASSIFIER_ROLE = CorePackage.MODEL_ELEMENT__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__COLLABORATION = CorePackage.MODEL_ELEMENT__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__COLLABORATION_INSTANCE_SET = CorePackage.MODEL_ELEMENT__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__PARTITION = CorePackage.MODEL_ELEMENT__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__ELEMENT_IMPORT = CorePackage.MODEL_ELEMENT__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__PARAMETER = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__STATE = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Transition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__TRANSITION = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.StateVertexImpl <em>State Vertex</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.StateVertexImpl
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getStateVertex()
	 * @generated
	 */
	int STATE_VERTEX = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__IS_SPECIFICATION = CorePackage.MODEL_ELEMENT__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__PRESENTATION = CorePackage.MODEL_ELEMENT__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__TARGET_FLOW = CorePackage.MODEL_ELEMENT__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__SOURCE_FLOW = CorePackage.MODEL_ELEMENT__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__DEFAULTED_PARAMETER = CorePackage.MODEL_ELEMENT__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__COMMENT = CorePackage.MODEL_ELEMENT__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__ELEMENT_RESIDENCE = CorePackage.MODEL_ELEMENT__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__TEMPLATE_PARAMETER = CorePackage.MODEL_ELEMENT__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__PARAMETER_TEMPLATE = CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__REFERENCE_TAG = CorePackage.MODEL_ELEMENT__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__TEMPLATE_ARGUMENT = CorePackage.MODEL_ELEMENT__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__BEHAVIOR = CorePackage.MODEL_ELEMENT__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__CLASSIFIER_ROLE = CorePackage.MODEL_ELEMENT__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__COLLABORATION = CorePackage.MODEL_ELEMENT__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__COLLABORATION_INSTANCE_SET = CorePackage.MODEL_ELEMENT__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__PARTITION = CorePackage.MODEL_ELEMENT__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__ELEMENT_IMPORT = CorePackage.MODEL_ELEMENT__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__CONTAINER = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__OUTGOING = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX__INCOMING = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>State Vertex</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_VERTEX_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.StateImpl <em>State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.StateImpl
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getState()
	 * @generated
	 */
	int STATE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__NAME = STATE_VERTEX__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__VISIBILITY = STATE_VERTEX__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__IS_SPECIFICATION = STATE_VERTEX__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__NAMESPACE = STATE_VERTEX__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__CLIENT_DEPENDENCY = STATE_VERTEX__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__CONSTRAINT = STATE_VERTEX__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__SUPPLIER_DEPENDENCY = STATE_VERTEX__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__PRESENTATION = STATE_VERTEX__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__TARGET_FLOW = STATE_VERTEX__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__SOURCE_FLOW = STATE_VERTEX__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__DEFAULTED_PARAMETER = STATE_VERTEX__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__COMMENT = STATE_VERTEX__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__ELEMENT_RESIDENCE = STATE_VERTEX__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__TEMPLATE_PARAMETER = STATE_VERTEX__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__PARAMETER_TEMPLATE = STATE_VERTEX__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__STEREOTYPE = STATE_VERTEX__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__TAGGED_VALUE = STATE_VERTEX__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__REFERENCE_TAG = STATE_VERTEX__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__TEMPLATE_ARGUMENT = STATE_VERTEX__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__BEHAVIOR = STATE_VERTEX__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__CLASSIFIER_ROLE = STATE_VERTEX__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__COLLABORATION = STATE_VERTEX__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__COLLABORATION_INSTANCE_SET = STATE_VERTEX__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__PARTITION = STATE_VERTEX__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__ELEMENT_IMPORT = STATE_VERTEX__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__CONTAINER = STATE_VERTEX__CONTAINER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__OUTGOING = STATE_VERTEX__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__INCOMING = STATE_VERTEX__INCOMING;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__ENTRY = STATE_VERTEX_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>State Machine</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__STATE_MACHINE = STATE_VERTEX_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Deferrable Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__DEFERRABLE_EVENT = STATE_VERTEX_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Internal Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__INTERNAL_TRANSITION = STATE_VERTEX_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Classifier In State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__CLASSIFIER_IN_STATE = STATE_VERTEX_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_FEATURE_COUNT = STATE_VERTEX_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.TimeEventImpl <em>Time Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.TimeEventImpl
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getTimeEvent()
	 * @generated
	 */
	int TIME_EVENT = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__NAME = EVENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__VISIBILITY = EVENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__IS_SPECIFICATION = EVENT__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__NAMESPACE = EVENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__CLIENT_DEPENDENCY = EVENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__CONSTRAINT = EVENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__SUPPLIER_DEPENDENCY = EVENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__PRESENTATION = EVENT__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__TARGET_FLOW = EVENT__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__SOURCE_FLOW = EVENT__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__DEFAULTED_PARAMETER = EVENT__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__COMMENT = EVENT__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__ELEMENT_RESIDENCE = EVENT__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__TEMPLATE_PARAMETER = EVENT__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__PARAMETER_TEMPLATE = EVENT__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__STEREOTYPE = EVENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__TAGGED_VALUE = EVENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__REFERENCE_TAG = EVENT__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__TEMPLATE_ARGUMENT = EVENT__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__BEHAVIOR = EVENT__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__CLASSIFIER_ROLE = EVENT__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__COLLABORATION = EVENT__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__COLLABORATION_INSTANCE_SET = EVENT__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__PARTITION = EVENT__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__ELEMENT_IMPORT = EVENT__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__PARAMETER = EVENT__PARAMETER;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__STATE = EVENT__STATE;

	/**
	 * The feature id for the '<em><b>Transition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__TRANSITION = EVENT__TRANSITION;

	/**
	 * The feature id for the '<em><b>When</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT__WHEN = EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Time Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EVENT_FEATURE_COUNT = EVENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.CallEventImpl <em>Call Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.CallEventImpl
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getCallEvent()
	 * @generated
	 */
	int CALL_EVENT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__NAME = EVENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__VISIBILITY = EVENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__IS_SPECIFICATION = EVENT__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__NAMESPACE = EVENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__CLIENT_DEPENDENCY = EVENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__CONSTRAINT = EVENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__SUPPLIER_DEPENDENCY = EVENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__PRESENTATION = EVENT__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__TARGET_FLOW = EVENT__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__SOURCE_FLOW = EVENT__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__DEFAULTED_PARAMETER = EVENT__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__COMMENT = EVENT__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__ELEMENT_RESIDENCE = EVENT__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__TEMPLATE_PARAMETER = EVENT__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__PARAMETER_TEMPLATE = EVENT__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__STEREOTYPE = EVENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__TAGGED_VALUE = EVENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__REFERENCE_TAG = EVENT__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__TEMPLATE_ARGUMENT = EVENT__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__BEHAVIOR = EVENT__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__CLASSIFIER_ROLE = EVENT__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__COLLABORATION = EVENT__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__COLLABORATION_INSTANCE_SET = EVENT__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__PARTITION = EVENT__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__ELEMENT_IMPORT = EVENT__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__PARAMETER = EVENT__PARAMETER;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__STATE = EVENT__STATE;

	/**
	 * The feature id for the '<em><b>Transition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__TRANSITION = EVENT__TRANSITION;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT__OPERATION = EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Call Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_EVENT_FEATURE_COUNT = EVENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.SignalEventImpl <em>Signal Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.SignalEventImpl
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getSignalEvent()
	 * @generated
	 */
	int SIGNAL_EVENT = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__NAME = EVENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__VISIBILITY = EVENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__IS_SPECIFICATION = EVENT__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__NAMESPACE = EVENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__CLIENT_DEPENDENCY = EVENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__CONSTRAINT = EVENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__SUPPLIER_DEPENDENCY = EVENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__PRESENTATION = EVENT__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__TARGET_FLOW = EVENT__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__SOURCE_FLOW = EVENT__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__DEFAULTED_PARAMETER = EVENT__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__COMMENT = EVENT__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__ELEMENT_RESIDENCE = EVENT__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__TEMPLATE_PARAMETER = EVENT__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__PARAMETER_TEMPLATE = EVENT__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__STEREOTYPE = EVENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__TAGGED_VALUE = EVENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__REFERENCE_TAG = EVENT__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__TEMPLATE_ARGUMENT = EVENT__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__BEHAVIOR = EVENT__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__CLASSIFIER_ROLE = EVENT__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__COLLABORATION = EVENT__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__COLLABORATION_INSTANCE_SET = EVENT__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__PARTITION = EVENT__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__ELEMENT_IMPORT = EVENT__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__PARAMETER = EVENT__PARAMETER;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__STATE = EVENT__STATE;

	/**
	 * The feature id for the '<em><b>Transition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__TRANSITION = EVENT__TRANSITION;

	/**
	 * The feature id for the '<em><b>Signal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT__SIGNAL = EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Signal Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_EVENT_FEATURE_COUNT = EVENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.TransitionImpl
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__IS_SPECIFICATION = CorePackage.MODEL_ELEMENT__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__PRESENTATION = CorePackage.MODEL_ELEMENT__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TARGET_FLOW = CorePackage.MODEL_ELEMENT__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__SOURCE_FLOW = CorePackage.MODEL_ELEMENT__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__DEFAULTED_PARAMETER = CorePackage.MODEL_ELEMENT__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__COMMENT = CorePackage.MODEL_ELEMENT__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__ELEMENT_RESIDENCE = CorePackage.MODEL_ELEMENT__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TEMPLATE_PARAMETER = CorePackage.MODEL_ELEMENT__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__PARAMETER_TEMPLATE = CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__REFERENCE_TAG = CorePackage.MODEL_ELEMENT__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TEMPLATE_ARGUMENT = CorePackage.MODEL_ELEMENT__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__BEHAVIOR = CorePackage.MODEL_ELEMENT__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__CLASSIFIER_ROLE = CorePackage.MODEL_ELEMENT__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__COLLABORATION = CorePackage.MODEL_ELEMENT__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__COLLABORATION_INSTANCE_SET = CorePackage.MODEL_ELEMENT__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__PARTITION = CorePackage.MODEL_ELEMENT__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__ELEMENT_IMPORT = CorePackage.MODEL_ELEMENT__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Guard</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__GUARD = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Effect</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__EFFECT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>State</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__STATE = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TRIGGER = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>State Machine</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__STATE_MACHINE = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__SOURCE = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TARGET = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.CompositeStateImpl <em>Composite State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.CompositeStateImpl
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getCompositeState()
	 * @generated
	 */
	int COMPOSITE_STATE = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__NAME = STATE__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__VISIBILITY = STATE__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__IS_SPECIFICATION = STATE__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__NAMESPACE = STATE__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__CLIENT_DEPENDENCY = STATE__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__CONSTRAINT = STATE__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__SUPPLIER_DEPENDENCY = STATE__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__PRESENTATION = STATE__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__TARGET_FLOW = STATE__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__SOURCE_FLOW = STATE__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__DEFAULTED_PARAMETER = STATE__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__COMMENT = STATE__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__ELEMENT_RESIDENCE = STATE__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__TEMPLATE_PARAMETER = STATE__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__PARAMETER_TEMPLATE = STATE__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__STEREOTYPE = STATE__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__TAGGED_VALUE = STATE__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__REFERENCE_TAG = STATE__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__TEMPLATE_ARGUMENT = STATE__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__BEHAVIOR = STATE__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__CLASSIFIER_ROLE = STATE__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__COLLABORATION = STATE__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__COLLABORATION_INSTANCE_SET = STATE__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__PARTITION = STATE__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__ELEMENT_IMPORT = STATE__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__CONTAINER = STATE__CONTAINER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__OUTGOING = STATE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__INCOMING = STATE__INCOMING;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__ENTRY = STATE__ENTRY;

	/**
	 * The feature id for the '<em><b>State Machine</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__STATE_MACHINE = STATE__STATE_MACHINE;

	/**
	 * The feature id for the '<em><b>Deferrable Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__DEFERRABLE_EVENT = STATE__DEFERRABLE_EVENT;

	/**
	 * The feature id for the '<em><b>Internal Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__INTERNAL_TRANSITION = STATE__INTERNAL_TRANSITION;

	/**
	 * The feature id for the '<em><b>Classifier In State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__CLASSIFIER_IN_STATE = STATE__CLASSIFIER_IN_STATE;

	/**
	 * The feature id for the '<em><b>Is Concurrent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__IS_CONCURRENT = STATE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Subvertex</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__SUBVERTEX = STATE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Composite State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE_FEATURE_COUNT = STATE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.ChangeEventImpl <em>Change Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.ChangeEventImpl
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getChangeEvent()
	 * @generated
	 */
	int CHANGE_EVENT = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__NAME = EVENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__VISIBILITY = EVENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__IS_SPECIFICATION = EVENT__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__NAMESPACE = EVENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__CLIENT_DEPENDENCY = EVENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__CONSTRAINT = EVENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__SUPPLIER_DEPENDENCY = EVENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__PRESENTATION = EVENT__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__TARGET_FLOW = EVENT__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__SOURCE_FLOW = EVENT__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__DEFAULTED_PARAMETER = EVENT__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__COMMENT = EVENT__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__ELEMENT_RESIDENCE = EVENT__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__TEMPLATE_PARAMETER = EVENT__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__PARAMETER_TEMPLATE = EVENT__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__STEREOTYPE = EVENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__TAGGED_VALUE = EVENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__REFERENCE_TAG = EVENT__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__TEMPLATE_ARGUMENT = EVENT__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__BEHAVIOR = EVENT__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__CLASSIFIER_ROLE = EVENT__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__COLLABORATION = EVENT__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__COLLABORATION_INSTANCE_SET = EVENT__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__PARTITION = EVENT__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__ELEMENT_IMPORT = EVENT__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__PARAMETER = EVENT__PARAMETER;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__STATE = EVENT__STATE;

	/**
	 * The feature id for the '<em><b>Transition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__TRANSITION = EVENT__TRANSITION;

	/**
	 * The feature id for the '<em><b>Change Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT__CHANGE_EXPRESSION = EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Change Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_EVENT_FEATURE_COUNT = EVENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.GuardImpl <em>Guard</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.GuardImpl
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getGuard()
	 * @generated
	 */
	int GUARD = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__IS_SPECIFICATION = CorePackage.MODEL_ELEMENT__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__PRESENTATION = CorePackage.MODEL_ELEMENT__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__TARGET_FLOW = CorePackage.MODEL_ELEMENT__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__SOURCE_FLOW = CorePackage.MODEL_ELEMENT__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__DEFAULTED_PARAMETER = CorePackage.MODEL_ELEMENT__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__COMMENT = CorePackage.MODEL_ELEMENT__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__ELEMENT_RESIDENCE = CorePackage.MODEL_ELEMENT__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__TEMPLATE_PARAMETER = CorePackage.MODEL_ELEMENT__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__PARAMETER_TEMPLATE = CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__REFERENCE_TAG = CorePackage.MODEL_ELEMENT__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__TEMPLATE_ARGUMENT = CorePackage.MODEL_ELEMENT__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__BEHAVIOR = CorePackage.MODEL_ELEMENT__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__CLASSIFIER_ROLE = CorePackage.MODEL_ELEMENT__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__COLLABORATION = CorePackage.MODEL_ELEMENT__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__COLLABORATION_INSTANCE_SET = CorePackage.MODEL_ELEMENT__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__PARTITION = CorePackage.MODEL_ELEMENT__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__ELEMENT_IMPORT = CorePackage.MODEL_ELEMENT__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__EXPRESSION = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Transition</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__TRANSITION = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Guard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.PseudostateImpl <em>Pseudostate</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.PseudostateImpl
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getPseudostate()
	 * @generated
	 */
	int PSEUDOSTATE = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__NAME = STATE_VERTEX__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__VISIBILITY = STATE_VERTEX__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__IS_SPECIFICATION = STATE_VERTEX__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__NAMESPACE = STATE_VERTEX__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__CLIENT_DEPENDENCY = STATE_VERTEX__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__CONSTRAINT = STATE_VERTEX__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__SUPPLIER_DEPENDENCY = STATE_VERTEX__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__PRESENTATION = STATE_VERTEX__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__TARGET_FLOW = STATE_VERTEX__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__SOURCE_FLOW = STATE_VERTEX__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__DEFAULTED_PARAMETER = STATE_VERTEX__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__COMMENT = STATE_VERTEX__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__ELEMENT_RESIDENCE = STATE_VERTEX__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__TEMPLATE_PARAMETER = STATE_VERTEX__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__PARAMETER_TEMPLATE = STATE_VERTEX__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__STEREOTYPE = STATE_VERTEX__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__TAGGED_VALUE = STATE_VERTEX__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__REFERENCE_TAG = STATE_VERTEX__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__TEMPLATE_ARGUMENT = STATE_VERTEX__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__BEHAVIOR = STATE_VERTEX__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__CLASSIFIER_ROLE = STATE_VERTEX__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__COLLABORATION = STATE_VERTEX__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__COLLABORATION_INSTANCE_SET = STATE_VERTEX__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__PARTITION = STATE_VERTEX__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__ELEMENT_IMPORT = STATE_VERTEX__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__CONTAINER = STATE_VERTEX__CONTAINER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__OUTGOING = STATE_VERTEX__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__INCOMING = STATE_VERTEX__INCOMING;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE__KIND = STATE_VERTEX_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Pseudostate</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PSEUDOSTATE_FEATURE_COUNT = STATE_VERTEX_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.SimpleStateImpl <em>Simple State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.SimpleStateImpl
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getSimpleState()
	 * @generated
	 */
	int SIMPLE_STATE = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__NAME = STATE__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__VISIBILITY = STATE__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__IS_SPECIFICATION = STATE__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__NAMESPACE = STATE__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__CLIENT_DEPENDENCY = STATE__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__CONSTRAINT = STATE__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__SUPPLIER_DEPENDENCY = STATE__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__PRESENTATION = STATE__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__TARGET_FLOW = STATE__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__SOURCE_FLOW = STATE__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__DEFAULTED_PARAMETER = STATE__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__COMMENT = STATE__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__ELEMENT_RESIDENCE = STATE__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__TEMPLATE_PARAMETER = STATE__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__PARAMETER_TEMPLATE = STATE__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__STEREOTYPE = STATE__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__TAGGED_VALUE = STATE__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__REFERENCE_TAG = STATE__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__TEMPLATE_ARGUMENT = STATE__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__BEHAVIOR = STATE__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__CLASSIFIER_ROLE = STATE__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__COLLABORATION = STATE__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__COLLABORATION_INSTANCE_SET = STATE__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__PARTITION = STATE__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__ELEMENT_IMPORT = STATE__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__CONTAINER = STATE__CONTAINER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__OUTGOING = STATE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__INCOMING = STATE__INCOMING;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__ENTRY = STATE__ENTRY;

	/**
	 * The feature id for the '<em><b>State Machine</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__STATE_MACHINE = STATE__STATE_MACHINE;

	/**
	 * The feature id for the '<em><b>Deferrable Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__DEFERRABLE_EVENT = STATE__DEFERRABLE_EVENT;

	/**
	 * The feature id for the '<em><b>Internal Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__INTERNAL_TRANSITION = STATE__INTERNAL_TRANSITION;

	/**
	 * The feature id for the '<em><b>Classifier In State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__CLASSIFIER_IN_STATE = STATE__CLASSIFIER_IN_STATE;

	/**
	 * The number of structural features of the '<em>Simple State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE_FEATURE_COUNT = STATE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.SubmachineStateImpl <em>Submachine State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.SubmachineStateImpl
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getSubmachineState()
	 * @generated
	 */
	int SUBMACHINE_STATE = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__NAME = COMPOSITE_STATE__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__VISIBILITY = COMPOSITE_STATE__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__IS_SPECIFICATION = COMPOSITE_STATE__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__NAMESPACE = COMPOSITE_STATE__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__CLIENT_DEPENDENCY = COMPOSITE_STATE__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__CONSTRAINT = COMPOSITE_STATE__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__SUPPLIER_DEPENDENCY = COMPOSITE_STATE__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__PRESENTATION = COMPOSITE_STATE__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__TARGET_FLOW = COMPOSITE_STATE__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__SOURCE_FLOW = COMPOSITE_STATE__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__DEFAULTED_PARAMETER = COMPOSITE_STATE__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__COMMENT = COMPOSITE_STATE__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__ELEMENT_RESIDENCE = COMPOSITE_STATE__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__TEMPLATE_PARAMETER = COMPOSITE_STATE__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__PARAMETER_TEMPLATE = COMPOSITE_STATE__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__STEREOTYPE = COMPOSITE_STATE__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__TAGGED_VALUE = COMPOSITE_STATE__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__REFERENCE_TAG = COMPOSITE_STATE__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__TEMPLATE_ARGUMENT = COMPOSITE_STATE__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__BEHAVIOR = COMPOSITE_STATE__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__CLASSIFIER_ROLE = COMPOSITE_STATE__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__COLLABORATION = COMPOSITE_STATE__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__COLLABORATION_INSTANCE_SET = COMPOSITE_STATE__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__PARTITION = COMPOSITE_STATE__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__ELEMENT_IMPORT = COMPOSITE_STATE__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__CONTAINER = COMPOSITE_STATE__CONTAINER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__OUTGOING = COMPOSITE_STATE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__INCOMING = COMPOSITE_STATE__INCOMING;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__ENTRY = COMPOSITE_STATE__ENTRY;

	/**
	 * The feature id for the '<em><b>State Machine</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__STATE_MACHINE = COMPOSITE_STATE__STATE_MACHINE;

	/**
	 * The feature id for the '<em><b>Deferrable Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__DEFERRABLE_EVENT = COMPOSITE_STATE__DEFERRABLE_EVENT;

	/**
	 * The feature id for the '<em><b>Internal Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__INTERNAL_TRANSITION = COMPOSITE_STATE__INTERNAL_TRANSITION;

	/**
	 * The feature id for the '<em><b>Classifier In State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__CLASSIFIER_IN_STATE = COMPOSITE_STATE__CLASSIFIER_IN_STATE;

	/**
	 * The feature id for the '<em><b>Is Concurrent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__IS_CONCURRENT = COMPOSITE_STATE__IS_CONCURRENT;

	/**
	 * The feature id for the '<em><b>Subvertex</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__SUBVERTEX = COMPOSITE_STATE__SUBVERTEX;

	/**
	 * The feature id for the '<em><b>Submachine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE__SUBMACHINE = COMPOSITE_STATE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Submachine State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBMACHINE_STATE_FEATURE_COUNT = COMPOSITE_STATE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.SynchStateImpl <em>Synch State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.SynchStateImpl
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getSynchState()
	 * @generated
	 */
	int SYNCH_STATE = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__NAME = STATE_VERTEX__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__VISIBILITY = STATE_VERTEX__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__IS_SPECIFICATION = STATE_VERTEX__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__NAMESPACE = STATE_VERTEX__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__CLIENT_DEPENDENCY = STATE_VERTEX__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__CONSTRAINT = STATE_VERTEX__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__SUPPLIER_DEPENDENCY = STATE_VERTEX__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__PRESENTATION = STATE_VERTEX__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__TARGET_FLOW = STATE_VERTEX__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__SOURCE_FLOW = STATE_VERTEX__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__DEFAULTED_PARAMETER = STATE_VERTEX__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__COMMENT = STATE_VERTEX__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__ELEMENT_RESIDENCE = STATE_VERTEX__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__TEMPLATE_PARAMETER = STATE_VERTEX__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__PARAMETER_TEMPLATE = STATE_VERTEX__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__STEREOTYPE = STATE_VERTEX__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__TAGGED_VALUE = STATE_VERTEX__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__REFERENCE_TAG = STATE_VERTEX__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__TEMPLATE_ARGUMENT = STATE_VERTEX__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__BEHAVIOR = STATE_VERTEX__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__CLASSIFIER_ROLE = STATE_VERTEX__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__COLLABORATION = STATE_VERTEX__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__COLLABORATION_INSTANCE_SET = STATE_VERTEX__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__PARTITION = STATE_VERTEX__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__ELEMENT_IMPORT = STATE_VERTEX__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__CONTAINER = STATE_VERTEX__CONTAINER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__OUTGOING = STATE_VERTEX__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__INCOMING = STATE_VERTEX__INCOMING;

	/**
	 * The feature id for the '<em><b>Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE__BOUND = STATE_VERTEX_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Synch State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNCH_STATE_FEATURE_COUNT = STATE_VERTEX_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.StubStateImpl <em>Stub State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.StubStateImpl
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getStubState()
	 * @generated
	 */
	int STUB_STATE = 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__NAME = STATE_VERTEX__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__VISIBILITY = STATE_VERTEX__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__IS_SPECIFICATION = STATE_VERTEX__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__NAMESPACE = STATE_VERTEX__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__CLIENT_DEPENDENCY = STATE_VERTEX__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__CONSTRAINT = STATE_VERTEX__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__SUPPLIER_DEPENDENCY = STATE_VERTEX__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__PRESENTATION = STATE_VERTEX__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__TARGET_FLOW = STATE_VERTEX__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__SOURCE_FLOW = STATE_VERTEX__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__DEFAULTED_PARAMETER = STATE_VERTEX__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__COMMENT = STATE_VERTEX__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__ELEMENT_RESIDENCE = STATE_VERTEX__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__TEMPLATE_PARAMETER = STATE_VERTEX__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__PARAMETER_TEMPLATE = STATE_VERTEX__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__STEREOTYPE = STATE_VERTEX__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__TAGGED_VALUE = STATE_VERTEX__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__REFERENCE_TAG = STATE_VERTEX__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__TEMPLATE_ARGUMENT = STATE_VERTEX__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__BEHAVIOR = STATE_VERTEX__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__CLASSIFIER_ROLE = STATE_VERTEX__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__COLLABORATION = STATE_VERTEX__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__COLLABORATION_INSTANCE_SET = STATE_VERTEX__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__PARTITION = STATE_VERTEX__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__ELEMENT_IMPORT = STATE_VERTEX__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__CONTAINER = STATE_VERTEX__CONTAINER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__OUTGOING = STATE_VERTEX__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__INCOMING = STATE_VERTEX__INCOMING;

	/**
	 * The feature id for the '<em><b>Reference State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE__REFERENCE_STATE = STATE_VERTEX_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Stub State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUB_STATE_FEATURE_COUNT = STATE_VERTEX_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.FinalStateImpl <em>Final State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.FinalStateImpl
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getFinalState()
	 * @generated
	 */
	int FINAL_STATE = 16;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__NAME = STATE__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__VISIBILITY = STATE__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__IS_SPECIFICATION = STATE__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__NAMESPACE = STATE__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__CLIENT_DEPENDENCY = STATE__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__CONSTRAINT = STATE__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__SUPPLIER_DEPENDENCY = STATE__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__PRESENTATION = STATE__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__TARGET_FLOW = STATE__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__SOURCE_FLOW = STATE__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__DEFAULTED_PARAMETER = STATE__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__COMMENT = STATE__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__ELEMENT_RESIDENCE = STATE__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__TEMPLATE_PARAMETER = STATE__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__PARAMETER_TEMPLATE = STATE__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__STEREOTYPE = STATE__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__TAGGED_VALUE = STATE__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__REFERENCE_TAG = STATE__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__TEMPLATE_ARGUMENT = STATE__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__BEHAVIOR = STATE__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__CLASSIFIER_ROLE = STATE__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__COLLABORATION = STATE__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__COLLABORATION_INSTANCE_SET = STATE__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__PARTITION = STATE__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__ELEMENT_IMPORT = STATE__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__CONTAINER = STATE__CONTAINER;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__OUTGOING = STATE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__INCOMING = STATE__INCOMING;

	/**
	 * The feature id for the '<em><b>Entry</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__ENTRY = STATE__ENTRY;

	/**
	 * The feature id for the '<em><b>State Machine</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__STATE_MACHINE = STATE__STATE_MACHINE;

	/**
	 * The feature id for the '<em><b>Deferrable Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__DEFERRABLE_EVENT = STATE__DEFERRABLE_EVENT;

	/**
	 * The feature id for the '<em><b>Internal Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__INTERNAL_TRANSITION = STATE__INTERNAL_TRANSITION;

	/**
	 * The feature id for the '<em><b>Classifier In State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE__CLASSIFIER_IN_STATE = STATE__CLASSIFIER_IN_STATE;

	/**
	 * The number of structural features of the '<em>Final State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINAL_STATE_FEATURE_COUNT = STATE_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine <em>State Machine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State Machine</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine
	 * @generated
	 */
	EClass getStateMachine();

	/**
	 * Returns the meta object for the reference '{@link net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine#getContext <em>Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Context</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine#getContext()
	 * @see #getStateMachine()
	 * @generated
	 */
	EReference getStateMachine_Context();

	/**
	 * Returns the meta object for the containment reference '{@link net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine#getTop <em>Top</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Top</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine#getTop()
	 * @see #getStateMachine()
	 * @generated
	 */
	EReference getStateMachine_Top();

	/**
	 * Returns the meta object for the containment reference list '{@link net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine#getTransitions <em>Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transitions</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine#getTransitions()
	 * @see #getStateMachine()
	 * @generated
	 */
	EReference getStateMachine_Transitions();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine#getSubmachineState <em>Submachine State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Submachine State</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine#getSubmachineState()
	 * @see #getStateMachine()
	 * @generated
	 */
	EReference getStateMachine_SubmachineState();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Event
	 * @generated
	 */
	EClass getEvent();

	/**
	 * Returns the meta object for the containment reference list '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Event#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameter</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Event#getParameter()
	 * @see #getEvent()
	 * @generated
	 */
	EReference getEvent_Parameter();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Event#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>State</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Event#getState()
	 * @see #getEvent()
	 * @generated
	 */
	EReference getEvent_State();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Event#getTransition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Transition</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Event#getTransition()
	 * @see #getEvent()
	 * @generated
	 */
	EReference getEvent_Transition();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.StateVertex <em>State Vertex</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State Vertex</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.StateVertex
	 * @generated
	 */
	EClass getStateVertex();

	/**
	 * Returns the meta object for the container reference '{@link net.jgsuess.uml14.behavioral_elements.state_machines.StateVertex#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Container</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.StateVertex#getContainer()
	 * @see #getStateVertex()
	 * @generated
	 */
	EReference getStateVertex_Container();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.state_machines.StateVertex#getOutgoing <em>Outgoing</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Outgoing</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.StateVertex#getOutgoing()
	 * @see #getStateVertex()
	 * @generated
	 */
	EReference getStateVertex_Outgoing();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.state_machines.StateVertex#getIncoming <em>Incoming</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Incoming</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.StateVertex#getIncoming()
	 * @see #getStateVertex()
	 * @generated
	 */
	EReference getStateVertex_Incoming();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State
	 * @generated
	 */
	EClass getState();

	/**
	 * Returns the meta object for the containment reference '{@link net.jgsuess.uml14.behavioral_elements.state_machines.State#getEntry <em>Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Entry</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State#getEntry()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_Entry();

	/**
	 * Returns the meta object for the container reference '{@link net.jgsuess.uml14.behavioral_elements.state_machines.State#getStateMachine <em>State Machine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>State Machine</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State#getStateMachine()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_StateMachine();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.state_machines.State#getDeferrableEvent <em>Deferrable Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Deferrable Event</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State#getDeferrableEvent()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_DeferrableEvent();

	/**
	 * Returns the meta object for the containment reference list '{@link net.jgsuess.uml14.behavioral_elements.state_machines.State#getInternalTransition <em>Internal Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Internal Transition</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State#getInternalTransition()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_InternalTransition();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.state_machines.State#getClassifierInState <em>Classifier In State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Classifier In State</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State#getClassifierInState()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_ClassifierInState();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.TimeEvent <em>Time Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time Event</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.TimeEvent
	 * @generated
	 */
	EClass getTimeEvent();

	/**
	 * Returns the meta object for the containment reference '{@link net.jgsuess.uml14.behavioral_elements.state_machines.TimeEvent#getWhen <em>When</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>When</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.TimeEvent#getWhen()
	 * @see #getTimeEvent()
	 * @generated
	 */
	EReference getTimeEvent_When();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.CallEvent <em>Call Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Call Event</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.CallEvent
	 * @generated
	 */
	EClass getCallEvent();

	/**
	 * Returns the meta object for the reference '{@link net.jgsuess.uml14.behavioral_elements.state_machines.CallEvent#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operation</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.CallEvent#getOperation()
	 * @see #getCallEvent()
	 * @generated
	 */
	EReference getCallEvent_Operation();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.SignalEvent <em>Signal Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signal Event</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.SignalEvent
	 * @generated
	 */
	EClass getSignalEvent();

	/**
	 * Returns the meta object for the reference '{@link net.jgsuess.uml14.behavioral_elements.state_machines.SignalEvent#getSignal <em>Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Signal</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.SignalEvent#getSignal()
	 * @see #getSignalEvent()
	 * @generated
	 */
	EReference getSignalEvent_Signal();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the containment reference '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getGuard <em>Guard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Guard</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getGuard()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Guard();

	/**
	 * Returns the meta object for the containment reference '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getEffect <em>Effect</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Effect</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getEffect()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Effect();

	/**
	 * Returns the meta object for the container reference '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>State</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getState()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_State();

	/**
	 * Returns the meta object for the reference '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getTrigger <em>Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Trigger</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getTrigger()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Trigger();

	/**
	 * Returns the meta object for the container reference '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getStateMachine <em>State Machine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>State Machine</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getStateMachine()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_StateMachine();

	/**
	 * Returns the meta object for the reference '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getSource()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Source();

	/**
	 * Returns the meta object for the reference '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getTarget()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Target();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.CompositeState <em>Composite State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composite State</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.CompositeState
	 * @generated
	 */
	EClass getCompositeState();

	/**
	 * Returns the meta object for the attribute '{@link net.jgsuess.uml14.behavioral_elements.state_machines.CompositeState#isIsConcurrent <em>Is Concurrent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Concurrent</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.CompositeState#isIsConcurrent()
	 * @see #getCompositeState()
	 * @generated
	 */
	EAttribute getCompositeState_IsConcurrent();

	/**
	 * Returns the meta object for the containment reference list '{@link net.jgsuess.uml14.behavioral_elements.state_machines.CompositeState#getSubvertex <em>Subvertex</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Subvertex</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.CompositeState#getSubvertex()
	 * @see #getCompositeState()
	 * @generated
	 */
	EReference getCompositeState_Subvertex();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.ChangeEvent <em>Change Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Change Event</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.ChangeEvent
	 * @generated
	 */
	EClass getChangeEvent();

	/**
	 * Returns the meta object for the containment reference '{@link net.jgsuess.uml14.behavioral_elements.state_machines.ChangeEvent#getChangeExpression <em>Change Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Change Expression</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.ChangeEvent#getChangeExpression()
	 * @see #getChangeEvent()
	 * @generated
	 */
	EReference getChangeEvent_ChangeExpression();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Guard <em>Guard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Guard</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Guard
	 * @generated
	 */
	EClass getGuard();

	/**
	 * Returns the meta object for the containment reference '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Guard#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Guard#getExpression()
	 * @see #getGuard()
	 * @generated
	 */
	EReference getGuard_Expression();

	/**
	 * Returns the meta object for the container reference '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Guard#getTransition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Transition</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Guard#getTransition()
	 * @see #getGuard()
	 * @generated
	 */
	EReference getGuard_Transition();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Pseudostate <em>Pseudostate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pseudostate</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Pseudostate
	 * @generated
	 */
	EClass getPseudostate();

	/**
	 * Returns the meta object for the attribute '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Pseudostate#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Pseudostate#getKind()
	 * @see #getPseudostate()
	 * @generated
	 */
	EAttribute getPseudostate_Kind();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.SimpleState <em>Simple State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple State</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.SimpleState
	 * @generated
	 */
	EClass getSimpleState();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.SubmachineState <em>Submachine State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Submachine State</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.SubmachineState
	 * @generated
	 */
	EClass getSubmachineState();

	/**
	 * Returns the meta object for the reference '{@link net.jgsuess.uml14.behavioral_elements.state_machines.SubmachineState#getSubmachine <em>Submachine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Submachine</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.SubmachineState#getSubmachine()
	 * @see #getSubmachineState()
	 * @generated
	 */
	EReference getSubmachineState_Submachine();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.SynchState <em>Synch State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Synch State</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.SynchState
	 * @generated
	 */
	EClass getSynchState();

	/**
	 * Returns the meta object for the attribute '{@link net.jgsuess.uml14.behavioral_elements.state_machines.SynchState#getBound <em>Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bound</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.SynchState#getBound()
	 * @see #getSynchState()
	 * @generated
	 */
	EAttribute getSynchState_Bound();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.StubState <em>Stub State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stub State</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.StubState
	 * @generated
	 */
	EClass getStubState();

	/**
	 * Returns the meta object for the attribute '{@link net.jgsuess.uml14.behavioral_elements.state_machines.StubState#getReferenceState <em>Reference State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reference State</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.StubState#getReferenceState()
	 * @see #getStubState()
	 * @generated
	 */
	EAttribute getStubState_ReferenceState();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.state_machines.FinalState <em>Final State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Final State</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.FinalState
	 * @generated
	 */
	EClass getFinalState();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	State_machinesFactory getState_machinesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.StateMachineImpl <em>State Machine</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.StateMachineImpl
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getStateMachine()
		 * @generated
		 */
		EClass STATE_MACHINE = eINSTANCE.getStateMachine();

		/**
		 * The meta object literal for the '<em><b>Context</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE__CONTEXT = eINSTANCE.getStateMachine_Context();

		/**
		 * The meta object literal for the '<em><b>Top</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE__TOP = eINSTANCE.getStateMachine_Top();

		/**
		 * The meta object literal for the '<em><b>Transitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE__TRANSITIONS = eINSTANCE.getStateMachine_Transitions();

		/**
		 * The meta object literal for the '<em><b>Submachine State</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE__SUBMACHINE_STATE = eINSTANCE.getStateMachine_SubmachineState();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.EventImpl
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getEvent()
		 * @generated
		 */
		EClass EVENT = eINSTANCE.getEvent();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT__PARAMETER = eINSTANCE.getEvent_Parameter();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT__STATE = eINSTANCE.getEvent_State();

		/**
		 * The meta object literal for the '<em><b>Transition</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT__TRANSITION = eINSTANCE.getEvent_Transition();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.StateVertexImpl <em>State Vertex</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.StateVertexImpl
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getStateVertex()
		 * @generated
		 */
		EClass STATE_VERTEX = eINSTANCE.getStateVertex();

		/**
		 * The meta object literal for the '<em><b>Container</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_VERTEX__CONTAINER = eINSTANCE.getStateVertex_Container();

		/**
		 * The meta object literal for the '<em><b>Outgoing</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_VERTEX__OUTGOING = eINSTANCE.getStateVertex_Outgoing();

		/**
		 * The meta object literal for the '<em><b>Incoming</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_VERTEX__INCOMING = eINSTANCE.getStateVertex_Incoming();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.StateImpl <em>State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.StateImpl
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getState()
		 * @generated
		 */
		EClass STATE = eINSTANCE.getState();

		/**
		 * The meta object literal for the '<em><b>Entry</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__ENTRY = eINSTANCE.getState_Entry();

		/**
		 * The meta object literal for the '<em><b>State Machine</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__STATE_MACHINE = eINSTANCE.getState_StateMachine();

		/**
		 * The meta object literal for the '<em><b>Deferrable Event</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__DEFERRABLE_EVENT = eINSTANCE.getState_DeferrableEvent();

		/**
		 * The meta object literal for the '<em><b>Internal Transition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__INTERNAL_TRANSITION = eINSTANCE.getState_InternalTransition();

		/**
		 * The meta object literal for the '<em><b>Classifier In State</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__CLASSIFIER_IN_STATE = eINSTANCE.getState_ClassifierInState();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.TimeEventImpl <em>Time Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.TimeEventImpl
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getTimeEvent()
		 * @generated
		 */
		EClass TIME_EVENT = eINSTANCE.getTimeEvent();

		/**
		 * The meta object literal for the '<em><b>When</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_EVENT__WHEN = eINSTANCE.getTimeEvent_When();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.CallEventImpl <em>Call Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.CallEventImpl
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getCallEvent()
		 * @generated
		 */
		EClass CALL_EVENT = eINSTANCE.getCallEvent();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_EVENT__OPERATION = eINSTANCE.getCallEvent_Operation();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.SignalEventImpl <em>Signal Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.SignalEventImpl
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getSignalEvent()
		 * @generated
		 */
		EClass SIGNAL_EVENT = eINSTANCE.getSignalEvent();

		/**
		 * The meta object literal for the '<em><b>Signal</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIGNAL_EVENT__SIGNAL = eINSTANCE.getSignalEvent_Signal();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.TransitionImpl <em>Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.TransitionImpl
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getTransition()
		 * @generated
		 */
		EClass TRANSITION = eINSTANCE.getTransition();

		/**
		 * The meta object literal for the '<em><b>Guard</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__GUARD = eINSTANCE.getTransition_Guard();

		/**
		 * The meta object literal for the '<em><b>Effect</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__EFFECT = eINSTANCE.getTransition_Effect();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__STATE = eINSTANCE.getTransition_State();

		/**
		 * The meta object literal for the '<em><b>Trigger</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__TRIGGER = eINSTANCE.getTransition_Trigger();

		/**
		 * The meta object literal for the '<em><b>State Machine</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__STATE_MACHINE = eINSTANCE.getTransition_StateMachine();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__SOURCE = eINSTANCE.getTransition_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__TARGET = eINSTANCE.getTransition_Target();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.CompositeStateImpl <em>Composite State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.CompositeStateImpl
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getCompositeState()
		 * @generated
		 */
		EClass COMPOSITE_STATE = eINSTANCE.getCompositeState();

		/**
		 * The meta object literal for the '<em><b>Is Concurrent</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPOSITE_STATE__IS_CONCURRENT = eINSTANCE.getCompositeState_IsConcurrent();

		/**
		 * The meta object literal for the '<em><b>Subvertex</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_STATE__SUBVERTEX = eINSTANCE.getCompositeState_Subvertex();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.ChangeEventImpl <em>Change Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.ChangeEventImpl
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getChangeEvent()
		 * @generated
		 */
		EClass CHANGE_EVENT = eINSTANCE.getChangeEvent();

		/**
		 * The meta object literal for the '<em><b>Change Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHANGE_EVENT__CHANGE_EXPRESSION = eINSTANCE.getChangeEvent_ChangeExpression();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.GuardImpl <em>Guard</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.GuardImpl
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getGuard()
		 * @generated
		 */
		EClass GUARD = eINSTANCE.getGuard();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GUARD__EXPRESSION = eINSTANCE.getGuard_Expression();

		/**
		 * The meta object literal for the '<em><b>Transition</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GUARD__TRANSITION = eINSTANCE.getGuard_Transition();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.PseudostateImpl <em>Pseudostate</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.PseudostateImpl
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getPseudostate()
		 * @generated
		 */
		EClass PSEUDOSTATE = eINSTANCE.getPseudostate();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PSEUDOSTATE__KIND = eINSTANCE.getPseudostate_Kind();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.SimpleStateImpl <em>Simple State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.SimpleStateImpl
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getSimpleState()
		 * @generated
		 */
		EClass SIMPLE_STATE = eINSTANCE.getSimpleState();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.SubmachineStateImpl <em>Submachine State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.SubmachineStateImpl
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getSubmachineState()
		 * @generated
		 */
		EClass SUBMACHINE_STATE = eINSTANCE.getSubmachineState();

		/**
		 * The meta object literal for the '<em><b>Submachine</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBMACHINE_STATE__SUBMACHINE = eINSTANCE.getSubmachineState_Submachine();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.SynchStateImpl <em>Synch State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.SynchStateImpl
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getSynchState()
		 * @generated
		 */
		EClass SYNCH_STATE = eINSTANCE.getSynchState();

		/**
		 * The meta object literal for the '<em><b>Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYNCH_STATE__BOUND = eINSTANCE.getSynchState_Bound();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.StubStateImpl <em>Stub State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.StubStateImpl
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getStubState()
		 * @generated
		 */
		EClass STUB_STATE = eINSTANCE.getStubState();

		/**
		 * The meta object literal for the '<em><b>Reference State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STUB_STATE__REFERENCE_STATE = eINSTANCE.getStubState_ReferenceState();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.FinalStateImpl <em>Final State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.FinalStateImpl
		 * @see net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl#getFinalState()
		 * @generated
		 */
		EClass FINAL_STATE = eINSTANCE.getFinalState();

	}

} //State_machinesPackage
