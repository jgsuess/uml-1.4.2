/**
 * <copyright>
 * </copyright>
 *
 * $Id: StateMachine.java,v 1.1 2012/04/23 09:31:19 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.state_machines;

import net.jgsuess.uml14.foundation.core.ModelElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State Machine</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine#getContext <em>Context</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine#getTop <em>Top</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine#getTransitions <em>Transitions</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine#getSubmachineState <em>Submachine State</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getStateMachine()
 * @model
 * @generated
 */
public interface StateMachine extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Context</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.ModelElement#getBehavior <em>Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Context</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Context</em>' reference.
	 * @see #setContext(ModelElement)
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getStateMachine_Context()
	 * @see net.jgsuess.uml14.foundation.core.ModelElement#getBehavior
	 * @model opposite="behavior"
	 * @generated
	 */
	ModelElement getContext();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine#getContext <em>Context</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Context</em>' reference.
	 * @see #getContext()
	 * @generated
	 */
	void setContext(ModelElement value);

	/**
	 * Returns the value of the '<em><b>Top</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.state_machines.State#getStateMachine <em>State Machine</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Top</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Top</em>' containment reference.
	 * @see #setTop(State)
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getStateMachine_Top()
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State#getStateMachine
	 * @model opposite="stateMachine" containment="true" required="true"
	 * @generated
	 */
	State getTop();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine#getTop <em>Top</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Top</em>' containment reference.
	 * @see #getTop()
	 * @generated
	 */
	void setTop(State value);

	/**
	 * Returns the value of the '<em><b>Transitions</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.state_machines.Transition}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getStateMachine <em>State Machine</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transitions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transitions</em>' containment reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getStateMachine_Transitions()
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getStateMachine
	 * @model opposite="stateMachine" containment="true"
	 * @generated
	 */
	EList<Transition> getTransitions();

	/**
	 * Returns the value of the '<em><b>Submachine State</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.state_machines.SubmachineState}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.state_machines.SubmachineState#getSubmachine <em>Submachine</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Submachine State</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Submachine State</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getStateMachine_SubmachineState()
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.SubmachineState#getSubmachine
	 * @model opposite="submachine"
	 * @generated
	 */
	EList<SubmachineState> getSubmachineState();

} // StateMachine
