/**
 * <copyright>
 * </copyright>
 *
 * $Id: StubState.java,v 1.1 2012/04/23 09:31:18 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.state_machines;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stub State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.StubState#getReferenceState <em>Reference State</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getStubState()
 * @model
 * @generated
 */
public interface StubState extends StateVertex {
	/**
	 * Returns the value of the '<em><b>Reference State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference State</em>' attribute.
	 * @see #setReferenceState(String)
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getStubState_ReferenceState()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Name"
	 * @generated
	 */
	String getReferenceState();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.StubState#getReferenceState <em>Reference State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference State</em>' attribute.
	 * @see #getReferenceState()
	 * @generated
	 */
	void setReferenceState(String value);

} // StubState
