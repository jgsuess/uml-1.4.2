/**
 * <copyright>
 * </copyright>
 *
 * $Id: Event.java,v 1.1 2012/04/23 09:31:19 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.state_machines;

import net.jgsuess.uml14.foundation.core.ModelElement;
import net.jgsuess.uml14.foundation.core.Parameter;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.Event#getParameter <em>Parameter</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.Event#getState <em>State</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.Event#getTransition <em>Transition</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getEvent()
 * @model abstract="true"
 * @generated
 */
public interface Event extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Parameter}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Parameter#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter</em>' containment reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getEvent_Parameter()
	 * @see net.jgsuess.uml14.foundation.core.Parameter#getEvent
	 * @model opposite="event" containment="true"
	 * @generated
	 */
	EList<Parameter> getParameter();

	/**
	 * Returns the value of the '<em><b>State</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.state_machines.State}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.state_machines.State#getDeferrableEvent <em>Deferrable Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getEvent_State()
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State#getDeferrableEvent
	 * @model opposite="deferrableEvent"
	 * @generated
	 */
	EList<State> getState();

	/**
	 * Returns the value of the '<em><b>Transition</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.state_machines.Transition}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getTrigger <em>Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transition</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getEvent_Transition()
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getTrigger
	 * @model opposite="trigger"
	 * @generated
	 */
	EList<Transition> getTransition();

} // Event
