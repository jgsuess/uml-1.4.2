/**
 * <copyright>
 * </copyright>
 *
 * $Id: SynchState.java,v 1.1 2012/04/23 09:31:18 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.state_machines;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Synch State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.SynchState#getBound <em>Bound</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getSynchState()
 * @model
 * @generated
 */
public interface SynchState extends StateVertex {
	/**
	 * Returns the value of the '<em><b>Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bound</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bound</em>' attribute.
	 * @see #setBound(long)
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getSynchState_Bound()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.UnlimitedInteger"
	 * @generated
	 */
	long getBound();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.SynchState#getBound <em>Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bound</em>' attribute.
	 * @see #getBound()
	 * @generated
	 */
	void setBound(long value);

} // SynchState
