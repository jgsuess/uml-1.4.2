/**
 * <copyright>
 * </copyright>
 *
 * $Id: State.java,v 1.1 2012/04/23 09:31:19 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.state_machines;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Action;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.State#getEntry <em>Entry</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.State#getStateMachine <em>State Machine</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.State#getDeferrableEvent <em>Deferrable Event</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.State#getInternalTransition <em>Internal Transition</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.State#getClassifierInState <em>Classifier In State</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getState()
 * @model abstract="true"
 * @generated
 */
public interface State extends StateVertex {
	/**
	 * Returns the value of the '<em><b>Entry</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry</em>' containment reference.
	 * @see #setEntry(Action)
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getState_Entry()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Action#getState
	 * @model opposite="state" containment="true"
	 * @generated
	 */
	Action getEntry();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.State#getEntry <em>Entry</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entry</em>' containment reference.
	 * @see #getEntry()
	 * @generated
	 */
	void setEntry(Action value);

	/**
	 * Returns the value of the '<em><b>State Machine</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine#getTop <em>Top</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State Machine</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State Machine</em>' container reference.
	 * @see #setStateMachine(StateMachine)
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getState_StateMachine()
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine#getTop
	 * @model opposite="top"
	 * @generated
	 */
	StateMachine getStateMachine();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.State#getStateMachine <em>State Machine</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State Machine</em>' container reference.
	 * @see #getStateMachine()
	 * @generated
	 */
	void setStateMachine(StateMachine value);

	/**
	 * Returns the value of the '<em><b>Deferrable Event</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.state_machines.Event}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Event#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deferrable Event</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deferrable Event</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getState_DeferrableEvent()
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Event#getState
	 * @model opposite="state"
	 * @generated
	 */
	EList<Event> getDeferrableEvent();

	/**
	 * Returns the value of the '<em><b>Internal Transition</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.state_machines.Transition}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal Transition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal Transition</em>' containment reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getState_InternalTransition()
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Transition#getState
	 * @model opposite="state" containment="true"
	 * @generated
	 */
	EList<Transition> getInternalTransition();

	/**
	 * Returns the value of the '<em><b>Classifier In State</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState#getInState <em>In State</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classifier In State</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classifier In State</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getState_ClassifierInState()
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState#getInState
	 * @model opposite="inState"
	 * @generated
	 */
	EList<ClassifierInState> getClassifierInState();

} // State
