/**
 * <copyright>
 * </copyright>
 *
 * $Id: CompositeState.java,v 1.1 2012/04/23 09:31:18 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.state_machines;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.CompositeState#isIsConcurrent <em>Is Concurrent</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.CompositeState#getSubvertex <em>Subvertex</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getCompositeState()
 * @model
 * @generated
 */
public interface CompositeState extends State {
	/**
	 * Returns the value of the '<em><b>Is Concurrent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Concurrent</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Concurrent</em>' attribute.
	 * @see #setIsConcurrent(boolean)
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getCompositeState_IsConcurrent()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Boolean"
	 * @generated
	 */
	boolean isIsConcurrent();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.CompositeState#isIsConcurrent <em>Is Concurrent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Concurrent</em>' attribute.
	 * @see #isIsConcurrent()
	 * @generated
	 */
	void setIsConcurrent(boolean value);

	/**
	 * Returns the value of the '<em><b>Subvertex</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.state_machines.StateVertex}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.state_machines.StateVertex#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subvertex</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subvertex</em>' containment reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getCompositeState_Subvertex()
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.StateVertex#getContainer
	 * @model opposite="container" containment="true"
	 * @generated
	 */
	EList<StateVertex> getSubvertex();

} // CompositeState
