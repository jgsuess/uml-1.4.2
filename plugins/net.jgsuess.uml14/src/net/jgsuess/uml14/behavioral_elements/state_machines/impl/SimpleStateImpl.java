/**
 * <copyright>
 * </copyright>
 *
 * $Id: SimpleStateImpl.java,v 1.1 2012/04/23 09:31:24 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.state_machines.impl;

import net.jgsuess.uml14.behavioral_elements.state_machines.SimpleState;
import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simple State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class SimpleStateImpl extends StateImpl implements SimpleState {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimpleStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return State_machinesPackage.Literals.SIMPLE_STATE;
	}

} //SimpleStateImpl
