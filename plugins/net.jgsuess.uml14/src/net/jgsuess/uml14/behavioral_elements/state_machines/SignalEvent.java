/**
 * <copyright>
 * </copyright>
 *
 * $Id: SignalEvent.java,v 1.1 2012/04/23 09:31:19 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.state_machines;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Signal;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signal Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.SignalEvent#getSignal <em>Signal</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getSignalEvent()
 * @model
 * @generated
 */
public interface SignalEvent extends Event {
	/**
	 * Returns the value of the '<em><b>Signal</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Signal#getOccurrence <em>Occurrence</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signal</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signal</em>' reference.
	 * @see #setSignal(Signal)
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getSignalEvent_Signal()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Signal#getOccurrence
	 * @model opposite="occurrence" required="true"
	 * @generated
	 */
	Signal getSignal();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.SignalEvent#getSignal <em>Signal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signal</em>' reference.
	 * @see #getSignal()
	 * @generated
	 */
	void setSignal(Signal value);

} // SignalEvent
