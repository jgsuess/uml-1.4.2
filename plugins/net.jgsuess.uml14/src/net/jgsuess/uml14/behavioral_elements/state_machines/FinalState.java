/**
 * <copyright>
 * </copyright>
 *
 * $Id: FinalState.java,v 1.1 2012/04/23 09:31:18 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.state_machines;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Final State</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getFinalState()
 * @model
 * @generated
 */
public interface FinalState extends State {
} // FinalState
