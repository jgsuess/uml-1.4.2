/**
 * <copyright>
 * </copyright>
 *
 * $Id: StateImpl.java,v 1.1 2012/04/23 09:31:26 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.state_machines.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage;
import net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Action;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;

import net.jgsuess.uml14.behavioral_elements.state_machines.Event;
import net.jgsuess.uml14.behavioral_elements.state_machines.State;
import net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine;
import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage;
import net.jgsuess.uml14.behavioral_elements.state_machines.Transition;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.StateImpl#getEntry <em>Entry</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.StateImpl#getStateMachine <em>State Machine</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.StateImpl#getDeferrableEvent <em>Deferrable Event</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.StateImpl#getInternalTransition <em>Internal Transition</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.StateImpl#getClassifierInState <em>Classifier In State</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class StateImpl extends StateVertexImpl implements State {
	/**
	 * The cached value of the '{@link #getEntry() <em>Entry</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntry()
	 * @generated
	 * @ordered
	 */
	protected Action entry;

	/**
	 * The cached value of the '{@link #getDeferrableEvent() <em>Deferrable Event</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeferrableEvent()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> deferrableEvent;

	/**
	 * The cached value of the '{@link #getInternalTransition() <em>Internal Transition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInternalTransition()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> internalTransition;

	/**
	 * The cached value of the '{@link #getClassifierInState() <em>Classifier In State</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassifierInState()
	 * @generated
	 * @ordered
	 */
	protected EList<ClassifierInState> classifierInState;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return State_machinesPackage.Literals.STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action getEntry() {
		return entry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEntry(Action newEntry, NotificationChain msgs) {
		Action oldEntry = entry;
		entry = newEntry;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, State_machinesPackage.STATE__ENTRY, oldEntry, newEntry);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntry(Action newEntry) {
		if (newEntry != entry) {
			NotificationChain msgs = null;
			if (entry != null)
				msgs = ((InternalEObject)entry).eInverseRemove(this, Common_behaviorPackage.ACTION__STATE, Action.class, msgs);
			if (newEntry != null)
				msgs = ((InternalEObject)newEntry).eInverseAdd(this, Common_behaviorPackage.ACTION__STATE, Action.class, msgs);
			msgs = basicSetEntry(newEntry, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, State_machinesPackage.STATE__ENTRY, newEntry, newEntry));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateMachine getStateMachine() {
		if (eContainerFeatureID() != State_machinesPackage.STATE__STATE_MACHINE) return null;
		return (StateMachine)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStateMachine(StateMachine newStateMachine, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newStateMachine, State_machinesPackage.STATE__STATE_MACHINE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStateMachine(StateMachine newStateMachine) {
		if (newStateMachine != eInternalContainer() || (eContainerFeatureID() != State_machinesPackage.STATE__STATE_MACHINE && newStateMachine != null)) {
			if (EcoreUtil.isAncestor(this, newStateMachine))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newStateMachine != null)
				msgs = ((InternalEObject)newStateMachine).eInverseAdd(this, State_machinesPackage.STATE_MACHINE__TOP, StateMachine.class, msgs);
			msgs = basicSetStateMachine(newStateMachine, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, State_machinesPackage.STATE__STATE_MACHINE, newStateMachine, newStateMachine));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getDeferrableEvent() {
		if (deferrableEvent == null) {
			deferrableEvent = new EObjectWithInverseResolvingEList.ManyInverse<Event>(Event.class, this, State_machinesPackage.STATE__DEFERRABLE_EVENT, State_machinesPackage.EVENT__STATE);
		}
		return deferrableEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getInternalTransition() {
		if (internalTransition == null) {
			internalTransition = new EObjectContainmentWithInverseEList<Transition>(Transition.class, this, State_machinesPackage.STATE__INTERNAL_TRANSITION, State_machinesPackage.TRANSITION__STATE);
		}
		return internalTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ClassifierInState> getClassifierInState() {
		if (classifierInState == null) {
			classifierInState = new EObjectWithInverseResolvingEList.ManyInverse<ClassifierInState>(ClassifierInState.class, this, State_machinesPackage.STATE__CLASSIFIER_IN_STATE, Activity_graphsPackage.CLASSIFIER_IN_STATE__IN_STATE);
		}
		return classifierInState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case State_machinesPackage.STATE__ENTRY:
				if (entry != null)
					msgs = ((InternalEObject)entry).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - State_machinesPackage.STATE__ENTRY, null, msgs);
				return basicSetEntry((Action)otherEnd, msgs);
			case State_machinesPackage.STATE__STATE_MACHINE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetStateMachine((StateMachine)otherEnd, msgs);
			case State_machinesPackage.STATE__DEFERRABLE_EVENT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getDeferrableEvent()).basicAdd(otherEnd, msgs);
			case State_machinesPackage.STATE__INTERNAL_TRANSITION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getInternalTransition()).basicAdd(otherEnd, msgs);
			case State_machinesPackage.STATE__CLASSIFIER_IN_STATE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getClassifierInState()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case State_machinesPackage.STATE__ENTRY:
				return basicSetEntry(null, msgs);
			case State_machinesPackage.STATE__STATE_MACHINE:
				return basicSetStateMachine(null, msgs);
			case State_machinesPackage.STATE__DEFERRABLE_EVENT:
				return ((InternalEList<?>)getDeferrableEvent()).basicRemove(otherEnd, msgs);
			case State_machinesPackage.STATE__INTERNAL_TRANSITION:
				return ((InternalEList<?>)getInternalTransition()).basicRemove(otherEnd, msgs);
			case State_machinesPackage.STATE__CLASSIFIER_IN_STATE:
				return ((InternalEList<?>)getClassifierInState()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case State_machinesPackage.STATE__STATE_MACHINE:
				return eInternalContainer().eInverseRemove(this, State_machinesPackage.STATE_MACHINE__TOP, StateMachine.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case State_machinesPackage.STATE__ENTRY:
				return getEntry();
			case State_machinesPackage.STATE__STATE_MACHINE:
				return getStateMachine();
			case State_machinesPackage.STATE__DEFERRABLE_EVENT:
				return getDeferrableEvent();
			case State_machinesPackage.STATE__INTERNAL_TRANSITION:
				return getInternalTransition();
			case State_machinesPackage.STATE__CLASSIFIER_IN_STATE:
				return getClassifierInState();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case State_machinesPackage.STATE__ENTRY:
				setEntry((Action)newValue);
				return;
			case State_machinesPackage.STATE__STATE_MACHINE:
				setStateMachine((StateMachine)newValue);
				return;
			case State_machinesPackage.STATE__DEFERRABLE_EVENT:
				getDeferrableEvent().clear();
				getDeferrableEvent().addAll((Collection<? extends Event>)newValue);
				return;
			case State_machinesPackage.STATE__INTERNAL_TRANSITION:
				getInternalTransition().clear();
				getInternalTransition().addAll((Collection<? extends Transition>)newValue);
				return;
			case State_machinesPackage.STATE__CLASSIFIER_IN_STATE:
				getClassifierInState().clear();
				getClassifierInState().addAll((Collection<? extends ClassifierInState>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case State_machinesPackage.STATE__ENTRY:
				setEntry((Action)null);
				return;
			case State_machinesPackage.STATE__STATE_MACHINE:
				setStateMachine((StateMachine)null);
				return;
			case State_machinesPackage.STATE__DEFERRABLE_EVENT:
				getDeferrableEvent().clear();
				return;
			case State_machinesPackage.STATE__INTERNAL_TRANSITION:
				getInternalTransition().clear();
				return;
			case State_machinesPackage.STATE__CLASSIFIER_IN_STATE:
				getClassifierInState().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case State_machinesPackage.STATE__ENTRY:
				return entry != null;
			case State_machinesPackage.STATE__STATE_MACHINE:
				return getStateMachine() != null;
			case State_machinesPackage.STATE__DEFERRABLE_EVENT:
				return deferrableEvent != null && !deferrableEvent.isEmpty();
			case State_machinesPackage.STATE__INTERNAL_TRANSITION:
				return internalTransition != null && !internalTransition.isEmpty();
			case State_machinesPackage.STATE__CLASSIFIER_IN_STATE:
				return classifierInState != null && !classifierInState.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //StateImpl
