/**
 * <copyright>
 * </copyright>
 *
 * $Id: TimeEvent.java,v 1.1 2012/04/23 09:31:18 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.state_machines;

import net.jgsuess.uml14.foundation.data_types.TimeExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.TimeEvent#getWhen <em>When</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getTimeEvent()
 * @model
 * @generated
 */
public interface TimeEvent extends Event {
	/**
	 * Returns the value of the '<em><b>When</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>When</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>When</em>' containment reference.
	 * @see #setWhen(TimeExpression)
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getTimeEvent_When()
	 * @model containment="true"
	 * @generated
	 */
	TimeExpression getWhen();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.TimeEvent#getWhen <em>When</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>When</em>' containment reference.
	 * @see #getWhen()
	 * @generated
	 */
	void setWhen(TimeExpression value);

} // TimeEvent
