/**
 * <copyright>
 * </copyright>
 *
 * $Id: CompositeStateImpl.java,v 1.1 2012/04/23 09:31:24 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.state_machines.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.state_machines.CompositeState;
import net.jgsuess.uml14.behavioral_elements.state_machines.StateVertex;
import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Composite State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.CompositeStateImpl#isIsConcurrent <em>Is Concurrent</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.impl.CompositeStateImpl#getSubvertex <em>Subvertex</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CompositeStateImpl extends StateImpl implements CompositeState {
	/**
	 * The default value of the '{@link #isIsConcurrent() <em>Is Concurrent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsConcurrent()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_CONCURRENT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsConcurrent() <em>Is Concurrent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsConcurrent()
	 * @generated
	 * @ordered
	 */
	protected boolean isConcurrent = IS_CONCURRENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSubvertex() <em>Subvertex</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubvertex()
	 * @generated
	 * @ordered
	 */
	protected EList<StateVertex> subvertex;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompositeStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return State_machinesPackage.Literals.COMPOSITE_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsConcurrent() {
		return isConcurrent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsConcurrent(boolean newIsConcurrent) {
		boolean oldIsConcurrent = isConcurrent;
		isConcurrent = newIsConcurrent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, State_machinesPackage.COMPOSITE_STATE__IS_CONCURRENT, oldIsConcurrent, isConcurrent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StateVertex> getSubvertex() {
		if (subvertex == null) {
			subvertex = new EObjectContainmentWithInverseEList<StateVertex>(StateVertex.class, this, State_machinesPackage.COMPOSITE_STATE__SUBVERTEX, State_machinesPackage.STATE_VERTEX__CONTAINER);
		}
		return subvertex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case State_machinesPackage.COMPOSITE_STATE__SUBVERTEX:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSubvertex()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case State_machinesPackage.COMPOSITE_STATE__SUBVERTEX:
				return ((InternalEList<?>)getSubvertex()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case State_machinesPackage.COMPOSITE_STATE__IS_CONCURRENT:
				return isIsConcurrent();
			case State_machinesPackage.COMPOSITE_STATE__SUBVERTEX:
				return getSubvertex();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case State_machinesPackage.COMPOSITE_STATE__IS_CONCURRENT:
				setIsConcurrent((Boolean)newValue);
				return;
			case State_machinesPackage.COMPOSITE_STATE__SUBVERTEX:
				getSubvertex().clear();
				getSubvertex().addAll((Collection<? extends StateVertex>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case State_machinesPackage.COMPOSITE_STATE__IS_CONCURRENT:
				setIsConcurrent(IS_CONCURRENT_EDEFAULT);
				return;
			case State_machinesPackage.COMPOSITE_STATE__SUBVERTEX:
				getSubvertex().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case State_machinesPackage.COMPOSITE_STATE__IS_CONCURRENT:
				return isConcurrent != IS_CONCURRENT_EDEFAULT;
			case State_machinesPackage.COMPOSITE_STATE__SUBVERTEX:
				return subvertex != null && !subvertex.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isConcurrent: ");
		result.append(isConcurrent);
		result.append(')');
		return result.toString();
	}

} //CompositeStateImpl
