/**
 * <copyright>
 * </copyright>
 *
 * $Id: SubmachineState.java,v 1.1 2012/04/23 09:31:18 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.state_machines;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Submachine State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.state_machines.SubmachineState#getSubmachine <em>Submachine</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getSubmachineState()
 * @model
 * @generated
 */
public interface SubmachineState extends CompositeState {
	/**
	 * Returns the value of the '<em><b>Submachine</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine#getSubmachineState <em>Submachine State</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Submachine</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Submachine</em>' reference.
	 * @see #setSubmachine(StateMachine)
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage#getSubmachineState_Submachine()
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine#getSubmachineState
	 * @model opposite="submachineState" required="true"
	 * @generated
	 */
	StateMachine getSubmachine();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.state_machines.SubmachineState#getSubmachine <em>Submachine</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Submachine</em>' reference.
	 * @see #getSubmachine()
	 * @generated
	 */
	void setSubmachine(StateMachine value);

} // SubmachineState
