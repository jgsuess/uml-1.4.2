/**
 * <copyright>
 * </copyright>
 *
 * $Id: Actor.java,v 1.1 2012/04/23 09:31:35 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.use_cases;

import net.jgsuess.uml14.foundation.core.Classifier;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage#getActor()
 * @model
 * @generated
 */
public interface Actor extends Classifier {
} // Actor
