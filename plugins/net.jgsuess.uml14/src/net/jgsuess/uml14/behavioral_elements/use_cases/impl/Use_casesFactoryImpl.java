/**
 * <copyright>
 * </copyright>
 *
 * $Id: Use_casesFactoryImpl.java,v 1.1 2012/04/23 09:31:38 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.use_cases.impl;

import net.jgsuess.uml14.behavioral_elements.use_cases.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Use_casesFactoryImpl extends EFactoryImpl implements Use_casesFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Use_casesFactory init() {
		try {
			Use_casesFactory theUse_casesFactory = (Use_casesFactory)EPackage.Registry.INSTANCE.getEFactory("http://jgsuess.net/uml14/use_cases"); 
			if (theUse_casesFactory != null) {
				return theUse_casesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Use_casesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Use_casesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Use_casesPackage.USE_CASE: return createUseCase();
			case Use_casesPackage.ACTOR: return createActor();
			case Use_casesPackage.USE_CASE_INSTANCE: return createUseCaseInstance();
			case Use_casesPackage.EXTEND: return createExtend();
			case Use_casesPackage.INCLUDE: return createInclude();
			case Use_casesPackage.EXTENSION_POINT: return createExtensionPoint();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseCase createUseCase() {
		UseCaseImpl useCase = new UseCaseImpl();
		return useCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Actor createActor() {
		ActorImpl actor = new ActorImpl();
		return actor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseCaseInstance createUseCaseInstance() {
		UseCaseInstanceImpl useCaseInstance = new UseCaseInstanceImpl();
		return useCaseInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Extend createExtend() {
		ExtendImpl extend = new ExtendImpl();
		return extend;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Include createInclude() {
		IncludeImpl include = new IncludeImpl();
		return include;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExtensionPoint createExtensionPoint() {
		ExtensionPointImpl extensionPoint = new ExtensionPointImpl();
		return extensionPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Use_casesPackage getUse_casesPackage() {
		return (Use_casesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Use_casesPackage getPackage() {
		return Use_casesPackage.eINSTANCE;
	}

} //Use_casesFactoryImpl
