/**
 * <copyright>
 * </copyright>
 *
 * $Id: Use_casesPackageImpl.java,v 1.1 2012/04/23 09:31:39 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.use_cases.impl;

import net.jgsuess.uml14.Uml14Package;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.Activity_graphsPackageImpl;

import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage;

import net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationsPackageImpl;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;

import net.jgsuess.uml14.behavioral_elements.common_behavior.impl.Common_behaviorPackageImpl;

import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage;

import net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl;

import net.jgsuess.uml14.behavioral_elements.use_cases.Actor;
import net.jgsuess.uml14.behavioral_elements.use_cases.Extend;
import net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint;
import net.jgsuess.uml14.behavioral_elements.use_cases.Include;
import net.jgsuess.uml14.behavioral_elements.use_cases.UseCase;
import net.jgsuess.uml14.behavioral_elements.use_cases.UseCaseInstance;
import net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesFactory;
import net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage;

import net.jgsuess.uml14.foundation.core.CorePackage;

import net.jgsuess.uml14.foundation.core.impl.CorePackageImpl;

import net.jgsuess.uml14.foundation.data_types.Data_typesPackage;

import net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl;

import net.jgsuess.uml14.impl.Uml14PackageImpl;

import net.jgsuess.uml14.model_management.Model_managementPackage;

import net.jgsuess.uml14.model_management.impl.Model_managementPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Use_casesPackageImpl extends EPackageImpl implements Use_casesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass useCaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass useCaseInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass extendEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass includeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass extensionPointEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Use_casesPackageImpl() {
		super(eNS_URI, Use_casesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link Use_casesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Use_casesPackage init() {
		if (isInited) return (Use_casesPackage)EPackage.Registry.INSTANCE.getEPackage(Use_casesPackage.eNS_URI);

		// Obtain or create and register package
		Use_casesPackageImpl theUse_casesPackage = (Use_casesPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof Use_casesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new Use_casesPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		Uml14PackageImpl theUml14Package = (Uml14PackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Uml14Package.eNS_URI) instanceof Uml14PackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Uml14Package.eNS_URI) : Uml14Package.eINSTANCE);
		Model_managementPackageImpl theModel_managementPackage = (Model_managementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Model_managementPackage.eNS_URI) instanceof Model_managementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Model_managementPackage.eNS_URI) : Model_managementPackage.eINSTANCE);
		Data_typesPackageImpl theData_typesPackage = (Data_typesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Data_typesPackage.eNS_URI) instanceof Data_typesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Data_typesPackage.eNS_URI) : Data_typesPackage.eINSTANCE);
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		Common_behaviorPackageImpl theCommon_behaviorPackage = (Common_behaviorPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Common_behaviorPackage.eNS_URI) instanceof Common_behaviorPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Common_behaviorPackage.eNS_URI) : Common_behaviorPackage.eINSTANCE);
		State_machinesPackageImpl theState_machinesPackage = (State_machinesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(State_machinesPackage.eNS_URI) instanceof State_machinesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(State_machinesPackage.eNS_URI) : State_machinesPackage.eINSTANCE);
		CollaborationsPackageImpl theCollaborationsPackage = (CollaborationsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CollaborationsPackage.eNS_URI) instanceof CollaborationsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CollaborationsPackage.eNS_URI) : CollaborationsPackage.eINSTANCE);
		Activity_graphsPackageImpl theActivity_graphsPackage = (Activity_graphsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Activity_graphsPackage.eNS_URI) instanceof Activity_graphsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Activity_graphsPackage.eNS_URI) : Activity_graphsPackage.eINSTANCE);

		// Create package meta-data objects
		theUse_casesPackage.createPackageContents();
		theUml14Package.createPackageContents();
		theModel_managementPackage.createPackageContents();
		theData_typesPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theCommon_behaviorPackage.createPackageContents();
		theState_machinesPackage.createPackageContents();
		theCollaborationsPackage.createPackageContents();
		theActivity_graphsPackage.createPackageContents();

		// Initialize created meta-data
		theUse_casesPackage.initializePackageContents();
		theUml14Package.initializePackageContents();
		theModel_managementPackage.initializePackageContents();
		theData_typesPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theCommon_behaviorPackage.initializePackageContents();
		theState_machinesPackage.initializePackageContents();
		theCollaborationsPackage.initializePackageContents();
		theActivity_graphsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theUse_casesPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Use_casesPackage.eNS_URI, theUse_casesPackage);
		return theUse_casesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUseCase() {
		return useCaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUseCase_Extender() {
		return (EReference)useCaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUseCase_Extend() {
		return (EReference)useCaseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUseCase_Includer() {
		return (EReference)useCaseEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUseCase_Include() {
		return (EReference)useCaseEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUseCase_ExtensionPoint() {
		return (EReference)useCaseEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActor() {
		return actorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUseCaseInstance() {
		return useCaseInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExtend() {
		return extendEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExtend_Condition() {
		return (EReference)extendEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExtend_Base() {
		return (EReference)extendEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExtend_Extension() {
		return (EReference)extendEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExtend_ExtensionPoint() {
		return (EReference)extendEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInclude() {
		return includeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInclude_Addition() {
		return (EReference)includeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInclude_Base() {
		return (EReference)includeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExtensionPoint() {
		return extensionPointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExtensionPoint_Location() {
		return (EAttribute)extensionPointEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExtensionPoint_UseCase() {
		return (EReference)extensionPointEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExtensionPoint_Extend() {
		return (EReference)extensionPointEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Use_casesFactory getUse_casesFactory() {
		return (Use_casesFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		useCaseEClass = createEClass(USE_CASE);
		createEReference(useCaseEClass, USE_CASE__EXTENDER);
		createEReference(useCaseEClass, USE_CASE__EXTEND);
		createEReference(useCaseEClass, USE_CASE__INCLUDER);
		createEReference(useCaseEClass, USE_CASE__INCLUDE);
		createEReference(useCaseEClass, USE_CASE__EXTENSION_POINT);

		actorEClass = createEClass(ACTOR);

		useCaseInstanceEClass = createEClass(USE_CASE_INSTANCE);

		extendEClass = createEClass(EXTEND);
		createEReference(extendEClass, EXTEND__CONDITION);
		createEReference(extendEClass, EXTEND__BASE);
		createEReference(extendEClass, EXTEND__EXTENSION);
		createEReference(extendEClass, EXTEND__EXTENSION_POINT);

		includeEClass = createEClass(INCLUDE);
		createEReference(includeEClass, INCLUDE__ADDITION);
		createEReference(includeEClass, INCLUDE__BASE);

		extensionPointEClass = createEClass(EXTENSION_POINT);
		createEAttribute(extensionPointEClass, EXTENSION_POINT__LOCATION);
		createEReference(extensionPointEClass, EXTENSION_POINT__USE_CASE);
		createEReference(extensionPointEClass, EXTENSION_POINT__EXTEND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		Common_behaviorPackage theCommon_behaviorPackage = (Common_behaviorPackage)EPackage.Registry.INSTANCE.getEPackage(Common_behaviorPackage.eNS_URI);
		Data_typesPackage theData_typesPackage = (Data_typesPackage)EPackage.Registry.INSTANCE.getEPackage(Data_typesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		useCaseEClass.getESuperTypes().add(theCorePackage.getClassifier());
		actorEClass.getESuperTypes().add(theCorePackage.getClassifier());
		useCaseInstanceEClass.getESuperTypes().add(theCommon_behaviorPackage.getInstance());
		extendEClass.getESuperTypes().add(theCorePackage.getRelationship());
		includeEClass.getESuperTypes().add(theCorePackage.getRelationship());
		extensionPointEClass.getESuperTypes().add(theCorePackage.getModelElement());

		// Initialize classes and features; add operations and parameters
		initEClass(useCaseEClass, UseCase.class, "UseCase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUseCase_Extender(), this.getExtend(), this.getExtend_Base(), "extender", null, 0, -1, UseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUseCase_Extend(), this.getExtend(), this.getExtend_Extension(), "extend", null, 0, -1, UseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUseCase_Includer(), this.getInclude(), this.getInclude_Addition(), "includer", null, 0, -1, UseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUseCase_Include(), this.getInclude(), this.getInclude_Base(), "include", null, 0, -1, UseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUseCase_ExtensionPoint(), this.getExtensionPoint(), this.getExtensionPoint_UseCase(), "extensionPoint", null, 0, -1, UseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(actorEClass, Actor.class, "Actor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(useCaseInstanceEClass, UseCaseInstance.class, "UseCaseInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(extendEClass, Extend.class, "Extend", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getExtend_Condition(), theData_typesPackage.getBooleanExpression(), null, "condition", null, 0, 1, Extend.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExtend_Base(), this.getUseCase(), this.getUseCase_Extender(), "base", null, 1, 1, Extend.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExtend_Extension(), this.getUseCase(), this.getUseCase_Extend(), "extension", null, 1, 1, Extend.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExtend_ExtensionPoint(), this.getExtensionPoint(), this.getExtensionPoint_Extend(), "extensionPoint", null, 1, -1, Extend.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(includeEClass, Include.class, "Include", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInclude_Addition(), this.getUseCase(), this.getUseCase_Includer(), "addition", null, 1, 1, Include.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInclude_Base(), this.getUseCase(), this.getUseCase_Include(), "base", null, 1, 1, Include.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(extensionPointEClass, ExtensionPoint.class, "ExtensionPoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExtensionPoint_Location(), theData_typesPackage.getLocationReference(), "location", null, 0, 1, ExtensionPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExtensionPoint_UseCase(), this.getUseCase(), this.getUseCase_ExtensionPoint(), "useCase", null, 1, 1, ExtensionPoint.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExtensionPoint_Extend(), this.getExtend(), this.getExtend_ExtensionPoint(), "extend", null, 0, -1, ExtensionPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

} //Use_casesPackageImpl
