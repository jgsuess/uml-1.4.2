/**
 * <copyright>
 * </copyright>
 *
 * $Id: Include.java,v 1.1 2012/04/23 09:31:35 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.use_cases;

import net.jgsuess.uml14.foundation.core.Relationship;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Include</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.Include#getAddition <em>Addition</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.Include#getBase <em>Base</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage#getInclude()
 * @model
 * @generated
 */
public interface Include extends Relationship {
	/**
	 * Returns the value of the '<em><b>Addition</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getIncluder <em>Includer</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Addition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Addition</em>' reference.
	 * @see #setAddition(UseCase)
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage#getInclude_Addition()
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getIncluder
	 * @model opposite="includer" required="true"
	 * @generated
	 */
	UseCase getAddition();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.use_cases.Include#getAddition <em>Addition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Addition</em>' reference.
	 * @see #getAddition()
	 * @generated
	 */
	void setAddition(UseCase value);

	/**
	 * Returns the value of the '<em><b>Base</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getInclude <em>Include</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base</em>' reference.
	 * @see #setBase(UseCase)
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage#getInclude_Base()
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getInclude
	 * @model opposite="include" required="true"
	 * @generated
	 */
	UseCase getBase();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.use_cases.Include#getBase <em>Base</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base</em>' reference.
	 * @see #getBase()
	 * @generated
	 */
	void setBase(UseCase value);

} // Include
