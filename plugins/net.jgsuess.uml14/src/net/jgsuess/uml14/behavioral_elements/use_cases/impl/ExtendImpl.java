/**
 * <copyright>
 * </copyright>
 *
 * $Id: ExtendImpl.java,v 1.1 2012/04/23 09:31:39 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.use_cases.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.use_cases.Extend;
import net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint;
import net.jgsuess.uml14.behavioral_elements.use_cases.UseCase;
import net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage;

import net.jgsuess.uml14.foundation.core.impl.RelationshipImpl;

import net.jgsuess.uml14.foundation.data_types.BooleanExpression;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Extend</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.impl.ExtendImpl#getCondition <em>Condition</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.impl.ExtendImpl#getBase <em>Base</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.impl.ExtendImpl#getExtension <em>Extension</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.impl.ExtendImpl#getExtensionPoint <em>Extension Point</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ExtendImpl extends RelationshipImpl implements Extend {
	/**
	 * The cached value of the '{@link #getCondition() <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected BooleanExpression condition;

	/**
	 * The cached value of the '{@link #getBase() <em>Base</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase()
	 * @generated
	 * @ordered
	 */
	protected UseCase base;

	/**
	 * The cached value of the '{@link #getExtension() <em>Extension</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtension()
	 * @generated
	 * @ordered
	 */
	protected UseCase extension;

	/**
	 * The cached value of the '{@link #getExtensionPoint() <em>Extension Point</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtensionPoint()
	 * @generated
	 * @ordered
	 */
	protected EList<ExtensionPoint> extensionPoint;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExtendImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Use_casesPackage.Literals.EXTEND;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanExpression getCondition() {
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCondition(BooleanExpression newCondition, NotificationChain msgs) {
		BooleanExpression oldCondition = condition;
		condition = newCondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Use_casesPackage.EXTEND__CONDITION, oldCondition, newCondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCondition(BooleanExpression newCondition) {
		if (newCondition != condition) {
			NotificationChain msgs = null;
			if (condition != null)
				msgs = ((InternalEObject)condition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Use_casesPackage.EXTEND__CONDITION, null, msgs);
			if (newCondition != null)
				msgs = ((InternalEObject)newCondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Use_casesPackage.EXTEND__CONDITION, null, msgs);
			msgs = basicSetCondition(newCondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Use_casesPackage.EXTEND__CONDITION, newCondition, newCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseCase getBase() {
		if (base != null && base.eIsProxy()) {
			InternalEObject oldBase = (InternalEObject)base;
			base = (UseCase)eResolveProxy(oldBase);
			if (base != oldBase) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Use_casesPackage.EXTEND__BASE, oldBase, base));
			}
		}
		return base;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseCase basicGetBase() {
		return base;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBase(UseCase newBase, NotificationChain msgs) {
		UseCase oldBase = base;
		base = newBase;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Use_casesPackage.EXTEND__BASE, oldBase, newBase);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase(UseCase newBase) {
		if (newBase != base) {
			NotificationChain msgs = null;
			if (base != null)
				msgs = ((InternalEObject)base).eInverseRemove(this, Use_casesPackage.USE_CASE__EXTENDER, UseCase.class, msgs);
			if (newBase != null)
				msgs = ((InternalEObject)newBase).eInverseAdd(this, Use_casesPackage.USE_CASE__EXTENDER, UseCase.class, msgs);
			msgs = basicSetBase(newBase, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Use_casesPackage.EXTEND__BASE, newBase, newBase));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseCase getExtension() {
		if (extension != null && extension.eIsProxy()) {
			InternalEObject oldExtension = (InternalEObject)extension;
			extension = (UseCase)eResolveProxy(oldExtension);
			if (extension != oldExtension) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Use_casesPackage.EXTEND__EXTENSION, oldExtension, extension));
			}
		}
		return extension;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseCase basicGetExtension() {
		return extension;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExtension(UseCase newExtension, NotificationChain msgs) {
		UseCase oldExtension = extension;
		extension = newExtension;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Use_casesPackage.EXTEND__EXTENSION, oldExtension, newExtension);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExtension(UseCase newExtension) {
		if (newExtension != extension) {
			NotificationChain msgs = null;
			if (extension != null)
				msgs = ((InternalEObject)extension).eInverseRemove(this, Use_casesPackage.USE_CASE__EXTEND, UseCase.class, msgs);
			if (newExtension != null)
				msgs = ((InternalEObject)newExtension).eInverseAdd(this, Use_casesPackage.USE_CASE__EXTEND, UseCase.class, msgs);
			msgs = basicSetExtension(newExtension, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Use_casesPackage.EXTEND__EXTENSION, newExtension, newExtension));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExtensionPoint> getExtensionPoint() {
		if (extensionPoint == null) {
			extensionPoint = new EObjectWithInverseResolvingEList.ManyInverse<ExtensionPoint>(ExtensionPoint.class, this, Use_casesPackage.EXTEND__EXTENSION_POINT, Use_casesPackage.EXTENSION_POINT__EXTEND);
		}
		return extensionPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Use_casesPackage.EXTEND__BASE:
				if (base != null)
					msgs = ((InternalEObject)base).eInverseRemove(this, Use_casesPackage.USE_CASE__EXTENDER, UseCase.class, msgs);
				return basicSetBase((UseCase)otherEnd, msgs);
			case Use_casesPackage.EXTEND__EXTENSION:
				if (extension != null)
					msgs = ((InternalEObject)extension).eInverseRemove(this, Use_casesPackage.USE_CASE__EXTEND, UseCase.class, msgs);
				return basicSetExtension((UseCase)otherEnd, msgs);
			case Use_casesPackage.EXTEND__EXTENSION_POINT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getExtensionPoint()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Use_casesPackage.EXTEND__CONDITION:
				return basicSetCondition(null, msgs);
			case Use_casesPackage.EXTEND__BASE:
				return basicSetBase(null, msgs);
			case Use_casesPackage.EXTEND__EXTENSION:
				return basicSetExtension(null, msgs);
			case Use_casesPackage.EXTEND__EXTENSION_POINT:
				return ((InternalEList<?>)getExtensionPoint()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Use_casesPackage.EXTEND__CONDITION:
				return getCondition();
			case Use_casesPackage.EXTEND__BASE:
				if (resolve) return getBase();
				return basicGetBase();
			case Use_casesPackage.EXTEND__EXTENSION:
				if (resolve) return getExtension();
				return basicGetExtension();
			case Use_casesPackage.EXTEND__EXTENSION_POINT:
				return getExtensionPoint();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Use_casesPackage.EXTEND__CONDITION:
				setCondition((BooleanExpression)newValue);
				return;
			case Use_casesPackage.EXTEND__BASE:
				setBase((UseCase)newValue);
				return;
			case Use_casesPackage.EXTEND__EXTENSION:
				setExtension((UseCase)newValue);
				return;
			case Use_casesPackage.EXTEND__EXTENSION_POINT:
				getExtensionPoint().clear();
				getExtensionPoint().addAll((Collection<? extends ExtensionPoint>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Use_casesPackage.EXTEND__CONDITION:
				setCondition((BooleanExpression)null);
				return;
			case Use_casesPackage.EXTEND__BASE:
				setBase((UseCase)null);
				return;
			case Use_casesPackage.EXTEND__EXTENSION:
				setExtension((UseCase)null);
				return;
			case Use_casesPackage.EXTEND__EXTENSION_POINT:
				getExtensionPoint().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Use_casesPackage.EXTEND__CONDITION:
				return condition != null;
			case Use_casesPackage.EXTEND__BASE:
				return base != null;
			case Use_casesPackage.EXTEND__EXTENSION:
				return extension != null;
			case Use_casesPackage.EXTEND__EXTENSION_POINT:
				return extensionPoint != null && !extensionPoint.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ExtendImpl
