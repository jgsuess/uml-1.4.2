/**
 * <copyright>
 * </copyright>
 *
 * $Id: Use_casesPackage.java,v 1.1 2012/04/23 09:31:34 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.use_cases;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;

import net.jgsuess.uml14.foundation.core.CorePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesFactory
 * @model kind="package"
 * @generated
 */
public interface Use_casesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "use_cases";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://jgsuess.net/uml14/use_cases";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "use_cases";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Use_casesPackage eINSTANCE = net.jgsuess.uml14.behavioral_elements.use_cases.impl.Use_casesPackageImpl.init();

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.use_cases.impl.UseCaseImpl <em>Use Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.UseCaseImpl
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.Use_casesPackageImpl#getUseCase()
	 * @generated
	 */
	int USE_CASE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__NAME = CorePackage.CLASSIFIER__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__VISIBILITY = CorePackage.CLASSIFIER__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__IS_SPECIFICATION = CorePackage.CLASSIFIER__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__NAMESPACE = CorePackage.CLASSIFIER__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__CLIENT_DEPENDENCY = CorePackage.CLASSIFIER__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__CONSTRAINT = CorePackage.CLASSIFIER__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__SUPPLIER_DEPENDENCY = CorePackage.CLASSIFIER__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__PRESENTATION = CorePackage.CLASSIFIER__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__TARGET_FLOW = CorePackage.CLASSIFIER__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__SOURCE_FLOW = CorePackage.CLASSIFIER__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__DEFAULTED_PARAMETER = CorePackage.CLASSIFIER__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__COMMENT = CorePackage.CLASSIFIER__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__ELEMENT_RESIDENCE = CorePackage.CLASSIFIER__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__TEMPLATE_PARAMETER = CorePackage.CLASSIFIER__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__PARAMETER_TEMPLATE = CorePackage.CLASSIFIER__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__STEREOTYPE = CorePackage.CLASSIFIER__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__TAGGED_VALUE = CorePackage.CLASSIFIER__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__REFERENCE_TAG = CorePackage.CLASSIFIER__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__TEMPLATE_ARGUMENT = CorePackage.CLASSIFIER__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__BEHAVIOR = CorePackage.CLASSIFIER__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__CLASSIFIER_ROLE = CorePackage.CLASSIFIER__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__COLLABORATION = CorePackage.CLASSIFIER__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__COLLABORATION_INSTANCE_SET = CorePackage.CLASSIFIER__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__PARTITION = CorePackage.CLASSIFIER__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__ELEMENT_IMPORT = CorePackage.CLASSIFIER__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Is Root</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__IS_ROOT = CorePackage.CLASSIFIER__IS_ROOT;

	/**
	 * The feature id for the '<em><b>Is Leaf</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__IS_LEAF = CorePackage.CLASSIFIER__IS_LEAF;

	/**
	 * The feature id for the '<em><b>Is Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__IS_ABSTRACT = CorePackage.CLASSIFIER__IS_ABSTRACT;

	/**
	 * The feature id for the '<em><b>Generalization</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__GENERALIZATION = CorePackage.CLASSIFIER__GENERALIZATION;

	/**
	 * The feature id for the '<em><b>Specialization</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__SPECIALIZATION = CorePackage.CLASSIFIER__SPECIALIZATION;

	/**
	 * The feature id for the '<em><b>Owned Element</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__OWNED_ELEMENT = CorePackage.CLASSIFIER__OWNED_ELEMENT;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__FEATURE = CorePackage.CLASSIFIER__FEATURE;

	/**
	 * The feature id for the '<em><b>Typed Feature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__TYPED_FEATURE = CorePackage.CLASSIFIER__TYPED_FEATURE;

	/**
	 * The feature id for the '<em><b>Typed Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__TYPED_PARAMETER = CorePackage.CLASSIFIER__TYPED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Association</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__ASSOCIATION = CorePackage.CLASSIFIER__ASSOCIATION;

	/**
	 * The feature id for the '<em><b>Specified End</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__SPECIFIED_END = CorePackage.CLASSIFIER__SPECIFIED_END;

	/**
	 * The feature id for the '<em><b>Powertype Range</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__POWERTYPE_RANGE = CorePackage.CLASSIFIER__POWERTYPE_RANGE;

	/**
	 * The feature id for the '<em><b>Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__INSTANCE = CorePackage.CLASSIFIER__INSTANCE;

	/**
	 * The feature id for the '<em><b>Create Action</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__CREATE_ACTION = CorePackage.CLASSIFIER__CREATE_ACTION;

	/**
	 * The feature id for the '<em><b>Classifier In State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__CLASSIFIER_IN_STATE = CorePackage.CLASSIFIER__CLASSIFIER_IN_STATE;

	/**
	 * The feature id for the '<em><b>Object Flow State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__OBJECT_FLOW_STATE = CorePackage.CLASSIFIER__OBJECT_FLOW_STATE;

	/**
	 * The feature id for the '<em><b>Extender</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__EXTENDER = CorePackage.CLASSIFIER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Extend</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__EXTEND = CorePackage.CLASSIFIER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Includer</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__INCLUDER = CorePackage.CLASSIFIER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Include</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__INCLUDE = CorePackage.CLASSIFIER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Extension Point</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE__EXTENSION_POINT = CorePackage.CLASSIFIER_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Use Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_FEATURE_COUNT = CorePackage.CLASSIFIER_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.use_cases.impl.ActorImpl <em>Actor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.ActorImpl
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.Use_casesPackageImpl#getActor()
	 * @generated
	 */
	int ACTOR = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__NAME = CorePackage.CLASSIFIER__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__VISIBILITY = CorePackage.CLASSIFIER__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__IS_SPECIFICATION = CorePackage.CLASSIFIER__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__NAMESPACE = CorePackage.CLASSIFIER__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__CLIENT_DEPENDENCY = CorePackage.CLASSIFIER__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__CONSTRAINT = CorePackage.CLASSIFIER__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__SUPPLIER_DEPENDENCY = CorePackage.CLASSIFIER__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__PRESENTATION = CorePackage.CLASSIFIER__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__TARGET_FLOW = CorePackage.CLASSIFIER__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__SOURCE_FLOW = CorePackage.CLASSIFIER__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__DEFAULTED_PARAMETER = CorePackage.CLASSIFIER__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__COMMENT = CorePackage.CLASSIFIER__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__ELEMENT_RESIDENCE = CorePackage.CLASSIFIER__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__TEMPLATE_PARAMETER = CorePackage.CLASSIFIER__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__PARAMETER_TEMPLATE = CorePackage.CLASSIFIER__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__STEREOTYPE = CorePackage.CLASSIFIER__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__TAGGED_VALUE = CorePackage.CLASSIFIER__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__REFERENCE_TAG = CorePackage.CLASSIFIER__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__TEMPLATE_ARGUMENT = CorePackage.CLASSIFIER__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__BEHAVIOR = CorePackage.CLASSIFIER__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__CLASSIFIER_ROLE = CorePackage.CLASSIFIER__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__COLLABORATION = CorePackage.CLASSIFIER__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__COLLABORATION_INSTANCE_SET = CorePackage.CLASSIFIER__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__PARTITION = CorePackage.CLASSIFIER__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__ELEMENT_IMPORT = CorePackage.CLASSIFIER__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Is Root</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__IS_ROOT = CorePackage.CLASSIFIER__IS_ROOT;

	/**
	 * The feature id for the '<em><b>Is Leaf</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__IS_LEAF = CorePackage.CLASSIFIER__IS_LEAF;

	/**
	 * The feature id for the '<em><b>Is Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__IS_ABSTRACT = CorePackage.CLASSIFIER__IS_ABSTRACT;

	/**
	 * The feature id for the '<em><b>Generalization</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__GENERALIZATION = CorePackage.CLASSIFIER__GENERALIZATION;

	/**
	 * The feature id for the '<em><b>Specialization</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__SPECIALIZATION = CorePackage.CLASSIFIER__SPECIALIZATION;

	/**
	 * The feature id for the '<em><b>Owned Element</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__OWNED_ELEMENT = CorePackage.CLASSIFIER__OWNED_ELEMENT;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__FEATURE = CorePackage.CLASSIFIER__FEATURE;

	/**
	 * The feature id for the '<em><b>Typed Feature</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__TYPED_FEATURE = CorePackage.CLASSIFIER__TYPED_FEATURE;

	/**
	 * The feature id for the '<em><b>Typed Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__TYPED_PARAMETER = CorePackage.CLASSIFIER__TYPED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Association</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__ASSOCIATION = CorePackage.CLASSIFIER__ASSOCIATION;

	/**
	 * The feature id for the '<em><b>Specified End</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__SPECIFIED_END = CorePackage.CLASSIFIER__SPECIFIED_END;

	/**
	 * The feature id for the '<em><b>Powertype Range</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__POWERTYPE_RANGE = CorePackage.CLASSIFIER__POWERTYPE_RANGE;

	/**
	 * The feature id for the '<em><b>Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__INSTANCE = CorePackage.CLASSIFIER__INSTANCE;

	/**
	 * The feature id for the '<em><b>Create Action</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__CREATE_ACTION = CorePackage.CLASSIFIER__CREATE_ACTION;

	/**
	 * The feature id for the '<em><b>Classifier In State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__CLASSIFIER_IN_STATE = CorePackage.CLASSIFIER__CLASSIFIER_IN_STATE;

	/**
	 * The feature id for the '<em><b>Object Flow State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__OBJECT_FLOW_STATE = CorePackage.CLASSIFIER__OBJECT_FLOW_STATE;

	/**
	 * The number of structural features of the '<em>Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR_FEATURE_COUNT = CorePackage.CLASSIFIER_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.use_cases.impl.UseCaseInstanceImpl <em>Use Case Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.UseCaseInstanceImpl
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.Use_casesPackageImpl#getUseCaseInstance()
	 * @generated
	 */
	int USE_CASE_INSTANCE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__NAME = Common_behaviorPackage.INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__VISIBILITY = Common_behaviorPackage.INSTANCE__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__IS_SPECIFICATION = Common_behaviorPackage.INSTANCE__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__NAMESPACE = Common_behaviorPackage.INSTANCE__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__CLIENT_DEPENDENCY = Common_behaviorPackage.INSTANCE__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__CONSTRAINT = Common_behaviorPackage.INSTANCE__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__SUPPLIER_DEPENDENCY = Common_behaviorPackage.INSTANCE__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__PRESENTATION = Common_behaviorPackage.INSTANCE__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__TARGET_FLOW = Common_behaviorPackage.INSTANCE__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__SOURCE_FLOW = Common_behaviorPackage.INSTANCE__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__DEFAULTED_PARAMETER = Common_behaviorPackage.INSTANCE__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__COMMENT = Common_behaviorPackage.INSTANCE__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__ELEMENT_RESIDENCE = Common_behaviorPackage.INSTANCE__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__TEMPLATE_PARAMETER = Common_behaviorPackage.INSTANCE__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__PARAMETER_TEMPLATE = Common_behaviorPackage.INSTANCE__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__STEREOTYPE = Common_behaviorPackage.INSTANCE__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__TAGGED_VALUE = Common_behaviorPackage.INSTANCE__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__REFERENCE_TAG = Common_behaviorPackage.INSTANCE__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__TEMPLATE_ARGUMENT = Common_behaviorPackage.INSTANCE__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__BEHAVIOR = Common_behaviorPackage.INSTANCE__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__CLASSIFIER_ROLE = Common_behaviorPackage.INSTANCE__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__COLLABORATION = Common_behaviorPackage.INSTANCE__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__COLLABORATION_INSTANCE_SET = Common_behaviorPackage.INSTANCE__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__PARTITION = Common_behaviorPackage.INSTANCE__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__ELEMENT_IMPORT = Common_behaviorPackage.INSTANCE__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__CLASSIFIER = Common_behaviorPackage.INSTANCE__CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Attribute Link</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__ATTRIBUTE_LINK = Common_behaviorPackage.INSTANCE__ATTRIBUTE_LINK;

	/**
	 * The feature id for the '<em><b>Link End</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__LINK_END = Common_behaviorPackage.INSTANCE__LINK_END;

	/**
	 * The feature id for the '<em><b>Slot</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__SLOT = Common_behaviorPackage.INSTANCE__SLOT;

	/**
	 * The feature id for the '<em><b>Stimulus</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__STIMULUS = Common_behaviorPackage.INSTANCE__STIMULUS;

	/**
	 * The feature id for the '<em><b>Component Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__COMPONENT_INSTANCE = Common_behaviorPackage.INSTANCE__COMPONENT_INSTANCE;

	/**
	 * The feature id for the '<em><b>Owned Instance</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__OWNED_INSTANCE = Common_behaviorPackage.INSTANCE__OWNED_INSTANCE;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__OWNER = Common_behaviorPackage.INSTANCE__OWNER;

	/**
	 * The feature id for the '<em><b>Owned Link</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__OWNED_LINK = Common_behaviorPackage.INSTANCE__OWNED_LINK;

	/**
	 * The feature id for the '<em><b>Played Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE__PLAYED_ROLE = Common_behaviorPackage.INSTANCE__PLAYED_ROLE;

	/**
	 * The number of structural features of the '<em>Use Case Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USE_CASE_INSTANCE_FEATURE_COUNT = Common_behaviorPackage.INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.use_cases.impl.ExtendImpl <em>Extend</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.ExtendImpl
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.Use_casesPackageImpl#getExtend()
	 * @generated
	 */
	int EXTEND = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__NAME = CorePackage.RELATIONSHIP__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__VISIBILITY = CorePackage.RELATIONSHIP__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__IS_SPECIFICATION = CorePackage.RELATIONSHIP__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__NAMESPACE = CorePackage.RELATIONSHIP__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__CLIENT_DEPENDENCY = CorePackage.RELATIONSHIP__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__CONSTRAINT = CorePackage.RELATIONSHIP__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__SUPPLIER_DEPENDENCY = CorePackage.RELATIONSHIP__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__PRESENTATION = CorePackage.RELATIONSHIP__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__TARGET_FLOW = CorePackage.RELATIONSHIP__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__SOURCE_FLOW = CorePackage.RELATIONSHIP__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__DEFAULTED_PARAMETER = CorePackage.RELATIONSHIP__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__COMMENT = CorePackage.RELATIONSHIP__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__ELEMENT_RESIDENCE = CorePackage.RELATIONSHIP__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__TEMPLATE_PARAMETER = CorePackage.RELATIONSHIP__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__PARAMETER_TEMPLATE = CorePackage.RELATIONSHIP__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__STEREOTYPE = CorePackage.RELATIONSHIP__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__TAGGED_VALUE = CorePackage.RELATIONSHIP__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__REFERENCE_TAG = CorePackage.RELATIONSHIP__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__TEMPLATE_ARGUMENT = CorePackage.RELATIONSHIP__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__BEHAVIOR = CorePackage.RELATIONSHIP__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__CLASSIFIER_ROLE = CorePackage.RELATIONSHIP__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__COLLABORATION = CorePackage.RELATIONSHIP__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__COLLABORATION_INSTANCE_SET = CorePackage.RELATIONSHIP__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__PARTITION = CorePackage.RELATIONSHIP__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__ELEMENT_IMPORT = CorePackage.RELATIONSHIP__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__CONDITION = CorePackage.RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__BASE = CorePackage.RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__EXTENSION = CorePackage.RELATIONSHIP_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Extension Point</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND__EXTENSION_POINT = CorePackage.RELATIONSHIP_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Extend</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTEND_FEATURE_COUNT = CorePackage.RELATIONSHIP_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.use_cases.impl.IncludeImpl <em>Include</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.IncludeImpl
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.Use_casesPackageImpl#getInclude()
	 * @generated
	 */
	int INCLUDE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__NAME = CorePackage.RELATIONSHIP__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__VISIBILITY = CorePackage.RELATIONSHIP__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__IS_SPECIFICATION = CorePackage.RELATIONSHIP__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__NAMESPACE = CorePackage.RELATIONSHIP__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__CLIENT_DEPENDENCY = CorePackage.RELATIONSHIP__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__CONSTRAINT = CorePackage.RELATIONSHIP__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__SUPPLIER_DEPENDENCY = CorePackage.RELATIONSHIP__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__PRESENTATION = CorePackage.RELATIONSHIP__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__TARGET_FLOW = CorePackage.RELATIONSHIP__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__SOURCE_FLOW = CorePackage.RELATIONSHIP__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__DEFAULTED_PARAMETER = CorePackage.RELATIONSHIP__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__COMMENT = CorePackage.RELATIONSHIP__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__ELEMENT_RESIDENCE = CorePackage.RELATIONSHIP__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__TEMPLATE_PARAMETER = CorePackage.RELATIONSHIP__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__PARAMETER_TEMPLATE = CorePackage.RELATIONSHIP__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__STEREOTYPE = CorePackage.RELATIONSHIP__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__TAGGED_VALUE = CorePackage.RELATIONSHIP__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__REFERENCE_TAG = CorePackage.RELATIONSHIP__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__TEMPLATE_ARGUMENT = CorePackage.RELATIONSHIP__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__BEHAVIOR = CorePackage.RELATIONSHIP__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__CLASSIFIER_ROLE = CorePackage.RELATIONSHIP__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__COLLABORATION = CorePackage.RELATIONSHIP__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__COLLABORATION_INSTANCE_SET = CorePackage.RELATIONSHIP__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__PARTITION = CorePackage.RELATIONSHIP__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__ELEMENT_IMPORT = CorePackage.RELATIONSHIP__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Addition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__ADDITION = CorePackage.RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE__BASE = CorePackage.RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Include</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCLUDE_FEATURE_COUNT = CorePackage.RELATIONSHIP_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.behavioral_elements.use_cases.impl.ExtensionPointImpl <em>Extension Point</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.ExtensionPointImpl
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.Use_casesPackageImpl#getExtensionPoint()
	 * @generated
	 */
	int EXTENSION_POINT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__NAME = CorePackage.MODEL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__VISIBILITY = CorePackage.MODEL_ELEMENT__VISIBILITY;

	/**
	 * The feature id for the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__IS_SPECIFICATION = CorePackage.MODEL_ELEMENT__IS_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__NAMESPACE = CorePackage.MODEL_ELEMENT__NAMESPACE;

	/**
	 * The feature id for the '<em><b>Client Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__CLIENT_DEPENDENCY = CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__CONSTRAINT = CorePackage.MODEL_ELEMENT__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Supplier Dependency</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__SUPPLIER_DEPENDENCY = CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY;

	/**
	 * The feature id for the '<em><b>Presentation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__PRESENTATION = CorePackage.MODEL_ELEMENT__PRESENTATION;

	/**
	 * The feature id for the '<em><b>Target Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__TARGET_FLOW = CorePackage.MODEL_ELEMENT__TARGET_FLOW;

	/**
	 * The feature id for the '<em><b>Source Flow</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__SOURCE_FLOW = CorePackage.MODEL_ELEMENT__SOURCE_FLOW;

	/**
	 * The feature id for the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__DEFAULTED_PARAMETER = CorePackage.MODEL_ELEMENT__DEFAULTED_PARAMETER;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__COMMENT = CorePackage.MODEL_ELEMENT__COMMENT;

	/**
	 * The feature id for the '<em><b>Element Residence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__ELEMENT_RESIDENCE = CorePackage.MODEL_ELEMENT__ELEMENT_RESIDENCE;

	/**
	 * The feature id for the '<em><b>Template Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__TEMPLATE_PARAMETER = CorePackage.MODEL_ELEMENT__TEMPLATE_PARAMETER;

	/**
	 * The feature id for the '<em><b>Parameter Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__PARAMETER_TEMPLATE = CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Stereotype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__STEREOTYPE = CorePackage.MODEL_ELEMENT__STEREOTYPE;

	/**
	 * The feature id for the '<em><b>Tagged Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__TAGGED_VALUE = CorePackage.MODEL_ELEMENT__TAGGED_VALUE;

	/**
	 * The feature id for the '<em><b>Reference Tag</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__REFERENCE_TAG = CorePackage.MODEL_ELEMENT__REFERENCE_TAG;

	/**
	 * The feature id for the '<em><b>Template Argument</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__TEMPLATE_ARGUMENT = CorePackage.MODEL_ELEMENT__TEMPLATE_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__BEHAVIOR = CorePackage.MODEL_ELEMENT__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Classifier Role</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__CLASSIFIER_ROLE = CorePackage.MODEL_ELEMENT__CLASSIFIER_ROLE;

	/**
	 * The feature id for the '<em><b>Collaboration</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__COLLABORATION = CorePackage.MODEL_ELEMENT__COLLABORATION;

	/**
	 * The feature id for the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__COLLABORATION_INSTANCE_SET = CorePackage.MODEL_ELEMENT__COLLABORATION_INSTANCE_SET;

	/**
	 * The feature id for the '<em><b>Partition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__PARTITION = CorePackage.MODEL_ELEMENT__PARTITION;

	/**
	 * The feature id for the '<em><b>Element Import</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__ELEMENT_IMPORT = CorePackage.MODEL_ELEMENT__ELEMENT_IMPORT;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__LOCATION = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Use Case</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__USE_CASE = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Extend</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT__EXTEND = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Extension Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_POINT_FEATURE_COUNT = CorePackage.MODEL_ELEMENT_FEATURE_COUNT + 3;


	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.use_cases.UseCase <em>Use Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Use Case</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.UseCase
	 * @generated
	 */
	EClass getUseCase();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getExtender <em>Extender</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Extender</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getExtender()
	 * @see #getUseCase()
	 * @generated
	 */
	EReference getUseCase_Extender();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getExtend <em>Extend</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Extend</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getExtend()
	 * @see #getUseCase()
	 * @generated
	 */
	EReference getUseCase_Extend();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getIncluder <em>Includer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Includer</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getIncluder()
	 * @see #getUseCase()
	 * @generated
	 */
	EReference getUseCase_Includer();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getInclude <em>Include</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Include</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getInclude()
	 * @see #getUseCase()
	 * @generated
	 */
	EReference getUseCase_Include();

	/**
	 * Returns the meta object for the containment reference list '{@link net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getExtensionPoint <em>Extension Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Extension Point</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getExtensionPoint()
	 * @see #getUseCase()
	 * @generated
	 */
	EReference getUseCase_ExtensionPoint();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.use_cases.Actor <em>Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Actor</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Actor
	 * @generated
	 */
	EClass getActor();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.use_cases.UseCaseInstance <em>Use Case Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Use Case Instance</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.UseCaseInstance
	 * @generated
	 */
	EClass getUseCaseInstance();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.use_cases.Extend <em>Extend</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extend</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Extend
	 * @generated
	 */
	EClass getExtend();

	/**
	 * Returns the meta object for the containment reference '{@link net.jgsuess.uml14.behavioral_elements.use_cases.Extend#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Extend#getCondition()
	 * @see #getExtend()
	 * @generated
	 */
	EReference getExtend_Condition();

	/**
	 * Returns the meta object for the reference '{@link net.jgsuess.uml14.behavioral_elements.use_cases.Extend#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Extend#getBase()
	 * @see #getExtend()
	 * @generated
	 */
	EReference getExtend_Base();

	/**
	 * Returns the meta object for the reference '{@link net.jgsuess.uml14.behavioral_elements.use_cases.Extend#getExtension <em>Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Extension</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Extend#getExtension()
	 * @see #getExtend()
	 * @generated
	 */
	EReference getExtend_Extension();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.use_cases.Extend#getExtensionPoint <em>Extension Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Extension Point</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Extend#getExtensionPoint()
	 * @see #getExtend()
	 * @generated
	 */
	EReference getExtend_ExtensionPoint();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.use_cases.Include <em>Include</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Include</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Include
	 * @generated
	 */
	EClass getInclude();

	/**
	 * Returns the meta object for the reference '{@link net.jgsuess.uml14.behavioral_elements.use_cases.Include#getAddition <em>Addition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Addition</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Include#getAddition()
	 * @see #getInclude()
	 * @generated
	 */
	EReference getInclude_Addition();

	/**
	 * Returns the meta object for the reference '{@link net.jgsuess.uml14.behavioral_elements.use_cases.Include#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Include#getBase()
	 * @see #getInclude()
	 * @generated
	 */
	EReference getInclude_Base();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint <em>Extension Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extension Point</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint
	 * @generated
	 */
	EClass getExtensionPoint();

	/**
	 * Returns the meta object for the attribute '{@link net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Location</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint#getLocation()
	 * @see #getExtensionPoint()
	 * @generated
	 */
	EAttribute getExtensionPoint_Location();

	/**
	 * Returns the meta object for the container reference '{@link net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint#getUseCase <em>Use Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Use Case</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint#getUseCase()
	 * @see #getExtensionPoint()
	 * @generated
	 */
	EReference getExtensionPoint_UseCase();

	/**
	 * Returns the meta object for the reference list '{@link net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint#getExtend <em>Extend</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Extend</em>'.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint#getExtend()
	 * @see #getExtensionPoint()
	 * @generated
	 */
	EReference getExtensionPoint_Extend();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Use_casesFactory getUse_casesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.use_cases.impl.UseCaseImpl <em>Use Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.UseCaseImpl
		 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.Use_casesPackageImpl#getUseCase()
		 * @generated
		 */
		EClass USE_CASE = eINSTANCE.getUseCase();

		/**
		 * The meta object literal for the '<em><b>Extender</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USE_CASE__EXTENDER = eINSTANCE.getUseCase_Extender();

		/**
		 * The meta object literal for the '<em><b>Extend</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USE_CASE__EXTEND = eINSTANCE.getUseCase_Extend();

		/**
		 * The meta object literal for the '<em><b>Includer</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USE_CASE__INCLUDER = eINSTANCE.getUseCase_Includer();

		/**
		 * The meta object literal for the '<em><b>Include</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USE_CASE__INCLUDE = eINSTANCE.getUseCase_Include();

		/**
		 * The meta object literal for the '<em><b>Extension Point</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USE_CASE__EXTENSION_POINT = eINSTANCE.getUseCase_ExtensionPoint();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.use_cases.impl.ActorImpl <em>Actor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.ActorImpl
		 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.Use_casesPackageImpl#getActor()
		 * @generated
		 */
		EClass ACTOR = eINSTANCE.getActor();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.use_cases.impl.UseCaseInstanceImpl <em>Use Case Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.UseCaseInstanceImpl
		 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.Use_casesPackageImpl#getUseCaseInstance()
		 * @generated
		 */
		EClass USE_CASE_INSTANCE = eINSTANCE.getUseCaseInstance();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.use_cases.impl.ExtendImpl <em>Extend</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.ExtendImpl
		 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.Use_casesPackageImpl#getExtend()
		 * @generated
		 */
		EClass EXTEND = eINSTANCE.getExtend();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXTEND__CONDITION = eINSTANCE.getExtend_Condition();

		/**
		 * The meta object literal for the '<em><b>Base</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXTEND__BASE = eINSTANCE.getExtend_Base();

		/**
		 * The meta object literal for the '<em><b>Extension</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXTEND__EXTENSION = eINSTANCE.getExtend_Extension();

		/**
		 * The meta object literal for the '<em><b>Extension Point</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXTEND__EXTENSION_POINT = eINSTANCE.getExtend_ExtensionPoint();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.use_cases.impl.IncludeImpl <em>Include</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.IncludeImpl
		 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.Use_casesPackageImpl#getInclude()
		 * @generated
		 */
		EClass INCLUDE = eINSTANCE.getInclude();

		/**
		 * The meta object literal for the '<em><b>Addition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INCLUDE__ADDITION = eINSTANCE.getInclude_Addition();

		/**
		 * The meta object literal for the '<em><b>Base</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INCLUDE__BASE = eINSTANCE.getInclude_Base();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.behavioral_elements.use_cases.impl.ExtensionPointImpl <em>Extension Point</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.ExtensionPointImpl
		 * @see net.jgsuess.uml14.behavioral_elements.use_cases.impl.Use_casesPackageImpl#getExtensionPoint()
		 * @generated
		 */
		EClass EXTENSION_POINT = eINSTANCE.getExtensionPoint();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTENSION_POINT__LOCATION = eINSTANCE.getExtensionPoint_Location();

		/**
		 * The meta object literal for the '<em><b>Use Case</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXTENSION_POINT__USE_CASE = eINSTANCE.getExtensionPoint_UseCase();

		/**
		 * The meta object literal for the '<em><b>Extend</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXTENSION_POINT__EXTEND = eINSTANCE.getExtensionPoint_Extend();

	}

} //Use_casesPackage
