/**
 * <copyright>
 * </copyright>
 *
 * $Id: UseCaseImpl.java,v 1.1 2012/04/23 09:31:39 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.use_cases.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.use_cases.Extend;
import net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint;
import net.jgsuess.uml14.behavioral_elements.use_cases.Include;
import net.jgsuess.uml14.behavioral_elements.use_cases.UseCase;
import net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage;

import net.jgsuess.uml14.foundation.core.impl.ClassifierImpl;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Use Case</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.impl.UseCaseImpl#getExtender <em>Extender</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.impl.UseCaseImpl#getExtend <em>Extend</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.impl.UseCaseImpl#getIncluder <em>Includer</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.impl.UseCaseImpl#getInclude <em>Include</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.impl.UseCaseImpl#getExtensionPoint <em>Extension Point</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UseCaseImpl extends ClassifierImpl implements UseCase {
	/**
	 * The cached value of the '{@link #getExtender() <em>Extender</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtender()
	 * @generated
	 * @ordered
	 */
	protected EList<Extend> extender;

	/**
	 * The cached value of the '{@link #getExtend() <em>Extend</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtend()
	 * @generated
	 * @ordered
	 */
	protected EList<Extend> extend;

	/**
	 * The cached value of the '{@link #getIncluder() <em>Includer</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncluder()
	 * @generated
	 * @ordered
	 */
	protected EList<Include> includer;

	/**
	 * The cached value of the '{@link #getInclude() <em>Include</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInclude()
	 * @generated
	 * @ordered
	 */
	protected EList<Include> include;

	/**
	 * The cached value of the '{@link #getExtensionPoint() <em>Extension Point</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtensionPoint()
	 * @generated
	 * @ordered
	 */
	protected EList<ExtensionPoint> extensionPoint;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UseCaseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Use_casesPackage.Literals.USE_CASE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Extend> getExtender() {
		if (extender == null) {
			extender = new EObjectWithInverseResolvingEList<Extend>(Extend.class, this, Use_casesPackage.USE_CASE__EXTENDER, Use_casesPackage.EXTEND__BASE);
		}
		return extender;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Extend> getExtend() {
		if (extend == null) {
			extend = new EObjectWithInverseResolvingEList<Extend>(Extend.class, this, Use_casesPackage.USE_CASE__EXTEND, Use_casesPackage.EXTEND__EXTENSION);
		}
		return extend;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Include> getIncluder() {
		if (includer == null) {
			includer = new EObjectWithInverseResolvingEList<Include>(Include.class, this, Use_casesPackage.USE_CASE__INCLUDER, Use_casesPackage.INCLUDE__ADDITION);
		}
		return includer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Include> getInclude() {
		if (include == null) {
			include = new EObjectWithInverseResolvingEList<Include>(Include.class, this, Use_casesPackage.USE_CASE__INCLUDE, Use_casesPackage.INCLUDE__BASE);
		}
		return include;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExtensionPoint> getExtensionPoint() {
		if (extensionPoint == null) {
			extensionPoint = new EObjectContainmentWithInverseEList<ExtensionPoint>(ExtensionPoint.class, this, Use_casesPackage.USE_CASE__EXTENSION_POINT, Use_casesPackage.EXTENSION_POINT__USE_CASE);
		}
		return extensionPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Use_casesPackage.USE_CASE__EXTENDER:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getExtender()).basicAdd(otherEnd, msgs);
			case Use_casesPackage.USE_CASE__EXTEND:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getExtend()).basicAdd(otherEnd, msgs);
			case Use_casesPackage.USE_CASE__INCLUDER:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getIncluder()).basicAdd(otherEnd, msgs);
			case Use_casesPackage.USE_CASE__INCLUDE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getInclude()).basicAdd(otherEnd, msgs);
			case Use_casesPackage.USE_CASE__EXTENSION_POINT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getExtensionPoint()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Use_casesPackage.USE_CASE__EXTENDER:
				return ((InternalEList<?>)getExtender()).basicRemove(otherEnd, msgs);
			case Use_casesPackage.USE_CASE__EXTEND:
				return ((InternalEList<?>)getExtend()).basicRemove(otherEnd, msgs);
			case Use_casesPackage.USE_CASE__INCLUDER:
				return ((InternalEList<?>)getIncluder()).basicRemove(otherEnd, msgs);
			case Use_casesPackage.USE_CASE__INCLUDE:
				return ((InternalEList<?>)getInclude()).basicRemove(otherEnd, msgs);
			case Use_casesPackage.USE_CASE__EXTENSION_POINT:
				return ((InternalEList<?>)getExtensionPoint()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Use_casesPackage.USE_CASE__EXTENDER:
				return getExtender();
			case Use_casesPackage.USE_CASE__EXTEND:
				return getExtend();
			case Use_casesPackage.USE_CASE__INCLUDER:
				return getIncluder();
			case Use_casesPackage.USE_CASE__INCLUDE:
				return getInclude();
			case Use_casesPackage.USE_CASE__EXTENSION_POINT:
				return getExtensionPoint();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Use_casesPackage.USE_CASE__EXTENDER:
				getExtender().clear();
				getExtender().addAll((Collection<? extends Extend>)newValue);
				return;
			case Use_casesPackage.USE_CASE__EXTEND:
				getExtend().clear();
				getExtend().addAll((Collection<? extends Extend>)newValue);
				return;
			case Use_casesPackage.USE_CASE__INCLUDER:
				getIncluder().clear();
				getIncluder().addAll((Collection<? extends Include>)newValue);
				return;
			case Use_casesPackage.USE_CASE__INCLUDE:
				getInclude().clear();
				getInclude().addAll((Collection<? extends Include>)newValue);
				return;
			case Use_casesPackage.USE_CASE__EXTENSION_POINT:
				getExtensionPoint().clear();
				getExtensionPoint().addAll((Collection<? extends ExtensionPoint>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Use_casesPackage.USE_CASE__EXTENDER:
				getExtender().clear();
				return;
			case Use_casesPackage.USE_CASE__EXTEND:
				getExtend().clear();
				return;
			case Use_casesPackage.USE_CASE__INCLUDER:
				getIncluder().clear();
				return;
			case Use_casesPackage.USE_CASE__INCLUDE:
				getInclude().clear();
				return;
			case Use_casesPackage.USE_CASE__EXTENSION_POINT:
				getExtensionPoint().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Use_casesPackage.USE_CASE__EXTENDER:
				return extender != null && !extender.isEmpty();
			case Use_casesPackage.USE_CASE__EXTEND:
				return extend != null && !extend.isEmpty();
			case Use_casesPackage.USE_CASE__INCLUDER:
				return includer != null && !includer.isEmpty();
			case Use_casesPackage.USE_CASE__INCLUDE:
				return include != null && !include.isEmpty();
			case Use_casesPackage.USE_CASE__EXTENSION_POINT:
				return extensionPoint != null && !extensionPoint.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //UseCaseImpl
