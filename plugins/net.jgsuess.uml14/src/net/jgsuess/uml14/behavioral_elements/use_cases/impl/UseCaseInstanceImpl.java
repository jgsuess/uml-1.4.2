/**
 * <copyright>
 * </copyright>
 *
 * $Id: UseCaseInstanceImpl.java,v 1.1 2012/04/23 09:31:38 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.use_cases.impl;

import net.jgsuess.uml14.behavioral_elements.common_behavior.impl.InstanceImpl;

import net.jgsuess.uml14.behavioral_elements.use_cases.UseCaseInstance;
import net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Use Case Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class UseCaseInstanceImpl extends InstanceImpl implements UseCaseInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UseCaseInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Use_casesPackage.Literals.USE_CASE_INSTANCE;
	}

} //UseCaseInstanceImpl
