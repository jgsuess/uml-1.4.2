/**
 * <copyright>
 * </copyright>
 *
 * $Id: Extend.java,v 1.1 2012/04/23 09:31:35 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.use_cases;

import net.jgsuess.uml14.foundation.core.Relationship;

import net.jgsuess.uml14.foundation.data_types.BooleanExpression;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Extend</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.Extend#getCondition <em>Condition</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.Extend#getBase <em>Base</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.Extend#getExtension <em>Extension</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.Extend#getExtensionPoint <em>Extension Point</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage#getExtend()
 * @model
 * @generated
 */
public interface Extend extends Relationship {
	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference.
	 * @see #setCondition(BooleanExpression)
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage#getExtend_Condition()
	 * @model containment="true"
	 * @generated
	 */
	BooleanExpression getCondition();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.use_cases.Extend#getCondition <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' containment reference.
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(BooleanExpression value);

	/**
	 * Returns the value of the '<em><b>Base</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getExtender <em>Extender</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base</em>' reference.
	 * @see #setBase(UseCase)
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage#getExtend_Base()
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getExtender
	 * @model opposite="extender" required="true"
	 * @generated
	 */
	UseCase getBase();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.use_cases.Extend#getBase <em>Base</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base</em>' reference.
	 * @see #getBase()
	 * @generated
	 */
	void setBase(UseCase value);

	/**
	 * Returns the value of the '<em><b>Extension</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getExtend <em>Extend</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extension</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extension</em>' reference.
	 * @see #setExtension(UseCase)
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage#getExtend_Extension()
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getExtend
	 * @model opposite="extend" required="true"
	 * @generated
	 */
	UseCase getExtension();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.use_cases.Extend#getExtension <em>Extension</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extension</em>' reference.
	 * @see #getExtension()
	 * @generated
	 */
	void setExtension(UseCase value);

	/**
	 * Returns the value of the '<em><b>Extension Point</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint#getExtend <em>Extend</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extension Point</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extension Point</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage#getExtend_ExtensionPoint()
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint#getExtend
	 * @model opposite="extend" required="true"
	 * @generated
	 */
	EList<ExtensionPoint> getExtensionPoint();

} // Extend
