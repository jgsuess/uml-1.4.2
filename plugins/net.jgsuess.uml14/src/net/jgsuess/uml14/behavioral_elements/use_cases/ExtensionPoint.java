/**
 * <copyright>
 * </copyright>
 *
 * $Id: ExtensionPoint.java,v 1.1 2012/04/23 09:31:35 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.use_cases;

import net.jgsuess.uml14.foundation.core.ModelElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Extension Point</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint#getLocation <em>Location</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint#getUseCase <em>Use Case</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint#getExtend <em>Extend</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage#getExtensionPoint()
 * @model
 * @generated
 */
public interface ExtensionPoint extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' attribute.
	 * @see #setLocation(String)
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage#getExtensionPoint_Location()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.LocationReference"
	 * @generated
	 */
	String getLocation();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint#getLocation <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' attribute.
	 * @see #getLocation()
	 * @generated
	 */
	void setLocation(String value);

	/**
	 * Returns the value of the '<em><b>Use Case</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getExtensionPoint <em>Extension Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Case</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Case</em>' container reference.
	 * @see #setUseCase(UseCase)
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage#getExtensionPoint_UseCase()
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getExtensionPoint
	 * @model opposite="extensionPoint" required="true"
	 * @generated
	 */
	UseCase getUseCase();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint#getUseCase <em>Use Case</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use Case</em>' container reference.
	 * @see #getUseCase()
	 * @generated
	 */
	void setUseCase(UseCase value);

	/**
	 * Returns the value of the '<em><b>Extend</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.use_cases.Extend}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.use_cases.Extend#getExtensionPoint <em>Extension Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extend</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extend</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage#getExtensionPoint_Extend()
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Extend#getExtensionPoint
	 * @model opposite="extensionPoint"
	 * @generated
	 */
	EList<Extend> getExtend();

} // ExtensionPoint
