/**
 * <copyright>
 * </copyright>
 *
 * $Id: UseCase.java,v 1.1 2012/04/23 09:31:35 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.use_cases;

import net.jgsuess.uml14.foundation.core.Classifier;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Use Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getExtender <em>Extender</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getExtend <em>Extend</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getIncluder <em>Includer</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getInclude <em>Include</em>}</li>
 *   <li>{@link net.jgsuess.uml14.behavioral_elements.use_cases.UseCase#getExtensionPoint <em>Extension Point</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage#getUseCase()
 * @model
 * @generated
 */
public interface UseCase extends Classifier {
	/**
	 * Returns the value of the '<em><b>Extender</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.use_cases.Extend}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.use_cases.Extend#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extender</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extender</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage#getUseCase_Extender()
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Extend#getBase
	 * @model opposite="base"
	 * @generated
	 */
	EList<Extend> getExtender();

	/**
	 * Returns the value of the '<em><b>Extend</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.use_cases.Extend}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.use_cases.Extend#getExtension <em>Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extend</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extend</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage#getUseCase_Extend()
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Extend#getExtension
	 * @model opposite="extension"
	 * @generated
	 */
	EList<Extend> getExtend();

	/**
	 * Returns the value of the '<em><b>Includer</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.use_cases.Include}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.use_cases.Include#getAddition <em>Addition</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Includer</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Includer</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage#getUseCase_Includer()
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Include#getAddition
	 * @model opposite="addition"
	 * @generated
	 */
	EList<Include> getIncluder();

	/**
	 * Returns the value of the '<em><b>Include</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.use_cases.Include}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.use_cases.Include#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Include</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Include</em>' reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage#getUseCase_Include()
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Include#getBase
	 * @model opposite="base"
	 * @generated
	 */
	EList<Include> getInclude();

	/**
	 * Returns the value of the '<em><b>Extension Point</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint#getUseCase <em>Use Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extension Point</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extension Point</em>' containment reference list.
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage#getUseCase_ExtensionPoint()
	 * @see net.jgsuess.uml14.behavioral_elements.use_cases.ExtensionPoint#getUseCase
	 * @model opposite="useCase" containment="true"
	 * @generated
	 */
	EList<ExtensionPoint> getExtensionPoint();

} // UseCase
