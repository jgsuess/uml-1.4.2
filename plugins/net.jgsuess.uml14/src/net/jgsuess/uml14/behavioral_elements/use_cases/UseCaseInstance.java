/**
 * <copyright>
 * </copyright>
 *
 * $Id: UseCaseInstance.java,v 1.1 2012/04/23 09:31:35 uqjsuss Exp $
 */
package net.jgsuess.uml14.behavioral_elements.use_cases;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Instance;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Use Case Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage#getUseCaseInstance()
 * @model
 * @generated
 */
public interface UseCaseInstance extends Instance {
} // UseCaseInstance
