/**
 * <copyright>
 * </copyright>
 *
 * $Id: Uml14PackageImpl.java,v 1.1 2012/04/23 09:31:24 uqjsuss Exp $
 */
package net.jgsuess.uml14.impl;

import net.jgsuess.uml14.Model;
import net.jgsuess.uml14.Uml14Factory;
import net.jgsuess.uml14.Uml14Package;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.Activity_graphsPackageImpl;

import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage;

import net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationsPackageImpl;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;

import net.jgsuess.uml14.behavioral_elements.common_behavior.impl.Common_behaviorPackageImpl;

import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage;

import net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl;

import net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage;

import net.jgsuess.uml14.behavioral_elements.use_cases.impl.Use_casesPackageImpl;

import net.jgsuess.uml14.foundation.core.CorePackage;

import net.jgsuess.uml14.foundation.core.impl.CorePackageImpl;

import net.jgsuess.uml14.foundation.data_types.Data_typesPackage;

import net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl;

import net.jgsuess.uml14.model_management.Model_managementPackage;

import net.jgsuess.uml14.model_management.impl.Model_managementPackageImpl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Uml14PackageImpl extends EPackageImpl implements Uml14Package {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see net.jgsuess.uml14.Uml14Package#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Uml14PackageImpl() {
		super(eNS_URI, Uml14Factory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link Uml14Package#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Uml14Package init() {
		if (isInited) return (Uml14Package)EPackage.Registry.INSTANCE.getEPackage(Uml14Package.eNS_URI);

		// Obtain or create and register package
		Uml14PackageImpl theUml14Package = (Uml14PackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof Uml14PackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new Uml14PackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		Model_managementPackageImpl theModel_managementPackage = (Model_managementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Model_managementPackage.eNS_URI) instanceof Model_managementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Model_managementPackage.eNS_URI) : Model_managementPackage.eINSTANCE);
		Data_typesPackageImpl theData_typesPackage = (Data_typesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Data_typesPackage.eNS_URI) instanceof Data_typesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Data_typesPackage.eNS_URI) : Data_typesPackage.eINSTANCE);
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		Common_behaviorPackageImpl theCommon_behaviorPackage = (Common_behaviorPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Common_behaviorPackage.eNS_URI) instanceof Common_behaviorPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Common_behaviorPackage.eNS_URI) : Common_behaviorPackage.eINSTANCE);
		Use_casesPackageImpl theUse_casesPackage = (Use_casesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Use_casesPackage.eNS_URI) instanceof Use_casesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Use_casesPackage.eNS_URI) : Use_casesPackage.eINSTANCE);
		State_machinesPackageImpl theState_machinesPackage = (State_machinesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(State_machinesPackage.eNS_URI) instanceof State_machinesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(State_machinesPackage.eNS_URI) : State_machinesPackage.eINSTANCE);
		CollaborationsPackageImpl theCollaborationsPackage = (CollaborationsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CollaborationsPackage.eNS_URI) instanceof CollaborationsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CollaborationsPackage.eNS_URI) : CollaborationsPackage.eINSTANCE);
		Activity_graphsPackageImpl theActivity_graphsPackage = (Activity_graphsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Activity_graphsPackage.eNS_URI) instanceof Activity_graphsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Activity_graphsPackage.eNS_URI) : Activity_graphsPackage.eINSTANCE);

		// Create package meta-data objects
		theUml14Package.createPackageContents();
		theModel_managementPackage.createPackageContents();
		theData_typesPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theCommon_behaviorPackage.createPackageContents();
		theUse_casesPackage.createPackageContents();
		theState_machinesPackage.createPackageContents();
		theCollaborationsPackage.createPackageContents();
		theActivity_graphsPackage.createPackageContents();

		// Initialize created meta-data
		theUml14Package.initializePackageContents();
		theModel_managementPackage.initializePackageContents();
		theData_typesPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theCommon_behaviorPackage.initializePackageContents();
		theUse_casesPackage.initializePackageContents();
		theState_machinesPackage.initializePackageContents();
		theCollaborationsPackage.initializePackageContents();
		theActivity_graphsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theUml14Package.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Uml14Package.eNS_URI, theUml14Package);
		return theUml14Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModel() {
		return modelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Uml14Factory getUml14Factory() {
		return (Uml14Factory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		modelEClass = createEClass(MODEL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		Model_managementPackage theModel_managementPackage = (Model_managementPackage)EPackage.Registry.INSTANCE.getEPackage(Model_managementPackage.eNS_URI);
		Data_typesPackage theData_typesPackage = (Data_typesPackage)EPackage.Registry.INSTANCE.getEPackage(Data_typesPackage.eNS_URI);
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		Common_behaviorPackage theCommon_behaviorPackage = (Common_behaviorPackage)EPackage.Registry.INSTANCE.getEPackage(Common_behaviorPackage.eNS_URI);
		Use_casesPackage theUse_casesPackage = (Use_casesPackage)EPackage.Registry.INSTANCE.getEPackage(Use_casesPackage.eNS_URI);
		State_machinesPackage theState_machinesPackage = (State_machinesPackage)EPackage.Registry.INSTANCE.getEPackage(State_machinesPackage.eNS_URI);
		CollaborationsPackage theCollaborationsPackage = (CollaborationsPackage)EPackage.Registry.INSTANCE.getEPackage(CollaborationsPackage.eNS_URI);
		Activity_graphsPackage theActivity_graphsPackage = (Activity_graphsPackage)EPackage.Registry.INSTANCE.getEPackage(Activity_graphsPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theModel_managementPackage);
		getESubpackages().add(theData_typesPackage);
		getESubpackages().add(theCorePackage);
		getESubpackages().add(theCommon_behaviorPackage);
		getESubpackages().add(theUse_casesPackage);
		getESubpackages().add(theState_machinesPackage);
		getESubpackages().add(theCollaborationsPackage);
		getESubpackages().add(theActivity_graphsPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		modelEClass.getESuperTypes().add(theModel_managementPackage.getPackage());

		// Initialize classes and features; add operations and parameters
		initEClass(modelEClass, Model.class, "Model", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //Uml14PackageImpl
