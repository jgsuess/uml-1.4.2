/**
 * <copyright>
 * </copyright>
 *
 * $Id: Model.java,v 1.1 2012/04/23 09:31:38 uqjsuss Exp $
 */
package net.jgsuess.uml14;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.jgsuess.uml14.Uml14Package#getModel()
 * @model
 * @generated
 */
public interface Model extends net.jgsuess.uml14.model_management.Package {
} // Model
