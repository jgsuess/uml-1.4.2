/**
 * <copyright>
 * </copyright>
 *
 * $Id: ElementImport.java,v 1.1 2012/04/23 09:31:22 uqjsuss Exp $
 */
package net.jgsuess.uml14.model_management;

import net.jgsuess.uml14.foundation.core.ModelElement;

import net.jgsuess.uml14.foundation.data_types.VisibilityKind;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element Import</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.model_management.ElementImport#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link net.jgsuess.uml14.model_management.ElementImport#getAlias <em>Alias</em>}</li>
 *   <li>{@link net.jgsuess.uml14.model_management.ElementImport#isIsSpecification <em>Is Specification</em>}</li>
 *   <li>{@link net.jgsuess.uml14.model_management.ElementImport#getImportedElement <em>Imported Element</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.model_management.Model_managementPackage#getElementImport()
 * @model
 * @generated
 */
public interface ElementImport extends EObject {
	/**
	 * Returns the value of the '<em><b>Visibility</b></em>' attribute.
	 * The literals are from the enumeration {@link net.jgsuess.uml14.foundation.data_types.VisibilityKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visibility</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visibility</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.VisibilityKind
	 * @see #setVisibility(VisibilityKind)
	 * @see net.jgsuess.uml14.model_management.Model_managementPackage#getElementImport_Visibility()
	 * @model
	 * @generated
	 */
	VisibilityKind getVisibility();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.model_management.ElementImport#getVisibility <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visibility</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.VisibilityKind
	 * @see #getVisibility()
	 * @generated
	 */
	void setVisibility(VisibilityKind value);

	/**
	 * Returns the value of the '<em><b>Alias</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alias</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alias</em>' attribute.
	 * @see #setAlias(String)
	 * @see net.jgsuess.uml14.model_management.Model_managementPackage#getElementImport_Alias()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Name"
	 * @generated
	 */
	String getAlias();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.model_management.ElementImport#getAlias <em>Alias</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alias</em>' attribute.
	 * @see #getAlias()
	 * @generated
	 */
	void setAlias(String value);

	/**
	 * Returns the value of the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Specification</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Specification</em>' attribute.
	 * @see #setIsSpecification(boolean)
	 * @see net.jgsuess.uml14.model_management.Model_managementPackage#getElementImport_IsSpecification()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Boolean"
	 * @generated
	 */
	boolean isIsSpecification();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.model_management.ElementImport#isIsSpecification <em>Is Specification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Specification</em>' attribute.
	 * @see #isIsSpecification()
	 * @generated
	 */
	void setIsSpecification(boolean value);

	/**
	 * Returns the value of the '<em><b>Imported Element</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.ModelElement#getElementImport <em>Element Import</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imported Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imported Element</em>' reference.
	 * @see #setImportedElement(ModelElement)
	 * @see net.jgsuess.uml14.model_management.Model_managementPackage#getElementImport_ImportedElement()
	 * @see net.jgsuess.uml14.foundation.core.ModelElement#getElementImport
	 * @model opposite="elementImport" required="true"
	 * @generated
	 */
	ModelElement getImportedElement();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.model_management.ElementImport#getImportedElement <em>Imported Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Imported Element</em>' reference.
	 * @see #getImportedElement()
	 * @generated
	 */
	void setImportedElement(ModelElement value);

} // ElementImport
