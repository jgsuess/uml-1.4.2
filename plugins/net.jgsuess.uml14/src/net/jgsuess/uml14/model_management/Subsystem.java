/**
 * <copyright>
 * </copyright>
 *
 * $Id: Subsystem.java,v 1.1 2012/04/23 09:31:22 uqjsuss Exp $
 */
package net.jgsuess.uml14.model_management;

import net.jgsuess.uml14.foundation.core.Classifier;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Subsystem</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.model_management.Subsystem#isIsInstantiable <em>Is Instantiable</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.model_management.Model_managementPackage#getSubsystem()
 * @model
 * @generated
 */
public interface Subsystem extends Classifier, net.jgsuess.uml14.model_management.Package {
	/**
	 * Returns the value of the '<em><b>Is Instantiable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Instantiable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Instantiable</em>' attribute.
	 * @see #setIsInstantiable(boolean)
	 * @see net.jgsuess.uml14.model_management.Model_managementPackage#getSubsystem_IsInstantiable()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Boolean"
	 * @generated
	 */
	boolean isIsInstantiable();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.model_management.Subsystem#isIsInstantiable <em>Is Instantiable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Instantiable</em>' attribute.
	 * @see #isIsInstantiable()
	 * @generated
	 */
	void setIsInstantiable(boolean value);

} // Subsystem
