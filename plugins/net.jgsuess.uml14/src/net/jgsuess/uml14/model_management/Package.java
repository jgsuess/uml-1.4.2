/**
 * <copyright>
 * </copyright>
 *
 * $Id: Package.java,v 1.1 2012/04/23 09:31:21 uqjsuss Exp $
 */
package net.jgsuess.uml14.model_management;

import net.jgsuess.uml14.foundation.core.GeneralizableElement;
import net.jgsuess.uml14.foundation.core.Namespace;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Package</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.jgsuess.uml14.model_management.Model_managementPackage#getPackage()
 * @model
 * @generated
 */
public interface Package extends GeneralizableElement, Namespace {
} // Package
