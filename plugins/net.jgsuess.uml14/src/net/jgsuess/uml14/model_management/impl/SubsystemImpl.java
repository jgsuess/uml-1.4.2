/**
 * <copyright>
 * </copyright>
 *
 * $Id: SubsystemImpl.java,v 1.1 2012/04/23 09:31:34 uqjsuss Exp $
 */
package net.jgsuess.uml14.model_management.impl;

import net.jgsuess.uml14.foundation.core.impl.ClassifierImpl;

import net.jgsuess.uml14.model_management.Model_managementPackage;
import net.jgsuess.uml14.model_management.Subsystem;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Subsystem</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.model_management.impl.SubsystemImpl#isIsInstantiable <em>Is Instantiable</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SubsystemImpl extends ClassifierImpl implements Subsystem {
	/**
	 * The default value of the '{@link #isIsInstantiable() <em>Is Instantiable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsInstantiable()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_INSTANTIABLE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsInstantiable() <em>Is Instantiable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsInstantiable()
	 * @generated
	 * @ordered
	 */
	protected boolean isInstantiable = IS_INSTANTIABLE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubsystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Model_managementPackage.Literals.SUBSYSTEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsInstantiable() {
		return isInstantiable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsInstantiable(boolean newIsInstantiable) {
		boolean oldIsInstantiable = isInstantiable;
		isInstantiable = newIsInstantiable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Model_managementPackage.SUBSYSTEM__IS_INSTANTIABLE, oldIsInstantiable, isInstantiable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Model_managementPackage.SUBSYSTEM__IS_INSTANTIABLE:
				return isIsInstantiable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Model_managementPackage.SUBSYSTEM__IS_INSTANTIABLE:
				setIsInstantiable((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Model_managementPackage.SUBSYSTEM__IS_INSTANTIABLE:
				setIsInstantiable(IS_INSTANTIABLE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Model_managementPackage.SUBSYSTEM__IS_INSTANTIABLE:
				return isInstantiable != IS_INSTANTIABLE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isInstantiable: ");
		result.append(isInstantiable);
		result.append(')');
		return result.toString();
	}

} //SubsystemImpl
