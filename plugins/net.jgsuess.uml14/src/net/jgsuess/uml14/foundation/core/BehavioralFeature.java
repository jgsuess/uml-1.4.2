/**
 * <copyright>
 * </copyright>
 *
 * $Id: BehavioralFeature.java,v 1.1 2012/04/23 09:31:26 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Signal;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Behavioral Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.BehavioralFeature#isIsQuery <em>Is Query</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.BehavioralFeature#getParameter <em>Parameter</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.BehavioralFeature#getRaisedSignal <em>Raised Signal</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getBehavioralFeature()
 * @model abstract="true"
 * @generated
 */
public interface BehavioralFeature extends Feature {
	/**
	 * Returns the value of the '<em><b>Is Query</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Query</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Query</em>' attribute.
	 * @see #setIsQuery(boolean)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getBehavioralFeature_IsQuery()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Boolean"
	 * @generated
	 */
	boolean isIsQuery();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.BehavioralFeature#isIsQuery <em>Is Query</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Query</em>' attribute.
	 * @see #isIsQuery()
	 * @generated
	 */
	void setIsQuery(boolean value);

	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Parameter}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Parameter#getBehavioralFeature <em>Behavioral Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter</em>' containment reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getBehavioralFeature_Parameter()
	 * @see net.jgsuess.uml14.foundation.core.Parameter#getBehavioralFeature
	 * @model opposite="behavioralFeature" containment="true"
	 * @generated
	 */
	EList<Parameter> getParameter();

	/**
	 * Returns the value of the '<em><b>Raised Signal</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.Signal}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Signal#getContext <em>Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Raised Signal</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Raised Signal</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getBehavioralFeature_RaisedSignal()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Signal#getContext
	 * @model opposite="context"
	 * @generated
	 */
	EList<Signal> getRaisedSignal();

} // BehavioralFeature
