/**
 * <copyright>
 * </copyright>
 *
 * $Id: Feature.java,v 1.1 2012/04/23 09:31:26 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import net.jgsuess.uml14.foundation.data_types.ScopeKind;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Feature#getOwnerScope <em>Owner Scope</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Feature#getOwner <em>Owner</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getFeature()
 * @model abstract="true"
 * @generated
 */
public interface Feature extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Owner Scope</b></em>' attribute.
	 * The literals are from the enumeration {@link net.jgsuess.uml14.foundation.data_types.ScopeKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner Scope</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owner Scope</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.ScopeKind
	 * @see #setOwnerScope(ScopeKind)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getFeature_OwnerScope()
	 * @model
	 * @generated
	 */
	ScopeKind getOwnerScope();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Feature#getOwnerScope <em>Owner Scope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owner Scope</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.ScopeKind
	 * @see #getOwnerScope()
	 * @generated
	 */
	void setOwnerScope(ScopeKind value);

	/**
	 * Returns the value of the '<em><b>Owner</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Classifier#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owner</em>' container reference.
	 * @see #setOwner(Classifier)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getFeature_Owner()
	 * @see net.jgsuess.uml14.foundation.core.Classifier#getFeature
	 * @model opposite="feature"
	 * @generated
	 */
	Classifier getOwner();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Feature#getOwner <em>Owner</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owner</em>' container reference.
	 * @see #getOwner()
	 * @generated
	 */
	void setOwner(Classifier value);

} // Feature
