/**
 * <copyright>
 * </copyright>
 *
 * $Id: Constraint.java,v 1.1 2012/04/23 09:31:26 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import net.jgsuess.uml14.foundation.data_types.BooleanExpression;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Constraint#getBody <em>Body</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Constraint#getConstrainedElement <em>Constrained Element</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Constraint#getConstrainedStereotype <em>Constrained Stereotype</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getConstraint()
 * @model
 * @generated
 */
public interface Constraint extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' containment reference.
	 * @see #setBody(BooleanExpression)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getConstraint_Body()
	 * @model containment="true"
	 * @generated
	 */
	BooleanExpression getBody();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Constraint#getBody <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body</em>' containment reference.
	 * @see #getBody()
	 * @generated
	 */
	void setBody(BooleanExpression value);

	/**
	 * Returns the value of the '<em><b>Constrained Element</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.ModelElement}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.ModelElement#getConstraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constrained Element</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constrained Element</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getConstraint_ConstrainedElement()
	 * @see net.jgsuess.uml14.foundation.core.ModelElement#getConstraint
	 * @model opposite="constraint"
	 * @generated
	 */
	EList<ModelElement> getConstrainedElement();

	/**
	 * Returns the value of the '<em><b>Constrained Stereotype</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Stereotype#getStereotypeConstraint <em>Stereotype Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constrained Stereotype</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constrained Stereotype</em>' container reference.
	 * @see #setConstrainedStereotype(Stereotype)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getConstraint_ConstrainedStereotype()
	 * @see net.jgsuess.uml14.foundation.core.Stereotype#getStereotypeConstraint
	 * @model opposite="stereotypeConstraint"
	 * @generated
	 */
	Stereotype getConstrainedStereotype();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Constraint#getConstrainedStereotype <em>Constrained Stereotype</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constrained Stereotype</em>' container reference.
	 * @see #getConstrainedStereotype()
	 * @generated
	 */
	void setConstrainedStereotype(Stereotype value);

} // Constraint
