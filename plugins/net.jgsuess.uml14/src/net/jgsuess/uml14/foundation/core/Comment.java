/**
 * <copyright>
 * </copyright>
 *
 * $Id: Comment.java,v 1.1 2012/04/23 09:31:26 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Comment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Comment#getBody <em>Body</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Comment#getAnnotatedElement <em>Annotated Element</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getComment()
 * @model
 * @generated
 */
public interface Comment extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Body</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' attribute.
	 * @see #setBody(String)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getComment_Body()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.String"
	 * @generated
	 */
	String getBody();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Comment#getBody <em>Body</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body</em>' attribute.
	 * @see #getBody()
	 * @generated
	 */
	void setBody(String value);

	/**
	 * Returns the value of the '<em><b>Annotated Element</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.ModelElement}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.ModelElement#getComment <em>Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotated Element</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotated Element</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getComment_AnnotatedElement()
	 * @see net.jgsuess.uml14.foundation.core.ModelElement#getComment
	 * @model opposite="comment"
	 * @generated
	 */
	EList<ModelElement> getAnnotatedElement();

} // Comment
