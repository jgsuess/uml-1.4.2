/**
 * <copyright>
 * </copyright>
 *
 * $Id: Stereotype.java,v 1.1 2012/04/23 09:31:26 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stereotype</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Stereotype#getIcon <em>Icon</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Stereotype#getBaseClass <em>Base Class</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Stereotype#getDefinedTag <em>Defined Tag</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Stereotype#getExtendedElement <em>Extended Element</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Stereotype#getStereotypeConstraint <em>Stereotype Constraint</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getStereotype()
 * @model
 * @generated
 */
public interface Stereotype extends GeneralizableElement {
	/**
	 * Returns the value of the '<em><b>Icon</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Icon</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Icon</em>' attribute.
	 * @see #setIcon(String)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getStereotype_Icon()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Geometry"
	 * @generated
	 */
	String getIcon();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Stereotype#getIcon <em>Icon</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Icon</em>' attribute.
	 * @see #getIcon()
	 * @generated
	 */
	void setIcon(String value);

	/**
	 * Returns the value of the '<em><b>Base Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Class</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Class</em>' attribute.
	 * @see #setBaseClass(String)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getStereotype_BaseClass()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Name"
	 * @generated
	 */
	String getBaseClass();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Stereotype#getBaseClass <em>Base Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Class</em>' attribute.
	 * @see #getBaseClass()
	 * @generated
	 */
	void setBaseClass(String value);

	/**
	 * Returns the value of the '<em><b>Defined Tag</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.TagDefinition}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.TagDefinition#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defined Tag</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defined Tag</em>' containment reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getStereotype_DefinedTag()
	 * @see net.jgsuess.uml14.foundation.core.TagDefinition#getOwner
	 * @model opposite="owner" containment="true"
	 * @generated
	 */
	EList<TagDefinition> getDefinedTag();

	/**
	 * Returns the value of the '<em><b>Extended Element</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.ModelElement}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.ModelElement#getStereotype <em>Stereotype</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extended Element</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extended Element</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getStereotype_ExtendedElement()
	 * @see net.jgsuess.uml14.foundation.core.ModelElement#getStereotype
	 * @model opposite="stereotype"
	 * @generated
	 */
	EList<ModelElement> getExtendedElement();

	/**
	 * Returns the value of the '<em><b>Stereotype Constraint</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Constraint}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Constraint#getConstrainedStereotype <em>Constrained Stereotype</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stereotype Constraint</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stereotype Constraint</em>' containment reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getStereotype_StereotypeConstraint()
	 * @see net.jgsuess.uml14.foundation.core.Constraint#getConstrainedStereotype
	 * @model opposite="constrainedStereotype" containment="true"
	 * @generated
	 */
	EList<Constraint> getStereotypeConstraint();

} // Stereotype
