/**
 * <copyright>
 * </copyright>
 *
 * $Id: AssociationEnd.java,v 1.1 2012/04/23 09:31:28 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole;

import net.jgsuess.uml14.behavioral_elements.common_behavior.LinkEnd;

import net.jgsuess.uml14.foundation.data_types.AggregationKind;
import net.jgsuess.uml14.foundation.data_types.ChangeableKind;
import net.jgsuess.uml14.foundation.data_types.Multiplicity;
import net.jgsuess.uml14.foundation.data_types.OrderingKind;
import net.jgsuess.uml14.foundation.data_types.ScopeKind;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Association End</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.AssociationEnd#isIsNavigable <em>Is Navigable</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getOrdering <em>Ordering</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getAggregation <em>Aggregation</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getTargetScope <em>Target Scope</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getMultiplicity <em>Multiplicity</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getChangeability <em>Changeability</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getAssociation <em>Association</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getQualifier <em>Qualifier</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getParticipant <em>Participant</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getSpecification <em>Specification</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getLinkEnd <em>Link End</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getAssociationEndRole <em>Association End Role</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAssociationEnd()
 * @model
 * @generated
 */
public interface AssociationEnd extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Is Navigable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Navigable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Navigable</em>' attribute.
	 * @see #setIsNavigable(boolean)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAssociationEnd_IsNavigable()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Boolean"
	 * @generated
	 */
	boolean isIsNavigable();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.AssociationEnd#isIsNavigable <em>Is Navigable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Navigable</em>' attribute.
	 * @see #isIsNavigable()
	 * @generated
	 */
	void setIsNavigable(boolean value);

	/**
	 * Returns the value of the '<em><b>Ordering</b></em>' attribute.
	 * The literals are from the enumeration {@link net.jgsuess.uml14.foundation.data_types.OrderingKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ordering</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ordering</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.OrderingKind
	 * @see #setOrdering(OrderingKind)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAssociationEnd_Ordering()
	 * @model
	 * @generated
	 */
	OrderingKind getOrdering();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getOrdering <em>Ordering</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ordering</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.OrderingKind
	 * @see #getOrdering()
	 * @generated
	 */
	void setOrdering(OrderingKind value);

	/**
	 * Returns the value of the '<em><b>Aggregation</b></em>' attribute.
	 * The literals are from the enumeration {@link net.jgsuess.uml14.foundation.data_types.AggregationKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aggregation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aggregation</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.AggregationKind
	 * @see #setAggregation(AggregationKind)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAssociationEnd_Aggregation()
	 * @model
	 * @generated
	 */
	AggregationKind getAggregation();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getAggregation <em>Aggregation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aggregation</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.AggregationKind
	 * @see #getAggregation()
	 * @generated
	 */
	void setAggregation(AggregationKind value);

	/**
	 * Returns the value of the '<em><b>Target Scope</b></em>' attribute.
	 * The literals are from the enumeration {@link net.jgsuess.uml14.foundation.data_types.ScopeKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Scope</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Scope</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.ScopeKind
	 * @see #setTargetScope(ScopeKind)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAssociationEnd_TargetScope()
	 * @model
	 * @generated
	 */
	ScopeKind getTargetScope();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getTargetScope <em>Target Scope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Scope</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.ScopeKind
	 * @see #getTargetScope()
	 * @generated
	 */
	void setTargetScope(ScopeKind value);

	/**
	 * Returns the value of the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multiplicity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiplicity</em>' containment reference.
	 * @see #setMultiplicity(Multiplicity)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAssociationEnd_Multiplicity()
	 * @model containment="true"
	 * @generated
	 */
	Multiplicity getMultiplicity();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getMultiplicity <em>Multiplicity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Multiplicity</em>' containment reference.
	 * @see #getMultiplicity()
	 * @generated
	 */
	void setMultiplicity(Multiplicity value);

	/**
	 * Returns the value of the '<em><b>Changeability</b></em>' attribute.
	 * The literals are from the enumeration {@link net.jgsuess.uml14.foundation.data_types.ChangeableKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Changeability</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Changeability</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.ChangeableKind
	 * @see #setChangeability(ChangeableKind)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAssociationEnd_Changeability()
	 * @model
	 * @generated
	 */
	ChangeableKind getChangeability();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getChangeability <em>Changeability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Changeability</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.ChangeableKind
	 * @see #getChangeability()
	 * @generated
	 */
	void setChangeability(ChangeableKind value);

	/**
	 * Returns the value of the '<em><b>Association</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Association#getConnection <em>Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Association</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Association</em>' container reference.
	 * @see #setAssociation(Association)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAssociationEnd_Association()
	 * @see net.jgsuess.uml14.foundation.core.Association#getConnection
	 * @model opposite="connection" required="true"
	 * @generated
	 */
	Association getAssociation();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getAssociation <em>Association</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Association</em>' container reference.
	 * @see #getAssociation()
	 * @generated
	 */
	void setAssociation(Association value);

	/**
	 * Returns the value of the '<em><b>Qualifier</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Attribute}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Attribute#getAssociationEnd <em>Association End</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Qualifier</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qualifier</em>' containment reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAssociationEnd_Qualifier()
	 * @see net.jgsuess.uml14.foundation.core.Attribute#getAssociationEnd
	 * @model opposite="associationEnd" containment="true"
	 * @generated
	 */
	EList<Attribute> getQualifier();

	/**
	 * Returns the value of the '<em><b>Participant</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Classifier#getAssociation <em>Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Participant</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Participant</em>' reference.
	 * @see #setParticipant(Classifier)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAssociationEnd_Participant()
	 * @see net.jgsuess.uml14.foundation.core.Classifier#getAssociation
	 * @model opposite="association" required="true"
	 * @generated
	 */
	Classifier getParticipant();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getParticipant <em>Participant</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Participant</em>' reference.
	 * @see #getParticipant()
	 * @generated
	 */
	void setParticipant(Classifier value);

	/**
	 * Returns the value of the '<em><b>Specification</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Classifier}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Classifier#getSpecifiedEnd <em>Specified End</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Specification</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specification</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAssociationEnd_Specification()
	 * @see net.jgsuess.uml14.foundation.core.Classifier#getSpecifiedEnd
	 * @model opposite="specifiedEnd"
	 * @generated
	 */
	EList<Classifier> getSpecification();

	/**
	 * Returns the value of the '<em><b>Link End</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.LinkEnd}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.LinkEnd#getAssociationEnd <em>Association End</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Link End</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Link End</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAssociationEnd_LinkEnd()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.LinkEnd#getAssociationEnd
	 * @model opposite="associationEnd"
	 * @generated
	 */
	EList<LinkEnd> getLinkEnd();

	/**
	 * Returns the value of the '<em><b>Association End Role</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Association End Role</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Association End Role</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAssociationEnd_AssociationEndRole()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole#getBase
	 * @model opposite="base"
	 * @generated
	 */
	EList<AssociationEndRole> getAssociationEndRole();

} // AssociationEnd
