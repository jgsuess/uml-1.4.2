/**
 * <copyright>
 * </copyright>
 *
 * $Id: Generalization.java,v 1.1 2012/04/23 09:31:26 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generalization</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Generalization#getDiscriminator <em>Discriminator</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Generalization#getChild <em>Child</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Generalization#getParent <em>Parent</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Generalization#getPowertype <em>Powertype</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getGeneralization()
 * @model
 * @generated
 */
public interface Generalization extends Relationship {
	/**
	 * Returns the value of the '<em><b>Discriminator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discriminator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discriminator</em>' attribute.
	 * @see #setDiscriminator(String)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getGeneralization_Discriminator()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Name"
	 * @generated
	 */
	String getDiscriminator();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Generalization#getDiscriminator <em>Discriminator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discriminator</em>' attribute.
	 * @see #getDiscriminator()
	 * @generated
	 */
	void setDiscriminator(String value);

	/**
	 * Returns the value of the '<em><b>Child</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.GeneralizableElement#getGeneralization <em>Generalization</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Child</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Child</em>' reference.
	 * @see #setChild(GeneralizableElement)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getGeneralization_Child()
	 * @see net.jgsuess.uml14.foundation.core.GeneralizableElement#getGeneralization
	 * @model opposite="generalization" required="true"
	 * @generated
	 */
	GeneralizableElement getChild();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Generalization#getChild <em>Child</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Child</em>' reference.
	 * @see #getChild()
	 * @generated
	 */
	void setChild(GeneralizableElement value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.GeneralizableElement#getSpecialization <em>Specialization</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' reference.
	 * @see #setParent(GeneralizableElement)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getGeneralization_Parent()
	 * @see net.jgsuess.uml14.foundation.core.GeneralizableElement#getSpecialization
	 * @model opposite="specialization" required="true"
	 * @generated
	 */
	GeneralizableElement getParent();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Generalization#getParent <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(GeneralizableElement value);

	/**
	 * Returns the value of the '<em><b>Powertype</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Classifier#getPowertypeRange <em>Powertype Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Powertype</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Powertype</em>' reference.
	 * @see #setPowertype(Classifier)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getGeneralization_Powertype()
	 * @see net.jgsuess.uml14.foundation.core.Classifier#getPowertypeRange
	 * @model opposite="powertypeRange"
	 * @generated
	 */
	Classifier getPowertype();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Generalization#getPowertype <em>Powertype</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Powertype</em>' reference.
	 * @see #getPowertype()
	 * @generated
	 */
	void setPowertype(Classifier value);

} // Generalization
