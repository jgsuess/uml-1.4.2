/**
 * <copyright>
 * </copyright>
 *
 * $Id: TaggedValue.java,v 1.1 2012/04/23 09:31:26 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tagged Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.TaggedValue#getDataValue <em>Data Value</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.TaggedValue#getModelElement <em>Model Element</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.TaggedValue#getType <em>Type</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.TaggedValue#getReferenceValue <em>Reference Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getTaggedValue()
 * @model
 * @generated
 */
public interface TaggedValue extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Data Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Value</em>' attribute.
	 * @see #setDataValue(String)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getTaggedValue_DataValue()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.String"
	 * @generated
	 */
	String getDataValue();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.TaggedValue#getDataValue <em>Data Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Value</em>' attribute.
	 * @see #getDataValue()
	 * @generated
	 */
	void setDataValue(String value);

	/**
	 * Returns the value of the '<em><b>Model Element</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.ModelElement#getTaggedValue <em>Tagged Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Element</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Element</em>' container reference.
	 * @see #setModelElement(ModelElement)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getTaggedValue_ModelElement()
	 * @see net.jgsuess.uml14.foundation.core.ModelElement#getTaggedValue
	 * @model opposite="taggedValue" required="true"
	 * @generated
	 */
	ModelElement getModelElement();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.TaggedValue#getModelElement <em>Model Element</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Element</em>' container reference.
	 * @see #getModelElement()
	 * @generated
	 */
	void setModelElement(ModelElement value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.TagDefinition#getTypedValue <em>Typed Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(TagDefinition)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getTaggedValue_Type()
	 * @see net.jgsuess.uml14.foundation.core.TagDefinition#getTypedValue
	 * @model opposite="typedValue" required="true"
	 * @generated
	 */
	TagDefinition getType();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.TaggedValue#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(TagDefinition value);

	/**
	 * Returns the value of the '<em><b>Reference Value</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.ModelElement}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.ModelElement#getReferenceTag <em>Reference Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference Value</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference Value</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getTaggedValue_ReferenceValue()
	 * @see net.jgsuess.uml14.foundation.core.ModelElement#getReferenceTag
	 * @model opposite="referenceTag"
	 * @generated
	 */
	EList<ModelElement> getReferenceValue();

} // TaggedValue
