/**
 * <copyright>
 * </copyright>
 *
 * $Id: ElementResidence.java,v 1.1 2012/04/23 09:31:26 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import net.jgsuess.uml14.foundation.data_types.VisibilityKind;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element Residence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ElementResidence#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ElementResidence#getResident <em>Resident</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ElementResidence#getContainer <em>Container</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getElementResidence()
 * @model
 * @generated
 */
public interface ElementResidence extends EObject {
	/**
	 * Returns the value of the '<em><b>Visibility</b></em>' attribute.
	 * The literals are from the enumeration {@link net.jgsuess.uml14.foundation.data_types.VisibilityKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visibility</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visibility</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.VisibilityKind
	 * @see #setVisibility(VisibilityKind)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getElementResidence_Visibility()
	 * @model
	 * @generated
	 */
	VisibilityKind getVisibility();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.ElementResidence#getVisibility <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visibility</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.VisibilityKind
	 * @see #getVisibility()
	 * @generated
	 */
	void setVisibility(VisibilityKind value);

	/**
	 * Returns the value of the '<em><b>Resident</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.ModelElement#getElementResidence <em>Element Residence</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resident</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resident</em>' reference.
	 * @see #setResident(ModelElement)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getElementResidence_Resident()
	 * @see net.jgsuess.uml14.foundation.core.ModelElement#getElementResidence
	 * @model opposite="elementResidence" required="true"
	 * @generated
	 */
	ModelElement getResident();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.ElementResidence#getResident <em>Resident</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resident</em>' reference.
	 * @see #getResident()
	 * @generated
	 */
	void setResident(ModelElement value);

	/**
	 * Returns the value of the '<em><b>Container</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Component#getResidentElement <em>Resident Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container</em>' container reference.
	 * @see #setContainer(Component)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getElementResidence_Container()
	 * @see net.jgsuess.uml14.foundation.core.Component#getResidentElement
	 * @model opposite="residentElement" required="true"
	 * @generated
	 */
	Component getContainer();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.ElementResidence#getContainer <em>Container</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Container</em>' container reference.
	 * @see #getContainer()
	 * @generated
	 */
	void setContainer(Component value);

} // ElementResidence
