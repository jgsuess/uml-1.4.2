/**
 * <copyright>
 * </copyright>
 *
 * $Id: AssociationImpl.java,v 1.1 2012/04/23 09:31:31 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Link;

import net.jgsuess.uml14.foundation.core.Association;
import net.jgsuess.uml14.foundation.core.AssociationEnd;
import net.jgsuess.uml14.foundation.core.CorePackage;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Association</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.AssociationImpl#getConnection <em>Connection</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.AssociationImpl#getLink <em>Link</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.AssociationImpl#getAssociationRole <em>Association Role</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AssociationImpl extends GeneralizableElementImpl implements Association {
	/**
	 * The cached value of the '{@link #getConnection() <em>Connection</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnection()
	 * @generated
	 * @ordered
	 */
	protected EList<AssociationEnd> connection;

	/**
	 * The cached value of the '{@link #getLink() <em>Link</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLink()
	 * @generated
	 * @ordered
	 */
	protected EList<Link> link;

	/**
	 * The cached value of the '{@link #getAssociationRole() <em>Association Role</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociationRole()
	 * @generated
	 * @ordered
	 */
	protected EList<AssociationRole> associationRole;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssociationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.ASSOCIATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssociationEnd> getConnection() {
		if (connection == null) {
			connection = new EObjectContainmentWithInverseEList<AssociationEnd>(AssociationEnd.class, this, CorePackage.ASSOCIATION__CONNECTION, CorePackage.ASSOCIATION_END__ASSOCIATION);
		}
		return connection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Link> getLink() {
		if (link == null) {
			link = new EObjectWithInverseResolvingEList<Link>(Link.class, this, CorePackage.ASSOCIATION__LINK, Common_behaviorPackage.LINK__ASSOCIATION);
		}
		return link;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssociationRole> getAssociationRole() {
		if (associationRole == null) {
			associationRole = new EObjectWithInverseResolvingEList<AssociationRole>(AssociationRole.class, this, CorePackage.ASSOCIATION__ASSOCIATION_ROLE, CollaborationsPackage.ASSOCIATION_ROLE__BASE);
		}
		return associationRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.ASSOCIATION__CONNECTION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getConnection()).basicAdd(otherEnd, msgs);
			case CorePackage.ASSOCIATION__LINK:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getLink()).basicAdd(otherEnd, msgs);
			case CorePackage.ASSOCIATION__ASSOCIATION_ROLE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAssociationRole()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.ASSOCIATION__CONNECTION:
				return ((InternalEList<?>)getConnection()).basicRemove(otherEnd, msgs);
			case CorePackage.ASSOCIATION__LINK:
				return ((InternalEList<?>)getLink()).basicRemove(otherEnd, msgs);
			case CorePackage.ASSOCIATION__ASSOCIATION_ROLE:
				return ((InternalEList<?>)getAssociationRole()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CorePackage.ASSOCIATION__CONNECTION:
				return getConnection();
			case CorePackage.ASSOCIATION__LINK:
				return getLink();
			case CorePackage.ASSOCIATION__ASSOCIATION_ROLE:
				return getAssociationRole();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CorePackage.ASSOCIATION__CONNECTION:
				getConnection().clear();
				getConnection().addAll((Collection<? extends AssociationEnd>)newValue);
				return;
			case CorePackage.ASSOCIATION__LINK:
				getLink().clear();
				getLink().addAll((Collection<? extends Link>)newValue);
				return;
			case CorePackage.ASSOCIATION__ASSOCIATION_ROLE:
				getAssociationRole().clear();
				getAssociationRole().addAll((Collection<? extends AssociationRole>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CorePackage.ASSOCIATION__CONNECTION:
				getConnection().clear();
				return;
			case CorePackage.ASSOCIATION__LINK:
				getLink().clear();
				return;
			case CorePackage.ASSOCIATION__ASSOCIATION_ROLE:
				getAssociationRole().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CorePackage.ASSOCIATION__CONNECTION:
				return connection != null && !connection.isEmpty();
			case CorePackage.ASSOCIATION__LINK:
				return link != null && !link.isEmpty();
			case CorePackage.ASSOCIATION__ASSOCIATION_ROLE:
				return associationRole != null && !associationRole.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AssociationImpl
