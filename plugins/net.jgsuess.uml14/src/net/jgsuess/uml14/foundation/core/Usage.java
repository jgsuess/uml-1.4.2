/**
 * <copyright>
 * </copyright>
 *
 * $Id: Usage.java,v 1.1 2012/04/23 09:31:26 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Usage</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getUsage()
 * @model
 * @generated
 */
public interface Usage extends Dependency {
} // Usage
