/**
 * <copyright>
 * </copyright>
 *
 * $Id: Namespace.java,v 1.1 2012/04/23 09:31:26 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Namespace</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Namespace#getOwnedElement <em>Owned Element</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getNamespace()
 * @model abstract="true"
 * @generated
 */
public interface Namespace extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Owned Element</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.ModelElement}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.ModelElement#getNamespace <em>Namespace</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Element</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Element</em>' containment reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getNamespace_OwnedElement()
	 * @see net.jgsuess.uml14.foundation.core.ModelElement#getNamespace
	 * @model opposite="namespace" containment="true"
	 * @generated
	 */
	EList<ModelElement> getOwnedElement();

} // Namespace
