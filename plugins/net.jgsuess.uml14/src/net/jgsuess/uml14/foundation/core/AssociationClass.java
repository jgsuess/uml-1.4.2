/**
 * <copyright>
 * </copyright>
 *
 * $Id: AssociationClass.java,v 1.1 2012/04/23 09:31:28 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Association Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAssociationClass()
 * @model
 * @generated
 */
public interface AssociationClass extends net.jgsuess.uml14.foundation.core.Class, Association {
} // AssociationClass
