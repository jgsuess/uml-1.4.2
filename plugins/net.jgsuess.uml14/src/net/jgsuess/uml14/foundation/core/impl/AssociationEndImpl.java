/**
 * <copyright>
 * </copyright>
 *
 * $Id: AssociationEndImpl.java,v 1.1 2012/04/23 09:31:34 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;
import net.jgsuess.uml14.behavioral_elements.common_behavior.LinkEnd;

import net.jgsuess.uml14.foundation.core.Association;
import net.jgsuess.uml14.foundation.core.AssociationEnd;
import net.jgsuess.uml14.foundation.core.Attribute;
import net.jgsuess.uml14.foundation.core.Classifier;
import net.jgsuess.uml14.foundation.core.CorePackage;

import net.jgsuess.uml14.foundation.data_types.AggregationKind;
import net.jgsuess.uml14.foundation.data_types.ChangeableKind;
import net.jgsuess.uml14.foundation.data_types.Multiplicity;
import net.jgsuess.uml14.foundation.data_types.OrderingKind;
import net.jgsuess.uml14.foundation.data_types.ScopeKind;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Association End</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.AssociationEndImpl#isIsNavigable <em>Is Navigable</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.AssociationEndImpl#getOrdering <em>Ordering</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.AssociationEndImpl#getAggregation <em>Aggregation</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.AssociationEndImpl#getTargetScope <em>Target Scope</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.AssociationEndImpl#getMultiplicity <em>Multiplicity</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.AssociationEndImpl#getChangeability <em>Changeability</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.AssociationEndImpl#getAssociation <em>Association</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.AssociationEndImpl#getQualifier <em>Qualifier</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.AssociationEndImpl#getParticipant <em>Participant</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.AssociationEndImpl#getSpecification <em>Specification</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.AssociationEndImpl#getLinkEnd <em>Link End</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.AssociationEndImpl#getAssociationEndRole <em>Association End Role</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AssociationEndImpl extends ModelElementImpl implements AssociationEnd {
	/**
	 * The default value of the '{@link #isIsNavigable() <em>Is Navigable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsNavigable()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_NAVIGABLE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsNavigable() <em>Is Navigable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsNavigable()
	 * @generated
	 * @ordered
	 */
	protected boolean isNavigable = IS_NAVIGABLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getOrdering() <em>Ordering</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrdering()
	 * @generated
	 * @ordered
	 */
	protected static final OrderingKind ORDERING_EDEFAULT = OrderingKind.OK_UNORDERED;

	/**
	 * The cached value of the '{@link #getOrdering() <em>Ordering</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrdering()
	 * @generated
	 * @ordered
	 */
	protected OrderingKind ordering = ORDERING_EDEFAULT;

	/**
	 * The default value of the '{@link #getAggregation() <em>Aggregation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAggregation()
	 * @generated
	 * @ordered
	 */
	protected static final AggregationKind AGGREGATION_EDEFAULT = AggregationKind.AK_NONE;

	/**
	 * The cached value of the '{@link #getAggregation() <em>Aggregation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAggregation()
	 * @generated
	 * @ordered
	 */
	protected AggregationKind aggregation = AGGREGATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getTargetScope() <em>Target Scope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetScope()
	 * @generated
	 * @ordered
	 */
	protected static final ScopeKind TARGET_SCOPE_EDEFAULT = ScopeKind.SK_INSTANCE;

	/**
	 * The cached value of the '{@link #getTargetScope() <em>Target Scope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetScope()
	 * @generated
	 * @ordered
	 */
	protected ScopeKind targetScope = TARGET_SCOPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMultiplicity() <em>Multiplicity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMultiplicity()
	 * @generated
	 * @ordered
	 */
	protected Multiplicity multiplicity;

	/**
	 * The default value of the '{@link #getChangeability() <em>Changeability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChangeability()
	 * @generated
	 * @ordered
	 */
	protected static final ChangeableKind CHANGEABILITY_EDEFAULT = ChangeableKind.CK_CHANGEABLE;

	/**
	 * The cached value of the '{@link #getChangeability() <em>Changeability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChangeability()
	 * @generated
	 * @ordered
	 */
	protected ChangeableKind changeability = CHANGEABILITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getQualifier() <em>Qualifier</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQualifier()
	 * @generated
	 * @ordered
	 */
	protected EList<Attribute> qualifier;

	/**
	 * The cached value of the '{@link #getParticipant() <em>Participant</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParticipant()
	 * @generated
	 * @ordered
	 */
	protected Classifier participant;

	/**
	 * The cached value of the '{@link #getSpecification() <em>Specification</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecification()
	 * @generated
	 * @ordered
	 */
	protected EList<Classifier> specification;

	/**
	 * The cached value of the '{@link #getLinkEnd() <em>Link End</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkEnd()
	 * @generated
	 * @ordered
	 */
	protected EList<LinkEnd> linkEnd;

	/**
	 * The cached value of the '{@link #getAssociationEndRole() <em>Association End Role</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociationEndRole()
	 * @generated
	 * @ordered
	 */
	protected EList<AssociationEndRole> associationEndRole;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssociationEndImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.ASSOCIATION_END;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsNavigable() {
		return isNavigable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsNavigable(boolean newIsNavigable) {
		boolean oldIsNavigable = isNavigable;
		isNavigable = newIsNavigable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.ASSOCIATION_END__IS_NAVIGABLE, oldIsNavigable, isNavigable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrderingKind getOrdering() {
		return ordering;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrdering(OrderingKind newOrdering) {
		OrderingKind oldOrdering = ordering;
		ordering = newOrdering == null ? ORDERING_EDEFAULT : newOrdering;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.ASSOCIATION_END__ORDERING, oldOrdering, ordering));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AggregationKind getAggregation() {
		return aggregation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAggregation(AggregationKind newAggregation) {
		AggregationKind oldAggregation = aggregation;
		aggregation = newAggregation == null ? AGGREGATION_EDEFAULT : newAggregation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.ASSOCIATION_END__AGGREGATION, oldAggregation, aggregation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScopeKind getTargetScope() {
		return targetScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetScope(ScopeKind newTargetScope) {
		ScopeKind oldTargetScope = targetScope;
		targetScope = newTargetScope == null ? TARGET_SCOPE_EDEFAULT : newTargetScope;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.ASSOCIATION_END__TARGET_SCOPE, oldTargetScope, targetScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Multiplicity getMultiplicity() {
		return multiplicity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMultiplicity(Multiplicity newMultiplicity, NotificationChain msgs) {
		Multiplicity oldMultiplicity = multiplicity;
		multiplicity = newMultiplicity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.ASSOCIATION_END__MULTIPLICITY, oldMultiplicity, newMultiplicity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMultiplicity(Multiplicity newMultiplicity) {
		if (newMultiplicity != multiplicity) {
			NotificationChain msgs = null;
			if (multiplicity != null)
				msgs = ((InternalEObject)multiplicity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.ASSOCIATION_END__MULTIPLICITY, null, msgs);
			if (newMultiplicity != null)
				msgs = ((InternalEObject)newMultiplicity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.ASSOCIATION_END__MULTIPLICITY, null, msgs);
			msgs = basicSetMultiplicity(newMultiplicity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.ASSOCIATION_END__MULTIPLICITY, newMultiplicity, newMultiplicity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeableKind getChangeability() {
		return changeability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeability(ChangeableKind newChangeability) {
		ChangeableKind oldChangeability = changeability;
		changeability = newChangeability == null ? CHANGEABILITY_EDEFAULT : newChangeability;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.ASSOCIATION_END__CHANGEABILITY, oldChangeability, changeability));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Association getAssociation() {
		if (eContainerFeatureID() != CorePackage.ASSOCIATION_END__ASSOCIATION) return null;
		return (Association)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssociation(Association newAssociation, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newAssociation, CorePackage.ASSOCIATION_END__ASSOCIATION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssociation(Association newAssociation) {
		if (newAssociation != eInternalContainer() || (eContainerFeatureID() != CorePackage.ASSOCIATION_END__ASSOCIATION && newAssociation != null)) {
			if (EcoreUtil.isAncestor(this, newAssociation))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newAssociation != null)
				msgs = ((InternalEObject)newAssociation).eInverseAdd(this, CorePackage.ASSOCIATION__CONNECTION, Association.class, msgs);
			msgs = basicSetAssociation(newAssociation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.ASSOCIATION_END__ASSOCIATION, newAssociation, newAssociation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Attribute> getQualifier() {
		if (qualifier == null) {
			qualifier = new EObjectContainmentWithInverseEList<Attribute>(Attribute.class, this, CorePackage.ASSOCIATION_END__QUALIFIER, CorePackage.ATTRIBUTE__ASSOCIATION_END);
		}
		return qualifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Classifier getParticipant() {
		if (participant != null && participant.eIsProxy()) {
			InternalEObject oldParticipant = (InternalEObject)participant;
			participant = (Classifier)eResolveProxy(oldParticipant);
			if (participant != oldParticipant) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.ASSOCIATION_END__PARTICIPANT, oldParticipant, participant));
			}
		}
		return participant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Classifier basicGetParticipant() {
		return participant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParticipant(Classifier newParticipant, NotificationChain msgs) {
		Classifier oldParticipant = participant;
		participant = newParticipant;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.ASSOCIATION_END__PARTICIPANT, oldParticipant, newParticipant);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParticipant(Classifier newParticipant) {
		if (newParticipant != participant) {
			NotificationChain msgs = null;
			if (participant != null)
				msgs = ((InternalEObject)participant).eInverseRemove(this, CorePackage.CLASSIFIER__ASSOCIATION, Classifier.class, msgs);
			if (newParticipant != null)
				msgs = ((InternalEObject)newParticipant).eInverseAdd(this, CorePackage.CLASSIFIER__ASSOCIATION, Classifier.class, msgs);
			msgs = basicSetParticipant(newParticipant, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.ASSOCIATION_END__PARTICIPANT, newParticipant, newParticipant));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Classifier> getSpecification() {
		if (specification == null) {
			specification = new EObjectWithInverseResolvingEList.ManyInverse<Classifier>(Classifier.class, this, CorePackage.ASSOCIATION_END__SPECIFICATION, CorePackage.CLASSIFIER__SPECIFIED_END);
		}
		return specification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LinkEnd> getLinkEnd() {
		if (linkEnd == null) {
			linkEnd = new EObjectWithInverseResolvingEList<LinkEnd>(LinkEnd.class, this, CorePackage.ASSOCIATION_END__LINK_END, Common_behaviorPackage.LINK_END__ASSOCIATION_END);
		}
		return linkEnd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssociationEndRole> getAssociationEndRole() {
		if (associationEndRole == null) {
			associationEndRole = new EObjectWithInverseResolvingEList<AssociationEndRole>(AssociationEndRole.class, this, CorePackage.ASSOCIATION_END__ASSOCIATION_END_ROLE, CollaborationsPackage.ASSOCIATION_END_ROLE__BASE);
		}
		return associationEndRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.ASSOCIATION_END__ASSOCIATION:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetAssociation((Association)otherEnd, msgs);
			case CorePackage.ASSOCIATION_END__QUALIFIER:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getQualifier()).basicAdd(otherEnd, msgs);
			case CorePackage.ASSOCIATION_END__PARTICIPANT:
				if (participant != null)
					msgs = ((InternalEObject)participant).eInverseRemove(this, CorePackage.CLASSIFIER__ASSOCIATION, Classifier.class, msgs);
				return basicSetParticipant((Classifier)otherEnd, msgs);
			case CorePackage.ASSOCIATION_END__SPECIFICATION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSpecification()).basicAdd(otherEnd, msgs);
			case CorePackage.ASSOCIATION_END__LINK_END:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getLinkEnd()).basicAdd(otherEnd, msgs);
			case CorePackage.ASSOCIATION_END__ASSOCIATION_END_ROLE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAssociationEndRole()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.ASSOCIATION_END__MULTIPLICITY:
				return basicSetMultiplicity(null, msgs);
			case CorePackage.ASSOCIATION_END__ASSOCIATION:
				return basicSetAssociation(null, msgs);
			case CorePackage.ASSOCIATION_END__QUALIFIER:
				return ((InternalEList<?>)getQualifier()).basicRemove(otherEnd, msgs);
			case CorePackage.ASSOCIATION_END__PARTICIPANT:
				return basicSetParticipant(null, msgs);
			case CorePackage.ASSOCIATION_END__SPECIFICATION:
				return ((InternalEList<?>)getSpecification()).basicRemove(otherEnd, msgs);
			case CorePackage.ASSOCIATION_END__LINK_END:
				return ((InternalEList<?>)getLinkEnd()).basicRemove(otherEnd, msgs);
			case CorePackage.ASSOCIATION_END__ASSOCIATION_END_ROLE:
				return ((InternalEList<?>)getAssociationEndRole()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CorePackage.ASSOCIATION_END__ASSOCIATION:
				return eInternalContainer().eInverseRemove(this, CorePackage.ASSOCIATION__CONNECTION, Association.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CorePackage.ASSOCIATION_END__IS_NAVIGABLE:
				return isIsNavigable();
			case CorePackage.ASSOCIATION_END__ORDERING:
				return getOrdering();
			case CorePackage.ASSOCIATION_END__AGGREGATION:
				return getAggregation();
			case CorePackage.ASSOCIATION_END__TARGET_SCOPE:
				return getTargetScope();
			case CorePackage.ASSOCIATION_END__MULTIPLICITY:
				return getMultiplicity();
			case CorePackage.ASSOCIATION_END__CHANGEABILITY:
				return getChangeability();
			case CorePackage.ASSOCIATION_END__ASSOCIATION:
				return getAssociation();
			case CorePackage.ASSOCIATION_END__QUALIFIER:
				return getQualifier();
			case CorePackage.ASSOCIATION_END__PARTICIPANT:
				if (resolve) return getParticipant();
				return basicGetParticipant();
			case CorePackage.ASSOCIATION_END__SPECIFICATION:
				return getSpecification();
			case CorePackage.ASSOCIATION_END__LINK_END:
				return getLinkEnd();
			case CorePackage.ASSOCIATION_END__ASSOCIATION_END_ROLE:
				return getAssociationEndRole();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CorePackage.ASSOCIATION_END__IS_NAVIGABLE:
				setIsNavigable((Boolean)newValue);
				return;
			case CorePackage.ASSOCIATION_END__ORDERING:
				setOrdering((OrderingKind)newValue);
				return;
			case CorePackage.ASSOCIATION_END__AGGREGATION:
				setAggregation((AggregationKind)newValue);
				return;
			case CorePackage.ASSOCIATION_END__TARGET_SCOPE:
				setTargetScope((ScopeKind)newValue);
				return;
			case CorePackage.ASSOCIATION_END__MULTIPLICITY:
				setMultiplicity((Multiplicity)newValue);
				return;
			case CorePackage.ASSOCIATION_END__CHANGEABILITY:
				setChangeability((ChangeableKind)newValue);
				return;
			case CorePackage.ASSOCIATION_END__ASSOCIATION:
				setAssociation((Association)newValue);
				return;
			case CorePackage.ASSOCIATION_END__QUALIFIER:
				getQualifier().clear();
				getQualifier().addAll((Collection<? extends Attribute>)newValue);
				return;
			case CorePackage.ASSOCIATION_END__PARTICIPANT:
				setParticipant((Classifier)newValue);
				return;
			case CorePackage.ASSOCIATION_END__SPECIFICATION:
				getSpecification().clear();
				getSpecification().addAll((Collection<? extends Classifier>)newValue);
				return;
			case CorePackage.ASSOCIATION_END__LINK_END:
				getLinkEnd().clear();
				getLinkEnd().addAll((Collection<? extends LinkEnd>)newValue);
				return;
			case CorePackage.ASSOCIATION_END__ASSOCIATION_END_ROLE:
				getAssociationEndRole().clear();
				getAssociationEndRole().addAll((Collection<? extends AssociationEndRole>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CorePackage.ASSOCIATION_END__IS_NAVIGABLE:
				setIsNavigable(IS_NAVIGABLE_EDEFAULT);
				return;
			case CorePackage.ASSOCIATION_END__ORDERING:
				setOrdering(ORDERING_EDEFAULT);
				return;
			case CorePackage.ASSOCIATION_END__AGGREGATION:
				setAggregation(AGGREGATION_EDEFAULT);
				return;
			case CorePackage.ASSOCIATION_END__TARGET_SCOPE:
				setTargetScope(TARGET_SCOPE_EDEFAULT);
				return;
			case CorePackage.ASSOCIATION_END__MULTIPLICITY:
				setMultiplicity((Multiplicity)null);
				return;
			case CorePackage.ASSOCIATION_END__CHANGEABILITY:
				setChangeability(CHANGEABILITY_EDEFAULT);
				return;
			case CorePackage.ASSOCIATION_END__ASSOCIATION:
				setAssociation((Association)null);
				return;
			case CorePackage.ASSOCIATION_END__QUALIFIER:
				getQualifier().clear();
				return;
			case CorePackage.ASSOCIATION_END__PARTICIPANT:
				setParticipant((Classifier)null);
				return;
			case CorePackage.ASSOCIATION_END__SPECIFICATION:
				getSpecification().clear();
				return;
			case CorePackage.ASSOCIATION_END__LINK_END:
				getLinkEnd().clear();
				return;
			case CorePackage.ASSOCIATION_END__ASSOCIATION_END_ROLE:
				getAssociationEndRole().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CorePackage.ASSOCIATION_END__IS_NAVIGABLE:
				return isNavigable != IS_NAVIGABLE_EDEFAULT;
			case CorePackage.ASSOCIATION_END__ORDERING:
				return ordering != ORDERING_EDEFAULT;
			case CorePackage.ASSOCIATION_END__AGGREGATION:
				return aggregation != AGGREGATION_EDEFAULT;
			case CorePackage.ASSOCIATION_END__TARGET_SCOPE:
				return targetScope != TARGET_SCOPE_EDEFAULT;
			case CorePackage.ASSOCIATION_END__MULTIPLICITY:
				return multiplicity != null;
			case CorePackage.ASSOCIATION_END__CHANGEABILITY:
				return changeability != CHANGEABILITY_EDEFAULT;
			case CorePackage.ASSOCIATION_END__ASSOCIATION:
				return getAssociation() != null;
			case CorePackage.ASSOCIATION_END__QUALIFIER:
				return qualifier != null && !qualifier.isEmpty();
			case CorePackage.ASSOCIATION_END__PARTICIPANT:
				return participant != null;
			case CorePackage.ASSOCIATION_END__SPECIFICATION:
				return specification != null && !specification.isEmpty();
			case CorePackage.ASSOCIATION_END__LINK_END:
				return linkEnd != null && !linkEnd.isEmpty();
			case CorePackage.ASSOCIATION_END__ASSOCIATION_END_ROLE:
				return associationEndRole != null && !associationEndRole.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isNavigable: ");
		result.append(isNavigable);
		result.append(", ordering: ");
		result.append(ordering);
		result.append(", aggregation: ");
		result.append(aggregation);
		result.append(", targetScope: ");
		result.append(targetScope);
		result.append(", changeability: ");
		result.append(changeability);
		result.append(')');
		return result.toString();
	}

} //AssociationEndImpl
