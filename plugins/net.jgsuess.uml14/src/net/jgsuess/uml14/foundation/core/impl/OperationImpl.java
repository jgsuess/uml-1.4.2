/**
 * <copyright>
 * </copyright>
 *
 * $Id: OperationImpl.java,v 1.1 2012/04/23 09:31:32 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.common_behavior.CallAction;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;

import net.jgsuess.uml14.behavioral_elements.state_machines.CallEvent;
import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage;

import net.jgsuess.uml14.foundation.core.CorePackage;
import net.jgsuess.uml14.foundation.core.Method;
import net.jgsuess.uml14.foundation.core.Operation;

import net.jgsuess.uml14.foundation.data_types.CallConcurrencyKind;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.OperationImpl#getConcurrency <em>Concurrency</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.OperationImpl#isIsRoot <em>Is Root</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.OperationImpl#isIsLeaf <em>Is Leaf</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.OperationImpl#isIsAbstract <em>Is Abstract</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.OperationImpl#getSpecification <em>Specification</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.OperationImpl#getMethod <em>Method</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.OperationImpl#getCallAction <em>Call Action</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.OperationImpl#getOccurrence <em>Occurrence</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OperationImpl extends BehavioralFeatureImpl implements Operation {
	/**
	 * The default value of the '{@link #getConcurrency() <em>Concurrency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConcurrency()
	 * @generated
	 * @ordered
	 */
	protected static final CallConcurrencyKind CONCURRENCY_EDEFAULT = CallConcurrencyKind.CCK_SEQUENTIAL;

	/**
	 * The cached value of the '{@link #getConcurrency() <em>Concurrency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConcurrency()
	 * @generated
	 * @ordered
	 */
	protected CallConcurrencyKind concurrency = CONCURRENCY_EDEFAULT;

	/**
	 * The default value of the '{@link #isIsRoot() <em>Is Root</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsRoot()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_ROOT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsRoot() <em>Is Root</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsRoot()
	 * @generated
	 * @ordered
	 */
	protected boolean isRoot = IS_ROOT_EDEFAULT;

	/**
	 * The default value of the '{@link #isIsLeaf() <em>Is Leaf</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsLeaf()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_LEAF_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsLeaf() <em>Is Leaf</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsLeaf()
	 * @generated
	 * @ordered
	 */
	protected boolean isLeaf = IS_LEAF_EDEFAULT;

	/**
	 * The default value of the '{@link #isIsAbstract() <em>Is Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsAbstract()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_ABSTRACT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsAbstract() <em>Is Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsAbstract()
	 * @generated
	 * @ordered
	 */
	protected boolean isAbstract = IS_ABSTRACT_EDEFAULT;

	/**
	 * The default value of the '{@link #getSpecification() <em>Specification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecification()
	 * @generated
	 * @ordered
	 */
	protected static final String SPECIFICATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSpecification() <em>Specification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecification()
	 * @generated
	 * @ordered
	 */
	protected String specification = SPECIFICATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMethod() <em>Method</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMethod()
	 * @generated
	 * @ordered
	 */
	protected EList<Method> method;

	/**
	 * The cached value of the '{@link #getCallAction() <em>Call Action</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCallAction()
	 * @generated
	 * @ordered
	 */
	protected EList<CallAction> callAction;

	/**
	 * The cached value of the '{@link #getOccurrence() <em>Occurrence</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOccurrence()
	 * @generated
	 * @ordered
	 */
	protected EList<CallEvent> occurrence;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallConcurrencyKind getConcurrency() {
		return concurrency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConcurrency(CallConcurrencyKind newConcurrency) {
		CallConcurrencyKind oldConcurrency = concurrency;
		concurrency = newConcurrency == null ? CONCURRENCY_EDEFAULT : newConcurrency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.OPERATION__CONCURRENCY, oldConcurrency, concurrency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsRoot() {
		return isRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsRoot(boolean newIsRoot) {
		boolean oldIsRoot = isRoot;
		isRoot = newIsRoot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.OPERATION__IS_ROOT, oldIsRoot, isRoot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsLeaf() {
		return isLeaf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsLeaf(boolean newIsLeaf) {
		boolean oldIsLeaf = isLeaf;
		isLeaf = newIsLeaf;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.OPERATION__IS_LEAF, oldIsLeaf, isLeaf));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsAbstract() {
		return isAbstract;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsAbstract(boolean newIsAbstract) {
		boolean oldIsAbstract = isAbstract;
		isAbstract = newIsAbstract;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.OPERATION__IS_ABSTRACT, oldIsAbstract, isAbstract));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSpecification() {
		return specification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecification(String newSpecification) {
		String oldSpecification = specification;
		specification = newSpecification;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.OPERATION__SPECIFICATION, oldSpecification, specification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Method> getMethod() {
		if (method == null) {
			method = new EObjectWithInverseResolvingEList<Method>(Method.class, this, CorePackage.OPERATION__METHOD, CorePackage.METHOD__SPECIFICATION);
		}
		return method;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CallAction> getCallAction() {
		if (callAction == null) {
			callAction = new EObjectWithInverseResolvingEList<CallAction>(CallAction.class, this, CorePackage.OPERATION__CALL_ACTION, Common_behaviorPackage.CALL_ACTION__OPERATION);
		}
		return callAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CallEvent> getOccurrence() {
		if (occurrence == null) {
			occurrence = new EObjectWithInverseResolvingEList<CallEvent>(CallEvent.class, this, CorePackage.OPERATION__OCCURRENCE, State_machinesPackage.CALL_EVENT__OPERATION);
		}
		return occurrence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.OPERATION__METHOD:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getMethod()).basicAdd(otherEnd, msgs);
			case CorePackage.OPERATION__CALL_ACTION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getCallAction()).basicAdd(otherEnd, msgs);
			case CorePackage.OPERATION__OCCURRENCE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOccurrence()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.OPERATION__METHOD:
				return ((InternalEList<?>)getMethod()).basicRemove(otherEnd, msgs);
			case CorePackage.OPERATION__CALL_ACTION:
				return ((InternalEList<?>)getCallAction()).basicRemove(otherEnd, msgs);
			case CorePackage.OPERATION__OCCURRENCE:
				return ((InternalEList<?>)getOccurrence()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CorePackage.OPERATION__CONCURRENCY:
				return getConcurrency();
			case CorePackage.OPERATION__IS_ROOT:
				return isIsRoot();
			case CorePackage.OPERATION__IS_LEAF:
				return isIsLeaf();
			case CorePackage.OPERATION__IS_ABSTRACT:
				return isIsAbstract();
			case CorePackage.OPERATION__SPECIFICATION:
				return getSpecification();
			case CorePackage.OPERATION__METHOD:
				return getMethod();
			case CorePackage.OPERATION__CALL_ACTION:
				return getCallAction();
			case CorePackage.OPERATION__OCCURRENCE:
				return getOccurrence();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CorePackage.OPERATION__CONCURRENCY:
				setConcurrency((CallConcurrencyKind)newValue);
				return;
			case CorePackage.OPERATION__IS_ROOT:
				setIsRoot((Boolean)newValue);
				return;
			case CorePackage.OPERATION__IS_LEAF:
				setIsLeaf((Boolean)newValue);
				return;
			case CorePackage.OPERATION__IS_ABSTRACT:
				setIsAbstract((Boolean)newValue);
				return;
			case CorePackage.OPERATION__SPECIFICATION:
				setSpecification((String)newValue);
				return;
			case CorePackage.OPERATION__METHOD:
				getMethod().clear();
				getMethod().addAll((Collection<? extends Method>)newValue);
				return;
			case CorePackage.OPERATION__CALL_ACTION:
				getCallAction().clear();
				getCallAction().addAll((Collection<? extends CallAction>)newValue);
				return;
			case CorePackage.OPERATION__OCCURRENCE:
				getOccurrence().clear();
				getOccurrence().addAll((Collection<? extends CallEvent>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CorePackage.OPERATION__CONCURRENCY:
				setConcurrency(CONCURRENCY_EDEFAULT);
				return;
			case CorePackage.OPERATION__IS_ROOT:
				setIsRoot(IS_ROOT_EDEFAULT);
				return;
			case CorePackage.OPERATION__IS_LEAF:
				setIsLeaf(IS_LEAF_EDEFAULT);
				return;
			case CorePackage.OPERATION__IS_ABSTRACT:
				setIsAbstract(IS_ABSTRACT_EDEFAULT);
				return;
			case CorePackage.OPERATION__SPECIFICATION:
				setSpecification(SPECIFICATION_EDEFAULT);
				return;
			case CorePackage.OPERATION__METHOD:
				getMethod().clear();
				return;
			case CorePackage.OPERATION__CALL_ACTION:
				getCallAction().clear();
				return;
			case CorePackage.OPERATION__OCCURRENCE:
				getOccurrence().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CorePackage.OPERATION__CONCURRENCY:
				return concurrency != CONCURRENCY_EDEFAULT;
			case CorePackage.OPERATION__IS_ROOT:
				return isRoot != IS_ROOT_EDEFAULT;
			case CorePackage.OPERATION__IS_LEAF:
				return isLeaf != IS_LEAF_EDEFAULT;
			case CorePackage.OPERATION__IS_ABSTRACT:
				return isAbstract != IS_ABSTRACT_EDEFAULT;
			case CorePackage.OPERATION__SPECIFICATION:
				return SPECIFICATION_EDEFAULT == null ? specification != null : !SPECIFICATION_EDEFAULT.equals(specification);
			case CorePackage.OPERATION__METHOD:
				return method != null && !method.isEmpty();
			case CorePackage.OPERATION__CALL_ACTION:
				return callAction != null && !callAction.isEmpty();
			case CorePackage.OPERATION__OCCURRENCE:
				return occurrence != null && !occurrence.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (concurrency: ");
		result.append(concurrency);
		result.append(", isRoot: ");
		result.append(isRoot);
		result.append(", isLeaf: ");
		result.append(isLeaf);
		result.append(", isAbstract: ");
		result.append(isAbstract);
		result.append(", specification: ");
		result.append(specification);
		result.append(')');
		return result.toString();
	}

} //OperationImpl
