/**
 * <copyright>
 * </copyright>
 *
 * $Id: TagDefinition.java,v 1.1 2012/04/23 09:31:28 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import net.jgsuess.uml14.foundation.data_types.Multiplicity;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tag Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.TagDefinition#getTagType <em>Tag Type</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.TagDefinition#getMultiplicity <em>Multiplicity</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.TagDefinition#getOwner <em>Owner</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.TagDefinition#getTypedValue <em>Typed Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getTagDefinition()
 * @model
 * @generated
 */
public interface TagDefinition extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Tag Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tag Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tag Type</em>' attribute.
	 * @see #setTagType(String)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getTagDefinition_TagType()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Name"
	 * @generated
	 */
	String getTagType();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.TagDefinition#getTagType <em>Tag Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tag Type</em>' attribute.
	 * @see #getTagType()
	 * @generated
	 */
	void setTagType(String value);

	/**
	 * Returns the value of the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multiplicity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiplicity</em>' containment reference.
	 * @see #setMultiplicity(Multiplicity)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getTagDefinition_Multiplicity()
	 * @model containment="true"
	 * @generated
	 */
	Multiplicity getMultiplicity();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.TagDefinition#getMultiplicity <em>Multiplicity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Multiplicity</em>' containment reference.
	 * @see #getMultiplicity()
	 * @generated
	 */
	void setMultiplicity(Multiplicity value);

	/**
	 * Returns the value of the '<em><b>Owner</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Stereotype#getDefinedTag <em>Defined Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owner</em>' container reference.
	 * @see #setOwner(Stereotype)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getTagDefinition_Owner()
	 * @see net.jgsuess.uml14.foundation.core.Stereotype#getDefinedTag
	 * @model opposite="definedTag"
	 * @generated
	 */
	Stereotype getOwner();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.TagDefinition#getOwner <em>Owner</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owner</em>' container reference.
	 * @see #getOwner()
	 * @generated
	 */
	void setOwner(Stereotype value);

	/**
	 * Returns the value of the '<em><b>Typed Value</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.TaggedValue}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.TaggedValue#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Typed Value</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Typed Value</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getTagDefinition_TypedValue()
	 * @see net.jgsuess.uml14.foundation.core.TaggedValue#getType
	 * @model opposite="type"
	 * @generated
	 */
	EList<TaggedValue> getTypedValue();

} // TagDefinition
