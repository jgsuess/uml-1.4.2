/**
 * <copyright>
 * </copyright>
 *
 * $Id: Artifact.java,v 1.1 2012/04/23 09:31:26 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Artifact</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Artifact#getImplementationLocation <em>Implementation Location</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getArtifact()
 * @model
 * @generated
 */
public interface Artifact extends Classifier {
	/**
	 * Returns the value of the '<em><b>Implementation Location</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Component}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Component#getImplementation <em>Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation Location</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation Location</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getArtifact_ImplementationLocation()
	 * @see net.jgsuess.uml14.foundation.core.Component#getImplementation
	 * @model opposite="implementation"
	 * @generated
	 */
	EList<Component> getImplementationLocation();

} // Artifact
