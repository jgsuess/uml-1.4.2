/**
 * <copyright>
 * </copyright>
 *
 * $Id: TemplateArgument.java,v 1.1 2012/04/23 09:31:26 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Argument</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.TemplateArgument#getBinding <em>Binding</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.TemplateArgument#getModelElement <em>Model Element</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getTemplateArgument()
 * @model
 * @generated
 */
public interface TemplateArgument extends EObject {
	/**
	 * Returns the value of the '<em><b>Binding</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Binding#getArgument <em>Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Binding</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binding</em>' container reference.
	 * @see #setBinding(Binding)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getTemplateArgument_Binding()
	 * @see net.jgsuess.uml14.foundation.core.Binding#getArgument
	 * @model opposite="argument" required="true"
	 * @generated
	 */
	Binding getBinding();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.TemplateArgument#getBinding <em>Binding</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Binding</em>' container reference.
	 * @see #getBinding()
	 * @generated
	 */
	void setBinding(Binding value);

	/**
	 * Returns the value of the '<em><b>Model Element</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.ModelElement#getTemplateArgument <em>Template Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Element</em>' reference.
	 * @see #setModelElement(ModelElement)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getTemplateArgument_ModelElement()
	 * @see net.jgsuess.uml14.foundation.core.ModelElement#getTemplateArgument
	 * @model opposite="templateArgument" required="true"
	 * @generated
	 */
	ModelElement getModelElement();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.TemplateArgument#getModelElement <em>Model Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Element</em>' reference.
	 * @see #getModelElement()
	 * @generated
	 */
	void setModelElement(ModelElement value);

} // TemplateArgument
