/**
 * <copyright>
 * </copyright>
 *
 * $Id: Primitive.java,v 1.1 2012/04/23 09:31:26 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Primitive</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getPrimitive()
 * @model
 * @generated
 */
public interface Primitive extends DataType {
} // Primitive
