/**
 * <copyright>
 * </copyright>
 *
 * $Id: GeneralizableElement.java,v 1.1 2012/04/23 09:31:28 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generalizable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.GeneralizableElement#isIsRoot <em>Is Root</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.GeneralizableElement#isIsLeaf <em>Is Leaf</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.GeneralizableElement#isIsAbstract <em>Is Abstract</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.GeneralizableElement#getGeneralization <em>Generalization</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.GeneralizableElement#getSpecialization <em>Specialization</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getGeneralizableElement()
 * @model abstract="true"
 * @generated
 */
public interface GeneralizableElement extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Is Root</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Root</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Root</em>' attribute.
	 * @see #setIsRoot(boolean)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getGeneralizableElement_IsRoot()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Boolean"
	 * @generated
	 */
	boolean isIsRoot();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.GeneralizableElement#isIsRoot <em>Is Root</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Root</em>' attribute.
	 * @see #isIsRoot()
	 * @generated
	 */
	void setIsRoot(boolean value);

	/**
	 * Returns the value of the '<em><b>Is Leaf</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Leaf</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Leaf</em>' attribute.
	 * @see #setIsLeaf(boolean)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getGeneralizableElement_IsLeaf()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Boolean"
	 * @generated
	 */
	boolean isIsLeaf();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.GeneralizableElement#isIsLeaf <em>Is Leaf</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Leaf</em>' attribute.
	 * @see #isIsLeaf()
	 * @generated
	 */
	void setIsLeaf(boolean value);

	/**
	 * Returns the value of the '<em><b>Is Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Abstract</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Abstract</em>' attribute.
	 * @see #setIsAbstract(boolean)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getGeneralizableElement_IsAbstract()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Boolean"
	 * @generated
	 */
	boolean isIsAbstract();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.GeneralizableElement#isIsAbstract <em>Is Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Abstract</em>' attribute.
	 * @see #isIsAbstract()
	 * @generated
	 */
	void setIsAbstract(boolean value);

	/**
	 * Returns the value of the '<em><b>Generalization</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Generalization}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Generalization#getChild <em>Child</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generalization</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generalization</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getGeneralizableElement_Generalization()
	 * @see net.jgsuess.uml14.foundation.core.Generalization#getChild
	 * @model opposite="child"
	 * @generated
	 */
	EList<Generalization> getGeneralization();

	/**
	 * Returns the value of the '<em><b>Specialization</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Generalization}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Generalization#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Specialization</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specialization</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getGeneralizableElement_Specialization()
	 * @see net.jgsuess.uml14.foundation.core.Generalization#getParent
	 * @model opposite="parent"
	 * @generated
	 */
	EList<Generalization> getSpecialization();

} // GeneralizableElement
