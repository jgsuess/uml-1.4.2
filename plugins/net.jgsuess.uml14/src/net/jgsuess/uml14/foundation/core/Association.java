/**
 * <copyright>
 * </copyright>
 *
 * $Id: Association.java,v 1.1 2012/04/23 09:31:26 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Link;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Association</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Association#getConnection <em>Connection</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Association#getLink <em>Link</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Association#getAssociationRole <em>Association Role</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAssociation()
 * @model
 * @generated
 */
public interface Association extends GeneralizableElement, Relationship {
	/**
	 * Returns the value of the '<em><b>Connection</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.AssociationEnd}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getAssociation <em>Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connection</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connection</em>' containment reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAssociation_Connection()
	 * @see net.jgsuess.uml14.foundation.core.AssociationEnd#getAssociation
	 * @model opposite="association" containment="true" lower="2"
	 * @generated
	 */
	EList<AssociationEnd> getConnection();

	/**
	 * Returns the value of the '<em><b>Link</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.Link}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Link#getAssociation <em>Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Link</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Link</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAssociation_Link()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Link#getAssociation
	 * @model opposite="association"
	 * @generated
	 */
	EList<Link> getLink();

	/**
	 * Returns the value of the '<em><b>Association Role</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Association Role</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Association Role</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAssociation_AssociationRole()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.AssociationRole#getBase
	 * @model opposite="base"
	 * @generated
	 */
	EList<AssociationRole> getAssociationRole();

} // Association
