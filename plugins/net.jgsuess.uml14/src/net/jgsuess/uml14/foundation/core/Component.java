/**
 * <copyright>
 * </copyright>
 *
 * $Id: Component.java,v 1.1 2012/04/23 09:31:26 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Component#getDeploymentLocation <em>Deployment Location</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Component#getResidentElement <em>Resident Element</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Component#getImplementation <em>Implementation</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getComponent()
 * @model
 * @generated
 */
public interface Component extends Classifier {
	/**
	 * Returns the value of the '<em><b>Deployment Location</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Node}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Node#getDeployedComponent <em>Deployed Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deployment Location</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deployment Location</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getComponent_DeploymentLocation()
	 * @see net.jgsuess.uml14.foundation.core.Node#getDeployedComponent
	 * @model opposite="deployedComponent"
	 * @generated
	 */
	EList<Node> getDeploymentLocation();

	/**
	 * Returns the value of the '<em><b>Resident Element</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.ElementResidence}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.ElementResidence#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resident Element</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resident Element</em>' containment reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getComponent_ResidentElement()
	 * @see net.jgsuess.uml14.foundation.core.ElementResidence#getContainer
	 * @model opposite="container" containment="true"
	 * @generated
	 */
	EList<ElementResidence> getResidentElement();

	/**
	 * Returns the value of the '<em><b>Implementation</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Artifact}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Artifact#getImplementationLocation <em>Implementation Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getComponent_Implementation()
	 * @see net.jgsuess.uml14.foundation.core.Artifact#getImplementationLocation
	 * @model opposite="implementationLocation"
	 * @generated
	 */
	EList<Artifact> getImplementation();

} // Component
