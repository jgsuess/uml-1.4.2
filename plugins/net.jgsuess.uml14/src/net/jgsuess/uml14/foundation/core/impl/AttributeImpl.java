/**
 * <copyright>
 * </copyright>
 *
 * $Id: AttributeImpl.java,v 1.1 2012/04/23 09:31:31 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage;

import net.jgsuess.uml14.behavioral_elements.common_behavior.AttributeLink;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;

import net.jgsuess.uml14.foundation.core.AssociationEnd;
import net.jgsuess.uml14.foundation.core.Attribute;
import net.jgsuess.uml14.foundation.core.CorePackage;

import net.jgsuess.uml14.foundation.data_types.Expression;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.AttributeImpl#getInitialValue <em>Initial Value</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.AttributeImpl#getAssociationEnd <em>Association End</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.AttributeImpl#getAttributeLink <em>Attribute Link</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.AttributeImpl#getAssociationEndRole <em>Association End Role</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AttributeImpl extends StructuralFeatureImpl implements Attribute {
	/**
	 * The cached value of the '{@link #getInitialValue() <em>Initial Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialValue()
	 * @generated
	 * @ordered
	 */
	protected Expression initialValue;

	/**
	 * The cached value of the '{@link #getAttributeLink() <em>Attribute Link</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributeLink()
	 * @generated
	 * @ordered
	 */
	protected EList<AttributeLink> attributeLink;

	/**
	 * The cached value of the '{@link #getAssociationEndRole() <em>Association End Role</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociationEndRole()
	 * @generated
	 * @ordered
	 */
	protected EList<AssociationEndRole> associationEndRole;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.ATTRIBUTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getInitialValue() {
		return initialValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInitialValue(Expression newInitialValue, NotificationChain msgs) {
		Expression oldInitialValue = initialValue;
		initialValue = newInitialValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.ATTRIBUTE__INITIAL_VALUE, oldInitialValue, newInitialValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialValue(Expression newInitialValue) {
		if (newInitialValue != initialValue) {
			NotificationChain msgs = null;
			if (initialValue != null)
				msgs = ((InternalEObject)initialValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.ATTRIBUTE__INITIAL_VALUE, null, msgs);
			if (newInitialValue != null)
				msgs = ((InternalEObject)newInitialValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.ATTRIBUTE__INITIAL_VALUE, null, msgs);
			msgs = basicSetInitialValue(newInitialValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.ATTRIBUTE__INITIAL_VALUE, newInitialValue, newInitialValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationEnd getAssociationEnd() {
		if (eContainerFeatureID() != CorePackage.ATTRIBUTE__ASSOCIATION_END) return null;
		return (AssociationEnd)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssociationEnd(AssociationEnd newAssociationEnd, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newAssociationEnd, CorePackage.ATTRIBUTE__ASSOCIATION_END, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssociationEnd(AssociationEnd newAssociationEnd) {
		if (newAssociationEnd != eInternalContainer() || (eContainerFeatureID() != CorePackage.ATTRIBUTE__ASSOCIATION_END && newAssociationEnd != null)) {
			if (EcoreUtil.isAncestor(this, newAssociationEnd))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newAssociationEnd != null)
				msgs = ((InternalEObject)newAssociationEnd).eInverseAdd(this, CorePackage.ASSOCIATION_END__QUALIFIER, AssociationEnd.class, msgs);
			msgs = basicSetAssociationEnd(newAssociationEnd, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.ATTRIBUTE__ASSOCIATION_END, newAssociationEnd, newAssociationEnd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttributeLink> getAttributeLink() {
		if (attributeLink == null) {
			attributeLink = new EObjectWithInverseResolvingEList<AttributeLink>(AttributeLink.class, this, CorePackage.ATTRIBUTE__ATTRIBUTE_LINK, Common_behaviorPackage.ATTRIBUTE_LINK__ATTRIBUTE);
		}
		return attributeLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssociationEndRole> getAssociationEndRole() {
		if (associationEndRole == null) {
			associationEndRole = new EObjectWithInverseResolvingEList.ManyInverse<AssociationEndRole>(AssociationEndRole.class, this, CorePackage.ATTRIBUTE__ASSOCIATION_END_ROLE, CollaborationsPackage.ASSOCIATION_END_ROLE__AVAILABLE_QUALIFIER);
		}
		return associationEndRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.ATTRIBUTE__ASSOCIATION_END:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetAssociationEnd((AssociationEnd)otherEnd, msgs);
			case CorePackage.ATTRIBUTE__ATTRIBUTE_LINK:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAttributeLink()).basicAdd(otherEnd, msgs);
			case CorePackage.ATTRIBUTE__ASSOCIATION_END_ROLE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAssociationEndRole()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.ATTRIBUTE__INITIAL_VALUE:
				return basicSetInitialValue(null, msgs);
			case CorePackage.ATTRIBUTE__ASSOCIATION_END:
				return basicSetAssociationEnd(null, msgs);
			case CorePackage.ATTRIBUTE__ATTRIBUTE_LINK:
				return ((InternalEList<?>)getAttributeLink()).basicRemove(otherEnd, msgs);
			case CorePackage.ATTRIBUTE__ASSOCIATION_END_ROLE:
				return ((InternalEList<?>)getAssociationEndRole()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CorePackage.ATTRIBUTE__ASSOCIATION_END:
				return eInternalContainer().eInverseRemove(this, CorePackage.ASSOCIATION_END__QUALIFIER, AssociationEnd.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CorePackage.ATTRIBUTE__INITIAL_VALUE:
				return getInitialValue();
			case CorePackage.ATTRIBUTE__ASSOCIATION_END:
				return getAssociationEnd();
			case CorePackage.ATTRIBUTE__ATTRIBUTE_LINK:
				return getAttributeLink();
			case CorePackage.ATTRIBUTE__ASSOCIATION_END_ROLE:
				return getAssociationEndRole();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CorePackage.ATTRIBUTE__INITIAL_VALUE:
				setInitialValue((Expression)newValue);
				return;
			case CorePackage.ATTRIBUTE__ASSOCIATION_END:
				setAssociationEnd((AssociationEnd)newValue);
				return;
			case CorePackage.ATTRIBUTE__ATTRIBUTE_LINK:
				getAttributeLink().clear();
				getAttributeLink().addAll((Collection<? extends AttributeLink>)newValue);
				return;
			case CorePackage.ATTRIBUTE__ASSOCIATION_END_ROLE:
				getAssociationEndRole().clear();
				getAssociationEndRole().addAll((Collection<? extends AssociationEndRole>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CorePackage.ATTRIBUTE__INITIAL_VALUE:
				setInitialValue((Expression)null);
				return;
			case CorePackage.ATTRIBUTE__ASSOCIATION_END:
				setAssociationEnd((AssociationEnd)null);
				return;
			case CorePackage.ATTRIBUTE__ATTRIBUTE_LINK:
				getAttributeLink().clear();
				return;
			case CorePackage.ATTRIBUTE__ASSOCIATION_END_ROLE:
				getAssociationEndRole().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CorePackage.ATTRIBUTE__INITIAL_VALUE:
				return initialValue != null;
			case CorePackage.ATTRIBUTE__ASSOCIATION_END:
				return getAssociationEnd() != null;
			case CorePackage.ATTRIBUTE__ATTRIBUTE_LINK:
				return attributeLink != null && !attributeLink.isEmpty();
			case CorePackage.ATTRIBUTE__ASSOCIATION_END_ROLE:
				return associationEndRole != null && !associationEndRole.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AttributeImpl
