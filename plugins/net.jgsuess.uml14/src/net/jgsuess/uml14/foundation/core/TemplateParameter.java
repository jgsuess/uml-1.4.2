/**
 * <copyright>
 * </copyright>
 *
 * $Id: TemplateParameter.java,v 1.1 2012/04/23 09:31:26 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.TemplateParameter#getDefaultElement <em>Default Element</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.TemplateParameter#getTemplate <em>Template</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.TemplateParameter#getParameter <em>Parameter</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getTemplateParameter()
 * @model
 * @generated
 */
public interface TemplateParameter extends EObject {
	/**
	 * Returns the value of the '<em><b>Default Element</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.ModelElement#getDefaultedParameter <em>Defaulted Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Element</em>' reference.
	 * @see #setDefaultElement(ModelElement)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getTemplateParameter_DefaultElement()
	 * @see net.jgsuess.uml14.foundation.core.ModelElement#getDefaultedParameter
	 * @model opposite="defaultedParameter"
	 * @generated
	 */
	ModelElement getDefaultElement();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.TemplateParameter#getDefaultElement <em>Default Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Element</em>' reference.
	 * @see #getDefaultElement()
	 * @generated
	 */
	void setDefaultElement(ModelElement value);

	/**
	 * Returns the value of the '<em><b>Template</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.ModelElement#getTemplateParameter <em>Template Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Template</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Template</em>' container reference.
	 * @see #setTemplate(ModelElement)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getTemplateParameter_Template()
	 * @see net.jgsuess.uml14.foundation.core.ModelElement#getTemplateParameter
	 * @model opposite="templateParameter" required="true"
	 * @generated
	 */
	ModelElement getTemplate();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.TemplateParameter#getTemplate <em>Template</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Template</em>' container reference.
	 * @see #getTemplate()
	 * @generated
	 */
	void setTemplate(ModelElement value);

	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.ModelElement#getParameterTemplate <em>Parameter Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter</em>' containment reference.
	 * @see #setParameter(ModelElement)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getTemplateParameter_Parameter()
	 * @see net.jgsuess.uml14.foundation.core.ModelElement#getParameterTemplate
	 * @model opposite="parameterTemplate" containment="true" required="true"
	 * @generated
	 */
	ModelElement getParameter();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.TemplateParameter#getParameter <em>Parameter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter</em>' containment reference.
	 * @see #getParameter()
	 * @generated
	 */
	void setParameter(ModelElement value);

} // TemplateParameter
