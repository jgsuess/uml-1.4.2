/**
 * <copyright>
 * </copyright>
 *
 * $Id: ModelElement.java,v 1.1 2012/04/23 09:31:28 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.Partition;

import net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole;
import net.jgsuess.uml14.behavioral_elements.collaborations.Collaboration;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationInstanceSet;

import net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine;

import net.jgsuess.uml14.foundation.data_types.VisibilityKind;

import net.jgsuess.uml14.model_management.ElementImport;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getName <em>Name</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#isIsSpecification <em>Is Specification</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getNamespace <em>Namespace</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getClientDependency <em>Client Dependency</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getConstraint <em>Constraint</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getSupplierDependency <em>Supplier Dependency</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getPresentation <em>Presentation</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getTargetFlow <em>Target Flow</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getSourceFlow <em>Source Flow</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getDefaultedParameter <em>Defaulted Parameter</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getComment <em>Comment</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getElementResidence <em>Element Residence</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getTemplateParameter <em>Template Parameter</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getParameterTemplate <em>Parameter Template</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getStereotype <em>Stereotype</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getTaggedValue <em>Tagged Value</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getReferenceTag <em>Reference Tag</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getTemplateArgument <em>Template Argument</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getBehavior <em>Behavior</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getClassifierRole <em>Classifier Role</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getCollaboration <em>Collaboration</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getCollaborationInstanceSet <em>Collaboration Instance Set</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getPartition <em>Partition</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ModelElement#getElementImport <em>Element Import</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement()
 * @model abstract="true"
 * @generated
 */
public interface ModelElement extends Element {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_Name()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Name"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.ModelElement#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Visibility</b></em>' attribute.
	 * The literals are from the enumeration {@link net.jgsuess.uml14.foundation.data_types.VisibilityKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visibility</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visibility</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.VisibilityKind
	 * @see #setVisibility(VisibilityKind)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_Visibility()
	 * @model
	 * @generated
	 */
	VisibilityKind getVisibility();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.ModelElement#getVisibility <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visibility</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.VisibilityKind
	 * @see #getVisibility()
	 * @generated
	 */
	void setVisibility(VisibilityKind value);

	/**
	 * Returns the value of the '<em><b>Is Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Specification</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Specification</em>' attribute.
	 * @see #setIsSpecification(boolean)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_IsSpecification()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Boolean"
	 * @generated
	 */
	boolean isIsSpecification();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.ModelElement#isIsSpecification <em>Is Specification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Specification</em>' attribute.
	 * @see #isIsSpecification()
	 * @generated
	 */
	void setIsSpecification(boolean value);

	/**
	 * Returns the value of the '<em><b>Namespace</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Namespace#getOwnedElement <em>Owned Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Namespace</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Namespace</em>' container reference.
	 * @see #setNamespace(Namespace)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_Namespace()
	 * @see net.jgsuess.uml14.foundation.core.Namespace#getOwnedElement
	 * @model opposite="ownedElement"
	 * @generated
	 */
	Namespace getNamespace();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.ModelElement#getNamespace <em>Namespace</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Namespace</em>' container reference.
	 * @see #getNamespace()
	 * @generated
	 */
	void setNamespace(Namespace value);

	/**
	 * Returns the value of the '<em><b>Client Dependency</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Dependency}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Dependency#getClient <em>Client</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Client Dependency</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Client Dependency</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_ClientDependency()
	 * @see net.jgsuess.uml14.foundation.core.Dependency#getClient
	 * @model opposite="client"
	 * @generated
	 */
	EList<Dependency> getClientDependency();

	/**
	 * Returns the value of the '<em><b>Constraint</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Constraint}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Constraint#getConstrainedElement <em>Constrained Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraint</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraint</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_Constraint()
	 * @see net.jgsuess.uml14.foundation.core.Constraint#getConstrainedElement
	 * @model opposite="constrainedElement"
	 * @generated
	 */
	EList<Constraint> getConstraint();

	/**
	 * Returns the value of the '<em><b>Supplier Dependency</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Dependency}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Dependency#getSupplier <em>Supplier</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Supplier Dependency</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Supplier Dependency</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_SupplierDependency()
	 * @see net.jgsuess.uml14.foundation.core.Dependency#getSupplier
	 * @model opposite="supplier"
	 * @generated
	 */
	EList<Dependency> getSupplierDependency();

	/**
	 * Returns the value of the '<em><b>Presentation</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.PresentationElement}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.PresentationElement#getSubject <em>Subject</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Presentation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Presentation</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_Presentation()
	 * @see net.jgsuess.uml14.foundation.core.PresentationElement#getSubject
	 * @model opposite="subject"
	 * @generated
	 */
	EList<PresentationElement> getPresentation();

	/**
	 * Returns the value of the '<em><b>Target Flow</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Flow}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Flow#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Flow</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Flow</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_TargetFlow()
	 * @see net.jgsuess.uml14.foundation.core.Flow#getTarget
	 * @model opposite="target"
	 * @generated
	 */
	EList<Flow> getTargetFlow();

	/**
	 * Returns the value of the '<em><b>Source Flow</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Flow}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Flow#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Flow</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Flow</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_SourceFlow()
	 * @see net.jgsuess.uml14.foundation.core.Flow#getSource
	 * @model opposite="source"
	 * @generated
	 */
	EList<Flow> getSourceFlow();

	/**
	 * Returns the value of the '<em><b>Defaulted Parameter</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.TemplateParameter}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.TemplateParameter#getDefaultElement <em>Default Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defaulted Parameter</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defaulted Parameter</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_DefaultedParameter()
	 * @see net.jgsuess.uml14.foundation.core.TemplateParameter#getDefaultElement
	 * @model opposite="defaultElement"
	 * @generated
	 */
	EList<TemplateParameter> getDefaultedParameter();

	/**
	 * Returns the value of the '<em><b>Comment</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Comment}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Comment#getAnnotatedElement <em>Annotated Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comment</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comment</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_Comment()
	 * @see net.jgsuess.uml14.foundation.core.Comment#getAnnotatedElement
	 * @model opposite="annotatedElement"
	 * @generated
	 */
	EList<Comment> getComment();

	/**
	 * Returns the value of the '<em><b>Element Residence</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.ElementResidence}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.ElementResidence#getResident <em>Resident</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element Residence</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Residence</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_ElementResidence()
	 * @see net.jgsuess.uml14.foundation.core.ElementResidence#getResident
	 * @model opposite="resident"
	 * @generated
	 */
	EList<ElementResidence> getElementResidence();

	/**
	 * Returns the value of the '<em><b>Template Parameter</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.TemplateParameter}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.TemplateParameter#getTemplate <em>Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Template Parameter</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Template Parameter</em>' containment reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_TemplateParameter()
	 * @see net.jgsuess.uml14.foundation.core.TemplateParameter#getTemplate
	 * @model opposite="template" containment="true"
	 * @generated
	 */
	EList<TemplateParameter> getTemplateParameter();

	/**
	 * Returns the value of the '<em><b>Parameter Template</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.TemplateParameter#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Template</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Template</em>' container reference.
	 * @see #setParameterTemplate(TemplateParameter)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_ParameterTemplate()
	 * @see net.jgsuess.uml14.foundation.core.TemplateParameter#getParameter
	 * @model opposite="parameter"
	 * @generated
	 */
	TemplateParameter getParameterTemplate();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.ModelElement#getParameterTemplate <em>Parameter Template</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Template</em>' container reference.
	 * @see #getParameterTemplate()
	 * @generated
	 */
	void setParameterTemplate(TemplateParameter value);

	/**
	 * Returns the value of the '<em><b>Stereotype</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Stereotype}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Stereotype#getExtendedElement <em>Extended Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stereotype</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stereotype</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_Stereotype()
	 * @see net.jgsuess.uml14.foundation.core.Stereotype#getExtendedElement
	 * @model opposite="extendedElement"
	 * @generated
	 */
	EList<Stereotype> getStereotype();

	/**
	 * Returns the value of the '<em><b>Tagged Value</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.TaggedValue}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.TaggedValue#getModelElement <em>Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tagged Value</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tagged Value</em>' containment reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_TaggedValue()
	 * @see net.jgsuess.uml14.foundation.core.TaggedValue#getModelElement
	 * @model opposite="modelElement" containment="true"
	 * @generated
	 */
	EList<TaggedValue> getTaggedValue();

	/**
	 * Returns the value of the '<em><b>Reference Tag</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.TaggedValue}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.TaggedValue#getReferenceValue <em>Reference Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference Tag</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference Tag</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_ReferenceTag()
	 * @see net.jgsuess.uml14.foundation.core.TaggedValue#getReferenceValue
	 * @model opposite="referenceValue"
	 * @generated
	 */
	EList<TaggedValue> getReferenceTag();

	/**
	 * Returns the value of the '<em><b>Template Argument</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.TemplateArgument}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.TemplateArgument#getModelElement <em>Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Template Argument</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Template Argument</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_TemplateArgument()
	 * @see net.jgsuess.uml14.foundation.core.TemplateArgument#getModelElement
	 * @model opposite="modelElement"
	 * @generated
	 */
	EList<TemplateArgument> getTemplateArgument();

	/**
	 * Returns the value of the '<em><b>Behavior</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine#getContext <em>Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Behavior</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Behavior</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_Behavior()
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine#getContext
	 * @model opposite="context"
	 * @generated
	 */
	EList<StateMachine> getBehavior();

	/**
	 * Returns the value of the '<em><b>Classifier Role</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole#getAvailableContents <em>Available Contents</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classifier Role</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classifier Role</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_ClassifierRole()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole#getAvailableContents
	 * @model opposite="availableContents"
	 * @generated
	 */
	EList<ClassifierRole> getClassifierRole();

	/**
	 * Returns the value of the '<em><b>Collaboration</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.collaborations.Collaboration}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.Collaboration#getConstrainingElement <em>Constraining Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collaboration</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collaboration</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_Collaboration()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.Collaboration#getConstrainingElement
	 * @model opposite="constrainingElement"
	 * @generated
	 */
	EList<Collaboration> getCollaboration();

	/**
	 * Returns the value of the '<em><b>Collaboration Instance Set</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationInstanceSet}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationInstanceSet#getConstrainingElement <em>Constraining Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collaboration Instance Set</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collaboration Instance Set</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_CollaborationInstanceSet()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationInstanceSet#getConstrainingElement
	 * @model opposite="constrainingElement"
	 * @generated
	 */
	EList<CollaborationInstanceSet> getCollaborationInstanceSet();

	/**
	 * Returns the value of the '<em><b>Partition</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.activity_graphs.Partition}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.Partition#getContents <em>Contents</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partition</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partition</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_Partition()
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.Partition#getContents
	 * @model opposite="contents"
	 * @generated
	 */
	EList<Partition> getPartition();

	/**
	 * Returns the value of the '<em><b>Element Import</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.model_management.ElementImport}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.model_management.ElementImport#getImportedElement <em>Imported Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element Import</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Import</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getModelElement_ElementImport()
	 * @see net.jgsuess.uml14.model_management.ElementImport#getImportedElement
	 * @model opposite="importedElement"
	 * @generated
	 */
	EList<ElementImport> getElementImport();

} // ModelElement
