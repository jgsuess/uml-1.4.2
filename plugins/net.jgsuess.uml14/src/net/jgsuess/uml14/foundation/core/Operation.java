/**
 * <copyright>
 * </copyright>
 *
 * $Id: Operation.java,v 1.1 2012/04/23 09:31:28 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import net.jgsuess.uml14.behavioral_elements.common_behavior.CallAction;

import net.jgsuess.uml14.behavioral_elements.state_machines.CallEvent;

import net.jgsuess.uml14.foundation.data_types.CallConcurrencyKind;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Operation#getConcurrency <em>Concurrency</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Operation#isIsRoot <em>Is Root</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Operation#isIsLeaf <em>Is Leaf</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Operation#isIsAbstract <em>Is Abstract</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Operation#getSpecification <em>Specification</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Operation#getMethod <em>Method</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Operation#getCallAction <em>Call Action</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Operation#getOccurrence <em>Occurrence</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getOperation()
 * @model
 * @generated
 */
public interface Operation extends BehavioralFeature {
	/**
	 * Returns the value of the '<em><b>Concurrency</b></em>' attribute.
	 * The literals are from the enumeration {@link net.jgsuess.uml14.foundation.data_types.CallConcurrencyKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Concurrency</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Concurrency</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.CallConcurrencyKind
	 * @see #setConcurrency(CallConcurrencyKind)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getOperation_Concurrency()
	 * @model
	 * @generated
	 */
	CallConcurrencyKind getConcurrency();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Operation#getConcurrency <em>Concurrency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Concurrency</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.CallConcurrencyKind
	 * @see #getConcurrency()
	 * @generated
	 */
	void setConcurrency(CallConcurrencyKind value);

	/**
	 * Returns the value of the '<em><b>Is Root</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Root</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Root</em>' attribute.
	 * @see #setIsRoot(boolean)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getOperation_IsRoot()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Boolean"
	 * @generated
	 */
	boolean isIsRoot();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Operation#isIsRoot <em>Is Root</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Root</em>' attribute.
	 * @see #isIsRoot()
	 * @generated
	 */
	void setIsRoot(boolean value);

	/**
	 * Returns the value of the '<em><b>Is Leaf</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Leaf</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Leaf</em>' attribute.
	 * @see #setIsLeaf(boolean)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getOperation_IsLeaf()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Boolean"
	 * @generated
	 */
	boolean isIsLeaf();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Operation#isIsLeaf <em>Is Leaf</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Leaf</em>' attribute.
	 * @see #isIsLeaf()
	 * @generated
	 */
	void setIsLeaf(boolean value);

	/**
	 * Returns the value of the '<em><b>Is Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Abstract</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Abstract</em>' attribute.
	 * @see #setIsAbstract(boolean)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getOperation_IsAbstract()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Boolean"
	 * @generated
	 */
	boolean isIsAbstract();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Operation#isIsAbstract <em>Is Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Abstract</em>' attribute.
	 * @see #isIsAbstract()
	 * @generated
	 */
	void setIsAbstract(boolean value);

	/**
	 * Returns the value of the '<em><b>Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Specification</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specification</em>' attribute.
	 * @see #setSpecification(String)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getOperation_Specification()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.String"
	 * @generated
	 */
	String getSpecification();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Operation#getSpecification <em>Specification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specification</em>' attribute.
	 * @see #getSpecification()
	 * @generated
	 */
	void setSpecification(String value);

	/**
	 * Returns the value of the '<em><b>Method</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Method}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Method#getSpecification <em>Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Method</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Method</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getOperation_Method()
	 * @see net.jgsuess.uml14.foundation.core.Method#getSpecification
	 * @model opposite="specification"
	 * @generated
	 */
	EList<Method> getMethod();

	/**
	 * Returns the value of the '<em><b>Call Action</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.CallAction}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.CallAction#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Call Action</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Call Action</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getOperation_CallAction()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.CallAction#getOperation
	 * @model opposite="operation"
	 * @generated
	 */
	EList<CallAction> getCallAction();

	/**
	 * Returns the value of the '<em><b>Occurrence</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.state_machines.CallEvent}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.state_machines.CallEvent#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Occurrence</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Occurrence</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getOperation_Occurrence()
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.CallEvent#getOperation
	 * @model opposite="operation"
	 * @generated
	 */
	EList<CallEvent> getOccurrence();

} // Operation
