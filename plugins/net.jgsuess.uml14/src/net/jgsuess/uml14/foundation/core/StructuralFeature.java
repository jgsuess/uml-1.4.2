/**
 * <copyright>
 * </copyright>
 *
 * $Id: StructuralFeature.java,v 1.1 2012/04/23 09:31:28 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import net.jgsuess.uml14.foundation.data_types.ChangeableKind;
import net.jgsuess.uml14.foundation.data_types.Multiplicity;
import net.jgsuess.uml14.foundation.data_types.OrderingKind;
import net.jgsuess.uml14.foundation.data_types.ScopeKind;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Structural Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.StructuralFeature#getMultiplicity <em>Multiplicity</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.StructuralFeature#getChangeability <em>Changeability</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.StructuralFeature#getTargetScope <em>Target Scope</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.StructuralFeature#getOrdering <em>Ordering</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.StructuralFeature#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getStructuralFeature()
 * @model abstract="true"
 * @generated
 */
public interface StructuralFeature extends Feature {
	/**
	 * Returns the value of the '<em><b>Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multiplicity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiplicity</em>' containment reference.
	 * @see #setMultiplicity(Multiplicity)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getStructuralFeature_Multiplicity()
	 * @model containment="true"
	 * @generated
	 */
	Multiplicity getMultiplicity();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.StructuralFeature#getMultiplicity <em>Multiplicity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Multiplicity</em>' containment reference.
	 * @see #getMultiplicity()
	 * @generated
	 */
	void setMultiplicity(Multiplicity value);

	/**
	 * Returns the value of the '<em><b>Changeability</b></em>' attribute.
	 * The literals are from the enumeration {@link net.jgsuess.uml14.foundation.data_types.ChangeableKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Changeability</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Changeability</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.ChangeableKind
	 * @see #setChangeability(ChangeableKind)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getStructuralFeature_Changeability()
	 * @model
	 * @generated
	 */
	ChangeableKind getChangeability();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.StructuralFeature#getChangeability <em>Changeability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Changeability</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.ChangeableKind
	 * @see #getChangeability()
	 * @generated
	 */
	void setChangeability(ChangeableKind value);

	/**
	 * Returns the value of the '<em><b>Target Scope</b></em>' attribute.
	 * The literals are from the enumeration {@link net.jgsuess.uml14.foundation.data_types.ScopeKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Scope</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Scope</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.ScopeKind
	 * @see #setTargetScope(ScopeKind)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getStructuralFeature_TargetScope()
	 * @model
	 * @generated
	 */
	ScopeKind getTargetScope();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.StructuralFeature#getTargetScope <em>Target Scope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Scope</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.ScopeKind
	 * @see #getTargetScope()
	 * @generated
	 */
	void setTargetScope(ScopeKind value);

	/**
	 * Returns the value of the '<em><b>Ordering</b></em>' attribute.
	 * The literals are from the enumeration {@link net.jgsuess.uml14.foundation.data_types.OrderingKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ordering</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ordering</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.OrderingKind
	 * @see #setOrdering(OrderingKind)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getStructuralFeature_Ordering()
	 * @model
	 * @generated
	 */
	OrderingKind getOrdering();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.StructuralFeature#getOrdering <em>Ordering</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ordering</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.OrderingKind
	 * @see #getOrdering()
	 * @generated
	 */
	void setOrdering(OrderingKind value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Classifier#getTypedFeature <em>Typed Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(Classifier)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getStructuralFeature_Type()
	 * @see net.jgsuess.uml14.foundation.core.Classifier#getTypedFeature
	 * @model opposite="typedFeature" required="true"
	 * @generated
	 */
	Classifier getType();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.StructuralFeature#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Classifier value);

} // StructuralFeature
