/**
 * <copyright>
 * </copyright>
 *
 * $Id: ProgrammingLanguageDataType.java,v 1.1 2012/04/23 09:31:27 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import net.jgsuess.uml14.foundation.data_types.TypeExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Programming Language Data Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.ProgrammingLanguageDataType#getExpression <em>Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getProgrammingLanguageDataType()
 * @model
 * @generated
 */
public interface ProgrammingLanguageDataType extends DataType {
	/**
	 * Returns the value of the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression</em>' containment reference.
	 * @see #setExpression(TypeExpression)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getProgrammingLanguageDataType_Expression()
	 * @model containment="true"
	 * @generated
	 */
	TypeExpression getExpression();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.ProgrammingLanguageDataType#getExpression <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression</em>' containment reference.
	 * @see #getExpression()
	 * @generated
	 */
	void setExpression(TypeExpression value);

} // ProgrammingLanguageDataType
