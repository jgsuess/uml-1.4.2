/**
 * <copyright>
 * </copyright>
 *
 * $Id: Method.java,v 1.1 2012/04/23 09:31:28 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import net.jgsuess.uml14.foundation.data_types.ProcedureExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Method</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Method#getBody <em>Body</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Method#getSpecification <em>Specification</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getMethod()
 * @model
 * @generated
 */
public interface Method extends BehavioralFeature {
	/**
	 * Returns the value of the '<em><b>Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' containment reference.
	 * @see #setBody(ProcedureExpression)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getMethod_Body()
	 * @model containment="true"
	 * @generated
	 */
	ProcedureExpression getBody();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Method#getBody <em>Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body</em>' containment reference.
	 * @see #getBody()
	 * @generated
	 */
	void setBody(ProcedureExpression value);

	/**
	 * Returns the value of the '<em><b>Specification</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Operation#getMethod <em>Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Specification</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specification</em>' reference.
	 * @see #setSpecification(Operation)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getMethod_Specification()
	 * @see net.jgsuess.uml14.foundation.core.Operation#getMethod
	 * @model opposite="method" required="true"
	 * @generated
	 */
	Operation getSpecification();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Method#getSpecification <em>Specification</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specification</em>' reference.
	 * @see #getSpecification()
	 * @generated
	 */
	void setSpecification(Operation value);

} // Method
