/**
 * <copyright>
 * </copyright>
 *
 * $Id: ComponentImpl.java,v 1.1 2012/04/23 09:31:33 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.impl;

import java.util.Collection;

import net.jgsuess.uml14.foundation.core.Artifact;
import net.jgsuess.uml14.foundation.core.Component;
import net.jgsuess.uml14.foundation.core.CorePackage;
import net.jgsuess.uml14.foundation.core.ElementResidence;
import net.jgsuess.uml14.foundation.core.Node;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ComponentImpl#getDeploymentLocation <em>Deployment Location</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ComponentImpl#getResidentElement <em>Resident Element</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ComponentImpl#getImplementation <em>Implementation</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ComponentImpl extends ClassifierImpl implements Component {
	/**
	 * The cached value of the '{@link #getDeploymentLocation() <em>Deployment Location</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeploymentLocation()
	 * @generated
	 * @ordered
	 */
	protected EList<Node> deploymentLocation;

	/**
	 * The cached value of the '{@link #getResidentElement() <em>Resident Element</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResidentElement()
	 * @generated
	 * @ordered
	 */
	protected EList<ElementResidence> residentElement;

	/**
	 * The cached value of the '{@link #getImplementation() <em>Implementation</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplementation()
	 * @generated
	 * @ordered
	 */
	protected EList<Artifact> implementation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.COMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Node> getDeploymentLocation() {
		if (deploymentLocation == null) {
			deploymentLocation = new EObjectWithInverseResolvingEList.ManyInverse<Node>(Node.class, this, CorePackage.COMPONENT__DEPLOYMENT_LOCATION, CorePackage.NODE__DEPLOYED_COMPONENT);
		}
		return deploymentLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElementResidence> getResidentElement() {
		if (residentElement == null) {
			residentElement = new EObjectContainmentWithInverseEList<ElementResidence>(ElementResidence.class, this, CorePackage.COMPONENT__RESIDENT_ELEMENT, CorePackage.ELEMENT_RESIDENCE__CONTAINER);
		}
		return residentElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Artifact> getImplementation() {
		if (implementation == null) {
			implementation = new EObjectWithInverseResolvingEList.ManyInverse<Artifact>(Artifact.class, this, CorePackage.COMPONENT__IMPLEMENTATION, CorePackage.ARTIFACT__IMPLEMENTATION_LOCATION);
		}
		return implementation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.COMPONENT__DEPLOYMENT_LOCATION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getDeploymentLocation()).basicAdd(otherEnd, msgs);
			case CorePackage.COMPONENT__RESIDENT_ELEMENT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getResidentElement()).basicAdd(otherEnd, msgs);
			case CorePackage.COMPONENT__IMPLEMENTATION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getImplementation()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.COMPONENT__DEPLOYMENT_LOCATION:
				return ((InternalEList<?>)getDeploymentLocation()).basicRemove(otherEnd, msgs);
			case CorePackage.COMPONENT__RESIDENT_ELEMENT:
				return ((InternalEList<?>)getResidentElement()).basicRemove(otherEnd, msgs);
			case CorePackage.COMPONENT__IMPLEMENTATION:
				return ((InternalEList<?>)getImplementation()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CorePackage.COMPONENT__DEPLOYMENT_LOCATION:
				return getDeploymentLocation();
			case CorePackage.COMPONENT__RESIDENT_ELEMENT:
				return getResidentElement();
			case CorePackage.COMPONENT__IMPLEMENTATION:
				return getImplementation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CorePackage.COMPONENT__DEPLOYMENT_LOCATION:
				getDeploymentLocation().clear();
				getDeploymentLocation().addAll((Collection<? extends Node>)newValue);
				return;
			case CorePackage.COMPONENT__RESIDENT_ELEMENT:
				getResidentElement().clear();
				getResidentElement().addAll((Collection<? extends ElementResidence>)newValue);
				return;
			case CorePackage.COMPONENT__IMPLEMENTATION:
				getImplementation().clear();
				getImplementation().addAll((Collection<? extends Artifact>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CorePackage.COMPONENT__DEPLOYMENT_LOCATION:
				getDeploymentLocation().clear();
				return;
			case CorePackage.COMPONENT__RESIDENT_ELEMENT:
				getResidentElement().clear();
				return;
			case CorePackage.COMPONENT__IMPLEMENTATION:
				getImplementation().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CorePackage.COMPONENT__DEPLOYMENT_LOCATION:
				return deploymentLocation != null && !deploymentLocation.isEmpty();
			case CorePackage.COMPONENT__RESIDENT_ELEMENT:
				return residentElement != null && !residentElement.isEmpty();
			case CorePackage.COMPONENT__IMPLEMENTATION:
				return implementation != null && !implementation.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ComponentImpl
