/**
 * <copyright>
 * </copyright>
 *
 * $Id: Interface.java,v 1.1 2012/04/23 09:31:26 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getInterface()
 * @model
 * @generated
 */
public interface Interface extends Classifier {
} // Interface
