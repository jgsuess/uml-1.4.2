/**
 * <copyright>
 * </copyright>
 *
 * $Id: Binding.java,v 1.1 2012/04/23 09:31:26 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binding</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Binding#getArgument <em>Argument</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getBinding()
 * @model
 * @generated
 */
public interface Binding extends Dependency {
	/**
	 * Returns the value of the '<em><b>Argument</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.TemplateArgument}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.TemplateArgument#getBinding <em>Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Argument</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Argument</em>' containment reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getBinding_Argument()
	 * @see net.jgsuess.uml14.foundation.core.TemplateArgument#getBinding
	 * @model opposite="binding" containment="true" required="true"
	 * @generated
	 */
	EList<TemplateArgument> getArgument();

} // Binding
