/**
 * <copyright>
 * </copyright>
 *
 * $Id: Classifier.java,v 1.1 2012/04/23 09:31:28 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState;
import net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState;

import net.jgsuess.uml14.behavioral_elements.common_behavior.CreateAction;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Instance;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Classifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Classifier#getFeature <em>Feature</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Classifier#getTypedFeature <em>Typed Feature</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Classifier#getTypedParameter <em>Typed Parameter</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Classifier#getAssociation <em>Association</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Classifier#getSpecifiedEnd <em>Specified End</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Classifier#getPowertypeRange <em>Powertype Range</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Classifier#getInstance <em>Instance</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Classifier#getCreateAction <em>Create Action</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Classifier#getClassifierInState <em>Classifier In State</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Classifier#getObjectFlowState <em>Object Flow State</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getClassifier()
 * @model abstract="true"
 * @generated
 */
public interface Classifier extends GeneralizableElement, Namespace {
	/**
	 * Returns the value of the '<em><b>Feature</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Feature}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Feature#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' containment reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getClassifier_Feature()
	 * @see net.jgsuess.uml14.foundation.core.Feature#getOwner
	 * @model opposite="owner" containment="true"
	 * @generated
	 */
	EList<Feature> getFeature();

	/**
	 * Returns the value of the '<em><b>Typed Feature</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.StructuralFeature}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.StructuralFeature#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Typed Feature</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Typed Feature</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getClassifier_TypedFeature()
	 * @see net.jgsuess.uml14.foundation.core.StructuralFeature#getType
	 * @model opposite="type"
	 * @generated
	 */
	EList<StructuralFeature> getTypedFeature();

	/**
	 * Returns the value of the '<em><b>Typed Parameter</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Parameter}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Parameter#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Typed Parameter</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Typed Parameter</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getClassifier_TypedParameter()
	 * @see net.jgsuess.uml14.foundation.core.Parameter#getType
	 * @model opposite="type"
	 * @generated
	 */
	EList<Parameter> getTypedParameter();

	/**
	 * Returns the value of the '<em><b>Association</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.AssociationEnd}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getParticipant <em>Participant</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Association</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Association</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getClassifier_Association()
	 * @see net.jgsuess.uml14.foundation.core.AssociationEnd#getParticipant
	 * @model opposite="participant"
	 * @generated
	 */
	EList<AssociationEnd> getAssociation();

	/**
	 * Returns the value of the '<em><b>Specified End</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.AssociationEnd}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getSpecification <em>Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Specified End</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specified End</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getClassifier_SpecifiedEnd()
	 * @see net.jgsuess.uml14.foundation.core.AssociationEnd#getSpecification
	 * @model opposite="specification"
	 * @generated
	 */
	EList<AssociationEnd> getSpecifiedEnd();

	/**
	 * Returns the value of the '<em><b>Powertype Range</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Generalization}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Generalization#getPowertype <em>Powertype</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Powertype Range</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Powertype Range</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getClassifier_PowertypeRange()
	 * @see net.jgsuess.uml14.foundation.core.Generalization#getPowertype
	 * @model opposite="powertype"
	 * @generated
	 */
	EList<Generalization> getPowertypeRange();

	/**
	 * Returns the value of the '<em><b>Instance</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.Instance}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.Instance#getClassifier <em>Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instance</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getClassifier_Instance()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.Instance#getClassifier
	 * @model opposite="classifier"
	 * @generated
	 */
	EList<Instance> getInstance();

	/**
	 * Returns the value of the '<em><b>Create Action</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.CreateAction}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.CreateAction#getInstantiation <em>Instantiation</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Create Action</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Create Action</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getClassifier_CreateAction()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.CreateAction#getInstantiation
	 * @model opposite="instantiation"
	 * @generated
	 */
	EList<CreateAction> getCreateAction();

	/**
	 * Returns the value of the '<em><b>Classifier In State</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classifier In State</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classifier In State</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getClassifier_ClassifierInState()
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState#getType
	 * @model opposite="type"
	 * @generated
	 */
	EList<ClassifierInState> getClassifierInState();

	/**
	 * Returns the value of the '<em><b>Object Flow State</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Flow State</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Flow State</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getClassifier_ObjectFlowState()
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState#getType
	 * @model opposite="type"
	 * @generated
	 */
	EList<ObjectFlowState> getObjectFlowState();

} // Classifier
