/**
 * <copyright>
 * </copyright>
 *
 * $Id: Permission.java,v 1.1 2012/04/23 09:31:27 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Permission</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getPermission()
 * @model
 * @generated
 */
public interface Permission extends Dependency {
} // Permission
