/**
 * <copyright>
 * </copyright>
 *
 * $Id: Enumeration.java,v 1.1 2012/04/23 09:31:28 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enumeration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Enumeration#getLiteral <em>Literal</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getEnumeration()
 * @model
 * @generated
 */
public interface Enumeration extends DataType {
	/**
	 * Returns the value of the '<em><b>Literal</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.EnumerationLiteral}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.EnumerationLiteral#getEnumeration <em>Enumeration</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Literal</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Literal</em>' containment reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getEnumeration_Literal()
	 * @see net.jgsuess.uml14.foundation.core.EnumerationLiteral#getEnumeration
	 * @model opposite="enumeration" containment="true" required="true"
	 * @generated
	 */
	EList<EnumerationLiteral> getLiteral();

} // Enumeration
