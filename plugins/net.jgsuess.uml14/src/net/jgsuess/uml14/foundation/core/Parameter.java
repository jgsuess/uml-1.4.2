/**
 * <copyright>
 * </copyright>
 *
 * $Id: Parameter.java,v 1.1 2012/04/23 09:31:26 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState;

import net.jgsuess.uml14.behavioral_elements.state_machines.Event;

import net.jgsuess.uml14.foundation.data_types.Expression;
import net.jgsuess.uml14.foundation.data_types.ParameterDirectionKind;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Parameter#getDefaultValue <em>Default Value</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Parameter#getKind <em>Kind</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Parameter#getBehavioralFeature <em>Behavioral Feature</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Parameter#getType <em>Type</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Parameter#getEvent <em>Event</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Parameter#getState <em>State</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getParameter()
 * @model
 * @generated
 */
public interface Parameter extends ModelElement {
	/**
	 * Returns the value of the '<em><b>Default Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Value</em>' containment reference.
	 * @see #setDefaultValue(Expression)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getParameter_DefaultValue()
	 * @model containment="true"
	 * @generated
	 */
	Expression getDefaultValue();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Parameter#getDefaultValue <em>Default Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Value</em>' containment reference.
	 * @see #getDefaultValue()
	 * @generated
	 */
	void setDefaultValue(Expression value);

	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link net.jgsuess.uml14.foundation.data_types.ParameterDirectionKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.ParameterDirectionKind
	 * @see #setKind(ParameterDirectionKind)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getParameter_Kind()
	 * @model
	 * @generated
	 */
	ParameterDirectionKind getKind();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Parameter#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see net.jgsuess.uml14.foundation.data_types.ParameterDirectionKind
	 * @see #getKind()
	 * @generated
	 */
	void setKind(ParameterDirectionKind value);

	/**
	 * Returns the value of the '<em><b>Behavioral Feature</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.BehavioralFeature#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Behavioral Feature</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Behavioral Feature</em>' container reference.
	 * @see #setBehavioralFeature(BehavioralFeature)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getParameter_BehavioralFeature()
	 * @see net.jgsuess.uml14.foundation.core.BehavioralFeature#getParameter
	 * @model opposite="parameter"
	 * @generated
	 */
	BehavioralFeature getBehavioralFeature();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Parameter#getBehavioralFeature <em>Behavioral Feature</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Behavioral Feature</em>' container reference.
	 * @see #getBehavioralFeature()
	 * @generated
	 */
	void setBehavioralFeature(BehavioralFeature value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Classifier#getTypedParameter <em>Typed Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(Classifier)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getParameter_Type()
	 * @see net.jgsuess.uml14.foundation.core.Classifier#getTypedParameter
	 * @model opposite="typedParameter" required="true"
	 * @generated
	 */
	Classifier getType();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Parameter#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Classifier value);

	/**
	 * Returns the value of the '<em><b>Event</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.state_machines.Event#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event</em>' container reference.
	 * @see #setEvent(Event)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getParameter_Event()
	 * @see net.jgsuess.uml14.behavioral_elements.state_machines.Event#getParameter
	 * @model opposite="parameter"
	 * @generated
	 */
	Event getEvent();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Parameter#getEvent <em>Event</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event</em>' container reference.
	 * @see #getEvent()
	 * @generated
	 */
	void setEvent(Event value);

	/**
	 * Returns the value of the '<em><b>State</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getParameter_State()
	 * @see net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState#getParameter
	 * @model opposite="parameter"
	 * @generated
	 */
	EList<ObjectFlowState> getState();

} // Parameter
