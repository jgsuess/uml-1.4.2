/**
 * <copyright>
 * </copyright>
 *
 * $Id: ElementResidenceImpl.java,v 1.1 2012/04/23 09:31:31 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.impl;

import net.jgsuess.uml14.foundation.core.Component;
import net.jgsuess.uml14.foundation.core.CorePackage;
import net.jgsuess.uml14.foundation.core.ElementResidence;
import net.jgsuess.uml14.foundation.core.ModelElement;

import net.jgsuess.uml14.foundation.data_types.VisibilityKind;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Element Residence</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ElementResidenceImpl#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ElementResidenceImpl#getResident <em>Resident</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ElementResidenceImpl#getContainer <em>Container</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ElementResidenceImpl extends EObjectImpl implements ElementResidence {
	/**
	 * The default value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibility()
	 * @generated
	 * @ordered
	 */
	protected static final VisibilityKind VISIBILITY_EDEFAULT = VisibilityKind.VK_PUBLIC;

	/**
	 * The cached value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibility()
	 * @generated
	 * @ordered
	 */
	protected VisibilityKind visibility = VISIBILITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getResident() <em>Resident</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResident()
	 * @generated
	 * @ordered
	 */
	protected ModelElement resident;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElementResidenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.ELEMENT_RESIDENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisibilityKind getVisibility() {
		return visibility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVisibility(VisibilityKind newVisibility) {
		VisibilityKind oldVisibility = visibility;
		visibility = newVisibility == null ? VISIBILITY_EDEFAULT : newVisibility;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.ELEMENT_RESIDENCE__VISIBILITY, oldVisibility, visibility));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelElement getResident() {
		if (resident != null && resident.eIsProxy()) {
			InternalEObject oldResident = (InternalEObject)resident;
			resident = (ModelElement)eResolveProxy(oldResident);
			if (resident != oldResident) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.ELEMENT_RESIDENCE__RESIDENT, oldResident, resident));
			}
		}
		return resident;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelElement basicGetResident() {
		return resident;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetResident(ModelElement newResident, NotificationChain msgs) {
		ModelElement oldResident = resident;
		resident = newResident;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.ELEMENT_RESIDENCE__RESIDENT, oldResident, newResident);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResident(ModelElement newResident) {
		if (newResident != resident) {
			NotificationChain msgs = null;
			if (resident != null)
				msgs = ((InternalEObject)resident).eInverseRemove(this, CorePackage.MODEL_ELEMENT__ELEMENT_RESIDENCE, ModelElement.class, msgs);
			if (newResident != null)
				msgs = ((InternalEObject)newResident).eInverseAdd(this, CorePackage.MODEL_ELEMENT__ELEMENT_RESIDENCE, ModelElement.class, msgs);
			msgs = basicSetResident(newResident, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.ELEMENT_RESIDENCE__RESIDENT, newResident, newResident));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component getContainer() {
		if (eContainerFeatureID() != CorePackage.ELEMENT_RESIDENCE__CONTAINER) return null;
		return (Component)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainer(Component newContainer, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newContainer, CorePackage.ELEMENT_RESIDENCE__CONTAINER, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainer(Component newContainer) {
		if (newContainer != eInternalContainer() || (eContainerFeatureID() != CorePackage.ELEMENT_RESIDENCE__CONTAINER && newContainer != null)) {
			if (EcoreUtil.isAncestor(this, newContainer))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newContainer != null)
				msgs = ((InternalEObject)newContainer).eInverseAdd(this, CorePackage.COMPONENT__RESIDENT_ELEMENT, Component.class, msgs);
			msgs = basicSetContainer(newContainer, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.ELEMENT_RESIDENCE__CONTAINER, newContainer, newContainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.ELEMENT_RESIDENCE__RESIDENT:
				if (resident != null)
					msgs = ((InternalEObject)resident).eInverseRemove(this, CorePackage.MODEL_ELEMENT__ELEMENT_RESIDENCE, ModelElement.class, msgs);
				return basicSetResident((ModelElement)otherEnd, msgs);
			case CorePackage.ELEMENT_RESIDENCE__CONTAINER:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetContainer((Component)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.ELEMENT_RESIDENCE__RESIDENT:
				return basicSetResident(null, msgs);
			case CorePackage.ELEMENT_RESIDENCE__CONTAINER:
				return basicSetContainer(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CorePackage.ELEMENT_RESIDENCE__CONTAINER:
				return eInternalContainer().eInverseRemove(this, CorePackage.COMPONENT__RESIDENT_ELEMENT, Component.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CorePackage.ELEMENT_RESIDENCE__VISIBILITY:
				return getVisibility();
			case CorePackage.ELEMENT_RESIDENCE__RESIDENT:
				if (resolve) return getResident();
				return basicGetResident();
			case CorePackage.ELEMENT_RESIDENCE__CONTAINER:
				return getContainer();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CorePackage.ELEMENT_RESIDENCE__VISIBILITY:
				setVisibility((VisibilityKind)newValue);
				return;
			case CorePackage.ELEMENT_RESIDENCE__RESIDENT:
				setResident((ModelElement)newValue);
				return;
			case CorePackage.ELEMENT_RESIDENCE__CONTAINER:
				setContainer((Component)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CorePackage.ELEMENT_RESIDENCE__VISIBILITY:
				setVisibility(VISIBILITY_EDEFAULT);
				return;
			case CorePackage.ELEMENT_RESIDENCE__RESIDENT:
				setResident((ModelElement)null);
				return;
			case CorePackage.ELEMENT_RESIDENCE__CONTAINER:
				setContainer((Component)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CorePackage.ELEMENT_RESIDENCE__VISIBILITY:
				return visibility != VISIBILITY_EDEFAULT;
			case CorePackage.ELEMENT_RESIDENCE__RESIDENT:
				return resident != null;
			case CorePackage.ELEMENT_RESIDENCE__CONTAINER:
				return getContainer() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (visibility: ");
		result.append(visibility);
		result.append(')');
		return result.toString();
	}

} //ElementResidenceImpl
