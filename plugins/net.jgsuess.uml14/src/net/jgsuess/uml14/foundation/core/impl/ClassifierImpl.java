/**
 * <copyright>
 * </copyright>
 *
 * $Id: ClassifierImpl.java,v 1.1 2012/04/23 09:31:33 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage;
import net.jgsuess.uml14.behavioral_elements.activity_graphs.ClassifierInState;
import net.jgsuess.uml14.behavioral_elements.activity_graphs.ObjectFlowState;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;
import net.jgsuess.uml14.behavioral_elements.common_behavior.CreateAction;
import net.jgsuess.uml14.behavioral_elements.common_behavior.Instance;

import net.jgsuess.uml14.foundation.core.AssociationEnd;
import net.jgsuess.uml14.foundation.core.Classifier;
import net.jgsuess.uml14.foundation.core.CorePackage;
import net.jgsuess.uml14.foundation.core.Feature;
import net.jgsuess.uml14.foundation.core.Generalization;
import net.jgsuess.uml14.foundation.core.ModelElement;
import net.jgsuess.uml14.foundation.core.Namespace;
import net.jgsuess.uml14.foundation.core.Parameter;
import net.jgsuess.uml14.foundation.core.StructuralFeature;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Classifier</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ClassifierImpl#getOwnedElement <em>Owned Element</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ClassifierImpl#getFeature <em>Feature</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ClassifierImpl#getTypedFeature <em>Typed Feature</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ClassifierImpl#getTypedParameter <em>Typed Parameter</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ClassifierImpl#getAssociation <em>Association</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ClassifierImpl#getSpecifiedEnd <em>Specified End</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ClassifierImpl#getPowertypeRange <em>Powertype Range</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ClassifierImpl#getInstance <em>Instance</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ClassifierImpl#getCreateAction <em>Create Action</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ClassifierImpl#getClassifierInState <em>Classifier In State</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ClassifierImpl#getObjectFlowState <em>Object Flow State</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class ClassifierImpl extends GeneralizableElementImpl implements Classifier {
	/**
	 * The cached value of the '{@link #getOwnedElement() <em>Owned Element</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedElement()
	 * @generated
	 * @ordered
	 */
	protected EList<ModelElement> ownedElement;

	/**
	 * The cached value of the '{@link #getFeature() <em>Feature</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeature()
	 * @generated
	 * @ordered
	 */
	protected EList<Feature> feature;

	/**
	 * The cached value of the '{@link #getTypedFeature() <em>Typed Feature</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypedFeature()
	 * @generated
	 * @ordered
	 */
	protected EList<StructuralFeature> typedFeature;

	/**
	 * The cached value of the '{@link #getTypedParameter() <em>Typed Parameter</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypedParameter()
	 * @generated
	 * @ordered
	 */
	protected EList<Parameter> typedParameter;

	/**
	 * The cached value of the '{@link #getAssociation() <em>Association</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociation()
	 * @generated
	 * @ordered
	 */
	protected EList<AssociationEnd> association;

	/**
	 * The cached value of the '{@link #getSpecifiedEnd() <em>Specified End</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecifiedEnd()
	 * @generated
	 * @ordered
	 */
	protected EList<AssociationEnd> specifiedEnd;

	/**
	 * The cached value of the '{@link #getPowertypeRange() <em>Powertype Range</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPowertypeRange()
	 * @generated
	 * @ordered
	 */
	protected EList<Generalization> powertypeRange;

	/**
	 * The cached value of the '{@link #getInstance() <em>Instance</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstance()
	 * @generated
	 * @ordered
	 */
	protected EList<Instance> instance;

	/**
	 * The cached value of the '{@link #getCreateAction() <em>Create Action</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreateAction()
	 * @generated
	 * @ordered
	 */
	protected EList<CreateAction> createAction;

	/**
	 * The cached value of the '{@link #getClassifierInState() <em>Classifier In State</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassifierInState()
	 * @generated
	 * @ordered
	 */
	protected EList<ClassifierInState> classifierInState;

	/**
	 * The cached value of the '{@link #getObjectFlowState() <em>Object Flow State</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectFlowState()
	 * @generated
	 * @ordered
	 */
	protected EList<ObjectFlowState> objectFlowState;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassifierImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.CLASSIFIER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModelElement> getOwnedElement() {
		if (ownedElement == null) {
			ownedElement = new EObjectContainmentWithInverseEList<ModelElement>(ModelElement.class, this, CorePackage.CLASSIFIER__OWNED_ELEMENT, CorePackage.MODEL_ELEMENT__NAMESPACE);
		}
		return ownedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Feature> getFeature() {
		if (feature == null) {
			feature = new EObjectContainmentWithInverseEList<Feature>(Feature.class, this, CorePackage.CLASSIFIER__FEATURE, CorePackage.FEATURE__OWNER);
		}
		return feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StructuralFeature> getTypedFeature() {
		if (typedFeature == null) {
			typedFeature = new EObjectWithInverseResolvingEList<StructuralFeature>(StructuralFeature.class, this, CorePackage.CLASSIFIER__TYPED_FEATURE, CorePackage.STRUCTURAL_FEATURE__TYPE);
		}
		return typedFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Parameter> getTypedParameter() {
		if (typedParameter == null) {
			typedParameter = new EObjectWithInverseResolvingEList<Parameter>(Parameter.class, this, CorePackage.CLASSIFIER__TYPED_PARAMETER, CorePackage.PARAMETER__TYPE);
		}
		return typedParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssociationEnd> getAssociation() {
		if (association == null) {
			association = new EObjectWithInverseResolvingEList<AssociationEnd>(AssociationEnd.class, this, CorePackage.CLASSIFIER__ASSOCIATION, CorePackage.ASSOCIATION_END__PARTICIPANT);
		}
		return association;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssociationEnd> getSpecifiedEnd() {
		if (specifiedEnd == null) {
			specifiedEnd = new EObjectWithInverseResolvingEList.ManyInverse<AssociationEnd>(AssociationEnd.class, this, CorePackage.CLASSIFIER__SPECIFIED_END, CorePackage.ASSOCIATION_END__SPECIFICATION);
		}
		return specifiedEnd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Generalization> getPowertypeRange() {
		if (powertypeRange == null) {
			powertypeRange = new EObjectWithInverseResolvingEList<Generalization>(Generalization.class, this, CorePackage.CLASSIFIER__POWERTYPE_RANGE, CorePackage.GENERALIZATION__POWERTYPE);
		}
		return powertypeRange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Instance> getInstance() {
		if (instance == null) {
			instance = new EObjectWithInverseResolvingEList.ManyInverse<Instance>(Instance.class, this, CorePackage.CLASSIFIER__INSTANCE, Common_behaviorPackage.INSTANCE__CLASSIFIER);
		}
		return instance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CreateAction> getCreateAction() {
		if (createAction == null) {
			createAction = new EObjectWithInverseResolvingEList<CreateAction>(CreateAction.class, this, CorePackage.CLASSIFIER__CREATE_ACTION, Common_behaviorPackage.CREATE_ACTION__INSTANTIATION);
		}
		return createAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ClassifierInState> getClassifierInState() {
		if (classifierInState == null) {
			classifierInState = new EObjectWithInverseResolvingEList<ClassifierInState>(ClassifierInState.class, this, CorePackage.CLASSIFIER__CLASSIFIER_IN_STATE, Activity_graphsPackage.CLASSIFIER_IN_STATE__TYPE);
		}
		return classifierInState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObjectFlowState> getObjectFlowState() {
		if (objectFlowState == null) {
			objectFlowState = new EObjectWithInverseResolvingEList<ObjectFlowState>(ObjectFlowState.class, this, CorePackage.CLASSIFIER__OBJECT_FLOW_STATE, Activity_graphsPackage.OBJECT_FLOW_STATE__TYPE);
		}
		return objectFlowState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.CLASSIFIER__OWNED_ELEMENT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOwnedElement()).basicAdd(otherEnd, msgs);
			case CorePackage.CLASSIFIER__FEATURE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getFeature()).basicAdd(otherEnd, msgs);
			case CorePackage.CLASSIFIER__TYPED_FEATURE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getTypedFeature()).basicAdd(otherEnd, msgs);
			case CorePackage.CLASSIFIER__TYPED_PARAMETER:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getTypedParameter()).basicAdd(otherEnd, msgs);
			case CorePackage.CLASSIFIER__ASSOCIATION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAssociation()).basicAdd(otherEnd, msgs);
			case CorePackage.CLASSIFIER__SPECIFIED_END:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSpecifiedEnd()).basicAdd(otherEnd, msgs);
			case CorePackage.CLASSIFIER__POWERTYPE_RANGE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPowertypeRange()).basicAdd(otherEnd, msgs);
			case CorePackage.CLASSIFIER__INSTANCE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getInstance()).basicAdd(otherEnd, msgs);
			case CorePackage.CLASSIFIER__CREATE_ACTION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getCreateAction()).basicAdd(otherEnd, msgs);
			case CorePackage.CLASSIFIER__CLASSIFIER_IN_STATE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getClassifierInState()).basicAdd(otherEnd, msgs);
			case CorePackage.CLASSIFIER__OBJECT_FLOW_STATE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getObjectFlowState()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.CLASSIFIER__OWNED_ELEMENT:
				return ((InternalEList<?>)getOwnedElement()).basicRemove(otherEnd, msgs);
			case CorePackage.CLASSIFIER__FEATURE:
				return ((InternalEList<?>)getFeature()).basicRemove(otherEnd, msgs);
			case CorePackage.CLASSIFIER__TYPED_FEATURE:
				return ((InternalEList<?>)getTypedFeature()).basicRemove(otherEnd, msgs);
			case CorePackage.CLASSIFIER__TYPED_PARAMETER:
				return ((InternalEList<?>)getTypedParameter()).basicRemove(otherEnd, msgs);
			case CorePackage.CLASSIFIER__ASSOCIATION:
				return ((InternalEList<?>)getAssociation()).basicRemove(otherEnd, msgs);
			case CorePackage.CLASSIFIER__SPECIFIED_END:
				return ((InternalEList<?>)getSpecifiedEnd()).basicRemove(otherEnd, msgs);
			case CorePackage.CLASSIFIER__POWERTYPE_RANGE:
				return ((InternalEList<?>)getPowertypeRange()).basicRemove(otherEnd, msgs);
			case CorePackage.CLASSIFIER__INSTANCE:
				return ((InternalEList<?>)getInstance()).basicRemove(otherEnd, msgs);
			case CorePackage.CLASSIFIER__CREATE_ACTION:
				return ((InternalEList<?>)getCreateAction()).basicRemove(otherEnd, msgs);
			case CorePackage.CLASSIFIER__CLASSIFIER_IN_STATE:
				return ((InternalEList<?>)getClassifierInState()).basicRemove(otherEnd, msgs);
			case CorePackage.CLASSIFIER__OBJECT_FLOW_STATE:
				return ((InternalEList<?>)getObjectFlowState()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CorePackage.CLASSIFIER__OWNED_ELEMENT:
				return getOwnedElement();
			case CorePackage.CLASSIFIER__FEATURE:
				return getFeature();
			case CorePackage.CLASSIFIER__TYPED_FEATURE:
				return getTypedFeature();
			case CorePackage.CLASSIFIER__TYPED_PARAMETER:
				return getTypedParameter();
			case CorePackage.CLASSIFIER__ASSOCIATION:
				return getAssociation();
			case CorePackage.CLASSIFIER__SPECIFIED_END:
				return getSpecifiedEnd();
			case CorePackage.CLASSIFIER__POWERTYPE_RANGE:
				return getPowertypeRange();
			case CorePackage.CLASSIFIER__INSTANCE:
				return getInstance();
			case CorePackage.CLASSIFIER__CREATE_ACTION:
				return getCreateAction();
			case CorePackage.CLASSIFIER__CLASSIFIER_IN_STATE:
				return getClassifierInState();
			case CorePackage.CLASSIFIER__OBJECT_FLOW_STATE:
				return getObjectFlowState();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CorePackage.CLASSIFIER__OWNED_ELEMENT:
				getOwnedElement().clear();
				getOwnedElement().addAll((Collection<? extends ModelElement>)newValue);
				return;
			case CorePackage.CLASSIFIER__FEATURE:
				getFeature().clear();
				getFeature().addAll((Collection<? extends Feature>)newValue);
				return;
			case CorePackage.CLASSIFIER__TYPED_FEATURE:
				getTypedFeature().clear();
				getTypedFeature().addAll((Collection<? extends StructuralFeature>)newValue);
				return;
			case CorePackage.CLASSIFIER__TYPED_PARAMETER:
				getTypedParameter().clear();
				getTypedParameter().addAll((Collection<? extends Parameter>)newValue);
				return;
			case CorePackage.CLASSIFIER__ASSOCIATION:
				getAssociation().clear();
				getAssociation().addAll((Collection<? extends AssociationEnd>)newValue);
				return;
			case CorePackage.CLASSIFIER__SPECIFIED_END:
				getSpecifiedEnd().clear();
				getSpecifiedEnd().addAll((Collection<? extends AssociationEnd>)newValue);
				return;
			case CorePackage.CLASSIFIER__POWERTYPE_RANGE:
				getPowertypeRange().clear();
				getPowertypeRange().addAll((Collection<? extends Generalization>)newValue);
				return;
			case CorePackage.CLASSIFIER__INSTANCE:
				getInstance().clear();
				getInstance().addAll((Collection<? extends Instance>)newValue);
				return;
			case CorePackage.CLASSIFIER__CREATE_ACTION:
				getCreateAction().clear();
				getCreateAction().addAll((Collection<? extends CreateAction>)newValue);
				return;
			case CorePackage.CLASSIFIER__CLASSIFIER_IN_STATE:
				getClassifierInState().clear();
				getClassifierInState().addAll((Collection<? extends ClassifierInState>)newValue);
				return;
			case CorePackage.CLASSIFIER__OBJECT_FLOW_STATE:
				getObjectFlowState().clear();
				getObjectFlowState().addAll((Collection<? extends ObjectFlowState>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CorePackage.CLASSIFIER__OWNED_ELEMENT:
				getOwnedElement().clear();
				return;
			case CorePackage.CLASSIFIER__FEATURE:
				getFeature().clear();
				return;
			case CorePackage.CLASSIFIER__TYPED_FEATURE:
				getTypedFeature().clear();
				return;
			case CorePackage.CLASSIFIER__TYPED_PARAMETER:
				getTypedParameter().clear();
				return;
			case CorePackage.CLASSIFIER__ASSOCIATION:
				getAssociation().clear();
				return;
			case CorePackage.CLASSIFIER__SPECIFIED_END:
				getSpecifiedEnd().clear();
				return;
			case CorePackage.CLASSIFIER__POWERTYPE_RANGE:
				getPowertypeRange().clear();
				return;
			case CorePackage.CLASSIFIER__INSTANCE:
				getInstance().clear();
				return;
			case CorePackage.CLASSIFIER__CREATE_ACTION:
				getCreateAction().clear();
				return;
			case CorePackage.CLASSIFIER__CLASSIFIER_IN_STATE:
				getClassifierInState().clear();
				return;
			case CorePackage.CLASSIFIER__OBJECT_FLOW_STATE:
				getObjectFlowState().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CorePackage.CLASSIFIER__OWNED_ELEMENT:
				return ownedElement != null && !ownedElement.isEmpty();
			case CorePackage.CLASSIFIER__FEATURE:
				return feature != null && !feature.isEmpty();
			case CorePackage.CLASSIFIER__TYPED_FEATURE:
				return typedFeature != null && !typedFeature.isEmpty();
			case CorePackage.CLASSIFIER__TYPED_PARAMETER:
				return typedParameter != null && !typedParameter.isEmpty();
			case CorePackage.CLASSIFIER__ASSOCIATION:
				return association != null && !association.isEmpty();
			case CorePackage.CLASSIFIER__SPECIFIED_END:
				return specifiedEnd != null && !specifiedEnd.isEmpty();
			case CorePackage.CLASSIFIER__POWERTYPE_RANGE:
				return powertypeRange != null && !powertypeRange.isEmpty();
			case CorePackage.CLASSIFIER__INSTANCE:
				return instance != null && !instance.isEmpty();
			case CorePackage.CLASSIFIER__CREATE_ACTION:
				return createAction != null && !createAction.isEmpty();
			case CorePackage.CLASSIFIER__CLASSIFIER_IN_STATE:
				return classifierInState != null && !classifierInState.isEmpty();
			case CorePackage.CLASSIFIER__OBJECT_FLOW_STATE:
				return objectFlowState != null && !objectFlowState.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Namespace.class) {
			switch (derivedFeatureID) {
				case CorePackage.CLASSIFIER__OWNED_ELEMENT: return CorePackage.NAMESPACE__OWNED_ELEMENT;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Namespace.class) {
			switch (baseFeatureID) {
				case CorePackage.NAMESPACE__OWNED_ELEMENT: return CorePackage.CLASSIFIER__OWNED_ELEMENT;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ClassifierImpl
