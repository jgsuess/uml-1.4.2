/**
 * <copyright>
 * </copyright>
 *
 * $Id: PresentationElement.java,v 1.1 2012/04/23 09:31:28 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Presentation Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.PresentationElement#getSubject <em>Subject</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getPresentationElement()
 * @model abstract="true"
 * @generated
 */
public interface PresentationElement extends Element {
	/**
	 * Returns the value of the '<em><b>Subject</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.ModelElement}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.ModelElement#getPresentation <em>Presentation</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subject</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subject</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getPresentationElement_Subject()
	 * @see net.jgsuess.uml14.foundation.core.ModelElement#getPresentation
	 * @model opposite="presentation"
	 * @generated
	 */
	EList<ModelElement> getSubject();

} // PresentationElement
