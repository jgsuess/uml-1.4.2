/**
 * <copyright>
 * </copyright>
 *
 * $Id: TemplateParameterImpl.java,v 1.1 2012/04/23 09:31:34 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.impl;

import net.jgsuess.uml14.foundation.core.CorePackage;
import net.jgsuess.uml14.foundation.core.ModelElement;
import net.jgsuess.uml14.foundation.core.TemplateParameter;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Template Parameter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.TemplateParameterImpl#getDefaultElement <em>Default Element</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.TemplateParameterImpl#getTemplate <em>Template</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.TemplateParameterImpl#getParameter <em>Parameter</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TemplateParameterImpl extends EObjectImpl implements TemplateParameter {
	/**
	 * The cached value of the '{@link #getDefaultElement() <em>Default Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultElement()
	 * @generated
	 * @ordered
	 */
	protected ModelElement defaultElement;

	/**
	 * The cached value of the '{@link #getParameter() <em>Parameter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameter()
	 * @generated
	 * @ordered
	 */
	protected ModelElement parameter;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TemplateParameterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.TEMPLATE_PARAMETER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelElement getDefaultElement() {
		if (defaultElement != null && defaultElement.eIsProxy()) {
			InternalEObject oldDefaultElement = (InternalEObject)defaultElement;
			defaultElement = (ModelElement)eResolveProxy(oldDefaultElement);
			if (defaultElement != oldDefaultElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CorePackage.TEMPLATE_PARAMETER__DEFAULT_ELEMENT, oldDefaultElement, defaultElement));
			}
		}
		return defaultElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelElement basicGetDefaultElement() {
		return defaultElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefaultElement(ModelElement newDefaultElement, NotificationChain msgs) {
		ModelElement oldDefaultElement = defaultElement;
		defaultElement = newDefaultElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.TEMPLATE_PARAMETER__DEFAULT_ELEMENT, oldDefaultElement, newDefaultElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultElement(ModelElement newDefaultElement) {
		if (newDefaultElement != defaultElement) {
			NotificationChain msgs = null;
			if (defaultElement != null)
				msgs = ((InternalEObject)defaultElement).eInverseRemove(this, CorePackage.MODEL_ELEMENT__DEFAULTED_PARAMETER, ModelElement.class, msgs);
			if (newDefaultElement != null)
				msgs = ((InternalEObject)newDefaultElement).eInverseAdd(this, CorePackage.MODEL_ELEMENT__DEFAULTED_PARAMETER, ModelElement.class, msgs);
			msgs = basicSetDefaultElement(newDefaultElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.TEMPLATE_PARAMETER__DEFAULT_ELEMENT, newDefaultElement, newDefaultElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelElement getTemplate() {
		if (eContainerFeatureID() != CorePackage.TEMPLATE_PARAMETER__TEMPLATE) return null;
		return (ModelElement)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTemplate(ModelElement newTemplate, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newTemplate, CorePackage.TEMPLATE_PARAMETER__TEMPLATE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTemplate(ModelElement newTemplate) {
		if (newTemplate != eInternalContainer() || (eContainerFeatureID() != CorePackage.TEMPLATE_PARAMETER__TEMPLATE && newTemplate != null)) {
			if (EcoreUtil.isAncestor(this, newTemplate))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newTemplate != null)
				msgs = ((InternalEObject)newTemplate).eInverseAdd(this, CorePackage.MODEL_ELEMENT__TEMPLATE_PARAMETER, ModelElement.class, msgs);
			msgs = basicSetTemplate(newTemplate, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.TEMPLATE_PARAMETER__TEMPLATE, newTemplate, newTemplate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelElement getParameter() {
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameter(ModelElement newParameter, NotificationChain msgs) {
		ModelElement oldParameter = parameter;
		parameter = newParameter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.TEMPLATE_PARAMETER__PARAMETER, oldParameter, newParameter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameter(ModelElement newParameter) {
		if (newParameter != parameter) {
			NotificationChain msgs = null;
			if (parameter != null)
				msgs = ((InternalEObject)parameter).eInverseRemove(this, CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE, ModelElement.class, msgs);
			if (newParameter != null)
				msgs = ((InternalEObject)newParameter).eInverseAdd(this, CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE, ModelElement.class, msgs);
			msgs = basicSetParameter(newParameter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.TEMPLATE_PARAMETER__PARAMETER, newParameter, newParameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.TEMPLATE_PARAMETER__DEFAULT_ELEMENT:
				if (defaultElement != null)
					msgs = ((InternalEObject)defaultElement).eInverseRemove(this, CorePackage.MODEL_ELEMENT__DEFAULTED_PARAMETER, ModelElement.class, msgs);
				return basicSetDefaultElement((ModelElement)otherEnd, msgs);
			case CorePackage.TEMPLATE_PARAMETER__TEMPLATE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetTemplate((ModelElement)otherEnd, msgs);
			case CorePackage.TEMPLATE_PARAMETER__PARAMETER:
				if (parameter != null)
					msgs = ((InternalEObject)parameter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.TEMPLATE_PARAMETER__PARAMETER, null, msgs);
				return basicSetParameter((ModelElement)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.TEMPLATE_PARAMETER__DEFAULT_ELEMENT:
				return basicSetDefaultElement(null, msgs);
			case CorePackage.TEMPLATE_PARAMETER__TEMPLATE:
				return basicSetTemplate(null, msgs);
			case CorePackage.TEMPLATE_PARAMETER__PARAMETER:
				return basicSetParameter(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CorePackage.TEMPLATE_PARAMETER__TEMPLATE:
				return eInternalContainer().eInverseRemove(this, CorePackage.MODEL_ELEMENT__TEMPLATE_PARAMETER, ModelElement.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CorePackage.TEMPLATE_PARAMETER__DEFAULT_ELEMENT:
				if (resolve) return getDefaultElement();
				return basicGetDefaultElement();
			case CorePackage.TEMPLATE_PARAMETER__TEMPLATE:
				return getTemplate();
			case CorePackage.TEMPLATE_PARAMETER__PARAMETER:
				return getParameter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CorePackage.TEMPLATE_PARAMETER__DEFAULT_ELEMENT:
				setDefaultElement((ModelElement)newValue);
				return;
			case CorePackage.TEMPLATE_PARAMETER__TEMPLATE:
				setTemplate((ModelElement)newValue);
				return;
			case CorePackage.TEMPLATE_PARAMETER__PARAMETER:
				setParameter((ModelElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CorePackage.TEMPLATE_PARAMETER__DEFAULT_ELEMENT:
				setDefaultElement((ModelElement)null);
				return;
			case CorePackage.TEMPLATE_PARAMETER__TEMPLATE:
				setTemplate((ModelElement)null);
				return;
			case CorePackage.TEMPLATE_PARAMETER__PARAMETER:
				setParameter((ModelElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CorePackage.TEMPLATE_PARAMETER__DEFAULT_ELEMENT:
				return defaultElement != null;
			case CorePackage.TEMPLATE_PARAMETER__TEMPLATE:
				return getTemplate() != null;
			case CorePackage.TEMPLATE_PARAMETER__PARAMETER:
				return parameter != null;
		}
		return super.eIsSet(featureID);
	}

} //TemplateParameterImpl
