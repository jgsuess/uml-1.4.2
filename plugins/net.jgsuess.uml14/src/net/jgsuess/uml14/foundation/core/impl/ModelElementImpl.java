/**
 * <copyright>
 * </copyright>
 *
 * $Id: ModelElementImpl.java,v 1.1 2012/04/23 09:31:32 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.impl;

import java.util.Collection;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage;
import net.jgsuess.uml14.behavioral_elements.activity_graphs.Partition;

import net.jgsuess.uml14.behavioral_elements.collaborations.ClassifierRole;
import net.jgsuess.uml14.behavioral_elements.collaborations.Collaboration;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationInstanceSet;
import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage;

import net.jgsuess.uml14.behavioral_elements.state_machines.StateMachine;
import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage;

import net.jgsuess.uml14.foundation.core.Comment;
import net.jgsuess.uml14.foundation.core.Constraint;
import net.jgsuess.uml14.foundation.core.CorePackage;
import net.jgsuess.uml14.foundation.core.Dependency;
import net.jgsuess.uml14.foundation.core.ElementResidence;
import net.jgsuess.uml14.foundation.core.Flow;
import net.jgsuess.uml14.foundation.core.ModelElement;
import net.jgsuess.uml14.foundation.core.Namespace;
import net.jgsuess.uml14.foundation.core.PresentationElement;
import net.jgsuess.uml14.foundation.core.Stereotype;
import net.jgsuess.uml14.foundation.core.TaggedValue;
import net.jgsuess.uml14.foundation.core.TemplateArgument;
import net.jgsuess.uml14.foundation.core.TemplateParameter;

import net.jgsuess.uml14.foundation.data_types.VisibilityKind;

import net.jgsuess.uml14.model_management.ElementImport;
import net.jgsuess.uml14.model_management.Model_managementPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getName <em>Name</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#isIsSpecification <em>Is Specification</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getNamespace <em>Namespace</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getClientDependency <em>Client Dependency</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getConstraint <em>Constraint</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getSupplierDependency <em>Supplier Dependency</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getPresentation <em>Presentation</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getTargetFlow <em>Target Flow</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getSourceFlow <em>Source Flow</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getDefaultedParameter <em>Defaulted Parameter</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getElementResidence <em>Element Residence</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getTemplateParameter <em>Template Parameter</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getParameterTemplate <em>Parameter Template</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getStereotype <em>Stereotype</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getTaggedValue <em>Tagged Value</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getReferenceTag <em>Reference Tag</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getTemplateArgument <em>Template Argument</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getBehavior <em>Behavior</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getClassifierRole <em>Classifier Role</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getCollaboration <em>Collaboration</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getCollaborationInstanceSet <em>Collaboration Instance Set</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getPartition <em>Partition</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.ModelElementImpl#getElementImport <em>Element Import</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class ModelElementImpl extends ElementImpl implements ModelElement {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibility()
	 * @generated
	 * @ordered
	 */
	protected static final VisibilityKind VISIBILITY_EDEFAULT = VisibilityKind.VK_PUBLIC;

	/**
	 * The cached value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibility()
	 * @generated
	 * @ordered
	 */
	protected VisibilityKind visibility = VISIBILITY_EDEFAULT;

	/**
	 * The default value of the '{@link #isIsSpecification() <em>Is Specification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsSpecification()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_SPECIFICATION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsSpecification() <em>Is Specification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsSpecification()
	 * @generated
	 * @ordered
	 */
	protected boolean isSpecification = IS_SPECIFICATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getClientDependency() <em>Client Dependency</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClientDependency()
	 * @generated
	 * @ordered
	 */
	protected EList<Dependency> clientDependency;

	/**
	 * The cached value of the '{@link #getConstraint() <em>Constraint</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraint()
	 * @generated
	 * @ordered
	 */
	protected EList<Constraint> constraint;

	/**
	 * The cached value of the '{@link #getSupplierDependency() <em>Supplier Dependency</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSupplierDependency()
	 * @generated
	 * @ordered
	 */
	protected EList<Dependency> supplierDependency;

	/**
	 * The cached value of the '{@link #getPresentation() <em>Presentation</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPresentation()
	 * @generated
	 * @ordered
	 */
	protected EList<PresentationElement> presentation;

	/**
	 * The cached value of the '{@link #getTargetFlow() <em>Target Flow</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetFlow()
	 * @generated
	 * @ordered
	 */
	protected EList<Flow> targetFlow;

	/**
	 * The cached value of the '{@link #getSourceFlow() <em>Source Flow</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceFlow()
	 * @generated
	 * @ordered
	 */
	protected EList<Flow> sourceFlow;

	/**
	 * The cached value of the '{@link #getDefaultedParameter() <em>Defaulted Parameter</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultedParameter()
	 * @generated
	 * @ordered
	 */
	protected EList<TemplateParameter> defaultedParameter;

	/**
	 * The cached value of the '{@link #getComment() <em>Comment</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected EList<Comment> comment;

	/**
	 * The cached value of the '{@link #getElementResidence() <em>Element Residence</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementResidence()
	 * @generated
	 * @ordered
	 */
	protected EList<ElementResidence> elementResidence;

	/**
	 * The cached value of the '{@link #getTemplateParameter() <em>Template Parameter</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemplateParameter()
	 * @generated
	 * @ordered
	 */
	protected EList<TemplateParameter> templateParameter;

	/**
	 * The cached value of the '{@link #getStereotype() <em>Stereotype</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStereotype()
	 * @generated
	 * @ordered
	 */
	protected EList<Stereotype> stereotype;

	/**
	 * The cached value of the '{@link #getTaggedValue() <em>Tagged Value</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaggedValue()
	 * @generated
	 * @ordered
	 */
	protected EList<TaggedValue> taggedValue;

	/**
	 * The cached value of the '{@link #getReferenceTag() <em>Reference Tag</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceTag()
	 * @generated
	 * @ordered
	 */
	protected EList<TaggedValue> referenceTag;

	/**
	 * The cached value of the '{@link #getTemplateArgument() <em>Template Argument</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemplateArgument()
	 * @generated
	 * @ordered
	 */
	protected EList<TemplateArgument> templateArgument;

	/**
	 * The cached value of the '{@link #getBehavior() <em>Behavior</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBehavior()
	 * @generated
	 * @ordered
	 */
	protected EList<StateMachine> behavior;

	/**
	 * The cached value of the '{@link #getClassifierRole() <em>Classifier Role</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassifierRole()
	 * @generated
	 * @ordered
	 */
	protected EList<ClassifierRole> classifierRole;

	/**
	 * The cached value of the '{@link #getCollaboration() <em>Collaboration</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollaboration()
	 * @generated
	 * @ordered
	 */
	protected EList<Collaboration> collaboration;

	/**
	 * The cached value of the '{@link #getCollaborationInstanceSet() <em>Collaboration Instance Set</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollaborationInstanceSet()
	 * @generated
	 * @ordered
	 */
	protected EList<CollaborationInstanceSet> collaborationInstanceSet;

	/**
	 * The cached value of the '{@link #getPartition() <em>Partition</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartition()
	 * @generated
	 * @ordered
	 */
	protected EList<Partition> partition;

	/**
	 * The cached value of the '{@link #getElementImport() <em>Element Import</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementImport()
	 * @generated
	 * @ordered
	 */
	protected EList<ElementImport> elementImport;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModelElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.MODEL_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.MODEL_ELEMENT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VisibilityKind getVisibility() {
		return visibility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVisibility(VisibilityKind newVisibility) {
		VisibilityKind oldVisibility = visibility;
		visibility = newVisibility == null ? VISIBILITY_EDEFAULT : newVisibility;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.MODEL_ELEMENT__VISIBILITY, oldVisibility, visibility));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsSpecification() {
		return isSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsSpecification(boolean newIsSpecification) {
		boolean oldIsSpecification = isSpecification;
		isSpecification = newIsSpecification;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.MODEL_ELEMENT__IS_SPECIFICATION, oldIsSpecification, isSpecification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Namespace getNamespace() {
		if (eContainerFeatureID() != CorePackage.MODEL_ELEMENT__NAMESPACE) return null;
		return (Namespace)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNamespace(Namespace newNamespace, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newNamespace, CorePackage.MODEL_ELEMENT__NAMESPACE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNamespace(Namespace newNamespace) {
		if (newNamespace != eInternalContainer() || (eContainerFeatureID() != CorePackage.MODEL_ELEMENT__NAMESPACE && newNamespace != null)) {
			if (EcoreUtil.isAncestor(this, newNamespace))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newNamespace != null)
				msgs = ((InternalEObject)newNamespace).eInverseAdd(this, CorePackage.NAMESPACE__OWNED_ELEMENT, Namespace.class, msgs);
			msgs = basicSetNamespace(newNamespace, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.MODEL_ELEMENT__NAMESPACE, newNamespace, newNamespace));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Dependency> getClientDependency() {
		if (clientDependency == null) {
			clientDependency = new EObjectWithInverseResolvingEList.ManyInverse<Dependency>(Dependency.class, this, CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY, CorePackage.DEPENDENCY__CLIENT);
		}
		return clientDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Constraint> getConstraint() {
		if (constraint == null) {
			constraint = new EObjectWithInverseResolvingEList.ManyInverse<Constraint>(Constraint.class, this, CorePackage.MODEL_ELEMENT__CONSTRAINT, CorePackage.CONSTRAINT__CONSTRAINED_ELEMENT);
		}
		return constraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Dependency> getSupplierDependency() {
		if (supplierDependency == null) {
			supplierDependency = new EObjectWithInverseResolvingEList.ManyInverse<Dependency>(Dependency.class, this, CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY, CorePackage.DEPENDENCY__SUPPLIER);
		}
		return supplierDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PresentationElement> getPresentation() {
		if (presentation == null) {
			presentation = new EObjectWithInverseResolvingEList.ManyInverse<PresentationElement>(PresentationElement.class, this, CorePackage.MODEL_ELEMENT__PRESENTATION, CorePackage.PRESENTATION_ELEMENT__SUBJECT);
		}
		return presentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Flow> getTargetFlow() {
		if (targetFlow == null) {
			targetFlow = new EObjectWithInverseResolvingEList.ManyInverse<Flow>(Flow.class, this, CorePackage.MODEL_ELEMENT__TARGET_FLOW, CorePackage.FLOW__TARGET);
		}
		return targetFlow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Flow> getSourceFlow() {
		if (sourceFlow == null) {
			sourceFlow = new EObjectWithInverseResolvingEList.ManyInverse<Flow>(Flow.class, this, CorePackage.MODEL_ELEMENT__SOURCE_FLOW, CorePackage.FLOW__SOURCE);
		}
		return sourceFlow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TemplateParameter> getDefaultedParameter() {
		if (defaultedParameter == null) {
			defaultedParameter = new EObjectWithInverseResolvingEList<TemplateParameter>(TemplateParameter.class, this, CorePackage.MODEL_ELEMENT__DEFAULTED_PARAMETER, CorePackage.TEMPLATE_PARAMETER__DEFAULT_ELEMENT);
		}
		return defaultedParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Comment> getComment() {
		if (comment == null) {
			comment = new EObjectWithInverseResolvingEList.ManyInverse<Comment>(Comment.class, this, CorePackage.MODEL_ELEMENT__COMMENT, CorePackage.COMMENT__ANNOTATED_ELEMENT);
		}
		return comment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElementResidence> getElementResidence() {
		if (elementResidence == null) {
			elementResidence = new EObjectWithInverseResolvingEList<ElementResidence>(ElementResidence.class, this, CorePackage.MODEL_ELEMENT__ELEMENT_RESIDENCE, CorePackage.ELEMENT_RESIDENCE__RESIDENT);
		}
		return elementResidence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TemplateParameter> getTemplateParameter() {
		if (templateParameter == null) {
			templateParameter = new EObjectContainmentWithInverseEList<TemplateParameter>(TemplateParameter.class, this, CorePackage.MODEL_ELEMENT__TEMPLATE_PARAMETER, CorePackage.TEMPLATE_PARAMETER__TEMPLATE);
		}
		return templateParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemplateParameter getParameterTemplate() {
		if (eContainerFeatureID() != CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE) return null;
		return (TemplateParameter)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameterTemplate(TemplateParameter newParameterTemplate, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParameterTemplate, CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterTemplate(TemplateParameter newParameterTemplate) {
		if (newParameterTemplate != eInternalContainer() || (eContainerFeatureID() != CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE && newParameterTemplate != null)) {
			if (EcoreUtil.isAncestor(this, newParameterTemplate))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParameterTemplate != null)
				msgs = ((InternalEObject)newParameterTemplate).eInverseAdd(this, CorePackage.TEMPLATE_PARAMETER__PARAMETER, TemplateParameter.class, msgs);
			msgs = basicSetParameterTemplate(newParameterTemplate, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE, newParameterTemplate, newParameterTemplate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Stereotype> getStereotype() {
		if (stereotype == null) {
			stereotype = new EObjectWithInverseResolvingEList.ManyInverse<Stereotype>(Stereotype.class, this, CorePackage.MODEL_ELEMENT__STEREOTYPE, CorePackage.STEREOTYPE__EXTENDED_ELEMENT);
		}
		return stereotype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaggedValue> getTaggedValue() {
		if (taggedValue == null) {
			taggedValue = new EObjectContainmentWithInverseEList<TaggedValue>(TaggedValue.class, this, CorePackage.MODEL_ELEMENT__TAGGED_VALUE, CorePackage.TAGGED_VALUE__MODEL_ELEMENT);
		}
		return taggedValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaggedValue> getReferenceTag() {
		if (referenceTag == null) {
			referenceTag = new EObjectWithInverseResolvingEList.ManyInverse<TaggedValue>(TaggedValue.class, this, CorePackage.MODEL_ELEMENT__REFERENCE_TAG, CorePackage.TAGGED_VALUE__REFERENCE_VALUE);
		}
		return referenceTag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TemplateArgument> getTemplateArgument() {
		if (templateArgument == null) {
			templateArgument = new EObjectWithInverseResolvingEList<TemplateArgument>(TemplateArgument.class, this, CorePackage.MODEL_ELEMENT__TEMPLATE_ARGUMENT, CorePackage.TEMPLATE_ARGUMENT__MODEL_ELEMENT);
		}
		return templateArgument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StateMachine> getBehavior() {
		if (behavior == null) {
			behavior = new EObjectWithInverseResolvingEList<StateMachine>(StateMachine.class, this, CorePackage.MODEL_ELEMENT__BEHAVIOR, State_machinesPackage.STATE_MACHINE__CONTEXT);
		}
		return behavior;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ClassifierRole> getClassifierRole() {
		if (classifierRole == null) {
			classifierRole = new EObjectWithInverseResolvingEList.ManyInverse<ClassifierRole>(ClassifierRole.class, this, CorePackage.MODEL_ELEMENT__CLASSIFIER_ROLE, CollaborationsPackage.CLASSIFIER_ROLE__AVAILABLE_CONTENTS);
		}
		return classifierRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Collaboration> getCollaboration() {
		if (collaboration == null) {
			collaboration = new EObjectWithInverseResolvingEList.ManyInverse<Collaboration>(Collaboration.class, this, CorePackage.MODEL_ELEMENT__COLLABORATION, CollaborationsPackage.COLLABORATION__CONSTRAINING_ELEMENT);
		}
		return collaboration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CollaborationInstanceSet> getCollaborationInstanceSet() {
		if (collaborationInstanceSet == null) {
			collaborationInstanceSet = new EObjectWithInverseResolvingEList.ManyInverse<CollaborationInstanceSet>(CollaborationInstanceSet.class, this, CorePackage.MODEL_ELEMENT__COLLABORATION_INSTANCE_SET, CollaborationsPackage.COLLABORATION_INSTANCE_SET__CONSTRAINING_ELEMENT);
		}
		return collaborationInstanceSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Partition> getPartition() {
		if (partition == null) {
			partition = new EObjectWithInverseResolvingEList.ManyInverse<Partition>(Partition.class, this, CorePackage.MODEL_ELEMENT__PARTITION, Activity_graphsPackage.PARTITION__CONTENTS);
		}
		return partition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElementImport> getElementImport() {
		if (elementImport == null) {
			elementImport = new EObjectWithInverseResolvingEList<ElementImport>(ElementImport.class, this, CorePackage.MODEL_ELEMENT__ELEMENT_IMPORT, Model_managementPackage.ELEMENT_IMPORT__IMPORTED_ELEMENT);
		}
		return elementImport;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.MODEL_ELEMENT__NAMESPACE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetNamespace((Namespace)otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getClientDependency()).basicAdd(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__CONSTRAINT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getConstraint()).basicAdd(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSupplierDependency()).basicAdd(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__PRESENTATION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPresentation()).basicAdd(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__TARGET_FLOW:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getTargetFlow()).basicAdd(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__SOURCE_FLOW:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSourceFlow()).basicAdd(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__DEFAULTED_PARAMETER:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getDefaultedParameter()).basicAdd(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__COMMENT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getComment()).basicAdd(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__ELEMENT_RESIDENCE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getElementResidence()).basicAdd(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__TEMPLATE_PARAMETER:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getTemplateParameter()).basicAdd(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParameterTemplate((TemplateParameter)otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__STEREOTYPE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getStereotype()).basicAdd(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__TAGGED_VALUE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getTaggedValue()).basicAdd(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__REFERENCE_TAG:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getReferenceTag()).basicAdd(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__TEMPLATE_ARGUMENT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getTemplateArgument()).basicAdd(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__BEHAVIOR:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getBehavior()).basicAdd(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__CLASSIFIER_ROLE:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getClassifierRole()).basicAdd(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__COLLABORATION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getCollaboration()).basicAdd(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__COLLABORATION_INSTANCE_SET:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getCollaborationInstanceSet()).basicAdd(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__PARTITION:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPartition()).basicAdd(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__ELEMENT_IMPORT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getElementImport()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.MODEL_ELEMENT__NAMESPACE:
				return basicSetNamespace(null, msgs);
			case CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY:
				return ((InternalEList<?>)getClientDependency()).basicRemove(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__CONSTRAINT:
				return ((InternalEList<?>)getConstraint()).basicRemove(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY:
				return ((InternalEList<?>)getSupplierDependency()).basicRemove(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__PRESENTATION:
				return ((InternalEList<?>)getPresentation()).basicRemove(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__TARGET_FLOW:
				return ((InternalEList<?>)getTargetFlow()).basicRemove(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__SOURCE_FLOW:
				return ((InternalEList<?>)getSourceFlow()).basicRemove(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__DEFAULTED_PARAMETER:
				return ((InternalEList<?>)getDefaultedParameter()).basicRemove(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__COMMENT:
				return ((InternalEList<?>)getComment()).basicRemove(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__ELEMENT_RESIDENCE:
				return ((InternalEList<?>)getElementResidence()).basicRemove(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__TEMPLATE_PARAMETER:
				return ((InternalEList<?>)getTemplateParameter()).basicRemove(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE:
				return basicSetParameterTemplate(null, msgs);
			case CorePackage.MODEL_ELEMENT__STEREOTYPE:
				return ((InternalEList<?>)getStereotype()).basicRemove(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__TAGGED_VALUE:
				return ((InternalEList<?>)getTaggedValue()).basicRemove(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__REFERENCE_TAG:
				return ((InternalEList<?>)getReferenceTag()).basicRemove(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__TEMPLATE_ARGUMENT:
				return ((InternalEList<?>)getTemplateArgument()).basicRemove(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__BEHAVIOR:
				return ((InternalEList<?>)getBehavior()).basicRemove(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__CLASSIFIER_ROLE:
				return ((InternalEList<?>)getClassifierRole()).basicRemove(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__COLLABORATION:
				return ((InternalEList<?>)getCollaboration()).basicRemove(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__COLLABORATION_INSTANCE_SET:
				return ((InternalEList<?>)getCollaborationInstanceSet()).basicRemove(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__PARTITION:
				return ((InternalEList<?>)getPartition()).basicRemove(otherEnd, msgs);
			case CorePackage.MODEL_ELEMENT__ELEMENT_IMPORT:
				return ((InternalEList<?>)getElementImport()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case CorePackage.MODEL_ELEMENT__NAMESPACE:
				return eInternalContainer().eInverseRemove(this, CorePackage.NAMESPACE__OWNED_ELEMENT, Namespace.class, msgs);
			case CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE:
				return eInternalContainer().eInverseRemove(this, CorePackage.TEMPLATE_PARAMETER__PARAMETER, TemplateParameter.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CorePackage.MODEL_ELEMENT__NAME:
				return getName();
			case CorePackage.MODEL_ELEMENT__VISIBILITY:
				return getVisibility();
			case CorePackage.MODEL_ELEMENT__IS_SPECIFICATION:
				return isIsSpecification();
			case CorePackage.MODEL_ELEMENT__NAMESPACE:
				return getNamespace();
			case CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY:
				return getClientDependency();
			case CorePackage.MODEL_ELEMENT__CONSTRAINT:
				return getConstraint();
			case CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY:
				return getSupplierDependency();
			case CorePackage.MODEL_ELEMENT__PRESENTATION:
				return getPresentation();
			case CorePackage.MODEL_ELEMENT__TARGET_FLOW:
				return getTargetFlow();
			case CorePackage.MODEL_ELEMENT__SOURCE_FLOW:
				return getSourceFlow();
			case CorePackage.MODEL_ELEMENT__DEFAULTED_PARAMETER:
				return getDefaultedParameter();
			case CorePackage.MODEL_ELEMENT__COMMENT:
				return getComment();
			case CorePackage.MODEL_ELEMENT__ELEMENT_RESIDENCE:
				return getElementResidence();
			case CorePackage.MODEL_ELEMENT__TEMPLATE_PARAMETER:
				return getTemplateParameter();
			case CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE:
				return getParameterTemplate();
			case CorePackage.MODEL_ELEMENT__STEREOTYPE:
				return getStereotype();
			case CorePackage.MODEL_ELEMENT__TAGGED_VALUE:
				return getTaggedValue();
			case CorePackage.MODEL_ELEMENT__REFERENCE_TAG:
				return getReferenceTag();
			case CorePackage.MODEL_ELEMENT__TEMPLATE_ARGUMENT:
				return getTemplateArgument();
			case CorePackage.MODEL_ELEMENT__BEHAVIOR:
				return getBehavior();
			case CorePackage.MODEL_ELEMENT__CLASSIFIER_ROLE:
				return getClassifierRole();
			case CorePackage.MODEL_ELEMENT__COLLABORATION:
				return getCollaboration();
			case CorePackage.MODEL_ELEMENT__COLLABORATION_INSTANCE_SET:
				return getCollaborationInstanceSet();
			case CorePackage.MODEL_ELEMENT__PARTITION:
				return getPartition();
			case CorePackage.MODEL_ELEMENT__ELEMENT_IMPORT:
				return getElementImport();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CorePackage.MODEL_ELEMENT__NAME:
				setName((String)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__VISIBILITY:
				setVisibility((VisibilityKind)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__IS_SPECIFICATION:
				setIsSpecification((Boolean)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__NAMESPACE:
				setNamespace((Namespace)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY:
				getClientDependency().clear();
				getClientDependency().addAll((Collection<? extends Dependency>)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__CONSTRAINT:
				getConstraint().clear();
				getConstraint().addAll((Collection<? extends Constraint>)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY:
				getSupplierDependency().clear();
				getSupplierDependency().addAll((Collection<? extends Dependency>)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__PRESENTATION:
				getPresentation().clear();
				getPresentation().addAll((Collection<? extends PresentationElement>)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__TARGET_FLOW:
				getTargetFlow().clear();
				getTargetFlow().addAll((Collection<? extends Flow>)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__SOURCE_FLOW:
				getSourceFlow().clear();
				getSourceFlow().addAll((Collection<? extends Flow>)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__DEFAULTED_PARAMETER:
				getDefaultedParameter().clear();
				getDefaultedParameter().addAll((Collection<? extends TemplateParameter>)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__COMMENT:
				getComment().clear();
				getComment().addAll((Collection<? extends Comment>)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__ELEMENT_RESIDENCE:
				getElementResidence().clear();
				getElementResidence().addAll((Collection<? extends ElementResidence>)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__TEMPLATE_PARAMETER:
				getTemplateParameter().clear();
				getTemplateParameter().addAll((Collection<? extends TemplateParameter>)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE:
				setParameterTemplate((TemplateParameter)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__STEREOTYPE:
				getStereotype().clear();
				getStereotype().addAll((Collection<? extends Stereotype>)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__TAGGED_VALUE:
				getTaggedValue().clear();
				getTaggedValue().addAll((Collection<? extends TaggedValue>)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__REFERENCE_TAG:
				getReferenceTag().clear();
				getReferenceTag().addAll((Collection<? extends TaggedValue>)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__TEMPLATE_ARGUMENT:
				getTemplateArgument().clear();
				getTemplateArgument().addAll((Collection<? extends TemplateArgument>)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__BEHAVIOR:
				getBehavior().clear();
				getBehavior().addAll((Collection<? extends StateMachine>)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__CLASSIFIER_ROLE:
				getClassifierRole().clear();
				getClassifierRole().addAll((Collection<? extends ClassifierRole>)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__COLLABORATION:
				getCollaboration().clear();
				getCollaboration().addAll((Collection<? extends Collaboration>)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__COLLABORATION_INSTANCE_SET:
				getCollaborationInstanceSet().clear();
				getCollaborationInstanceSet().addAll((Collection<? extends CollaborationInstanceSet>)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__PARTITION:
				getPartition().clear();
				getPartition().addAll((Collection<? extends Partition>)newValue);
				return;
			case CorePackage.MODEL_ELEMENT__ELEMENT_IMPORT:
				getElementImport().clear();
				getElementImport().addAll((Collection<? extends ElementImport>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CorePackage.MODEL_ELEMENT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case CorePackage.MODEL_ELEMENT__VISIBILITY:
				setVisibility(VISIBILITY_EDEFAULT);
				return;
			case CorePackage.MODEL_ELEMENT__IS_SPECIFICATION:
				setIsSpecification(IS_SPECIFICATION_EDEFAULT);
				return;
			case CorePackage.MODEL_ELEMENT__NAMESPACE:
				setNamespace((Namespace)null);
				return;
			case CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY:
				getClientDependency().clear();
				return;
			case CorePackage.MODEL_ELEMENT__CONSTRAINT:
				getConstraint().clear();
				return;
			case CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY:
				getSupplierDependency().clear();
				return;
			case CorePackage.MODEL_ELEMENT__PRESENTATION:
				getPresentation().clear();
				return;
			case CorePackage.MODEL_ELEMENT__TARGET_FLOW:
				getTargetFlow().clear();
				return;
			case CorePackage.MODEL_ELEMENT__SOURCE_FLOW:
				getSourceFlow().clear();
				return;
			case CorePackage.MODEL_ELEMENT__DEFAULTED_PARAMETER:
				getDefaultedParameter().clear();
				return;
			case CorePackage.MODEL_ELEMENT__COMMENT:
				getComment().clear();
				return;
			case CorePackage.MODEL_ELEMENT__ELEMENT_RESIDENCE:
				getElementResidence().clear();
				return;
			case CorePackage.MODEL_ELEMENT__TEMPLATE_PARAMETER:
				getTemplateParameter().clear();
				return;
			case CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE:
				setParameterTemplate((TemplateParameter)null);
				return;
			case CorePackage.MODEL_ELEMENT__STEREOTYPE:
				getStereotype().clear();
				return;
			case CorePackage.MODEL_ELEMENT__TAGGED_VALUE:
				getTaggedValue().clear();
				return;
			case CorePackage.MODEL_ELEMENT__REFERENCE_TAG:
				getReferenceTag().clear();
				return;
			case CorePackage.MODEL_ELEMENT__TEMPLATE_ARGUMENT:
				getTemplateArgument().clear();
				return;
			case CorePackage.MODEL_ELEMENT__BEHAVIOR:
				getBehavior().clear();
				return;
			case CorePackage.MODEL_ELEMENT__CLASSIFIER_ROLE:
				getClassifierRole().clear();
				return;
			case CorePackage.MODEL_ELEMENT__COLLABORATION:
				getCollaboration().clear();
				return;
			case CorePackage.MODEL_ELEMENT__COLLABORATION_INSTANCE_SET:
				getCollaborationInstanceSet().clear();
				return;
			case CorePackage.MODEL_ELEMENT__PARTITION:
				getPartition().clear();
				return;
			case CorePackage.MODEL_ELEMENT__ELEMENT_IMPORT:
				getElementImport().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CorePackage.MODEL_ELEMENT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case CorePackage.MODEL_ELEMENT__VISIBILITY:
				return visibility != VISIBILITY_EDEFAULT;
			case CorePackage.MODEL_ELEMENT__IS_SPECIFICATION:
				return isSpecification != IS_SPECIFICATION_EDEFAULT;
			case CorePackage.MODEL_ELEMENT__NAMESPACE:
				return getNamespace() != null;
			case CorePackage.MODEL_ELEMENT__CLIENT_DEPENDENCY:
				return clientDependency != null && !clientDependency.isEmpty();
			case CorePackage.MODEL_ELEMENT__CONSTRAINT:
				return constraint != null && !constraint.isEmpty();
			case CorePackage.MODEL_ELEMENT__SUPPLIER_DEPENDENCY:
				return supplierDependency != null && !supplierDependency.isEmpty();
			case CorePackage.MODEL_ELEMENT__PRESENTATION:
				return presentation != null && !presentation.isEmpty();
			case CorePackage.MODEL_ELEMENT__TARGET_FLOW:
				return targetFlow != null && !targetFlow.isEmpty();
			case CorePackage.MODEL_ELEMENT__SOURCE_FLOW:
				return sourceFlow != null && !sourceFlow.isEmpty();
			case CorePackage.MODEL_ELEMENT__DEFAULTED_PARAMETER:
				return defaultedParameter != null && !defaultedParameter.isEmpty();
			case CorePackage.MODEL_ELEMENT__COMMENT:
				return comment != null && !comment.isEmpty();
			case CorePackage.MODEL_ELEMENT__ELEMENT_RESIDENCE:
				return elementResidence != null && !elementResidence.isEmpty();
			case CorePackage.MODEL_ELEMENT__TEMPLATE_PARAMETER:
				return templateParameter != null && !templateParameter.isEmpty();
			case CorePackage.MODEL_ELEMENT__PARAMETER_TEMPLATE:
				return getParameterTemplate() != null;
			case CorePackage.MODEL_ELEMENT__STEREOTYPE:
				return stereotype != null && !stereotype.isEmpty();
			case CorePackage.MODEL_ELEMENT__TAGGED_VALUE:
				return taggedValue != null && !taggedValue.isEmpty();
			case CorePackage.MODEL_ELEMENT__REFERENCE_TAG:
				return referenceTag != null && !referenceTag.isEmpty();
			case CorePackage.MODEL_ELEMENT__TEMPLATE_ARGUMENT:
				return templateArgument != null && !templateArgument.isEmpty();
			case CorePackage.MODEL_ELEMENT__BEHAVIOR:
				return behavior != null && !behavior.isEmpty();
			case CorePackage.MODEL_ELEMENT__CLASSIFIER_ROLE:
				return classifierRole != null && !classifierRole.isEmpty();
			case CorePackage.MODEL_ELEMENT__COLLABORATION:
				return collaboration != null && !collaboration.isEmpty();
			case CorePackage.MODEL_ELEMENT__COLLABORATION_INSTANCE_SET:
				return collaborationInstanceSet != null && !collaborationInstanceSet.isEmpty();
			case CorePackage.MODEL_ELEMENT__PARTITION:
				return partition != null && !partition.isEmpty();
			case CorePackage.MODEL_ELEMENT__ELEMENT_IMPORT:
				return elementImport != null && !elementImport.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", visibility: ");
		result.append(visibility);
		result.append(", isSpecification: ");
		result.append(isSpecification);
		result.append(')');
		return result.toString();
	}

} //ModelElementImpl
