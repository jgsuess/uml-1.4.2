/**
 * <copyright>
 * </copyright>
 *
 * $Id: Node.java,v 1.1 2012/04/23 09:31:28 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Node#getDeployedComponent <em>Deployed Component</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getNode()
 * @model
 * @generated
 */
public interface Node extends Classifier {
	/**
	 * Returns the value of the '<em><b>Deployed Component</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.Component}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.Component#getDeploymentLocation <em>Deployment Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deployed Component</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deployed Component</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getNode_DeployedComponent()
	 * @see net.jgsuess.uml14.foundation.core.Component#getDeploymentLocation
	 * @model opposite="deploymentLocation"
	 * @generated
	 */
	EList<Component> getDeployedComponent();

} // Node
