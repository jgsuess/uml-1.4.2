/**
 * <copyright>
 * </copyright>
 *
 * $Id: Flow.java,v 1.1 2012/04/23 09:31:28 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Flow</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Flow#getTarget <em>Target</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Flow#getSource <em>Source</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getFlow()
 * @model
 * @generated
 */
public interface Flow extends Relationship {
	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.ModelElement}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.ModelElement#getTargetFlow <em>Target Flow</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getFlow_Target()
	 * @see net.jgsuess.uml14.foundation.core.ModelElement#getTargetFlow
	 * @model opposite="targetFlow"
	 * @generated
	 */
	EList<ModelElement> getTarget();

	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.core.ModelElement}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.ModelElement#getSourceFlow <em>Source Flow</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getFlow_Source()
	 * @see net.jgsuess.uml14.foundation.core.ModelElement#getSourceFlow
	 * @model opposite="sourceFlow"
	 * @generated
	 */
	EList<ModelElement> getSource();

} // Flow
