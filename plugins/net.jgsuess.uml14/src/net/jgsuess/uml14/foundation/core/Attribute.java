/**
 * <copyright>
 * </copyright>
 *
 * $Id: Attribute.java,v 1.1 2012/04/23 09:31:27 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core;

import net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole;

import net.jgsuess.uml14.behavioral_elements.common_behavior.AttributeLink;

import net.jgsuess.uml14.foundation.data_types.Expression;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Attribute#getInitialValue <em>Initial Value</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Attribute#getAssociationEnd <em>Association End</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Attribute#getAttributeLink <em>Attribute Link</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.Attribute#getAssociationEndRole <em>Association End Role</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAttribute()
 * @model
 * @generated
 */
public interface Attribute extends StructuralFeature {
	/**
	 * Returns the value of the '<em><b>Initial Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Value</em>' containment reference.
	 * @see #setInitialValue(Expression)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAttribute_InitialValue()
	 * @model containment="true"
	 * @generated
	 */
	Expression getInitialValue();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Attribute#getInitialValue <em>Initial Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Value</em>' containment reference.
	 * @see #getInitialValue()
	 * @generated
	 */
	void setInitialValue(Expression value);

	/**
	 * Returns the value of the '<em><b>Association End</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.core.AssociationEnd#getQualifier <em>Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Association End</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Association End</em>' container reference.
	 * @see #setAssociationEnd(AssociationEnd)
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAttribute_AssociationEnd()
	 * @see net.jgsuess.uml14.foundation.core.AssociationEnd#getQualifier
	 * @model opposite="qualifier"
	 * @generated
	 */
	AssociationEnd getAssociationEnd();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.core.Attribute#getAssociationEnd <em>Association End</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Association End</em>' container reference.
	 * @see #getAssociationEnd()
	 * @generated
	 */
	void setAssociationEnd(AssociationEnd value);

	/**
	 * Returns the value of the '<em><b>Attribute Link</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.common_behavior.AttributeLink}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.common_behavior.AttributeLink#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute Link</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Link</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAttribute_AttributeLink()
	 * @see net.jgsuess.uml14.behavioral_elements.common_behavior.AttributeLink#getAttribute
	 * @model opposite="attribute"
	 * @generated
	 */
	EList<AttributeLink> getAttributeLink();

	/**
	 * Returns the value of the '<em><b>Association End Role</b></em>' reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole#getAvailableQualifier <em>Available Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Association End Role</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Association End Role</em>' reference list.
	 * @see net.jgsuess.uml14.foundation.core.CorePackage#getAttribute_AssociationEndRole()
	 * @see net.jgsuess.uml14.behavioral_elements.collaborations.AssociationEndRole#getAvailableQualifier
	 * @model opposite="availableQualifier"
	 * @generated
	 */
	EList<AssociationEndRole> getAssociationEndRole();

} // Attribute
