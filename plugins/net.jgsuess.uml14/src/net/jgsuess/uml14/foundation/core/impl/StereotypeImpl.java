/**
 * <copyright>
 * </copyright>
 *
 * $Id: StereotypeImpl.java,v 1.1 2012/04/23 09:31:33 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.core.impl;

import java.util.Collection;

import net.jgsuess.uml14.foundation.core.Constraint;
import net.jgsuess.uml14.foundation.core.CorePackage;
import net.jgsuess.uml14.foundation.core.ModelElement;
import net.jgsuess.uml14.foundation.core.Stereotype;
import net.jgsuess.uml14.foundation.core.TagDefinition;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Stereotype</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.StereotypeImpl#getIcon <em>Icon</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.StereotypeImpl#getBaseClass <em>Base Class</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.StereotypeImpl#getDefinedTag <em>Defined Tag</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.StereotypeImpl#getExtendedElement <em>Extended Element</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.core.impl.StereotypeImpl#getStereotypeConstraint <em>Stereotype Constraint</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class StereotypeImpl extends GeneralizableElementImpl implements Stereotype {
	/**
	 * The default value of the '{@link #getIcon() <em>Icon</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIcon()
	 * @generated
	 * @ordered
	 */
	protected static final String ICON_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIcon() <em>Icon</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIcon()
	 * @generated
	 * @ordered
	 */
	protected String icon = ICON_EDEFAULT;

	/**
	 * The default value of the '{@link #getBaseClass() <em>Base Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseClass()
	 * @generated
	 * @ordered
	 */
	protected static final String BASE_CLASS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBaseClass() <em>Base Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseClass()
	 * @generated
	 * @ordered
	 */
	protected String baseClass = BASE_CLASS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDefinedTag() <em>Defined Tag</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefinedTag()
	 * @generated
	 * @ordered
	 */
	protected EList<TagDefinition> definedTag;

	/**
	 * The cached value of the '{@link #getExtendedElement() <em>Extended Element</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtendedElement()
	 * @generated
	 * @ordered
	 */
	protected EList<ModelElement> extendedElement;

	/**
	 * The cached value of the '{@link #getStereotypeConstraint() <em>Stereotype Constraint</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStereotypeConstraint()
	 * @generated
	 * @ordered
	 */
	protected EList<Constraint> stereotypeConstraint;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StereotypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.STEREOTYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIcon(String newIcon) {
		String oldIcon = icon;
		icon = newIcon;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.STEREOTYPE__ICON, oldIcon, icon));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBaseClass() {
		return baseClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBaseClass(String newBaseClass) {
		String oldBaseClass = baseClass;
		baseClass = newBaseClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.STEREOTYPE__BASE_CLASS, oldBaseClass, baseClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TagDefinition> getDefinedTag() {
		if (definedTag == null) {
			definedTag = new EObjectContainmentWithInverseEList<TagDefinition>(TagDefinition.class, this, CorePackage.STEREOTYPE__DEFINED_TAG, CorePackage.TAG_DEFINITION__OWNER);
		}
		return definedTag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModelElement> getExtendedElement() {
		if (extendedElement == null) {
			extendedElement = new EObjectWithInverseResolvingEList.ManyInverse<ModelElement>(ModelElement.class, this, CorePackage.STEREOTYPE__EXTENDED_ELEMENT, CorePackage.MODEL_ELEMENT__STEREOTYPE);
		}
		return extendedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Constraint> getStereotypeConstraint() {
		if (stereotypeConstraint == null) {
			stereotypeConstraint = new EObjectContainmentWithInverseEList<Constraint>(Constraint.class, this, CorePackage.STEREOTYPE__STEREOTYPE_CONSTRAINT, CorePackage.CONSTRAINT__CONSTRAINED_STEREOTYPE);
		}
		return stereotypeConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.STEREOTYPE__DEFINED_TAG:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getDefinedTag()).basicAdd(otherEnd, msgs);
			case CorePackage.STEREOTYPE__EXTENDED_ELEMENT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getExtendedElement()).basicAdd(otherEnd, msgs);
			case CorePackage.STEREOTYPE__STEREOTYPE_CONSTRAINT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getStereotypeConstraint()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.STEREOTYPE__DEFINED_TAG:
				return ((InternalEList<?>)getDefinedTag()).basicRemove(otherEnd, msgs);
			case CorePackage.STEREOTYPE__EXTENDED_ELEMENT:
				return ((InternalEList<?>)getExtendedElement()).basicRemove(otherEnd, msgs);
			case CorePackage.STEREOTYPE__STEREOTYPE_CONSTRAINT:
				return ((InternalEList<?>)getStereotypeConstraint()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CorePackage.STEREOTYPE__ICON:
				return getIcon();
			case CorePackage.STEREOTYPE__BASE_CLASS:
				return getBaseClass();
			case CorePackage.STEREOTYPE__DEFINED_TAG:
				return getDefinedTag();
			case CorePackage.STEREOTYPE__EXTENDED_ELEMENT:
				return getExtendedElement();
			case CorePackage.STEREOTYPE__STEREOTYPE_CONSTRAINT:
				return getStereotypeConstraint();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CorePackage.STEREOTYPE__ICON:
				setIcon((String)newValue);
				return;
			case CorePackage.STEREOTYPE__BASE_CLASS:
				setBaseClass((String)newValue);
				return;
			case CorePackage.STEREOTYPE__DEFINED_TAG:
				getDefinedTag().clear();
				getDefinedTag().addAll((Collection<? extends TagDefinition>)newValue);
				return;
			case CorePackage.STEREOTYPE__EXTENDED_ELEMENT:
				getExtendedElement().clear();
				getExtendedElement().addAll((Collection<? extends ModelElement>)newValue);
				return;
			case CorePackage.STEREOTYPE__STEREOTYPE_CONSTRAINT:
				getStereotypeConstraint().clear();
				getStereotypeConstraint().addAll((Collection<? extends Constraint>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CorePackage.STEREOTYPE__ICON:
				setIcon(ICON_EDEFAULT);
				return;
			case CorePackage.STEREOTYPE__BASE_CLASS:
				setBaseClass(BASE_CLASS_EDEFAULT);
				return;
			case CorePackage.STEREOTYPE__DEFINED_TAG:
				getDefinedTag().clear();
				return;
			case CorePackage.STEREOTYPE__EXTENDED_ELEMENT:
				getExtendedElement().clear();
				return;
			case CorePackage.STEREOTYPE__STEREOTYPE_CONSTRAINT:
				getStereotypeConstraint().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CorePackage.STEREOTYPE__ICON:
				return ICON_EDEFAULT == null ? icon != null : !ICON_EDEFAULT.equals(icon);
			case CorePackage.STEREOTYPE__BASE_CLASS:
				return BASE_CLASS_EDEFAULT == null ? baseClass != null : !BASE_CLASS_EDEFAULT.equals(baseClass);
			case CorePackage.STEREOTYPE__DEFINED_TAG:
				return definedTag != null && !definedTag.isEmpty();
			case CorePackage.STEREOTYPE__EXTENDED_ELEMENT:
				return extendedElement != null && !extendedElement.isEmpty();
			case CorePackage.STEREOTYPE__STEREOTYPE_CONSTRAINT:
				return stereotypeConstraint != null && !stereotypeConstraint.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (icon: ");
		result.append(icon);
		result.append(", baseClass: ");
		result.append(baseClass);
		result.append(')');
		return result.toString();
	}

} //StereotypeImpl
