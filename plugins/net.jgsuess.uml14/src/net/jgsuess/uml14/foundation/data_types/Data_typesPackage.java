/**
 * <copyright>
 * </copyright>
 *
 * $Id: Data_typesPackage.java,v 1.1 2012/04/23 09:31:29 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.data_types;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see net.jgsuess.uml14.foundation.data_types.Data_typesFactory
 * @model kind="package"
 * @generated
 */
public interface Data_typesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "data_types";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://jgsuess.net/uml14/data_types";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "data_types";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Data_typesPackage eINSTANCE = net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl.init();

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.foundation.data_types.impl.MultiplicityImpl <em>Multiplicity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.impl.MultiplicityImpl
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getMultiplicity()
	 * @generated
	 */
	int MULTIPLICITY = 0;

	/**
	 * The feature id for the '<em><b>Range</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICITY__RANGE = 0;

	/**
	 * The number of structural features of the '<em>Multiplicity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.foundation.data_types.impl.MultiplicityRangeImpl <em>Multiplicity Range</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.impl.MultiplicityRangeImpl
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getMultiplicityRange()
	 * @generated
	 */
	int MULTIPLICITY_RANGE = 1;

	/**
	 * The feature id for the '<em><b>Lower</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICITY_RANGE__LOWER = 0;

	/**
	 * The feature id for the '<em><b>Upper</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICITY_RANGE__UPPER = 1;

	/**
	 * The feature id for the '<em><b>Multiplicity</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICITY_RANGE__MULTIPLICITY = 2;

	/**
	 * The number of structural features of the '<em>Multiplicity Range</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICITY_RANGE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.foundation.data_types.impl.ExpressionImpl <em>Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.impl.ExpressionImpl
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getExpression()
	 * @generated
	 */
	int EXPRESSION = 2;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__LANGUAGE = 0;

	/**
	 * The feature id for the '<em><b>Body</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__BODY = 1;

	/**
	 * The number of structural features of the '<em>Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.foundation.data_types.impl.BooleanExpressionImpl <em>Boolean Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.impl.BooleanExpressionImpl
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getBooleanExpression()
	 * @generated
	 */
	int BOOLEAN_EXPRESSION = 3;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_EXPRESSION__LANGUAGE = EXPRESSION__LANGUAGE;

	/**
	 * The feature id for the '<em><b>Body</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_EXPRESSION__BODY = EXPRESSION__BODY;

	/**
	 * The number of structural features of the '<em>Boolean Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.foundation.data_types.impl.TypeExpressionImpl <em>Type Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.impl.TypeExpressionImpl
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getTypeExpression()
	 * @generated
	 */
	int TYPE_EXPRESSION = 4;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_EXPRESSION__LANGUAGE = EXPRESSION__LANGUAGE;

	/**
	 * The feature id for the '<em><b>Body</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_EXPRESSION__BODY = EXPRESSION__BODY;

	/**
	 * The number of structural features of the '<em>Type Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.foundation.data_types.impl.MappingExpressionImpl <em>Mapping Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.impl.MappingExpressionImpl
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getMappingExpression()
	 * @generated
	 */
	int MAPPING_EXPRESSION = 5;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPING_EXPRESSION__LANGUAGE = EXPRESSION__LANGUAGE;

	/**
	 * The feature id for the '<em><b>Body</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPING_EXPRESSION__BODY = EXPRESSION__BODY;

	/**
	 * The number of structural features of the '<em>Mapping Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPING_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.foundation.data_types.impl.ProcedureExpressionImpl <em>Procedure Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.impl.ProcedureExpressionImpl
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getProcedureExpression()
	 * @generated
	 */
	int PROCEDURE_EXPRESSION = 6;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_EXPRESSION__LANGUAGE = EXPRESSION__LANGUAGE;

	/**
	 * The feature id for the '<em><b>Body</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_EXPRESSION__BODY = EXPRESSION__BODY;

	/**
	 * The number of structural features of the '<em>Procedure Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCEDURE_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.foundation.data_types.impl.ObjectSetExpressionImpl <em>Object Set Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.impl.ObjectSetExpressionImpl
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getObjectSetExpression()
	 * @generated
	 */
	int OBJECT_SET_EXPRESSION = 7;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SET_EXPRESSION__LANGUAGE = EXPRESSION__LANGUAGE;

	/**
	 * The feature id for the '<em><b>Body</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SET_EXPRESSION__BODY = EXPRESSION__BODY;

	/**
	 * The number of structural features of the '<em>Object Set Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_SET_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.foundation.data_types.impl.ActionExpressionImpl <em>Action Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.impl.ActionExpressionImpl
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getActionExpression()
	 * @generated
	 */
	int ACTION_EXPRESSION = 8;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_EXPRESSION__LANGUAGE = EXPRESSION__LANGUAGE;

	/**
	 * The feature id for the '<em><b>Body</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_EXPRESSION__BODY = EXPRESSION__BODY;

	/**
	 * The number of structural features of the '<em>Action Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.foundation.data_types.impl.IterationExpressionImpl <em>Iteration Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.impl.IterationExpressionImpl
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getIterationExpression()
	 * @generated
	 */
	int ITERATION_EXPRESSION = 9;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATION_EXPRESSION__LANGUAGE = EXPRESSION__LANGUAGE;

	/**
	 * The feature id for the '<em><b>Body</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATION_EXPRESSION__BODY = EXPRESSION__BODY;

	/**
	 * The number of structural features of the '<em>Iteration Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATION_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.foundation.data_types.impl.TimeExpressionImpl <em>Time Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.impl.TimeExpressionImpl
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getTimeExpression()
	 * @generated
	 */
	int TIME_EXPRESSION = 10;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EXPRESSION__LANGUAGE = EXPRESSION__LANGUAGE;

	/**
	 * The feature id for the '<em><b>Body</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EXPRESSION__BODY = EXPRESSION__BODY;

	/**
	 * The number of structural features of the '<em>Time Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.foundation.data_types.impl.ArgListsExpressionImpl <em>Arg Lists Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.impl.ArgListsExpressionImpl
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getArgListsExpression()
	 * @generated
	 */
	int ARG_LISTS_EXPRESSION = 11;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARG_LISTS_EXPRESSION__LANGUAGE = EXPRESSION__LANGUAGE;

	/**
	 * The feature id for the '<em><b>Body</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARG_LISTS_EXPRESSION__BODY = EXPRESSION__BODY;

	/**
	 * The number of structural features of the '<em>Arg Lists Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARG_LISTS_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.foundation.data_types.AggregationKind <em>Aggregation Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.AggregationKind
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getAggregationKind()
	 * @generated
	 */
	int AGGREGATION_KIND = 12;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.foundation.data_types.CallConcurrencyKind <em>Call Concurrency Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.CallConcurrencyKind
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getCallConcurrencyKind()
	 * @generated
	 */
	int CALL_CONCURRENCY_KIND = 13;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.foundation.data_types.ChangeableKind <em>Changeable Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.ChangeableKind
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getChangeableKind()
	 * @generated
	 */
	int CHANGEABLE_KIND = 14;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.foundation.data_types.OrderingKind <em>Ordering Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.OrderingKind
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getOrderingKind()
	 * @generated
	 */
	int ORDERING_KIND = 15;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.foundation.data_types.ParameterDirectionKind <em>Parameter Direction Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.ParameterDirectionKind
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getParameterDirectionKind()
	 * @generated
	 */
	int PARAMETER_DIRECTION_KIND = 16;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.foundation.data_types.ScopeKind <em>Scope Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.ScopeKind
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getScopeKind()
	 * @generated
	 */
	int SCOPE_KIND = 17;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.foundation.data_types.VisibilityKind <em>Visibility Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.VisibilityKind
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getVisibilityKind()
	 * @generated
	 */
	int VISIBILITY_KIND = 18;

	/**
	 * The meta object id for the '{@link net.jgsuess.uml14.foundation.data_types.PseudostateKind <em>Pseudostate Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.PseudostateKind
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getPseudostateKind()
	 * @generated
	 */
	int PSEUDOSTATE_KIND = 19;

	/**
	 * The meta object id for the '<em>Integer</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getInteger()
	 * @generated
	 */
	int INTEGER = 20;

	/**
	 * The meta object id for the '<em>Unlimited Integer</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getUnlimitedInteger()
	 * @generated
	 */
	int UNLIMITED_INTEGER = 21;

	/**
	 * The meta object id for the '<em>String</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getString()
	 * @generated
	 */
	int STRING = 22;

	/**
	 * The meta object id for the '<em>Boolean</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getBoolean()
	 * @generated
	 */
	int BOOLEAN = 23;

	/**
	 * The meta object id for the '<em>Name</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getName_()
	 * @generated
	 */
	int NAME = 24;

	/**
	 * The meta object id for the '<em>Location Reference</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getLocationReference()
	 * @generated
	 */
	int LOCATION_REFERENCE = 25;

	/**
	 * The meta object id for the '<em>Geometry</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getGeometry()
	 * @generated
	 */
	int GEOMETRY = 26;


	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.foundation.data_types.Multiplicity <em>Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multiplicity</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.Multiplicity
	 * @generated
	 */
	EClass getMultiplicity();

	/**
	 * Returns the meta object for the containment reference list '{@link net.jgsuess.uml14.foundation.data_types.Multiplicity#getRange <em>Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Range</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.Multiplicity#getRange()
	 * @see #getMultiplicity()
	 * @generated
	 */
	EReference getMultiplicity_Range();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.foundation.data_types.MultiplicityRange <em>Multiplicity Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multiplicity Range</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.MultiplicityRange
	 * @generated
	 */
	EClass getMultiplicityRange();

	/**
	 * Returns the meta object for the attribute '{@link net.jgsuess.uml14.foundation.data_types.MultiplicityRange#getLower <em>Lower</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lower</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.MultiplicityRange#getLower()
	 * @see #getMultiplicityRange()
	 * @generated
	 */
	EAttribute getMultiplicityRange_Lower();

	/**
	 * Returns the meta object for the attribute '{@link net.jgsuess.uml14.foundation.data_types.MultiplicityRange#getUpper <em>Upper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Upper</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.MultiplicityRange#getUpper()
	 * @see #getMultiplicityRange()
	 * @generated
	 */
	EAttribute getMultiplicityRange_Upper();

	/**
	 * Returns the meta object for the container reference '{@link net.jgsuess.uml14.foundation.data_types.MultiplicityRange#getMultiplicity <em>Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Multiplicity</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.MultiplicityRange#getMultiplicity()
	 * @see #getMultiplicityRange()
	 * @generated
	 */
	EReference getMultiplicityRange_Multiplicity();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.foundation.data_types.Expression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.Expression
	 * @generated
	 */
	EClass getExpression();

	/**
	 * Returns the meta object for the attribute '{@link net.jgsuess.uml14.foundation.data_types.Expression#getLanguage <em>Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Language</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.Expression#getLanguage()
	 * @see #getExpression()
	 * @generated
	 */
	EAttribute getExpression_Language();

	/**
	 * Returns the meta object for the attribute '{@link net.jgsuess.uml14.foundation.data_types.Expression#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Body</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.Expression#getBody()
	 * @see #getExpression()
	 * @generated
	 */
	EAttribute getExpression_Body();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.foundation.data_types.BooleanExpression <em>Boolean Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Expression</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.BooleanExpression
	 * @generated
	 */
	EClass getBooleanExpression();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.foundation.data_types.TypeExpression <em>Type Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Expression</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.TypeExpression
	 * @generated
	 */
	EClass getTypeExpression();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.foundation.data_types.MappingExpression <em>Mapping Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mapping Expression</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.MappingExpression
	 * @generated
	 */
	EClass getMappingExpression();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.foundation.data_types.ProcedureExpression <em>Procedure Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Procedure Expression</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.ProcedureExpression
	 * @generated
	 */
	EClass getProcedureExpression();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.foundation.data_types.ObjectSetExpression <em>Object Set Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Set Expression</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.ObjectSetExpression
	 * @generated
	 */
	EClass getObjectSetExpression();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.foundation.data_types.ActionExpression <em>Action Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action Expression</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.ActionExpression
	 * @generated
	 */
	EClass getActionExpression();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.foundation.data_types.IterationExpression <em>Iteration Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Iteration Expression</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.IterationExpression
	 * @generated
	 */
	EClass getIterationExpression();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.foundation.data_types.TimeExpression <em>Time Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time Expression</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.TimeExpression
	 * @generated
	 */
	EClass getTimeExpression();

	/**
	 * Returns the meta object for class '{@link net.jgsuess.uml14.foundation.data_types.ArgListsExpression <em>Arg Lists Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Arg Lists Expression</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.ArgListsExpression
	 * @generated
	 */
	EClass getArgListsExpression();

	/**
	 * Returns the meta object for enum '{@link net.jgsuess.uml14.foundation.data_types.AggregationKind <em>Aggregation Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Aggregation Kind</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.AggregationKind
	 * @generated
	 */
	EEnum getAggregationKind();

	/**
	 * Returns the meta object for enum '{@link net.jgsuess.uml14.foundation.data_types.CallConcurrencyKind <em>Call Concurrency Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Call Concurrency Kind</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.CallConcurrencyKind
	 * @generated
	 */
	EEnum getCallConcurrencyKind();

	/**
	 * Returns the meta object for enum '{@link net.jgsuess.uml14.foundation.data_types.ChangeableKind <em>Changeable Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Changeable Kind</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.ChangeableKind
	 * @generated
	 */
	EEnum getChangeableKind();

	/**
	 * Returns the meta object for enum '{@link net.jgsuess.uml14.foundation.data_types.OrderingKind <em>Ordering Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Ordering Kind</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.OrderingKind
	 * @generated
	 */
	EEnum getOrderingKind();

	/**
	 * Returns the meta object for enum '{@link net.jgsuess.uml14.foundation.data_types.ParameterDirectionKind <em>Parameter Direction Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Parameter Direction Kind</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.ParameterDirectionKind
	 * @generated
	 */
	EEnum getParameterDirectionKind();

	/**
	 * Returns the meta object for enum '{@link net.jgsuess.uml14.foundation.data_types.ScopeKind <em>Scope Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Scope Kind</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.ScopeKind
	 * @generated
	 */
	EEnum getScopeKind();

	/**
	 * Returns the meta object for enum '{@link net.jgsuess.uml14.foundation.data_types.VisibilityKind <em>Visibility Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Visibility Kind</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.VisibilityKind
	 * @generated
	 */
	EEnum getVisibilityKind();

	/**
	 * Returns the meta object for enum '{@link net.jgsuess.uml14.foundation.data_types.PseudostateKind <em>Pseudostate Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Pseudostate Kind</em>'.
	 * @see net.jgsuess.uml14.foundation.data_types.PseudostateKind
	 * @generated
	 */
	EEnum getPseudostateKind();

	/**
	 * Returns the meta object for data type '<em>Integer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Integer</em>'.
	 * @model instanceClass="long"
	 * @generated
	 */
	EDataType getInteger();

	/**
	 * Returns the meta object for data type '<em>Unlimited Integer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Unlimited Integer</em>'.
	 * @model instanceClass="long"
	 * @generated
	 */
	EDataType getUnlimitedInteger();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>String</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getString();

	/**
	 * Returns the meta object for data type '<em>Boolean</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Boolean</em>'.
	 * @model instanceClass="boolean"
	 * @generated
	 */
	EDataType getBoolean();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Name</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getName_();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Location Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Location Reference</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getLocationReference();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Geometry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Geometry</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getGeometry();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Data_typesFactory getData_typesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.foundation.data_types.impl.MultiplicityImpl <em>Multiplicity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.impl.MultiplicityImpl
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getMultiplicity()
		 * @generated
		 */
		EClass MULTIPLICITY = eINSTANCE.getMultiplicity();

		/**
		 * The meta object literal for the '<em><b>Range</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MULTIPLICITY__RANGE = eINSTANCE.getMultiplicity_Range();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.foundation.data_types.impl.MultiplicityRangeImpl <em>Multiplicity Range</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.impl.MultiplicityRangeImpl
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getMultiplicityRange()
		 * @generated
		 */
		EClass MULTIPLICITY_RANGE = eINSTANCE.getMultiplicityRange();

		/**
		 * The meta object literal for the '<em><b>Lower</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MULTIPLICITY_RANGE__LOWER = eINSTANCE.getMultiplicityRange_Lower();

		/**
		 * The meta object literal for the '<em><b>Upper</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MULTIPLICITY_RANGE__UPPER = eINSTANCE.getMultiplicityRange_Upper();

		/**
		 * The meta object literal for the '<em><b>Multiplicity</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MULTIPLICITY_RANGE__MULTIPLICITY = eINSTANCE.getMultiplicityRange_Multiplicity();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.foundation.data_types.impl.ExpressionImpl <em>Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.impl.ExpressionImpl
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getExpression()
		 * @generated
		 */
		EClass EXPRESSION = eINSTANCE.getExpression();

		/**
		 * The meta object literal for the '<em><b>Language</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXPRESSION__LANGUAGE = eINSTANCE.getExpression_Language();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXPRESSION__BODY = eINSTANCE.getExpression_Body();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.foundation.data_types.impl.BooleanExpressionImpl <em>Boolean Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.impl.BooleanExpressionImpl
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getBooleanExpression()
		 * @generated
		 */
		EClass BOOLEAN_EXPRESSION = eINSTANCE.getBooleanExpression();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.foundation.data_types.impl.TypeExpressionImpl <em>Type Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.impl.TypeExpressionImpl
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getTypeExpression()
		 * @generated
		 */
		EClass TYPE_EXPRESSION = eINSTANCE.getTypeExpression();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.foundation.data_types.impl.MappingExpressionImpl <em>Mapping Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.impl.MappingExpressionImpl
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getMappingExpression()
		 * @generated
		 */
		EClass MAPPING_EXPRESSION = eINSTANCE.getMappingExpression();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.foundation.data_types.impl.ProcedureExpressionImpl <em>Procedure Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.impl.ProcedureExpressionImpl
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getProcedureExpression()
		 * @generated
		 */
		EClass PROCEDURE_EXPRESSION = eINSTANCE.getProcedureExpression();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.foundation.data_types.impl.ObjectSetExpressionImpl <em>Object Set Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.impl.ObjectSetExpressionImpl
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getObjectSetExpression()
		 * @generated
		 */
		EClass OBJECT_SET_EXPRESSION = eINSTANCE.getObjectSetExpression();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.foundation.data_types.impl.ActionExpressionImpl <em>Action Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.impl.ActionExpressionImpl
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getActionExpression()
		 * @generated
		 */
		EClass ACTION_EXPRESSION = eINSTANCE.getActionExpression();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.foundation.data_types.impl.IterationExpressionImpl <em>Iteration Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.impl.IterationExpressionImpl
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getIterationExpression()
		 * @generated
		 */
		EClass ITERATION_EXPRESSION = eINSTANCE.getIterationExpression();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.foundation.data_types.impl.TimeExpressionImpl <em>Time Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.impl.TimeExpressionImpl
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getTimeExpression()
		 * @generated
		 */
		EClass TIME_EXPRESSION = eINSTANCE.getTimeExpression();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.foundation.data_types.impl.ArgListsExpressionImpl <em>Arg Lists Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.impl.ArgListsExpressionImpl
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getArgListsExpression()
		 * @generated
		 */
		EClass ARG_LISTS_EXPRESSION = eINSTANCE.getArgListsExpression();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.foundation.data_types.AggregationKind <em>Aggregation Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.AggregationKind
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getAggregationKind()
		 * @generated
		 */
		EEnum AGGREGATION_KIND = eINSTANCE.getAggregationKind();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.foundation.data_types.CallConcurrencyKind <em>Call Concurrency Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.CallConcurrencyKind
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getCallConcurrencyKind()
		 * @generated
		 */
		EEnum CALL_CONCURRENCY_KIND = eINSTANCE.getCallConcurrencyKind();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.foundation.data_types.ChangeableKind <em>Changeable Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.ChangeableKind
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getChangeableKind()
		 * @generated
		 */
		EEnum CHANGEABLE_KIND = eINSTANCE.getChangeableKind();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.foundation.data_types.OrderingKind <em>Ordering Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.OrderingKind
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getOrderingKind()
		 * @generated
		 */
		EEnum ORDERING_KIND = eINSTANCE.getOrderingKind();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.foundation.data_types.ParameterDirectionKind <em>Parameter Direction Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.ParameterDirectionKind
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getParameterDirectionKind()
		 * @generated
		 */
		EEnum PARAMETER_DIRECTION_KIND = eINSTANCE.getParameterDirectionKind();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.foundation.data_types.ScopeKind <em>Scope Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.ScopeKind
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getScopeKind()
		 * @generated
		 */
		EEnum SCOPE_KIND = eINSTANCE.getScopeKind();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.foundation.data_types.VisibilityKind <em>Visibility Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.VisibilityKind
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getVisibilityKind()
		 * @generated
		 */
		EEnum VISIBILITY_KIND = eINSTANCE.getVisibilityKind();

		/**
		 * The meta object literal for the '{@link net.jgsuess.uml14.foundation.data_types.PseudostateKind <em>Pseudostate Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.PseudostateKind
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getPseudostateKind()
		 * @generated
		 */
		EEnum PSEUDOSTATE_KIND = eINSTANCE.getPseudostateKind();

		/**
		 * The meta object literal for the '<em>Integer</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getInteger()
		 * @generated
		 */
		EDataType INTEGER = eINSTANCE.getInteger();

		/**
		 * The meta object literal for the '<em>Unlimited Integer</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getUnlimitedInteger()
		 * @generated
		 */
		EDataType UNLIMITED_INTEGER = eINSTANCE.getUnlimitedInteger();

		/**
		 * The meta object literal for the '<em>String</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getString()
		 * @generated
		 */
		EDataType STRING = eINSTANCE.getString();

		/**
		 * The meta object literal for the '<em>Boolean</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getBoolean()
		 * @generated
		 */
		EDataType BOOLEAN = eINSTANCE.getBoolean();

		/**
		 * The meta object literal for the '<em>Name</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getName_()
		 * @generated
		 */
		EDataType NAME = eINSTANCE.getName_();

		/**
		 * The meta object literal for the '<em>Location Reference</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getLocationReference()
		 * @generated
		 */
		EDataType LOCATION_REFERENCE = eINSTANCE.getLocationReference();

		/**
		 * The meta object literal for the '<em>Geometry</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see net.jgsuess.uml14.foundation.data_types.impl.Data_typesPackageImpl#getGeometry()
		 * @generated
		 */
		EDataType GEOMETRY = eINSTANCE.getGeometry();

	}

} //Data_typesPackage
