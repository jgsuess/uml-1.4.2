/**
 * <copyright>
 * </copyright>
 *
 * $Id: Data_typesFactory.java,v 1.1 2012/04/23 09:31:29 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.data_types;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see net.jgsuess.uml14.foundation.data_types.Data_typesPackage
 * @generated
 */
public interface Data_typesFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Data_typesFactory eINSTANCE = net.jgsuess.uml14.foundation.data_types.impl.Data_typesFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Multiplicity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Multiplicity</em>'.
	 * @generated
	 */
	Multiplicity createMultiplicity();

	/**
	 * Returns a new object of class '<em>Multiplicity Range</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Multiplicity Range</em>'.
	 * @generated
	 */
	MultiplicityRange createMultiplicityRange();

	/**
	 * Returns a new object of class '<em>Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Expression</em>'.
	 * @generated
	 */
	Expression createExpression();

	/**
	 * Returns a new object of class '<em>Boolean Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Boolean Expression</em>'.
	 * @generated
	 */
	BooleanExpression createBooleanExpression();

	/**
	 * Returns a new object of class '<em>Type Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Type Expression</em>'.
	 * @generated
	 */
	TypeExpression createTypeExpression();

	/**
	 * Returns a new object of class '<em>Mapping Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Mapping Expression</em>'.
	 * @generated
	 */
	MappingExpression createMappingExpression();

	/**
	 * Returns a new object of class '<em>Procedure Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Procedure Expression</em>'.
	 * @generated
	 */
	ProcedureExpression createProcedureExpression();

	/**
	 * Returns a new object of class '<em>Object Set Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Object Set Expression</em>'.
	 * @generated
	 */
	ObjectSetExpression createObjectSetExpression();

	/**
	 * Returns a new object of class '<em>Action Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Action Expression</em>'.
	 * @generated
	 */
	ActionExpression createActionExpression();

	/**
	 * Returns a new object of class '<em>Iteration Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Iteration Expression</em>'.
	 * @generated
	 */
	IterationExpression createIterationExpression();

	/**
	 * Returns a new object of class '<em>Time Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Time Expression</em>'.
	 * @generated
	 */
	TimeExpression createTimeExpression();

	/**
	 * Returns a new object of class '<em>Arg Lists Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Arg Lists Expression</em>'.
	 * @generated
	 */
	ArgListsExpression createArgListsExpression();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Data_typesPackage getData_typesPackage();

} //Data_typesFactory
