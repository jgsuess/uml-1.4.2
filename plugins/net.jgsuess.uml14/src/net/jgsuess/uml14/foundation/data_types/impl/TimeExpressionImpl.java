/**
 * <copyright>
 * </copyright>
 *
 * $Id: TimeExpressionImpl.java,v 1.1 2012/04/23 09:31:20 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.data_types.impl;

import net.jgsuess.uml14.foundation.data_types.Data_typesPackage;
import net.jgsuess.uml14.foundation.data_types.TimeExpression;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class TimeExpressionImpl extends ExpressionImpl implements TimeExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimeExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Data_typesPackage.Literals.TIME_EXPRESSION;
	}

} //TimeExpressionImpl
