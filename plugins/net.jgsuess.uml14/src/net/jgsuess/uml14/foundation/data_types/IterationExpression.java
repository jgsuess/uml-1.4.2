/**
 * <copyright>
 * </copyright>
 *
 * $Id: IterationExpression.java,v 1.1 2012/04/23 09:31:29 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.data_types;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Iteration Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.jgsuess.uml14.foundation.data_types.Data_typesPackage#getIterationExpression()
 * @model
 * @generated
 */
public interface IterationExpression extends Expression {
} // IterationExpression
