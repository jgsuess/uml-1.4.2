/**
 * <copyright>
 * </copyright>
 *
 * $Id: PseudostateKind.java,v 1.1 2012/04/23 09:31:29 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.data_types;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Pseudostate Kind</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see net.jgsuess.uml14.foundation.data_types.Data_typesPackage#getPseudostateKind()
 * @model
 * @generated
 */
public enum PseudostateKind implements Enumerator {
	/**
	 * The '<em><b>Pk choice</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PK_CHOICE_VALUE
	 * @generated
	 * @ordered
	 */
	PK_CHOICE(0, "pk_choice", "pk_choice"),

	/**
	 * The '<em><b>Pk deep History</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PK_DEEP_HISTORY_VALUE
	 * @generated
	 * @ordered
	 */
	PK_DEEP_HISTORY(1, "pk_deepHistory", "pk_deepHistory"),

	/**
	 * The '<em><b>Pk fork</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PK_FORK_VALUE
	 * @generated
	 * @ordered
	 */
	PK_FORK(2, "pk_fork", "pk_fork"),

	/**
	 * The '<em><b>Pk initial</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PK_INITIAL_VALUE
	 * @generated
	 * @ordered
	 */
	PK_INITIAL(3, "pk_initial", "pk_initial"),

	/**
	 * The '<em><b>Pk join</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PK_JOIN_VALUE
	 * @generated
	 * @ordered
	 */
	PK_JOIN(4, "pk_join", "pk_join"),

	/**
	 * The '<em><b>Pk junction</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PK_JUNCTION_VALUE
	 * @generated
	 * @ordered
	 */
	PK_JUNCTION(5, "pk_junction", "pk_junction"),

	/**
	 * The '<em><b>Pk shallow History</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PK_SHALLOW_HISTORY_VALUE
	 * @generated
	 * @ordered
	 */
	PK_SHALLOW_HISTORY(6, "pk_shallowHistory", "pk_shallowHistory");

	/**
	 * The '<em><b>Pk choice</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Pk choice</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PK_CHOICE
	 * @model name="pk_choice"
	 * @generated
	 * @ordered
	 */
	public static final int PK_CHOICE_VALUE = 0;

	/**
	 * The '<em><b>Pk deep History</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Pk deep History</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PK_DEEP_HISTORY
	 * @model name="pk_deepHistory"
	 * @generated
	 * @ordered
	 */
	public static final int PK_DEEP_HISTORY_VALUE = 1;

	/**
	 * The '<em><b>Pk fork</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Pk fork</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PK_FORK
	 * @model name="pk_fork"
	 * @generated
	 * @ordered
	 */
	public static final int PK_FORK_VALUE = 2;

	/**
	 * The '<em><b>Pk initial</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Pk initial</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PK_INITIAL
	 * @model name="pk_initial"
	 * @generated
	 * @ordered
	 */
	public static final int PK_INITIAL_VALUE = 3;

	/**
	 * The '<em><b>Pk join</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Pk join</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PK_JOIN
	 * @model name="pk_join"
	 * @generated
	 * @ordered
	 */
	public static final int PK_JOIN_VALUE = 4;

	/**
	 * The '<em><b>Pk junction</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Pk junction</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PK_JUNCTION
	 * @model name="pk_junction"
	 * @generated
	 * @ordered
	 */
	public static final int PK_JUNCTION_VALUE = 5;

	/**
	 * The '<em><b>Pk shallow History</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Pk shallow History</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PK_SHALLOW_HISTORY
	 * @model name="pk_shallowHistory"
	 * @generated
	 * @ordered
	 */
	public static final int PK_SHALLOW_HISTORY_VALUE = 6;

	/**
	 * An array of all the '<em><b>Pseudostate Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final PseudostateKind[] VALUES_ARRAY =
		new PseudostateKind[] {
			PK_CHOICE,
			PK_DEEP_HISTORY,
			PK_FORK,
			PK_INITIAL,
			PK_JOIN,
			PK_JUNCTION,
			PK_SHALLOW_HISTORY,
		};

	/**
	 * A public read-only list of all the '<em><b>Pseudostate Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<PseudostateKind> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Pseudostate Kind</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static PseudostateKind get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			PseudostateKind result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Pseudostate Kind</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static PseudostateKind getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			PseudostateKind result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Pseudostate Kind</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static PseudostateKind get(int value) {
		switch (value) {
			case PK_CHOICE_VALUE: return PK_CHOICE;
			case PK_DEEP_HISTORY_VALUE: return PK_DEEP_HISTORY;
			case PK_FORK_VALUE: return PK_FORK;
			case PK_INITIAL_VALUE: return PK_INITIAL;
			case PK_JOIN_VALUE: return PK_JOIN;
			case PK_JUNCTION_VALUE: return PK_JUNCTION;
			case PK_SHALLOW_HISTORY_VALUE: return PK_SHALLOW_HISTORY;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private PseudostateKind(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //PseudostateKind
