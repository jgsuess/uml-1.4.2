/**
 * <copyright>
 * </copyright>
 *
 * $Id: ArgListsExpressionImpl.java,v 1.1 2012/04/23 09:31:19 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.data_types.impl;

import net.jgsuess.uml14.foundation.data_types.ArgListsExpression;
import net.jgsuess.uml14.foundation.data_types.Data_typesPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Arg Lists Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ArgListsExpressionImpl extends ExpressionImpl implements ArgListsExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArgListsExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Data_typesPackage.Literals.ARG_LISTS_EXPRESSION;
	}

} //ArgListsExpressionImpl
