/**
 * <copyright>
 * </copyright>
 *
 * $Id: Multiplicity.java,v 1.1 2012/04/23 09:31:29 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.data_types;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multiplicity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.data_types.Multiplicity#getRange <em>Range</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.data_types.Data_typesPackage#getMultiplicity()
 * @model
 * @generated
 */
public interface Multiplicity extends EObject {
	/**
	 * Returns the value of the '<em><b>Range</b></em>' containment reference list.
	 * The list contents are of type {@link net.jgsuess.uml14.foundation.data_types.MultiplicityRange}.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.data_types.MultiplicityRange#getMultiplicity <em>Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range</em>' containment reference list.
	 * @see net.jgsuess.uml14.foundation.data_types.Data_typesPackage#getMultiplicity_Range()
	 * @see net.jgsuess.uml14.foundation.data_types.MultiplicityRange#getMultiplicity
	 * @model opposite="multiplicity" containment="true" required="true"
	 * @generated
	 */
	EList<MultiplicityRange> getRange();

} // Multiplicity
