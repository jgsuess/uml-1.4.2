/**
 * <copyright>
 * </copyright>
 *
 * $Id: MultiplicityRange.java,v 1.1 2012/04/23 09:31:30 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.data_types;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multiplicity Range</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.data_types.MultiplicityRange#getLower <em>Lower</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.data_types.MultiplicityRange#getUpper <em>Upper</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.data_types.MultiplicityRange#getMultiplicity <em>Multiplicity</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.data_types.Data_typesPackage#getMultiplicityRange()
 * @model
 * @generated
 */
public interface MultiplicityRange extends EObject {
	/**
	 * Returns the value of the '<em><b>Lower</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lower</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lower</em>' attribute.
	 * @see #setLower(long)
	 * @see net.jgsuess.uml14.foundation.data_types.Data_typesPackage#getMultiplicityRange_Lower()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Integer"
	 * @generated
	 */
	long getLower();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.data_types.MultiplicityRange#getLower <em>Lower</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lower</em>' attribute.
	 * @see #getLower()
	 * @generated
	 */
	void setLower(long value);

	/**
	 * Returns the value of the '<em><b>Upper</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Upper</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Upper</em>' attribute.
	 * @see #setUpper(long)
	 * @see net.jgsuess.uml14.foundation.data_types.Data_typesPackage#getMultiplicityRange_Upper()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.UnlimitedInteger"
	 * @generated
	 */
	long getUpper();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.data_types.MultiplicityRange#getUpper <em>Upper</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Upper</em>' attribute.
	 * @see #getUpper()
	 * @generated
	 */
	void setUpper(long value);

	/**
	 * Returns the value of the '<em><b>Multiplicity</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link net.jgsuess.uml14.foundation.data_types.Multiplicity#getRange <em>Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multiplicity</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiplicity</em>' container reference.
	 * @see #setMultiplicity(Multiplicity)
	 * @see net.jgsuess.uml14.foundation.data_types.Data_typesPackage#getMultiplicityRange_Multiplicity()
	 * @see net.jgsuess.uml14.foundation.data_types.Multiplicity#getRange
	 * @model opposite="range" required="true"
	 * @generated
	 */
	Multiplicity getMultiplicity();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.data_types.MultiplicityRange#getMultiplicity <em>Multiplicity</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Multiplicity</em>' container reference.
	 * @see #getMultiplicity()
	 * @generated
	 */
	void setMultiplicity(Multiplicity value);

} // MultiplicityRange
