/**
 * <copyright>
 * </copyright>
 *
 * $Id: Data_typesPackageImpl.java,v 1.1 2012/04/23 09:31:20 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.data_types.impl;

import net.jgsuess.uml14.Uml14Package;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.Activity_graphsPackage;

import net.jgsuess.uml14.behavioral_elements.activity_graphs.impl.Activity_graphsPackageImpl;

import net.jgsuess.uml14.behavioral_elements.collaborations.CollaborationsPackage;

import net.jgsuess.uml14.behavioral_elements.collaborations.impl.CollaborationsPackageImpl;

import net.jgsuess.uml14.behavioral_elements.common_behavior.Common_behaviorPackage;

import net.jgsuess.uml14.behavioral_elements.common_behavior.impl.Common_behaviorPackageImpl;

import net.jgsuess.uml14.behavioral_elements.state_machines.State_machinesPackage;

import net.jgsuess.uml14.behavioral_elements.state_machines.impl.State_machinesPackageImpl;

import net.jgsuess.uml14.behavioral_elements.use_cases.Use_casesPackage;

import net.jgsuess.uml14.behavioral_elements.use_cases.impl.Use_casesPackageImpl;

import net.jgsuess.uml14.foundation.core.CorePackage;

import net.jgsuess.uml14.foundation.core.impl.CorePackageImpl;

import net.jgsuess.uml14.foundation.data_types.ActionExpression;
import net.jgsuess.uml14.foundation.data_types.AggregationKind;
import net.jgsuess.uml14.foundation.data_types.ArgListsExpression;
import net.jgsuess.uml14.foundation.data_types.BooleanExpression;
import net.jgsuess.uml14.foundation.data_types.CallConcurrencyKind;
import net.jgsuess.uml14.foundation.data_types.ChangeableKind;
import net.jgsuess.uml14.foundation.data_types.Data_typesFactory;
import net.jgsuess.uml14.foundation.data_types.Data_typesPackage;
import net.jgsuess.uml14.foundation.data_types.Expression;
import net.jgsuess.uml14.foundation.data_types.IterationExpression;
import net.jgsuess.uml14.foundation.data_types.MappingExpression;
import net.jgsuess.uml14.foundation.data_types.Multiplicity;
import net.jgsuess.uml14.foundation.data_types.MultiplicityRange;
import net.jgsuess.uml14.foundation.data_types.ObjectSetExpression;
import net.jgsuess.uml14.foundation.data_types.OrderingKind;
import net.jgsuess.uml14.foundation.data_types.ParameterDirectionKind;
import net.jgsuess.uml14.foundation.data_types.ProcedureExpression;
import net.jgsuess.uml14.foundation.data_types.PseudostateKind;
import net.jgsuess.uml14.foundation.data_types.ScopeKind;
import net.jgsuess.uml14.foundation.data_types.TimeExpression;
import net.jgsuess.uml14.foundation.data_types.TypeExpression;
import net.jgsuess.uml14.foundation.data_types.VisibilityKind;

import net.jgsuess.uml14.impl.Uml14PackageImpl;

import net.jgsuess.uml14.model_management.Model_managementPackage;

import net.jgsuess.uml14.model_management.impl.Model_managementPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Data_typesPackageImpl extends EPackageImpl implements Data_typesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multiplicityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multiplicityRangeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass expressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass booleanExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typeExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mappingExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass procedureExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass objectSetExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actionExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iterationExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timeExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass argListsExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum aggregationKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum callConcurrencyKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum changeableKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum orderingKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum parameterDirectionKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum scopeKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum visibilityKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum pseudostateKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType integerEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType unlimitedIntegerEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType stringEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType booleanEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType nameEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType locationReferenceEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType geometryEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see net.jgsuess.uml14.foundation.data_types.Data_typesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Data_typesPackageImpl() {
		super(eNS_URI, Data_typesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link Data_typesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Data_typesPackage init() {
		if (isInited) return (Data_typesPackage)EPackage.Registry.INSTANCE.getEPackage(Data_typesPackage.eNS_URI);

		// Obtain or create and register package
		Data_typesPackageImpl theData_typesPackage = (Data_typesPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof Data_typesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new Data_typesPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		Uml14PackageImpl theUml14Package = (Uml14PackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Uml14Package.eNS_URI) instanceof Uml14PackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Uml14Package.eNS_URI) : Uml14Package.eINSTANCE);
		Model_managementPackageImpl theModel_managementPackage = (Model_managementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Model_managementPackage.eNS_URI) instanceof Model_managementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Model_managementPackage.eNS_URI) : Model_managementPackage.eINSTANCE);
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		Common_behaviorPackageImpl theCommon_behaviorPackage = (Common_behaviorPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Common_behaviorPackage.eNS_URI) instanceof Common_behaviorPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Common_behaviorPackage.eNS_URI) : Common_behaviorPackage.eINSTANCE);
		Use_casesPackageImpl theUse_casesPackage = (Use_casesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Use_casesPackage.eNS_URI) instanceof Use_casesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Use_casesPackage.eNS_URI) : Use_casesPackage.eINSTANCE);
		State_machinesPackageImpl theState_machinesPackage = (State_machinesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(State_machinesPackage.eNS_URI) instanceof State_machinesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(State_machinesPackage.eNS_URI) : State_machinesPackage.eINSTANCE);
		CollaborationsPackageImpl theCollaborationsPackage = (CollaborationsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CollaborationsPackage.eNS_URI) instanceof CollaborationsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CollaborationsPackage.eNS_URI) : CollaborationsPackage.eINSTANCE);
		Activity_graphsPackageImpl theActivity_graphsPackage = (Activity_graphsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Activity_graphsPackage.eNS_URI) instanceof Activity_graphsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Activity_graphsPackage.eNS_URI) : Activity_graphsPackage.eINSTANCE);

		// Create package meta-data objects
		theData_typesPackage.createPackageContents();
		theUml14Package.createPackageContents();
		theModel_managementPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theCommon_behaviorPackage.createPackageContents();
		theUse_casesPackage.createPackageContents();
		theState_machinesPackage.createPackageContents();
		theCollaborationsPackage.createPackageContents();
		theActivity_graphsPackage.createPackageContents();

		// Initialize created meta-data
		theData_typesPackage.initializePackageContents();
		theUml14Package.initializePackageContents();
		theModel_managementPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theCommon_behaviorPackage.initializePackageContents();
		theUse_casesPackage.initializePackageContents();
		theState_machinesPackage.initializePackageContents();
		theCollaborationsPackage.initializePackageContents();
		theActivity_graphsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theData_typesPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Data_typesPackage.eNS_URI, theData_typesPackage);
		return theData_typesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMultiplicity() {
		return multiplicityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMultiplicity_Range() {
		return (EReference)multiplicityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMultiplicityRange() {
		return multiplicityRangeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMultiplicityRange_Lower() {
		return (EAttribute)multiplicityRangeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMultiplicityRange_Upper() {
		return (EAttribute)multiplicityRangeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMultiplicityRange_Multiplicity() {
		return (EReference)multiplicityRangeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExpression() {
		return expressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExpression_Language() {
		return (EAttribute)expressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExpression_Body() {
		return (EAttribute)expressionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBooleanExpression() {
		return booleanExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTypeExpression() {
		return typeExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMappingExpression() {
		return mappingExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcedureExpression() {
		return procedureExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObjectSetExpression() {
		return objectSetExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActionExpression() {
		return actionExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIterationExpression() {
		return iterationExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimeExpression() {
		return timeExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getArgListsExpression() {
		return argListsExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAggregationKind() {
		return aggregationKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCallConcurrencyKind() {
		return callConcurrencyKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getChangeableKind() {
		return changeableKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getOrderingKind() {
		return orderingKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getParameterDirectionKind() {
		return parameterDirectionKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getScopeKind() {
		return scopeKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getVisibilityKind() {
		return visibilityKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getPseudostateKind() {
		return pseudostateKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getInteger() {
		return integerEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getUnlimitedInteger() {
		return unlimitedIntegerEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getString() {
		return stringEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getBoolean() {
		return booleanEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getName_() {
		return nameEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getLocationReference() {
		return locationReferenceEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getGeometry() {
		return geometryEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Data_typesFactory getData_typesFactory() {
		return (Data_typesFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		multiplicityEClass = createEClass(MULTIPLICITY);
		createEReference(multiplicityEClass, MULTIPLICITY__RANGE);

		multiplicityRangeEClass = createEClass(MULTIPLICITY_RANGE);
		createEAttribute(multiplicityRangeEClass, MULTIPLICITY_RANGE__LOWER);
		createEAttribute(multiplicityRangeEClass, MULTIPLICITY_RANGE__UPPER);
		createEReference(multiplicityRangeEClass, MULTIPLICITY_RANGE__MULTIPLICITY);

		expressionEClass = createEClass(EXPRESSION);
		createEAttribute(expressionEClass, EXPRESSION__LANGUAGE);
		createEAttribute(expressionEClass, EXPRESSION__BODY);

		booleanExpressionEClass = createEClass(BOOLEAN_EXPRESSION);

		typeExpressionEClass = createEClass(TYPE_EXPRESSION);

		mappingExpressionEClass = createEClass(MAPPING_EXPRESSION);

		procedureExpressionEClass = createEClass(PROCEDURE_EXPRESSION);

		objectSetExpressionEClass = createEClass(OBJECT_SET_EXPRESSION);

		actionExpressionEClass = createEClass(ACTION_EXPRESSION);

		iterationExpressionEClass = createEClass(ITERATION_EXPRESSION);

		timeExpressionEClass = createEClass(TIME_EXPRESSION);

		argListsExpressionEClass = createEClass(ARG_LISTS_EXPRESSION);

		// Create enums
		aggregationKindEEnum = createEEnum(AGGREGATION_KIND);
		callConcurrencyKindEEnum = createEEnum(CALL_CONCURRENCY_KIND);
		changeableKindEEnum = createEEnum(CHANGEABLE_KIND);
		orderingKindEEnum = createEEnum(ORDERING_KIND);
		parameterDirectionKindEEnum = createEEnum(PARAMETER_DIRECTION_KIND);
		scopeKindEEnum = createEEnum(SCOPE_KIND);
		visibilityKindEEnum = createEEnum(VISIBILITY_KIND);
		pseudostateKindEEnum = createEEnum(PSEUDOSTATE_KIND);

		// Create data types
		integerEDataType = createEDataType(INTEGER);
		unlimitedIntegerEDataType = createEDataType(UNLIMITED_INTEGER);
		stringEDataType = createEDataType(STRING);
		booleanEDataType = createEDataType(BOOLEAN);
		nameEDataType = createEDataType(NAME);
		locationReferenceEDataType = createEDataType(LOCATION_REFERENCE);
		geometryEDataType = createEDataType(GEOMETRY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		booleanExpressionEClass.getESuperTypes().add(this.getExpression());
		typeExpressionEClass.getESuperTypes().add(this.getExpression());
		mappingExpressionEClass.getESuperTypes().add(this.getExpression());
		procedureExpressionEClass.getESuperTypes().add(this.getExpression());
		objectSetExpressionEClass.getESuperTypes().add(this.getExpression());
		actionExpressionEClass.getESuperTypes().add(this.getExpression());
		iterationExpressionEClass.getESuperTypes().add(this.getExpression());
		timeExpressionEClass.getESuperTypes().add(this.getExpression());
		argListsExpressionEClass.getESuperTypes().add(this.getExpression());

		// Initialize classes and features; add operations and parameters
		initEClass(multiplicityEClass, Multiplicity.class, "Multiplicity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMultiplicity_Range(), this.getMultiplicityRange(), this.getMultiplicityRange_Multiplicity(), "range", null, 1, -1, Multiplicity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(multiplicityRangeEClass, MultiplicityRange.class, "MultiplicityRange", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMultiplicityRange_Lower(), this.getInteger(), "lower", null, 0, 1, MultiplicityRange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMultiplicityRange_Upper(), this.getUnlimitedInteger(), "upper", null, 0, 1, MultiplicityRange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMultiplicityRange_Multiplicity(), this.getMultiplicity(), this.getMultiplicity_Range(), "multiplicity", null, 1, 1, MultiplicityRange.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(expressionEClass, Expression.class, "Expression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExpression_Language(), this.getName_(), "language", null, 0, 1, Expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExpression_Body(), this.getString(), "body", null, 0, 1, Expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(booleanExpressionEClass, BooleanExpression.class, "BooleanExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(typeExpressionEClass, TypeExpression.class, "TypeExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(mappingExpressionEClass, MappingExpression.class, "MappingExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(procedureExpressionEClass, ProcedureExpression.class, "ProcedureExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(objectSetExpressionEClass, ObjectSetExpression.class, "ObjectSetExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(actionExpressionEClass, ActionExpression.class, "ActionExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(iterationExpressionEClass, IterationExpression.class, "IterationExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(timeExpressionEClass, TimeExpression.class, "TimeExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(argListsExpressionEClass, ArgListsExpression.class, "ArgListsExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(aggregationKindEEnum, AggregationKind.class, "AggregationKind");
		addEEnumLiteral(aggregationKindEEnum, AggregationKind.AK_NONE);
		addEEnumLiteral(aggregationKindEEnum, AggregationKind.AK_AGGREGATE);
		addEEnumLiteral(aggregationKindEEnum, AggregationKind.AK_COMPOSITE);

		initEEnum(callConcurrencyKindEEnum, CallConcurrencyKind.class, "CallConcurrencyKind");
		addEEnumLiteral(callConcurrencyKindEEnum, CallConcurrencyKind.CCK_SEQUENTIAL);
		addEEnumLiteral(callConcurrencyKindEEnum, CallConcurrencyKind.CCK_GUARDED);
		addEEnumLiteral(callConcurrencyKindEEnum, CallConcurrencyKind.CCK_CONCURRENT);

		initEEnum(changeableKindEEnum, ChangeableKind.class, "ChangeableKind");
		addEEnumLiteral(changeableKindEEnum, ChangeableKind.CK_CHANGEABLE);
		addEEnumLiteral(changeableKindEEnum, ChangeableKind.CK_FROZEN);
		addEEnumLiteral(changeableKindEEnum, ChangeableKind.CK_ADD_ONLY);

		initEEnum(orderingKindEEnum, OrderingKind.class, "OrderingKind");
		addEEnumLiteral(orderingKindEEnum, OrderingKind.OK_UNORDERED);
		addEEnumLiteral(orderingKindEEnum, OrderingKind.OK_ORDERED);

		initEEnum(parameterDirectionKindEEnum, ParameterDirectionKind.class, "ParameterDirectionKind");
		addEEnumLiteral(parameterDirectionKindEEnum, ParameterDirectionKind.PDK_IN);
		addEEnumLiteral(parameterDirectionKindEEnum, ParameterDirectionKind.PDK_INOUT);
		addEEnumLiteral(parameterDirectionKindEEnum, ParameterDirectionKind.PDK_OUT);
		addEEnumLiteral(parameterDirectionKindEEnum, ParameterDirectionKind.PDK_RETURN);

		initEEnum(scopeKindEEnum, ScopeKind.class, "ScopeKind");
		addEEnumLiteral(scopeKindEEnum, ScopeKind.SK_INSTANCE);
		addEEnumLiteral(scopeKindEEnum, ScopeKind.SK_CLASSIFIER);

		initEEnum(visibilityKindEEnum, VisibilityKind.class, "VisibilityKind");
		addEEnumLiteral(visibilityKindEEnum, VisibilityKind.VK_PUBLIC);
		addEEnumLiteral(visibilityKindEEnum, VisibilityKind.VK_PROTECTED);
		addEEnumLiteral(visibilityKindEEnum, VisibilityKind.VK_PRIVATE);
		addEEnumLiteral(visibilityKindEEnum, VisibilityKind.VK_PACKAGE);

		initEEnum(pseudostateKindEEnum, PseudostateKind.class, "PseudostateKind");
		addEEnumLiteral(pseudostateKindEEnum, PseudostateKind.PK_CHOICE);
		addEEnumLiteral(pseudostateKindEEnum, PseudostateKind.PK_DEEP_HISTORY);
		addEEnumLiteral(pseudostateKindEEnum, PseudostateKind.PK_FORK);
		addEEnumLiteral(pseudostateKindEEnum, PseudostateKind.PK_INITIAL);
		addEEnumLiteral(pseudostateKindEEnum, PseudostateKind.PK_JOIN);
		addEEnumLiteral(pseudostateKindEEnum, PseudostateKind.PK_JUNCTION);
		addEEnumLiteral(pseudostateKindEEnum, PseudostateKind.PK_SHALLOW_HISTORY);

		// Initialize data types
		initEDataType(integerEDataType, long.class, "Integer", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(unlimitedIntegerEDataType, long.class, "UnlimitedInteger", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(stringEDataType, String.class, "String", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(booleanEDataType, boolean.class, "Boolean", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(nameEDataType, String.class, "Name", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(locationReferenceEDataType, String.class, "LocationReference", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(geometryEDataType, String.class, "Geometry", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
	}

} //Data_typesPackageImpl
