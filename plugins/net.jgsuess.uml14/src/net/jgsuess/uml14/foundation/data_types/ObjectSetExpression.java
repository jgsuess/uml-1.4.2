/**
 * <copyright>
 * </copyright>
 *
 * $Id: ObjectSetExpression.java,v 1.1 2012/04/23 09:31:29 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.data_types;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object Set Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.jgsuess.uml14.foundation.data_types.Data_typesPackage#getObjectSetExpression()
 * @model
 * @generated
 */
public interface ObjectSetExpression extends Expression {
} // ObjectSetExpression
