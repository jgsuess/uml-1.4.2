/**
 * <copyright>
 * </copyright>
 *
 * $Id: Expression.java,v 1.1 2012/04/23 09:31:29 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.data_types;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link net.jgsuess.uml14.foundation.data_types.Expression#getLanguage <em>Language</em>}</li>
 *   <li>{@link net.jgsuess.uml14.foundation.data_types.Expression#getBody <em>Body</em>}</li>
 * </ul>
 * </p>
 *
 * @see net.jgsuess.uml14.foundation.data_types.Data_typesPackage#getExpression()
 * @model
 * @generated
 */
public interface Expression extends EObject {
	/**
	 * Returns the value of the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Language</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Language</em>' attribute.
	 * @see #setLanguage(String)
	 * @see net.jgsuess.uml14.foundation.data_types.Data_typesPackage#getExpression_Language()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.Name"
	 * @generated
	 */
	String getLanguage();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.data_types.Expression#getLanguage <em>Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Language</em>' attribute.
	 * @see #getLanguage()
	 * @generated
	 */
	void setLanguage(String value);

	/**
	 * Returns the value of the '<em><b>Body</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' attribute.
	 * @see #setBody(String)
	 * @see net.jgsuess.uml14.foundation.data_types.Data_typesPackage#getExpression_Body()
	 * @model dataType="net.jgsuess.uml14.foundation.data_types.String"
	 * @generated
	 */
	String getBody();

	/**
	 * Sets the value of the '{@link net.jgsuess.uml14.foundation.data_types.Expression#getBody <em>Body</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body</em>' attribute.
	 * @see #getBody()
	 * @generated
	 */
	void setBody(String value);

} // Expression
