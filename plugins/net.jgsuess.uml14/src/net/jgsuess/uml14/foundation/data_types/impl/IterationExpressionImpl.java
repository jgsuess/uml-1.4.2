/**
 * <copyright>
 * </copyright>
 *
 * $Id: IterationExpressionImpl.java,v 1.1 2012/04/23 09:31:20 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.data_types.impl;

import net.jgsuess.uml14.foundation.data_types.Data_typesPackage;
import net.jgsuess.uml14.foundation.data_types.IterationExpression;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Iteration Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class IterationExpressionImpl extends ExpressionImpl implements IterationExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IterationExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Data_typesPackage.Literals.ITERATION_EXPRESSION;
	}

} //IterationExpressionImpl
