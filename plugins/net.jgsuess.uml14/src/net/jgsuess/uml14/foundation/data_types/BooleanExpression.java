/**
 * <copyright>
 * </copyright>
 *
 * $Id: BooleanExpression.java,v 1.1 2012/04/23 09:31:30 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.data_types;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.jgsuess.uml14.foundation.data_types.Data_typesPackage#getBooleanExpression()
 * @model
 * @generated
 */
public interface BooleanExpression extends Expression {
} // BooleanExpression
