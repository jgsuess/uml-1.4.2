/**
 * <copyright>
 * </copyright>
 *
 * $Id: ProcedureExpression.java,v 1.1 2012/04/23 09:31:30 uqjsuss Exp $
 */
package net.jgsuess.uml14.foundation.data_types;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Procedure Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see net.jgsuess.uml14.foundation.data_types.Data_typesPackage#getProcedureExpression()
 * @model
 * @generated
 */
public interface ProcedureExpression extends Expression {
} // ProcedureExpression
