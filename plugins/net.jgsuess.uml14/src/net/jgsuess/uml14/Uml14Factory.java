/**
 * <copyright>
 * </copyright>
 *
 * $Id: Uml14Factory.java,v 1.1 2012/04/23 09:31:37 uqjsuss Exp $
 */
package net.jgsuess.uml14;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see net.jgsuess.uml14.Uml14Package
 * @generated
 */
public interface Uml14Factory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Uml14Factory eINSTANCE = net.jgsuess.uml14.impl.Uml14FactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model</em>'.
	 * @generated
	 */
	Model createModel();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Uml14Package getUml14Package();

} //Uml14Factory
