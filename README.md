# UML 1.4.2.

## Description

This project provides a set of Eclipse EMF plugins that contain a rendering of the UML 1.4.2 standard (ISO-IEC-19501) for experimentation.
It is intended to be as close to the standard and as well-documented as possible.

## Canonical source

The source of UML 1.4.2 is [hosted on
GitLab.com](https://gitlab.com/jgsuess/uml-1.4.2)

## Documentation

There is no documentation at this time.

## Deployment and Use

There is no exported site at this time.